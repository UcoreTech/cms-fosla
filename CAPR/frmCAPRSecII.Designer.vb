<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCAPRSecII
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label84 = New System.Windows.Forms.Label
        Me.txtCETF = New System.Windows.Forms.TextBox
        Me.txtPatronageRefnd = New System.Windows.Forms.TextBox
        Me.txtOptionalReserve = New System.Windows.Forms.TextBox
        Me.txtInterstShareCap = New System.Windows.Forms.TextBox
        Me.txtGeneralReserve = New System.Windows.Forms.TextBox
        Me.txtAmtDistribution = New System.Windows.Forms.TextBox
        Me.txtReserves = New System.Windows.Forms.TextBox
        Me.txtAllocCETF = New System.Windows.Forms.TextBox
        Me.txtDonations = New System.Windows.Forms.TextBox
        Me.txtAllocOptionl = New System.Windows.Forms.TextBox
        Me.txtPaidShareCapital = New System.Windows.Forms.TextBox
        Me.txtAllocGenReserv = New System.Windows.Forms.TextBox
        Me.txtLiabilities = New System.Windows.Forms.TextBox
        Me.txtUndvdNetSurplus = New System.Windows.Forms.TextBox
        Me.txtAR = New System.Windows.Forms.TextBox
        Me.txtExpenses = New System.Windows.Forms.TextBox
        Me.txtLoanReceivable = New System.Windows.Forms.TextBox
        Me.txtRevenues = New System.Windows.Forms.TextBox
        Me.txtAssets = New System.Windows.Forms.TextBox
        Me.txtOperatins = New System.Windows.Forms.TextBox
        Me.txtFinanCon = New System.Windows.Forms.TextBox
        Me.Label59 = New System.Windows.Forms.Label
        Me.Label82 = New System.Windows.Forms.Label
        Me.Label60 = New System.Windows.Forms.Label
        Me.Label81 = New System.Windows.Forms.Label
        Me.Label61 = New System.Windows.Forms.Label
        Me.Label80 = New System.Windows.Forms.Label
        Me.Label62 = New System.Windows.Forms.Label
        Me.Label79 = New System.Windows.Forms.Label
        Me.Label63 = New System.Windows.Forms.Label
        Me.Label78 = New System.Windows.Forms.Label
        Me.Label64 = New System.Windows.Forms.Label
        Me.Label77 = New System.Windows.Forms.Label
        Me.Label65 = New System.Windows.Forms.Label
        Me.Label76 = New System.Windows.Forms.Label
        Me.Label66 = New System.Windows.Forms.Label
        Me.Label75 = New System.Windows.Forms.Label
        Me.Label67 = New System.Windows.Forms.Label
        Me.Label74 = New System.Windows.Forms.Label
        Me.Label68 = New System.Windows.Forms.Label
        Me.Label73 = New System.Windows.Forms.Label
        Me.Label69 = New System.Windows.Forms.Label
        Me.txtSR = New System.Windows.Forms.TextBox
        Me.txtLoanReleases = New System.Windows.Forms.TextBox
        Me.txtVolOfTransction = New System.Windows.Forms.TextBox
        Me.Label72 = New System.Windows.Forms.Label
        Me.Label70 = New System.Windows.Forms.Label
        Me.Label71 = New System.Windows.Forms.Label
        Me.BtnPrevious = New System.Windows.Forms.Button
        Me.BtnNextOK = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label84
        '
        Me.Label84.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label84.AutoSize = True
        Me.Label84.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label84.Location = New System.Drawing.Point(11, 3)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(666, 23)
        Me.Label84.TabIndex = 51
        Me.Label84.Text = "II. Financial Information for all Types of Coops with Unaudited Financial Stateme" & _
            "nts"
        '
        'txtCETF
        '
        Me.txtCETF.Location = New System.Drawing.Point(179, 323)
        Me.txtCETF.Name = "txtCETF"
        Me.txtCETF.Size = New System.Drawing.Size(191, 23)
        Me.txtCETF.TabIndex = 133
        '
        'txtPatronageRefnd
        '
        Me.txtPatronageRefnd.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtPatronageRefnd.Location = New System.Drawing.Point(622, 294)
        Me.txtPatronageRefnd.Name = "txtPatronageRefnd"
        Me.txtPatronageRefnd.Size = New System.Drawing.Size(180, 23)
        Me.txtPatronageRefnd.TabIndex = 132
        '
        'txtOptionalReserve
        '
        Me.txtOptionalReserve.Location = New System.Drawing.Point(179, 294)
        Me.txtOptionalReserve.Name = "txtOptionalReserve"
        Me.txtOptionalReserve.Size = New System.Drawing.Size(191, 23)
        Me.txtOptionalReserve.TabIndex = 131
        '
        'txtInterstShareCap
        '
        Me.txtInterstShareCap.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtInterstShareCap.Location = New System.Drawing.Point(621, 265)
        Me.txtInterstShareCap.Name = "txtInterstShareCap"
        Me.txtInterstShareCap.Size = New System.Drawing.Size(180, 23)
        Me.txtInterstShareCap.TabIndex = 124
        '
        'txtGeneralReserve
        '
        Me.txtGeneralReserve.Location = New System.Drawing.Point(179, 265)
        Me.txtGeneralReserve.Name = "txtGeneralReserve"
        Me.txtGeneralReserve.Size = New System.Drawing.Size(191, 23)
        Me.txtGeneralReserve.TabIndex = 123
        '
        'txtAmtDistribution
        '
        Me.txtAmtDistribution.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAmtDistribution.Location = New System.Drawing.Point(622, 236)
        Me.txtAmtDistribution.Name = "txtAmtDistribution"
        Me.txtAmtDistribution.Size = New System.Drawing.Size(180, 23)
        Me.txtAmtDistribution.TabIndex = 122
        '
        'txtReserves
        '
        Me.txtReserves.Location = New System.Drawing.Point(179, 236)
        Me.txtReserves.Name = "txtReserves"
        Me.txtReserves.Size = New System.Drawing.Size(191, 23)
        Me.txtReserves.TabIndex = 127
        '
        'txtAllocCETF
        '
        Me.txtAllocCETF.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAllocCETF.Location = New System.Drawing.Point(622, 207)
        Me.txtAllocCETF.Name = "txtAllocCETF"
        Me.txtAllocCETF.Size = New System.Drawing.Size(180, 23)
        Me.txtAllocCETF.TabIndex = 126
        '
        'txtDonations
        '
        Me.txtDonations.Location = New System.Drawing.Point(179, 207)
        Me.txtDonations.Name = "txtDonations"
        Me.txtDonations.Size = New System.Drawing.Size(191, 23)
        Me.txtDonations.TabIndex = 125
        '
        'txtAllocOptionl
        '
        Me.txtAllocOptionl.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAllocOptionl.Location = New System.Drawing.Point(622, 178)
        Me.txtAllocOptionl.Name = "txtAllocOptionl"
        Me.txtAllocOptionl.Size = New System.Drawing.Size(180, 23)
        Me.txtAllocOptionl.TabIndex = 142
        '
        'txtPaidShareCapital
        '
        Me.txtPaidShareCapital.Location = New System.Drawing.Point(179, 178)
        Me.txtPaidShareCapital.Name = "txtPaidShareCapital"
        Me.txtPaidShareCapital.Size = New System.Drawing.Size(191, 23)
        Me.txtPaidShareCapital.TabIndex = 141
        '
        'txtAllocGenReserv
        '
        Me.txtAllocGenReserv.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtAllocGenReserv.Location = New System.Drawing.Point(622, 149)
        Me.txtAllocGenReserv.Name = "txtAllocGenReserv"
        Me.txtAllocGenReserv.Size = New System.Drawing.Size(180, 23)
        Me.txtAllocGenReserv.TabIndex = 140
        '
        'txtLiabilities
        '
        Me.txtLiabilities.Location = New System.Drawing.Point(179, 149)
        Me.txtLiabilities.Name = "txtLiabilities"
        Me.txtLiabilities.Size = New System.Drawing.Size(191, 23)
        Me.txtLiabilities.TabIndex = 145
        '
        'txtUndvdNetSurplus
        '
        Me.txtUndvdNetSurplus.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtUndvdNetSurplus.Location = New System.Drawing.Point(622, 120)
        Me.txtUndvdNetSurplus.Name = "txtUndvdNetSurplus"
        Me.txtUndvdNetSurplus.Size = New System.Drawing.Size(180, 23)
        Me.txtUndvdNetSurplus.TabIndex = 144
        '
        'txtAR
        '
        Me.txtAR.Location = New System.Drawing.Point(179, 120)
        Me.txtAR.Name = "txtAR"
        Me.txtAR.Size = New System.Drawing.Size(191, 23)
        Me.txtAR.TabIndex = 143
        '
        'txtExpenses
        '
        Me.txtExpenses.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtExpenses.Location = New System.Drawing.Point(622, 91)
        Me.txtExpenses.Name = "txtExpenses"
        Me.txtExpenses.Size = New System.Drawing.Size(180, 23)
        Me.txtExpenses.TabIndex = 136
        '
        'txtLoanReceivable
        '
        Me.txtLoanReceivable.Location = New System.Drawing.Point(179, 91)
        Me.txtLoanReceivable.Name = "txtLoanReceivable"
        Me.txtLoanReceivable.Size = New System.Drawing.Size(191, 23)
        Me.txtLoanReceivable.TabIndex = 135
        '
        'txtRevenues
        '
        Me.txtRevenues.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtRevenues.Location = New System.Drawing.Point(622, 62)
        Me.txtRevenues.Name = "txtRevenues"
        Me.txtRevenues.Size = New System.Drawing.Size(180, 23)
        Me.txtRevenues.TabIndex = 134
        '
        'txtAssets
        '
        Me.txtAssets.Location = New System.Drawing.Point(179, 62)
        Me.txtAssets.Name = "txtAssets"
        Me.txtAssets.Size = New System.Drawing.Size(191, 23)
        Me.txtAssets.TabIndex = 139
        '
        'txtOperatins
        '
        Me.txtOperatins.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtOperatins.Location = New System.Drawing.Point(622, 33)
        Me.txtOperatins.Name = "txtOperatins"
        Me.txtOperatins.Size = New System.Drawing.Size(180, 23)
        Me.txtOperatins.TabIndex = 138
        '
        'txtFinanCon
        '
        Me.txtFinanCon.Location = New System.Drawing.Point(179, 33)
        Me.txtFinanCon.Name = "txtFinanCon"
        Me.txtFinanCon.Size = New System.Drawing.Size(191, 23)
        Me.txtFinanCon.TabIndex = 137
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Location = New System.Drawing.Point(12, 36)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(116, 15)
        Me.Label59.TabIndex = 106
        Me.Label59.Text = "Financial Condition"
        '
        'Label82
        '
        Me.Label82.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label82.AutoSize = True
        Me.Label82.Location = New System.Drawing.Point(439, 297)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(104, 15)
        Me.Label82.TabIndex = 105
        Me.Label82.Text = "Patronage Refund"
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.Location = New System.Drawing.Point(12, 65)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(42, 15)
        Me.Label60.TabIndex = 104
        Me.Label60.Text = "Assets"
        '
        'Label81
        '
        Me.Label81.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label81.AutoSize = True
        Me.Label81.Location = New System.Drawing.Point(439, 268)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(143, 15)
        Me.Label81.TabIndex = 109
        Me.Label81.Text = "Interest on Share Capital"
        '
        'Label61
        '
        Me.Label61.AutoSize = True
        Me.Label61.Location = New System.Drawing.Point(35, 94)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(93, 15)
        Me.Label61.TabIndex = 108
        Me.Label61.Text = "Loan receivable"
        '
        'Label80
        '
        Me.Label80.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label80.AutoSize = True
        Me.Label80.Location = New System.Drawing.Point(412, 239)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(138, 15)
        Me.Label80.TabIndex = 107
        Me.Label80.Text = "Amount for Distribution"
        '
        'Label62
        '
        Me.Label62.AutoSize = True
        Me.Label62.Location = New System.Drawing.Point(35, 123)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(117, 15)
        Me.Label62.TabIndex = 100
        Me.Label62.Text = "Accounts receivable"
        '
        'Label79
        '
        Me.Label79.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label79.AutoSize = True
        Me.Label79.Location = New System.Drawing.Point(412, 210)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(111, 15)
        Me.Label79.TabIndex = 99
        Me.Label79.Text = "Allocation for CETF"
        '
        'Label63
        '
        Me.Label63.AutoSize = True
        Me.Label63.Location = New System.Drawing.Point(12, 152)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(62, 15)
        Me.Label63.TabIndex = 98
        Me.Label63.Text = "Liabilities"
        '
        'Label78
        '
        Me.Label78.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label78.AutoSize = True
        Me.Label78.Location = New System.Drawing.Point(412, 181)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(165, 15)
        Me.Label78.TabIndex = 103
        Me.Label78.Text = "Allocation for Optional Fund"
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.Location = New System.Drawing.Point(12, 181)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(127, 15)
        Me.Label64.TabIndex = 102
        Me.Label64.Text = "Paid-up Share Capital"
        '
        'Label77
        '
        Me.Label77.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label77.AutoSize = True
        Me.Label77.Location = New System.Drawing.Point(412, 152)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(204, 15)
        Me.Label77.TabIndex = 101
        Me.Label77.Text = "Allocation for General Reserve Fund"
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.Location = New System.Drawing.Point(12, 210)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(128, 15)
        Me.Label65.TabIndex = 118
        Me.Label65.Text = "Donations and Grants"
        '
        'Label76
        '
        Me.Label76.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label76.AutoSize = True
        Me.Label76.Location = New System.Drawing.Point(395, 123)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(129, 15)
        Me.Label76.TabIndex = 117
        Me.Label76.Text = "Undivided Net Surplus"
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Location = New System.Drawing.Point(12, 239)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(55, 15)
        Me.Label66.TabIndex = 116
        Me.Label66.Text = "Reserves"
        '
        'Label75
        '
        Me.Label75.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label75.AutoSize = True
        Me.Label75.Location = New System.Drawing.Point(395, 94)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(57, 15)
        Me.Label75.TabIndex = 121
        Me.Label75.Text = "Expenses"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Location = New System.Drawing.Point(35, 268)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(125, 15)
        Me.Label67.TabIndex = 120
        Me.Label67.Text = "General Reserve Fund"
        '
        'Label74
        '
        Me.Label74.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label74.AutoSize = True
        Me.Label74.Location = New System.Drawing.Point(395, 65)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(58, 15)
        Me.Label74.TabIndex = 119
        Me.Label74.Text = "Revenues"
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.Location = New System.Drawing.Point(35, 297)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(131, 15)
        Me.Label68.TabIndex = 112
        Me.Label68.Text = "Optional Reserve Fund"
        '
        'Label73
        '
        Me.Label73.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label73.AutoSize = True
        Me.Label73.Location = New System.Drawing.Point(395, 36)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(69, 15)
        Me.Label73.TabIndex = 111
        Me.Label73.Text = "Operations"
        '
        'Label69
        '
        Me.Label69.AutoSize = True
        Me.Label69.Location = New System.Drawing.Point(35, 326)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(32, 15)
        Me.Label69.TabIndex = 110
        Me.Label69.Text = "CETF"
        '
        'txtSR
        '
        Me.txtSR.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSR.Location = New System.Drawing.Point(179, 412)
        Me.txtSR.Name = "txtSR"
        Me.txtSR.Size = New System.Drawing.Size(191, 23)
        Me.txtSR.TabIndex = 151
        '
        'txtLoanReleases
        '
        Me.txtLoanReleases.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtLoanReleases.Location = New System.Drawing.Point(179, 383)
        Me.txtLoanReleases.Name = "txtLoanReleases"
        Me.txtLoanReleases.Size = New System.Drawing.Size(191, 23)
        Me.txtLoanReleases.TabIndex = 150
        '
        'txtVolOfTransction
        '
        Me.txtVolOfTransction.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtVolOfTransction.Location = New System.Drawing.Point(179, 354)
        Me.txtVolOfTransction.Name = "txtVolOfTransction"
        Me.txtVolOfTransction.Size = New System.Drawing.Size(191, 23)
        Me.txtVolOfTransction.TabIndex = 149
        '
        'Label72
        '
        Me.Label72.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label72.AutoSize = True
        Me.Label72.Location = New System.Drawing.Point(35, 415)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(128, 15)
        Me.Label72.TabIndex = 148
        Me.Label72.Text = "Sales / Gross Receipts"
        '
        'Label70
        '
        Me.Label70.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label70.AutoSize = True
        Me.Label70.Location = New System.Drawing.Point(12, 357)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(137, 15)
        Me.Label70.TabIndex = 147
        Me.Label70.Text = "Volume of Transactions"
        '
        'Label71
        '
        Me.Label71.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label71.AutoSize = True
        Me.Label71.Location = New System.Drawing.Point(35, 386)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(82, 15)
        Me.Label71.TabIndex = 146
        Me.Label71.Text = "Loan releases"
        '
        'BtnPrevious
        '
        Me.BtnPrevious.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPrevious.Location = New System.Drawing.Point(618, 442)
        Me.BtnPrevious.Name = "BtnPrevious"
        Me.BtnPrevious.Size = New System.Drawing.Size(87, 27)
        Me.BtnPrevious.TabIndex = 152
        Me.BtnPrevious.Text = "Previous"
        Me.BtnPrevious.UseVisualStyleBackColor = True
        '
        'BtnNextOK
        '
        Me.BtnNextOK.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnNextOK.Location = New System.Drawing.Point(711, 442)
        Me.BtnNextOK.Name = "BtnNextOK"
        Me.BtnNextOK.Size = New System.Drawing.Size(87, 27)
        Me.BtnNextOK.TabIndex = 153
        Me.BtnNextOK.Text = "Next"
        Me.BtnNextOK.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.Location = New System.Drawing.Point(15, 442)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(84, 27)
        Me.btnCancel.TabIndex = 154
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'frmCAPRSecII
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(813, 475)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.BtnPrevious)
        Me.Controls.Add(Me.BtnNextOK)
        Me.Controls.Add(Me.txtSR)
        Me.Controls.Add(Me.txtLoanReleases)
        Me.Controls.Add(Me.txtVolOfTransction)
        Me.Controls.Add(Me.Label72)
        Me.Controls.Add(Me.Label70)
        Me.Controls.Add(Me.Label71)
        Me.Controls.Add(Me.txtCETF)
        Me.Controls.Add(Me.txtPatronageRefnd)
        Me.Controls.Add(Me.txtOptionalReserve)
        Me.Controls.Add(Me.txtInterstShareCap)
        Me.Controls.Add(Me.txtGeneralReserve)
        Me.Controls.Add(Me.txtAmtDistribution)
        Me.Controls.Add(Me.txtReserves)
        Me.Controls.Add(Me.txtAllocCETF)
        Me.Controls.Add(Me.txtDonations)
        Me.Controls.Add(Me.txtAllocOptionl)
        Me.Controls.Add(Me.txtPaidShareCapital)
        Me.Controls.Add(Me.txtAllocGenReserv)
        Me.Controls.Add(Me.txtLiabilities)
        Me.Controls.Add(Me.txtUndvdNetSurplus)
        Me.Controls.Add(Me.txtAR)
        Me.Controls.Add(Me.txtExpenses)
        Me.Controls.Add(Me.txtLoanReceivable)
        Me.Controls.Add(Me.txtRevenues)
        Me.Controls.Add(Me.txtAssets)
        Me.Controls.Add(Me.txtOperatins)
        Me.Controls.Add(Me.txtFinanCon)
        Me.Controls.Add(Me.Label59)
        Me.Controls.Add(Me.Label82)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.Label81)
        Me.Controls.Add(Me.Label61)
        Me.Controls.Add(Me.Label80)
        Me.Controls.Add(Me.Label62)
        Me.Controls.Add(Me.Label79)
        Me.Controls.Add(Me.Label63)
        Me.Controls.Add(Me.Label78)
        Me.Controls.Add(Me.Label64)
        Me.Controls.Add(Me.Label77)
        Me.Controls.Add(Me.Label65)
        Me.Controls.Add(Me.Label76)
        Me.Controls.Add(Me.Label66)
        Me.Controls.Add(Me.Label75)
        Me.Controls.Add(Me.Label67)
        Me.Controls.Add(Me.Label74)
        Me.Controls.Add(Me.Label68)
        Me.Controls.Add(Me.Label73)
        Me.Controls.Add(Me.Label69)
        Me.Controls.Add(Me.Label84)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MinimumSize = New System.Drawing.Size(819, 499)
        Me.Name = "frmCAPRSecII"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cooperative Annual Performance Report - [Section II]"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents txtCETF As System.Windows.Forms.TextBox
    Friend WithEvents txtPatronageRefnd As System.Windows.Forms.TextBox
    Friend WithEvents txtOptionalReserve As System.Windows.Forms.TextBox
    Friend WithEvents txtInterstShareCap As System.Windows.Forms.TextBox
    Friend WithEvents txtGeneralReserve As System.Windows.Forms.TextBox
    Friend WithEvents txtAmtDistribution As System.Windows.Forms.TextBox
    Friend WithEvents txtReserves As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocCETF As System.Windows.Forms.TextBox
    Friend WithEvents txtDonations As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocOptionl As System.Windows.Forms.TextBox
    Friend WithEvents txtPaidShareCapital As System.Windows.Forms.TextBox
    Friend WithEvents txtAllocGenReserv As System.Windows.Forms.TextBox
    Friend WithEvents txtLiabilities As System.Windows.Forms.TextBox
    Friend WithEvents txtUndvdNetSurplus As System.Windows.Forms.TextBox
    Friend WithEvents txtAR As System.Windows.Forms.TextBox
    Friend WithEvents txtExpenses As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanReceivable As System.Windows.Forms.TextBox
    Friend WithEvents txtRevenues As System.Windows.Forms.TextBox
    Friend WithEvents txtAssets As System.Windows.Forms.TextBox
    Friend WithEvents txtOperatins As System.Windows.Forms.TextBox
    Friend WithEvents txtFinanCon As System.Windows.Forms.TextBox
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents txtSR As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanReleases As System.Windows.Forms.TextBox
    Friend WithEvents txtVolOfTransction As System.Windows.Forms.TextBox
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents BtnPrevious As System.Windows.Forms.Button
    Friend WithEvents BtnNextOK As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
End Class
