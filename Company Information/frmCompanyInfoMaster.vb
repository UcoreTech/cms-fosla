﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmCompanyInfoMaster

    Private gcon As New Clsappconfiguration
    Public fxkey As String

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        Select Case btnEdit.Text
            Case "Edit"
                GroupBox1.Enabled = True
                btnEdit.Text = "Update"
            Case "Update"
                'Update Script Here
                UpdateCompany()
                LoadCompany()
                GroupBox1.Enabled = False
                btnEdit.Text = "Edit"
                frmMain.LoadCooperativeName()
                'If MsgBox("To take effect you need to restart your system. Restart Now?", vbYesNo, "Message") = vbYes Then
                '    Application.Restart()
                'Else
                '    Exit Sub
                'End If
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub frmCompanyInfoMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GroupBox1.Enabled = False
        LoadCompany()
    End Sub

    Private Sub UpdateCompany()
        Dim MyGuid As Guid = New Guid(fxkey)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_InsertUpdate_CompanyInfoMaster",
                           New SqlParameter("@fxkey", MyGuid),
                           New SqlParameter("@CoopName", txtCompanyName.Text),
                           New SqlParameter("@CoopDesc", txtCompanyDesc.Text),
                           New SqlParameter("@Address", txtAddress.Text),
                           New SqlParameter("@ContactNo", txtContactNo.Text))
        Catch ex As Exception
            MessageBox.Show("Error:" + ex.ToString)
        End Try
    End Sub

    Private Sub LoadCompany()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_SelectCompany_Master")

        While rd.Read
            fxkey = rd("fxkey").ToString
            txtCompanyName.Text = rd("CoopName").ToString
            txtCompanyDesc.Text = rd("CoopDesc").ToString
            txtAddress.Text = rd("Address").ToString
            txtContactNo.Text = rd("ContactNo").ToString
        End While
    End Sub
End Class