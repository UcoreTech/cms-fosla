﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBrowseAccounts

    Dim con As New Clsappconfiguration
    Dim cs As String = con.cnstring

    Private Sub frmBrowseAccounts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SearchListAccts("")
    End Sub

    Private Sub SearchListAccts(ByVal key As String)
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(cs, "_SearchListAccounts",
                                          New SqlParameter("@key", key))
            dgvAccounts.DataSource = ds.Tables(0)
            dgvAccounts.Columns(0).Width = 50
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchListAccts(txtSearch.Text)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub dgvAccounts_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAccounts.DoubleClick
        Try
            With dgvAccounts
                frmMasterfile_LoanType.GetAccountForContribution(.SelectedRows(0).Cells(0).Value.ToString, .SelectedRows(0).Cells(1).Value.ToString)
            End With
            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class