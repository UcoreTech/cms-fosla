﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmChargesSetting

    Private mycon As New Clsappconfiguration
    Public loancode As String

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmChargesSetting_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadSetting()
    End Sub

    Private Sub LoadSetting()
        Try
            dgvList.Rows.Clear()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_ChargesSetting_Select",
                                                              New SqlParameter("@loancode", CInt(loancode)))
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add(rd(0).ToString, rd(1).ToString, rd(2), rd(3).ToString)
                    End With
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub SaveSetting()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList.Rows(i)
                    SqlHelper.ExecuteNonQuery(mycon.cnstring, "_ChargesSetting_InsertUpdate",
                                              New SqlParameter("@fkLoanCode", loancode),
                                              New SqlParameter("@AcctCode", .Cells(0).Value.ToString),
                                              New SqlParameter("@AcctTitle", .Cells(1).Value.ToString),
                                              New SqlParameter("@fbAmortized", .Cells(2).Value),
                                              New SqlParameter("@fdTerms", .Cells(3).Value))
                End With
            Next
            MsgBox("Save Success.", vbInformation, "Done")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        SaveSetting()
    End Sub

End Class
