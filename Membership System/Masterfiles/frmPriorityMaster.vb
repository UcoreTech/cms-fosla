﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmPriorityMaster
    '##########################
    'Module : Priority Table (MoveUp - MoveDown)
    'Created by: Vincent Nacar
    'Date: 7/4/2014
    '########################
    Dim mycon As New Clsappconfiguration

    Public loantypeCode As String
    Public fcLoanType As String
    Public isEmpty As Boolean

    Private Sub frmPriorityMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'lblLoanType.Text = loantypeCode + " - " + fcLoanType
        lblLoanType.Text = frmMasterfile_LoanType.txtLoanCode.Text + " - " + fcLoanType
        LoadPriority()
    End Sub

    Private Sub InsertPriority(ByVal stat As String)
        Dim ctr As Integer = 0
        Dim desc As String = ""
        Dim no As Integer = 1

        'Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_GetCharges_ToPrioritize",
        '                                                   New SqlParameter("@LoanTypeCode", loantypeCode))
        'While rd.Read
        '    SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Masterfile_InsertUpdate_Priority",
        '                               New SqlParameter("@Stat", stat),
        '                               New SqlParameter("@LoanCode", loantypeCode),
        '                               New SqlParameter("@Desc", rd(0).ToString),
        '                               New SqlParameter("@no", no))
        '    no += 1
        'End While

        While ctr <> 3
            Select Case ctr
                Case 0
                    desc = "Loans"
                    no = 1
                Case 1
                    desc = "Bereavement"
                    no = 2
                Case 2
                    desc = "Other Deductions"
                    no = 3
            End Select

            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Masterfile_InsertUpdate_Priority",
                                       New SqlParameter("@Stat", stat),
                                       New SqlParameter("@LoanCode", loantypeCode),
                                       New SqlParameter("@Desc", desc),
                                       New SqlParameter("@no", no))

            ctr += 1
        End While
        RefreshPriority()

    End Sub

    Private Sub UpdatePriority(ByVal stat As String)
        Try
            'Dim desc As String = dgvPriority.SelectedRows(0).Cells(0).Value.ToString
            'Dim no As String = dgvPriority.SelectedRows(0).Cells(1).Value.ToString

            'SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Masterfile_InsertUpdate_Priority",
            '                           New SqlParameter("@Stat", stat),
            '                           New SqlParameter("@LoanCode", loantypeCode),
            '                           New SqlParameter("@Desc", desc),
            '                           New SqlParameter("@no", no))
            'RefreshPriority()
            Select Case stat
                Case "+"
                    Dim xRow As Integer = dgvPriority.CurrentRow.Index
                    Dim myPos As Integer = dgvPriority.Rows(xRow).Cells(0).Value
                    Dim getPos As Integer = myPos + 1
                    Dim maxRow As Integer = dgvPriority.RowCount
                    If getPos > maxRow Then
                        getPos = 1
                        dgvPriority.Rows(xRow).Cells(0).Value = getPos.ToString
                        dgvPriority.Rows(0).Cells(0).Value = maxRow.ToString
                    Else
                        dgvPriority.Rows(xRow).Cells(0).Value = getPos.ToString
                        dgvPriority.Rows(xRow + 1).Cells(0).Value = myPos.ToString
                    End If
                    dgvPriority.Sort(dgvPriority.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
                    Exit Sub
                Case "-"
                    Dim xRow As Integer = dgvPriority.CurrentRow.Index
                    Dim myPos As Integer = dgvPriority.Rows(xRow).Cells(0).Value
                    Dim getPos As Integer = myPos - 1
                    Dim maxRow As Integer = dgvPriority.RowCount
                    If getPos < 1 Then
                        getPos = maxRow
                        dgvPriority.Rows(xRow).Cells(0).Value = getPos.ToString
                        dgvPriority.Rows(maxRow - 1).Cells(0).Value = "1"
                    Else
                        dgvPriority.Rows(xRow).Cells(0).Value = getPos.ToString
                        dgvPriority.Rows(xRow - 1).Cells(0).Value = myPos.ToString
                    End If
                    dgvPriority.Sort(dgvPriority.Columns(0), System.ComponentModel.ListSortDirection.Ascending)
                    Exit Sub
            End Select
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub LoadPriority()
        dgvPriority.Rows.Clear()

        Try
            Dim mainRD As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Select_Priority_v2",
                                                                  New SqlParameter("@loanCode", Convert.ToInt32(loantypeCode)))
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.cnstring, "_Masterfile_LoadPriority",
                                           New SqlParameter("@LoanCode", Convert.ToInt32(loantypeCode)))
            'If ds.Tables(0).Rows.Count = 0 Then
            '    LoadDefault()
            'Else
            If mainRD.HasRows Then
                While mainRD.Read
                    dgvPriority.Rows.Add(mainRD(0).ToString, mainRD(1).ToString, mainRD(2).ToString, mainRD(3), mainRD(4).ToString)
                End While
            Else
                Dim ictr As Integer = 1
                If rd.HasRows Then
                    While rd.Read
                        dgvPriority.Rows.Add(ictr.ToString, rd(0), rd(1), False, rd(2))
                        ictr += 1
                    End While
                End If
            End If
            ' dgvPriority.Columns(1).Visible = False
            'End If
        Catch ex As Exception
            'Throw
        End Try
    End Sub

    Private Sub RefreshPriority()
        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Masterfile_LoadPriority",
                                           New SqlParameter("@LoanCode", Convert.ToInt32(loantypeCode)))
            dgvPriority.DataSource = ds.Tables(0)
            dgvPriority.Columns(1).Visible = False
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub LoadDefault()
        InsertPriority("")
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        'UpdatePriority("")
        DeletePriority()
        For i As Integer = 0 To dgvPriority.RowCount - 1
            With dgvPriority.Rows(i)
                SavePriority(.Cells(0).Value, .Cells(1).Value.ToString, .Cells(2).Value.ToString, .Cells(3).Value, .Cells(4).Value.ToString)
            End With
        Next
        frmMsgBox.txtMessage.Text = "Saving Success!"
        frmMsgBox.ShowDialog()
        'Me.Close()
    End Sub

    Private Sub SavePriority(ByVal pos As Integer, ByVal code As String, ByVal title As String, ByVal isactive As Boolean, ByVal ptype As String)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Insert_Priority_v2",
                                      New SqlParameter("@loanCode", loantypeCode),
                                      New SqlParameter("@Pos", pos),
                                      New SqlParameter("@Code", code),
                                      New SqlParameter("@Title", title),
                                      New SqlParameter("@fbActive", isactive),
                                      New SqlParameter("@type", ptype))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub DeletePriority()
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "_DELETE_Priority_2",
                                      New SqlParameter("@loanCode", loantypeCode))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnMoveUp_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveUp.Click
        UpdatePriority("-")
    End Sub

    Private Sub btnMoveDown_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMoveDown.Click
        UpdatePriority("+")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        DeletePrior()
        LoadPriority()
    End Sub

    Private Sub DeletePrior()
        SqlHelper.ExecuteNonQuery(mycon.cnstring, "Delete_Priority",
                                  New SqlParameter("@LoanCode", Convert.ToInt32(loantypeCode)))
    End Sub
End Class