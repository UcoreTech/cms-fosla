Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class frmMasterfile_LoanType_Priority

    Private gCon As New Clsappconfiguration
    Private errormsg As String


    Private Sub frmMasterfile_LoanType_Priority_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Call LoadTransactionsPriority()
            Call NumberAllRows()
        Catch ex As Exception
            Exit Sub
            'MessageBox.Show(ex.Message, "Transactions Priority", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub LoadTransactionsPriority()
        Try
            grdTransactionsPriority.Columns.Clear()

            'Declare Loan Type List on the Datagridview
            Dim colLoanType As New DataGridViewComboBoxColumn
            With colLoanType
                .Items.Clear()
                .FlatStyle = FlatStyle.Flat
                .HeaderText = "Loan Type"
                .DataPropertyName = "fk_LoanType"
                .Name = "fk_LoanType"
                .DataSource = LoadLoanTypes().Tables(0)
                .DisplayMember = "fcLoanTypeName"
                .ValueMember = "pk_LoanType"
                .DropDownWidth = 300
                .Width = 100
                .AutoComplete = True

                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .AutoComplete = True
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True

            End With


            'Declare Transacsaction Type List on the Datagridview
            Dim colTransactionList As New DataGridViewComboBoxColumn
            With colTransactionList
                .Items.Clear()
                .FlatStyle = FlatStyle.Flat
                .HeaderText = "Transaction Type"
                .DataPropertyName = "Transaction Type"
                .Name = "Transaction Type"
                .DropDownWidth = 300
                .Width = 100
                .AutoComplete = True
                .DisplayStyle = DataGridViewComboBoxDisplayStyle.ComboBox
                .AutoComplete = True
                .MaxDropDownItems = 10
                .Resizable = DataGridViewTriState.True

                'Load Collection
                .Items.Add(DBNull.Value)
                .Items.Add("Loans")
                .Items.Add("Penalty")
                .Items.Add("Contribution")
                .Items.Add("Bereavement")
            End With

            Dim colMoveUp As New DataGridViewButtonColumn
            With colMoveUp
                .Name = "moveUp"
                .Text = "Move Up"
                .UseColumnTextForButtonValue = True
                .HeaderText = ""
            End With

            Dim colMoveDown As New DataGridViewButtonColumn
            With colMoveDown
                .Name = "moveDown"
                .Text = "Move Down"
                .UseColumnTextForButtonValue = True
                .HeaderText = ""
            End With

            'Load Configured Prioritization
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Transaction_Priority")
            grdTransactionsPriority.DataSource = ds.Tables(0)

            'Add ComboboxColumns/Buttons to Datagrid
            grdTransactionsPriority.Columns.Add(colLoanType)
            grdTransactionsPriority.Columns.Add(colTransactionList)
            grdTransactionsPriority.Columns.Add(colMoveUp)
            grdTransactionsPriority.Columns.Add(colMoveDown)

            ' add a ColumnChanging event handler for the table. 
            AddHandler ds.Tables(0).ColumnChanging, New _
                   DataColumnChangeEventHandler(AddressOf Column_Changing)

            'Format Columns
            grdTransactionsPriority.Columns("Description").Width = 200
            grdTransactionsPriority.Columns("Description").SortMode = DataGridViewColumnSortMode.NotSortable

            'Hide Unnecessary Columns
            grdTransactionsPriority.Columns("fk_LoanType").Visible = False
            grdTransactionsPriority.Columns("Priority No").Visible = False
            grdTransactionsPriority.Columns("fbIsLoan").Visible = False
            grdTransactionsPriority.Columns("fbIsPenalty").Visible = False
            grdTransactionsPriority.Columns("fbIsContributions").Visible = False
            grdTransactionsPriority.Columns("fbIsBereavement").Visible = False
            grdTransactionsPriority.Columns("Transaction Type").Visible = False
            grdTransactionsPriority.Columns("pk_paymentPriority").Visible = False

        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub NumberAllRows()
        For i As Integer = 0 To grdTransactionsPriority.Rows.Count - 1
            grdTransactionsPriority.Rows(i).HeaderCell.Value = i.ToString()
        Next i
    End Sub

    Private Shared Sub Column_Changing(ByVal sender As Object, ByVal e As DataColumnChangeEventArgs)
        If e.Column.ColumnName = "fk_LoanType" Or e.Column.ColumnName = "fcType" Then
            If e.ProposedValue Is Nothing Then
                e.ProposedValue = DBNull.Value
            End If
        End If
    End Sub

    Private Function LoadLoanTypes() As DataSet
        Try
            Dim ds As New DataSet

            ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Priority_LoadLoanTypes")

            Return ds
        Catch ex As Exception
            Exit Function
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub grdTransactionsPriority_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles grdTransactionsPriority.CellClick
        'Validation no.1 Check if the user did not click the row header
        If e.ColumnIndex >= 0 Then
            If grdTransactionsPriority.Columns(e.ColumnIndex).Name = "moveUp" Or grdTransactionsPriority.Columns(e.ColumnIndex).Name = "moveDown" Then

                'Validation no.2: check if new record
                If grdTransactionsPriority.Item("pk_paymentPriority", e.RowIndex).Value IsNot DBNull.Value Then

                    'Validation no.3 Check if the user did not click the Column Header
                    If e.RowIndex >= 0 Then
                        Dim buttonText As String = grdTransactionsPriority.Item(e.ColumnIndex, e.RowIndex).Value

                        Select Case buttonText
                            Case "Move Up"
                                Call MoveUp(e.RowIndex)
                                Call LoadTransactionsPriority()
                                If e.RowIndex > 0 Then
                                    grdTransactionsPriority.Rows(e.RowIndex - 1).Selected = True
                                End If

                            Case "Move Down"
                                Call MoveDown(e.RowIndex)
                                Call LoadTransactionsPriority()
                                grdTransactionsPriority.Rows(e.RowIndex + 1).Selected = True

                        End Select
                    End If
                Else
                    MessageBox.Show("You have tried to add a Row without updating the records." + vbNewLine + "Please update first. or press refresh button.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private Sub grdTransactionsPriority_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdTransactionsPriority.DataError
        Exit Sub
    End Sub

    Private Sub grdTransactionsPriority_RowsAdded(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles grdTransactionsPriority.RowsAdded
        Try
            Call NumberAllRows()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub grdTransactionsPriority_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles grdTransactionsPriority.RowsRemoved
        Try
            Call NumberAllRows()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub MoveUp(ByVal rowindex As Integer)
        Dim getPriorityID_Existing As System.Guid
        Dim getPriorityID_Above As System.Guid
        Try
            If rowindex > 0 Then
                getPriorityID_Existing = grdTransactionsPriority.Item("pk_paymentPriority", rowindex).Value

                getPriorityID_Above = grdTransactionsPriority.Item("pk_paymentPriority", rowindex - 1).Value
            End If

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Priority_MoveUP", _
                    New SqlParameter("@pk_paymentPriority_existing", getPriorityID_Existing), _
                    New SqlParameter("@pk_paymentPriority_above", getPriorityID_Above))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub MoveDown(ByVal rowindex As Integer)
        Dim getPriorityID_Existing As System.Guid
        Dim getPriorityID_Below As System.Guid
        Try
            If rowindex <> grdTransactionsPriority.RowCount - 2 Then
                getPriorityID_Existing = grdTransactionsPriority.Item("pk_paymentPriority", rowindex).Value

                getPriorityID_Below = grdTransactionsPriority.Item("pk_paymentPriority", rowindex + 1).Value
            End If

            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Priority_MoveDown", _
                    New SqlParameter("@pk_paymentPriority_existing", getPriorityID_Existing), _
                    New SqlParameter("@pk_paymentPriority_below", getPriorityID_Below))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnReferesh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Call LoadTransactionsPriority()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Dim priorityID As Guid
        Dim desc As String
        Dim loanTypeID As Guid
        Dim priorityNo As Integer
        Dim transType As String

        Try
            'Validation First
            For Each xRow As DataGridViewRow In grdTransactionsPriority.Rows
                If xRow.IsNewRow = False Then
                    priorityID = IIf(xRow.Cells("pk_paymentPriority").Value Is DBNull.Value, Nothing, xRow.Cells("pk_paymentPriority").Value)
                    desc = xRow.Cells("Description").Value.ToString()
                    loanTypeID = IIf(xRow.Cells("fk_LoanType").Value Is DBNull.Value, Nothing, xRow.Cells("fk_LoanType").Value)
                    priorityNo = IIf(xRow.Cells("Priority No").Value Is DBNull.Value, Nothing, xRow.Cells("Priority No").Value)
                    transType = xRow.Cells("Transaction Type").Value.ToString()

                    Call ValidateRequiredFields(priorityID, desc, loanTypeID, priorityNo, transType)
                End If
            Next

            'Record
            For Each xRow As DataGridViewRow In grdTransactionsPriority.Rows
                If xRow.IsNewRow = False Then
                    priorityID = IIf(xRow.Cells("pk_paymentPriority").Value Is DBNull.Value, Nothing, xRow.Cells("pk_paymentPriority").Value)
                    desc = xRow.Cells("Description").Value.ToString()
                    loanTypeID = IIf(xRow.Cells("fk_LoanType").Value Is DBNull.Value, Nothing, xRow.Cells("fk_LoanType").Value)
                    priorityNo = IIf(xRow.Cells("Priority No").Value Is DBNull.Value, Nothing, xRow.Cells("Priority No").Value)
                    transType = xRow.Cells("Transaction Type").Value.ToString()

                    Call RecordAndUpdatePriority(priorityID, desc, loanTypeID, priorityNo, transType)
                End If
            Next

            MessageBox.Show("You have updated the configurations successfully.", "Priority", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Call LoadTransactionsPriority()
        Catch ex As NullReferenceException
            MessageBox.Show(errormsg, "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            errormsg = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Priority", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ValidateRequiredFields(ByVal priorityID As Guid, ByVal desc As String, ByVal loanTypeID As Guid, ByVal priorityNo As Integer, ByVal transType As String)
        If desc Is Nothing Or desc = "" Then
            Dim ex As New NullReferenceException
            errormsg = "User Error! You have not entered the Description. Please complete this first before proceeding."
            Exit Sub
        End If

        If transType Is Nothing Or transType = "" Then
            Dim ex As New NullReferenceException
            errormsg = "User Error! You have not entered the Transaction type. Please complete this first before proceeding."
            Exit Sub
        End If
    End Sub

    Private Sub RecordAndUpdatePriority(ByVal priorityID As Guid, ByVal desc As String, ByVal loanTypeID As Guid, ByVal priorityNo As Integer, ByVal transType As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Priority_InsertUpdate", _
                New SqlParameter("@pk_paymentPriority", priorityID), _
                New SqlParameter("@description", desc), _
                New SqlParameter("@fk_LoanType", loanTypeID), _
                New SqlParameter("@fnPriority ", priorityNo), _
                New SqlParameter("@fcType", transType))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub grdTransactionsPriority_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles grdTransactionsPriority.UserDeletingRow
        If MessageBox.Show("Are you sure you want to delete this record?", "User Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Try
                Dim configID As Guid = grdTransactionsPriority.Item("pk_paymentPriority", e.Row.Index).Value
                Call DeleteConfiguration(configID)
                MessageBox.Show("You have sucessfully deleted this configuration.", "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Delete Record", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        Else
            e.Cancel = True
        End If
    End Sub

    Private Sub DeleteConfiguration(ByVal configID As Guid)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_m_Priority_Delete", _
                New SqlParameter("@pk_paymentPriority", configID))
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

End Class