Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_NewMemberRequirements
#Region "Public Variables"
    Public loanNumber As String
    Public loantype As String

#End Region
#Region "Public Property"
    Public Property getloanNumber() As String
        Get
            Return loanNumber
        End Get
        Set(ByVal value As String)
            loanNumber = value
        End Set
    End Property
    Public Property getloantype() As String
        Get
            Return loantype
        End Get
        Set(ByVal value As String)
            loantype = value
        End Set
    End Property
#End Region
    Private Sub cmdUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate.Click
        Dim i As Integer
        Dim guid As String
        For i = 0 To Me.grdRequirements.Rows.Count - 1
            If Me.grdRequirements.Item(2, i).Value = True Then
                guid = Me.grdRequirements.Item(0, i).Value.ToString()
                Call InsertUpdateSelectUpdates(loanNumber, Me.grdRequirements.Item(0, i).Value.ToString())
            End If
        Next
        Me.Close()
        'frmExistingLoans.cmdForward.Enabled = True
    End Sub

    Private Sub cmdCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel.Click
        Me.Close()
    End Sub

    Private Sub frmNewMemberRequirements_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.lblloans.Text = "Requirements for " + loantype
        Call ViewRequirementsPerloanType(loantype)
    End Sub
#Region "View Requirements Per loan type"
    Private Sub ViewRequirementsPerloanType(ByVal Loantype As String)
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("MSS_Loans_Retrieve_LoanRequirements_PerLoan", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()

        With cmd.Parameters
            .Add("@loanType", SqlDbType.VarChar, 50).Value = Loantype
        End With

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "CIMS_m_LoanType_Requirements")

            Me.grdRequirements.DataSource = ds
            Me.grdRequirements.DataMember = "CIMS_m_LoanType_Requirements"
            Me.grdRequirements.Columns(0).Visible = False
            Me.grdRequirements.Columns(1).Width = 190

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Requirements Per Loan")
        End Try
    End Sub
#End Region
#Region "Get Selected Requirements base on loan type"
    Private Sub InsertUpdateSelectUpdates(ByVal loanumber As String, ByVal loanGuid As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "MSS_Loans_AddEdit_MemberLoanRequirements", _
                                      New SqlParameter("@loanNo", loanumber), _
                                      New SqlParameter("@pk_LoanRequirements", loanGuid), _
                                      New SqlParameter("@fbActive", 1))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Insert/Update Requirement")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region
End Class