﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmMasterfile_Group
    Dim mycon As New Clsappconfiguration
    Dim fxid As Long

    Private Sub frmMasterfile_Group_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GroupList()
    End Sub

    Private Sub ControlSetup(ByVal mode As String)
        If mode = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            GroupBox1.Enabled = True
            txtdesc.Clear()
            Exit Sub
        End If
        If mode = "Save" Then
            Try
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Masterfile_Group_Addnew",
                                           New SqlParameter("@Fc_GroupDesc", txtdesc.Text))
                MessageBox.Show("Record Succesfully Added!")
                GroupList()
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            txtdesc.Clear()
            GroupList()
            Exit Sub
        End If
        If mode = "Close" Then
            Me.Close()
        End If
        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            GroupBox1.Enabled = False
            txtdesc.Clear()
            Exit Sub
        End If
        If mode = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Enabled = False
            GroupBox1.Enabled = True
            Exit Sub
        End If
        If mode = "Update" Then
            Try
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Masterfile_Edit",
                                           New SqlParameter("@fxGroupID", fxid),
                                           New SqlParameter("@fcGroupDesc", txtdesc.Text))
                MessageBox.Show("Record Succesfully Updated!")
                GroupList()
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            GroupBox1.Enabled = False
            txtdesc.Clear()
            GroupList()
            Exit Sub
        End If
        If mode = "Delete" Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                Try
                    SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Masterfile_GroupDelete",
                                               New SqlParameter("@fxGroupID", fxid))
                    txtdesc.Clear()
                    MessageBox.Show("Record Deleted!")
                    GroupList()
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                End Try
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If btnsave.Text = "New" Then
            ControlSetup("New")
        Else
            ControlSetup("Save")
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            ControlSetup("Edit")
            Exit Sub
        End If
        If btnupdate.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        ControlSetup("Delete")
    End Sub

    Private Sub GroupList()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Masterfile_SelectAll")
        dgvlist.DataSource = ds.Tables(0)
        dgvlist.Columns(0).HeaderText = "ID"
        dgvlist.Columns(0).Visible = False
        dgvlist.Columns(1).HeaderText = "Group Description"
    End Sub

    Private Sub dgvlist_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvlist.CellClick
        For Each rows As DataGridViewRow In dgvlist.SelectedRows
           fxid = Convert.ToInt64(dgvlist.SelectedRows(0).Cells(0).Value)
            txtdesc.Text = dgvlist.SelectedRows(0).Cells(1).Value.ToString
        Next
    End Sub
End Class