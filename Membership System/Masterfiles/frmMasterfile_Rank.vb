﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_Rank
    Dim mycon As New Clsappconfiguration
    Dim fxid As Integer

    Private Sub frmMasterfile_Rank_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadRank()
    End Sub

    Private Sub ControlSetup(ByVal mode As String)
        If mode = "New" Then
            btnedit.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnnew.Text = "Save"
            txtdesc.Enabled = True
            Exit Sub
        End If
        If mode = "Save" Then
            Try
                'Save code
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Insert_Rank_Master",
                                           New SqlParameter("@Desc", txtdesc.Text))
                LoadRank()
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
            btnedit.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnnew.Text = "New"
            Exit Sub
        End If
        If mode = "Close" Then
            Me.Close()
        End If
        If mode = "Cancel" Then
            btnedit.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnnew.Text = "New"
            btnedit.Text = "Edit"
            btnnew.Enabled = True
            txtdesc.Enabled = False
            Exit Sub
        End If
        If mode = "Edit" Then
            btnedit.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnnew.Enabled = False
            txtdesc.Enabled = True
            Exit Sub
        End If
        If mode = "Update" Then
            Try
                'Update code
                SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Update_Rank_Master",
                                           New SqlParameter("@id", fxid),
                                           New SqlParameter("@fcDesc", txtdesc.Text))

                LoadRank()
            Catch ex As Exception
                MessageBox.Show(ex.ToString)
            End Try
            btnnew.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnnew.Text = "New"
            btnedit.Text = "Edit"
            txtdesc.Enabled = False
            Exit Sub
        End If
        If mode = "Delete" Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                Try
                    'Delete Code here
                    SqlHelper.ExecuteNonQuery(mycon.cnstring, "_Delete_Rank_Master",
                                               New SqlParameter("@id", fxid))
                    LoadRank()
                    txtdesc.Clear()
                Catch ex As Exception
                    MessageBox.Show(ex.ToString)
                End Try
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        If btnnew.Text = "New" Then
            ControlSetup("New")
            txtdesc.Clear()
        Else
            ControlSetup("Save")
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnedit.Click
        If btnedit.Text = "Edit" Then
            ControlSetup("Edit")
            Exit Sub
        End If
        If btnedit.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        ControlSetup("Delete")
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub

    Private Sub LoadRank()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_Select_Rank_Master")
        dgvlist.DataSource = ds.Tables(0)
        dgvlist.Columns(0).Visible = False
    End Sub

    Private Sub dgvlist_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvlist.Click
        Try
            fxid = Convert.ToInt32(dgvlist.SelectedRows(0).Cells(0).Value.ToString)
            txtdesc.Text = dgvlist.SelectedRows(0).Cells(1).Value.ToString
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try

    End Sub
End Class