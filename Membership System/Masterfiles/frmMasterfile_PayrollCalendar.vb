Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_PayrollCalendar
#Region "Variable"
    Private deduct As Boolean
    Private pksched As String
    Private fkpaycode As String

#End Region
#Region "Property"
    Private Property getdeduct() As Boolean
        Get
            Return deduct
        End Get
        Set(ByVal value As Boolean)
            deduct = value
        End Set
    End Property
    Private Property getpkschedn() As String
        Get
            Return pksched
        End Get
        Set(ByVal value As String)
            pksched = value
        End Set
    End Property
    Private Property getfkpaycode() As String
        Get
            Return fkpaycode
        End Get
        Set(ByVal value As String)
            fkpaycode = value
        End Set
    End Property
#End Region
#Region "Function"
    Public Function Numeric(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "Load Payroll Code"
    Private Sub LoadPayrollCode()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode")
        Try
            While rd.Read
                Me.cbopaycode.Text = rd.Item(1)
                Me.cbopaycode.Items.Add(rd.Item(1))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PayrollCodeParam")
        End Try
    End Sub
#End Region
#Region "Paycode with param"
    Private Sub PayrollCodeParam(ByVal desc As String)
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCodeParam", _
                                    New SqlParameter("@paydesc", desc))
        Try
            While rd.Read
                getfkpaycode() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PayrollCodeParam")
        End Try
    End Sub
#End Region
#Region "Payrollsched with paramemter"
    Private Sub loadPayschedwithparam(ByVal paycodeid As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_PaydateSchedule_Load", _
                                    New SqlParameter("@fk_paycode", paycodeid))
        Try
            With Me.lvlCalendar
                .Clear()
                .View = View.Details
                .FullRowSelect = True
                .GridLines = True
                .Columns.Add("Year", 60, HorizontalAlignment.Left)
                .Columns.Add("Period Start", 100, HorizontalAlignment.Left)
                .Columns.Add("Period End", 100, HorizontalAlignment.Left)
                .Columns.Add("Pay Date", 100, HorizontalAlignment.Left)
                .Columns.Add("Is Deductible", 100, HorizontalAlignment.Left)
                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .subitems.add(rd.Item(1))
                            .subitems.add(rd.Item(2))
                            .subitems.add(rd.Item(3))
                            .subitems.add(rd.Item(4))
                            .subitems.add(rd.Item(5))
                            .subitems.add(rd.Item(6))
                            .subitems.add(rd.Item(7))
                        End With
                    End While
                    rd.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "loadPayschedwithparam")
                End Try
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region
    Private Sub frmPaycodeCalendar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'CIMS_Masterfiles_PaydateSchedule_AddEdit
        'CIMS_Masterfiles_PaydateSchedule_Load
        'CIMS_Masterfiles_PaydateSchedule_Delete
        Call LoadPayrollCode()
        Call initialpaycodeload()
        Me.txtyear.Text = Format(Now, "yyyy")
    End Sub
#Region "initial paycode load"
    Private Sub initialpaycodeload()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Member_LoadPayCode_Initial")
        Try
            While rd.Read
                Me.cbopaycode.Text = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "initialpaycodeload")
        End Try
    End Sub
#End Region
#Region "Add/Edit PayCalendar"
    Private Sub AddEditPaycalendar(ByVal year As String, ByVal datefrom As Date, ByVal dateto As Date, _
                                  ByVal paydate As Date, ByVal deduct As Boolean, ByVal pk_schedule As String, ByVal fk_paycode As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_PaydateSchedule_AddEdit", _
                                      New SqlParameter("@pk_Paydate_Schedule", pk_schedule), _
                                      New SqlParameter("@fk_PayCode", fk_paycode), _
                                      New SqlParameter("@dtPeriodFrom", datefrom), _
                                      New SqlParameter("@dtPeriodTo", dateto), _
                                      New SqlParameter("@dtPaydate", paydate), _
                                      New SqlParameter("@dtYear", year), _
                                      New SqlParameter("@fbDeducted", deduct))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditPaycalendar")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then

                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(88, 5)
            ElseIf btnsave.Text = "Save" Then
                Call AddEditPaycalendar(Me.txtyear.Text, Me.dtfrom.Value, Me.dtto.Value, Me.dtpaydate.Value, deduct, "", fkpaycode)
                Call loadPayschedwithparam(fkpaycode)
                btnsave.Text = "New"
                btnsave.Image = My.Resources.new3
                btnclose.Text = "Close"
                btnclose.Location = New System.Drawing.Point(218, 4)
                btndelete.Visible = True
                btnupdate.Visible = True
            Else
                MessageBox.Show("Empty field found! ", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtyear.Text = Me.lvlCalendar.SelectedItems(0).Text
                Me.dtfrom.Text = Me.lvlCalendar.SelectedItems(0).SubItems(1).Text
                Me.dtto.Text = Me.lvlCalendar.SelectedItems(0).SubItems(2).Text
                Me.dtpaydate.Text = Me.lvlCalendar.SelectedItems(0).SubItems(3).Text
                Me.chkDeductive.Checked = Me.lvlCalendar.SelectedItems(0).SubItems(4).Text
                Me.cbopaycode.Text = Me.lvlCalendar.SelectedItems(0).SubItems(5).Text
                getfkpaycode() = Me.lvlCalendar.SelectedItems(0).SubItems(6).Text
                getpkschedn() = Me.lvlCalendar.SelectedItems(0).SubItems(7).Text

                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(164, 5)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                Call AddEditPaycalendar(Me.txtyear.Text, Me.dtfrom.Value, Me.dtto.Value, Me.dtpaydate.Value, deduct, pksched, fkpaycode)
                Call loadPayschedwithparam(fkpaycode)
                btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(218, 4)
                btnclose.Text = "Close"
                Me.btnsave.Enabled = True
                Me.btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Delete"
    Private Sub Delete(ByVal paysched As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_PaydateSchedule_Delete", _
                                    New SqlParameter("@pk_Paydate_Schedule", paysched))

            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getpkschedn() = Me.lvlCalendar.SelectedItems(0).SubItems(7).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call Delete(pksched)
                Call loadPayschedwithparam(fkpaycode)
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(244, 5)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub chkDeductive_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkDeductive.CheckedChanged
        If Me.chkDeductive.Checked = True Then
            getdeduct() = True
        Else
            getdeduct() = False
        End If
    End Sub

    Private Sub cbopaycode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles cbopaycode.KeyPress
        e.KeyChar = Microsoft.VisualBasic.ChrW(0)
    End Sub

    Private Sub cbopaycode_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopaycode.TextChanged
        Call PayrollCodeParam(Me.cbopaycode.Text)
        Call loadPayschedwithparam(fkpaycode)
    End Sub
#Region "Calendar keydownkeypressclick"
    Private Sub Calendarkeydownkeypressclick()
        Try
            Me.txtyear.Text = Me.lvlCalendar.SelectedItems(0).Text
            Me.dtfrom.Text = Me.lvlCalendar.SelectedItems(0).SubItems(1).Text
            Me.dtto.Text = Me.lvlCalendar.SelectedItems(0).SubItems(2).Text
            Me.dtpaydate.Text = Me.lvlCalendar.SelectedItems(0).SubItems(3).Text
            Me.chkDeductive.Checked = Me.lvlCalendar.SelectedItems(0).SubItems(4).Text
            Me.cbopaycode.Text = Me.lvlCalendar.SelectedItems(0).SubItems(5).Text
            getfkpaycode() = Me.lvlCalendar.SelectedItems(0).SubItems(6).Text
            getpkschedn() = Me.lvlCalendar.SelectedItems(0).SubItems(7).Text
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Calendarkeydownkeypressclick")
        End Try
    End Sub
#End Region

    Private Sub lvlCalendar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlCalendar.Click
        Call Calendarkeydownkeypressclick()
    End Sub


    Private Sub lvlCalendar_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlCalendar.KeyDown
        Call Calendarkeydownkeypressclick()
    End Sub

    Private Sub lvlCalendar_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlCalendar.KeyUp
        Call Calendarkeydownkeypressclick()
    End Sub

    Private Sub txtyear_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtyear.KeyPress
        e.Handled = Not Numeric(e.KeyChar)
    End Sub

    Private Sub lvlCalendar_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvlCalendar.SelectedIndexChanged

    End Sub

    Private Sub cbopaycode_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbopaycode.SelectedIndexChanged

    End Sub

    Private Sub cbopaycode_TextUpdate(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbopaycode.TextUpdate

    End Sub
End Class