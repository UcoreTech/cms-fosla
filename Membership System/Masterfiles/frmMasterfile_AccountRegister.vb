﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlTypes

Public Class frmMasterfile_AccountRegister
    Private KeyAccount, EmployeeID, fxKeyAccountRegister As String
    Private mycon As New Clsappconfiguration

    Public Property GetAccountID() As String
        Get
            Return KeyAccount
        End Get
        Set(ByVal value As String)
            KeyAccount = value
        End Set
    End Property

    Public Property GetEmployeeID() As String
        Get
            Return EmployeeID
        End Get
        Set(ByVal value As String)
            EmployeeID = value
        End Set
    End Property

    Private Sub ControlSetup(ByVal mode As String)
        If mode = "New" Then
            btnupdate.Enabled = False
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Text = "Save"
            btnBrowseAccount.Enabled = True
            btnBrowseEmployee.Enabled = True
            btnBrowseAccountNumber.Enabled = True

            txtSchedule.ReadOnly = False
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            Exit Sub
        End If
        If mode = "Save" Then
            Add_AccountRegister(txtAccountNumber.Text, KeyAccount, EmployeeID, txtName.Text, CType(txtSchedule.Text, Double), False)
            UpdateAccountInAccounting(txtAccountNumber.Text, Date.Now)
            Load_AccountRegister()
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.ReadOnly = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            Exit Sub
        End If

        If mode = "Close" Then
            Me.Close()
        End If

        If mode = "Cancel" Then
            btnupdate.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnsave.Enabled = True
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.ReadOnly = True
            txtaccountcode.Clear()
            txtaccountname.Clear()
            txtAccountNumber.Clear()
            txtName.Clear()
            txtSchedule.Clear()
            Exit Sub
        End If
        If mode = "Edit" Then
            btnupdate.Text = "Update"
            btndelete.Enabled = False
            btnclose.Text = "Cancel"
            btnsave.Enabled = False
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.ReadOnly = False
            Exit Sub
        End If
        If mode = "Update" Then
            Update_AccountRegister(fxKeyAccountRegister, txtAccountNumber.Text, KeyAccount, EmployeeID, txtName.Text, CType(txtSchedule.Text, Decimal), False)
            Load_AccountRegister()
            btnsave.Enabled = True
            btndelete.Enabled = True
            btnclose.Text = "Close"
            btnsave.Text = "New"
            btnupdate.Text = "Edit"
            btnBrowseAccount.Enabled = False
            btnBrowseEmployee.Enabled = False
            btnBrowseAccountNumber.Enabled = False

            txtSchedule.ReadOnly = True
            txtSchedule.Clear()
            txtaccountcode.Clear()
            txtaccountname.Clear()
            Exit Sub
        End If
        If mode = "Delete" Then
            If MsgBox("Are you sure you want to permanently delete this record?", vbYesNo, "Confirmation") = vbYes Then
                Delete_AccountRegister(fxKeyAccountRegister)
                UpdateAccountInAccounting(txtAccountNumber.Text, System.Data.SqlTypes.SqlDateTime.Null)
                Load_AccountRegister()
                btnBrowseAccount.Enabled = True
                btnBrowseEmployee.Enabled = True

                txtSchedule.ReadOnly = False
                txtaccountcode.Clear()
                txtaccountname.Clear()
                txtAccountNumber.Clear()
                txtName.Clear()
                txtSchedule.Clear()
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterfile_Accounts.GetActive = 2
        frmMasterfile_Accounts.ShowDialog()
    End Sub

    Private Sub btnBrowseEmployee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseEmployee.Click
        frmMember_List.ShowDialog()
    End Sub

    Private Sub btnBrowseAccountNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccountNumber.Click
        frmMasterfile_AccountNumber.GetID = 3
        frmMasterfile_AccountNumber.ShowDialog()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Close" Then
            ControlSetup("Close")
        Else
            ControlSetup("Cancel")
        End If
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        If btnsave.Text = "New" Then
            ControlSetup("New")
        Else
            If txtaccountcode.Text <> "" And txtAccountNumber.Text <> "" And txtName.Text <> "" And txtSchedule.Text <> "" Then
                ControlSetup("Save")
            Else
                MessageBox.Show("Fill up form!", "Save Account Register", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        End If
    End Sub
    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If btnupdate.Text = "Edit" Then
            If dgvListAccountRegister.SelectedCells.Count <> 0 Then
                ControlSetup("Edit")
                Exit Sub
            End If
        End If
        If btnupdate.Text = "Update" Then
            ControlSetup("Update")
            Exit Sub
        End If
    End Sub
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If dgvListAccountRegister.SelectedCells.Count <> 0 Then
            ControlSetup("Delete")
        End If
    End Sub

    Private Sub Update_AccountRegister(ByVal fxKeyAccountRegister As String, ByVal fcDocNumber As String, ByVal fxKey_Account As String,
                                        ByVal fcEmployeeNo As String,
                                        ByVal fcFullname As String, ByVal fnSchedule As Decimal, ByVal fbClosed As Boolean)
        Dim MyGuid As New Guid(fxKey_Account)
        Dim MyGuids As New Guid(fxKeyAccountRegister)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Update",
                                      New SqlParameter("@fxKeyAccountRegister", MyGuids),
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcEmployeeNo", fcEmployeeNo),
                                      New SqlParameter("@fcFullname", fcFullname),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Add_AccountRegister(ByVal fcDocNumber As String, ByVal fxKey_Account As String,
                                        ByVal fcEmployeeNo As String,
                                        ByVal fcFullname As String, ByVal fnSchedule As Double, ByVal fbClosed As Boolean)
        Dim MyGuid As New Guid(fxKey_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Insert",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fxKey_Account", MyGuid),
                                      New SqlParameter("@fcEmployeeNo", fcEmployeeNo),
                                      New SqlParameter("@fcFullname", fcFullname),
                                      New SqlParameter("@fnSchedule", fnSchedule),
                                      New SqlParameter("@fbClosed", fbClosed))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Add Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Delete_AccountRegister(ByVal fxKey_Account As String)
        Dim MyGuid As New Guid(fxKey_Account)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_Delete",
                                      New SqlParameter("@fxKeyAccountRegister", MyGuid))
            MessageBox.Show("Record Succesfully Added!")
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Delete Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Try
            SqlHelper.ExecuteNonQuery(mycon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Load_AccountRegister()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.cnstring, "CIMS_Member_AccountRegister_Load")

            With dgvListAccountRegister
                .DataSource = ds.Tables(0)
                .Columns(0).Visible = False
                .Columns(1).Visible = False

                .Columns(2).HeaderText = "Account Number"
                .Columns(2).Width = 100
                .Columns(3).HeaderText = "Employee ID"
                .Columns(3).Width = 100
                .Columns(4).HeaderText = "Name"
                .Columns(5).HeaderText = "Account"
                .Columns(6).HeaderText = "Schedule"
                .Columns(6).Width = 80
                .Columns(7).HeaderText = "Closed"
                .Columns(7).Width = 80
            End With
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Load Account Register....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmMasterfile_AccountRegister_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvListAccountRegister.Columns.Clear()
        Load_AccountRegister()
    End Sub

    Private Sub dgvListAccountRegister_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListAccountRegister.CellClick
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxKeyAccountRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value.ToString
        Next
    End Sub

    Private Sub dgvListAccountRegister_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyUp
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxKeyAccountRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value.ToString
        Next
    End Sub

    Private Sub dgvListAccountRegister_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvListAccountRegister.KeyDown
        For Each Row As DataGridViewRow In dgvListAccountRegister.SelectedRows
            fxKeyAccountRegister = Row.Cells(0).Value.ToString
            txtAccountNumber.Text = Row.Cells(2).Value.ToString
            txtaccountname.Text = Row.Cells(5).Value.ToString
            txtName.Text = Row.Cells(4).Value.ToString
            txtSchedule.Text = Row.Cells(6).Value.ToString
        Next
    End Sub
End Class