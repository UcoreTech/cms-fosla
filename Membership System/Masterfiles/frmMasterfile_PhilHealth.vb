Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMasterfile_PhilHealth
#Region "Var"
    Private fnBracketNo As Integer
#End Region
#Region "VarProperty"
    Private Property getfnBracketNo() As Integer
        Get
            Return fnBracketNo
        End Get
        Set(ByVal value As Integer)
            fnBracketNo = value
        End Set
    End Property
#End Region
#Region "AddEdit Contribution"
    Private Sub AddEditContribution(ByVal salarybase As Decimal, ByVal upperlimit As Decimal, ByVal phshare As Decimal, ByVal bracketno As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_Philhealth_AddEdit", _
                                      New SqlParameter("@fnSalaryBase", salarybase), _
                                      New SqlParameter("@fnUpperLimit", upperlimit), _
                                      New SqlParameter("@fnPHShare", phshare), _
                                      New SqlParameter("@fnBracketNo", bracketno))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditContribution")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region
#Region "Function"
    Public Function Numbers(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "Delete Contribution"
    Private Sub DeleteContributionPhil(ByVal BracketNo As Integer)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Contribution_Philhealth_Delete", _
                                      New SqlParameter("@fnBracketNo", BracketNo))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteContributionPhil")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Contribution"
    Private Sub ViewContributionPhil()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Contribution_Philhealth_Select")
        With Me.listContribution
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Salary Base", 100, HorizontalAlignment.Left)
            .Columns.Add("Upper Limit", 100, HorizontalAlignment.Left)
            .Columns.Add("PH Share", 100, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                        .SubItems.Add(rd.Item(2))
                        .SubItems.Add(rd.Item(3))
                    End With
                End While
                rd.Close()
            Catch ex As Exception

            End Try

        End With

    End Sub
#End Region
#Region "Cleartext"
    Private Sub Cleartext()
        Me.txtSB.Text = "0.00"
        Me.txtUL.Text = "0.00"
        Me.txtPhShare.Text = "0.00"
    End Sub
#End Region
    Private Sub frmContribution_PhilHealth_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewContributionPhil()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call Cleartext()
                Label1.Text = "Add PhilHealth Contribution"
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(86, 7)

            ElseIf btnsave.Text = "Save" Then
                If Me.txtSB.Text <> "" And Me.txtUL.Text <> "" And Me.txtPhShare.Text <> "" Then
                    Label1.Text = "PhilHealth Contribution"
                    Call AddEditContribution(Decimal.Parse(Me.txtSB.Text), Decimal.Parse(Me.txtUL.Text), Decimal.Parse(Me.txtPhShare.Text), 0)
                    Call ViewContributionPhil()
                    Call Cleartext()
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Label1.Text = "Edit Pag-ibig Contribution"
                Me.txtSB.Text = Me.listContribution.SelectedItems(0).Text
                Me.txtUL.Text = Me.listContribution.SelectedItems(0).SubItems(1).Text
                Me.txtPhShare.Text = Me.listContribution.SelectedItems(0).SubItems(2).Text
                getfnBracketNo() = Me.listContribution.SelectedItems(0).SubItems(3).Text
                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(164, 7)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                If Me.txtSB.Text <> "" And Me.txtUL.Text <> "" And Me.txtPhShare.Text <> "" Then
                    Label1.Text = "Pag-ibig Contribution"
                    Call AddEditContribution(Decimal.Parse(Me.txtSB.Text), Decimal.Parse(Me.txtUL.Text), Decimal.Parse(Me.txtPhShare.Text), fnBracketNo)
                    Call ViewContributionPhil()
                    Call Cleartext()
                    btndelete.Visible = True
                    btnupdate.Text = "Edit"
                    btnupdate.Image = My.Resources.edit1
                    btnclose.Text = "Cancel"
                    btnclose.Location = New System.Drawing.Point(218, 6)
                    btnclose.Text = "Close"
                    Me.btnsave.Enabled = True
                    Me.btndelete.Visible = True

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getfnBracketNo() = Me.listContribution.SelectedItems(0).SubItems(3).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteContributionPhil(fnBracketNo)
                Call ViewContributionPhil()
                Call Cleartext()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "PhilHealth Contribution"
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(244, 7)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub txtSB_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtSB.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtUL_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUL.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

    Private Sub txtPhShare_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPhShare.KeyPress
        e.Handled = Not Numbers(e.KeyChar)
    End Sub

End Class