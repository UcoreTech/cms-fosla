'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: August 10, 2010                    ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInterest_Main

    Private Gcon As New Clsappconfiguration
    Private xMonthInt As Integer = 0
    Private xYear As Integer
    Private xJan As String = ""
    Private xFeb As String = ""
    Private xMar As String = ""
    Private xAprl As String = ""
    Private xMay As String = ""
    Private xJun As String = ""
    Private xJul As String = ""
    Private xAug As String = ""
    Private xSept As String = ""
    Private xOct As String = ""
    Private xNov As String = ""
    Private xDec As String = ""

    Private Sub loadYearValue()

        BWLJan.StatusValue = False
        BWLFeb.StatusValue = False
        BWLMar.StatusValue = False
        BWLAprl.StatusValue = False
        BWLMay.StatusValue = False
        BWLJun.StatusValue = False
        BWLJul.StatusValue = False
        BWLAug.StatusValue = False
        BWLSept.StatusValue = False
        BWLOct.StatusValue = False
        BWLNov.StatusValue = False
        BWLDec.StatusValue = False

        BWLJan.ButtonEnabled = True
        BWLFeb.ButtonEnabled = True
        BWLMar.ButtonEnabled = True
        BWLAprl.ButtonEnabled = True
        BWLMay.ButtonEnabled = True
        BWLJun.ButtonEnabled = True
        BWLJul.ButtonEnabled = True
        BWLAug.ButtonEnabled = True
        BWLSept.ButtonEnabled = True
        BWLOct.ButtonEnabled = True
        BWLNov.ButtonEnabled = True
        BWLDec.ButtonEnabled = True

        xJan = ""
        xFeb = ""
        xMar = ""
        xAprl = ""
        xMay = ""
        xJun = ""
        xJul = ""
        xAug = ""
        xSept = ""
        xOct = ""
        xDec = ""
        xNov = ""

        Dim sSqlCmd As String = "CIMS_Accrual_Interest_Indicator '" & CboYear.Text & "'"
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(Gcon.cnstring, CommandType.Text, sSqlCmd)
            While rd.Read
                Select Case rd.Item(0).ToString
                    Case 1
                        BWLJan.StatusValue = True
                        BWLJan.ButtonEnabled = False
                        xJan = "Accrued"
                    Case 2
                        BWLFeb.StatusValue = True
                        BWLFeb.ButtonEnabled = False
                        xFeb = "Accrued"
                    Case 3
                        BWLMar.StatusValue = True
                        BWLMar.ButtonEnabled = False
                        xMar = "Accrued"
                    Case 4
                        BWLAprl.StatusValue = True
                        BWLAprl.ButtonEnabled = False
                        xAprl = "Accrued"
                    Case 5
                        BWLMay.StatusValue = True
                        BWLMay.ButtonEnabled = False
                        xMay = "Accrued"
                    Case 6
                        BWLJun.StatusValue = True
                        BWLJun.ButtonEnabled = False
                        xJun = "Accrued"
                    Case 7
                        BWLJul.StatusValue = True
                        BWLJul.ButtonEnabled = False
                        xJul = "Accrued"
                    Case 8
                        BWLAug.StatusValue = True
                        BWLAug.ButtonEnabled = False
                        xAug = "Accrued"
                    Case 9
                        BWLSept.StatusValue = True
                        BWLSept.ButtonEnabled = False
                        xSept = "Accrued"
                    Case 10
                        BWLOct.StatusValue = True
                        BWLOct.ButtonEnabled = False
                        xOct = "Accrued"
                    Case 11
                        BWLNov.StatusValue = True
                        BWLNov.ButtonEnabled = False
                        xNov = "Accrued"
                    Case 12
                        BWLDec.StatusValue = True
                        BWLDec.ButtonEnabled = False
                        xDec = "Accrued"
                End Select
            End While
        End Using
    End Sub

    Private Sub frmInterestIncome_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CboYear.SelectedText = Now.Year.ToString
        loadYearValue()
    End Sub

    Private Sub CboYear_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CboYear.SelectedIndexChanged
        loadYearValue()
        xYear = CboYear.Text
    End Sub

    Private Sub BtnAccrue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnAccrue.Click
        If xMonthInt <> 0 Then
            Dim xResponse As MsgBoxResult = MsgBox("Are you sure you want to accrue the month that you've selected?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirm")
            If xResponse = MsgBoxResult.Yes Then
                xYear = CboYear.Text
                Dim xForm As New frmInterest_Accrue
                xForm.xYear = xYear
                xForm.xMonthInt = xMonthInt
                xForm.ShowDialog()
                loadYearValue()
                xMonthInt = 0
            End If
        Else
            MsgBox("Please check first the month to be accrued.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
        End If
    End Sub

    Private Sub buttonClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles BWLJan.buttonClick, BWLFeb.buttonClick, BWLMar.buttonClick, _
                                                                                        BWLAprl.buttonClick, BWLMay.buttonClick, BWLJun.buttonClick, _
                                                                                        BWLJul.buttonClick, BWLAug.buttonClick, BWLSept.buttonClick, _
                                                                                        BWLOct.buttonClick, BWLNov.buttonClick, BWLDec.buttonClick

        If sender.StatusValue = False Then
            xMonthInt = sender.Tag
            Select Case sender.name
                Case "BWLJan"
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLFeb"
                    BWLJan.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLMar"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLAprl"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLMay"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLJun"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLJul"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLAug"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLSept"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLOct"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLNov"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLDec.ButtonEnabled = False

                Case "BWLDec"
                    BWLJan.ButtonEnabled = False
                    BWLFeb.ButtonEnabled = False
                    BWLMar.ButtonEnabled = False
                    BWLAprl.ButtonEnabled = False
                    BWLMay.ButtonEnabled = False
                    BWLJun.ButtonEnabled = False
                    BWLJul.ButtonEnabled = False
                    BWLAug.ButtonEnabled = False
                    BWLSept.ButtonEnabled = False
                    BWLOct.ButtonEnabled = False
                    BWLNov.ButtonEnabled = False
            End Select
        Else
            xMonthInt = 0
            If xJan <> "Accrued" Then
                BWLJan.ButtonEnabled = True
            End If
            If xFeb <> "Accrued" Then
                BWLFeb.ButtonEnabled = True
            End If
            If xMar <> "Accrued" Then
                BWLMar.ButtonEnabled = True
            End If
            If xAprl <> "Accrued" Then
                BWLAprl.ButtonEnabled = True
            End If
            If xMay <> "Accrued" Then
                BWLMay.ButtonEnabled = True
            End If
            If xJun <> "Accrued" Then
                BWLJun.ButtonEnabled = True
            End If
            If xJul <> "Accrued" Then
                BWLJul.ButtonEnabled = True
            End If
            If xAug <> "Accrued" Then
                BWLAug.ButtonEnabled = True
            End If
            If xSept <> "Accrued" Then
                BWLSept.ButtonEnabled = True
            End If
            If xOct <> "Accrued" Then
                BWLOct.ButtonEnabled = True
            End If
            If xNov <> "Accrued" Then
                BWLNov.ButtonEnabled = True
            End If
            If xDec <> "Accrued" Then
                BWLDec.ButtonEnabled = True
            End If
        End If
    End Sub

    Private Sub BtnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click
        Dim xForm As New frmInterest_ReportOption
        xForm.ShowDialog()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub
End Class