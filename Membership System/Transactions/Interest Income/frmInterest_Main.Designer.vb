<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInterest_Main
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmInterest_Main))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtnPreview = New System.Windows.Forms.Button()
        Me.BtnAccrue = New System.Windows.Forms.Button()
        Me.CboYear = New System.Windows.Forms.ComboBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.BWLJan = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLFeb = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLMar = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLAprl = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLMay = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLJun = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLJul = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLAug = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLSept = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLOct = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLNov = New ButtonWithLabel.ButtonWithLabel()
        Me.BWLDec = New ButtonWithLabel.ButtonWithLabel()
        Me.PanelFooter = New System.Windows.Forms.Panel()
        Me.GroupBox1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.PanelFooter.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(9, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(32, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Year:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtnPreview)
        Me.GroupBox1.Controls.Add(Me.BtnAccrue)
        Me.GroupBox1.Controls.Add(Me.CboYear)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(347, 60)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'BtnPreview
        '
        Me.BtnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnPreview.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.BtnPreview.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnPreview.Image = Global.WindowsApplication2.My.Resources.Resources.printer
        Me.BtnPreview.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnPreview.Location = New System.Drawing.Point(216, 19)
        Me.BtnPreview.Name = "BtnPreview"
        Me.BtnPreview.Size = New System.Drawing.Size(125, 32)
        Me.BtnPreview.TabIndex = 3
        Me.BtnPreview.Text = "Preview Report"
        Me.BtnPreview.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnPreview.UseVisualStyleBackColor = True
        '
        'BtnAccrue
        '
        Me.BtnAccrue.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAccrue.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.BtnAccrue.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnAccrue.Location = New System.Drawing.Point(127, 19)
        Me.BtnAccrue.Name = "BtnAccrue"
        Me.BtnAccrue.Size = New System.Drawing.Size(86, 32)
        Me.BtnAccrue.TabIndex = 2
        Me.BtnAccrue.Text = "Accrue"
        Me.BtnAccrue.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnAccrue.UseVisualStyleBackColor = True
        '
        'CboYear
        '
        Me.CboYear.FormattingEnabled = True
        Me.CboYear.Items.AddRange(New Object() {"2007", "2008", "2009", "2010", "2011", "2012", "2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020"})
        Me.CboYear.Location = New System.Drawing.Point(12, 25)
        Me.CboYear.Name = "CboYear"
        Me.CboYear.Size = New System.Drawing.Size(109, 21)
        Me.CboYear.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(264, 6)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(80, 28)
        Me.btnClose.TabIndex = 4
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLJan)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLFeb)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLMar)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLAprl)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLMay)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLJun)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLJul)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLAug)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLSept)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLOct)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLNov)
        Me.FlowLayoutPanel1.Controls.Add(Me.BWLDec)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 60)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(347, 258)
        Me.FlowLayoutPanel1.TabIndex = 1
        '
        'BWLJan
        '
        Me.BWLJan.BackColor = System.Drawing.Color.Transparent
        Me.BWLJan.ButtonEnabled = True
        Me.BWLJan.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJan.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLJan.ButtonImage = Nothing
        Me.BWLJan.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLJan.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLJan.ButtonText = ""
        Me.BWLJan.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLJan.ImageChecked = CType(resources.GetObject("BWLJan.ImageChecked"), System.Drawing.Image)
        Me.BWLJan.ImageUnchecked = Nothing
        Me.BWLJan.LabelBackColor = System.Drawing.Color.Green
        Me.BWLJan.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJan.LabelForeColor = System.Drawing.Color.White
        Me.BWLJan.LabelText = "January"
        Me.BWLJan.Location = New System.Drawing.Point(3, 3)
        Me.BWLJan.Name = "BWLJan"
        Me.BWLJan.Size = New System.Drawing.Size(80, 76)
        Me.BWLJan.StatusValue = False
        Me.BWLJan.TabIndex = 0
        Me.BWLJan.Tag = "1"
        '
        'BWLFeb
        '
        Me.BWLFeb.BackColor = System.Drawing.Color.Transparent
        Me.BWLFeb.ButtonEnabled = True
        Me.BWLFeb.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLFeb.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLFeb.ButtonImage = Nothing
        Me.BWLFeb.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLFeb.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLFeb.ButtonText = ""
        Me.BWLFeb.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLFeb.ImageChecked = CType(resources.GetObject("BWLFeb.ImageChecked"), System.Drawing.Image)
        Me.BWLFeb.ImageUnchecked = Nothing
        Me.BWLFeb.LabelBackColor = System.Drawing.Color.Green
        Me.BWLFeb.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLFeb.LabelForeColor = System.Drawing.Color.White
        Me.BWLFeb.LabelText = "February"
        Me.BWLFeb.Location = New System.Drawing.Point(89, 3)
        Me.BWLFeb.Name = "BWLFeb"
        Me.BWLFeb.Size = New System.Drawing.Size(80, 76)
        Me.BWLFeb.StatusValue = False
        Me.BWLFeb.TabIndex = 1
        Me.BWLFeb.Tag = "2"
        '
        'BWLMar
        '
        Me.BWLMar.BackColor = System.Drawing.Color.Transparent
        Me.BWLMar.ButtonEnabled = True
        Me.BWLMar.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLMar.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLMar.ButtonImage = Nothing
        Me.BWLMar.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLMar.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLMar.ButtonText = ""
        Me.BWLMar.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLMar.ImageChecked = CType(resources.GetObject("BWLMar.ImageChecked"), System.Drawing.Image)
        Me.BWLMar.ImageUnchecked = Nothing
        Me.BWLMar.LabelBackColor = System.Drawing.Color.Green
        Me.BWLMar.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLMar.LabelForeColor = System.Drawing.Color.White
        Me.BWLMar.LabelText = "March"
        Me.BWLMar.Location = New System.Drawing.Point(175, 3)
        Me.BWLMar.Name = "BWLMar"
        Me.BWLMar.Size = New System.Drawing.Size(80, 76)
        Me.BWLMar.StatusValue = False
        Me.BWLMar.TabIndex = 2
        Me.BWLMar.Tag = "3"
        '
        'BWLAprl
        '
        Me.BWLAprl.BackColor = System.Drawing.Color.Transparent
        Me.BWLAprl.ButtonEnabled = True
        Me.BWLAprl.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLAprl.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLAprl.ButtonImage = Nothing
        Me.BWLAprl.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLAprl.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLAprl.ButtonText = ""
        Me.BWLAprl.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLAprl.ImageChecked = CType(resources.GetObject("BWLAprl.ImageChecked"), System.Drawing.Image)
        Me.BWLAprl.ImageUnchecked = Nothing
        Me.BWLAprl.LabelBackColor = System.Drawing.Color.Green
        Me.BWLAprl.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLAprl.LabelForeColor = System.Drawing.Color.White
        Me.BWLAprl.LabelText = "April"
        Me.BWLAprl.Location = New System.Drawing.Point(261, 3)
        Me.BWLAprl.Name = "BWLAprl"
        Me.BWLAprl.Size = New System.Drawing.Size(80, 76)
        Me.BWLAprl.StatusValue = False
        Me.BWLAprl.TabIndex = 3
        Me.BWLAprl.Tag = "4"
        '
        'BWLMay
        '
        Me.BWLMay.BackColor = System.Drawing.Color.Transparent
        Me.BWLMay.ButtonEnabled = True
        Me.BWLMay.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLMay.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLMay.ButtonImage = Nothing
        Me.BWLMay.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLMay.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLMay.ButtonText = ""
        Me.BWLMay.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLMay.ImageChecked = CType(resources.GetObject("BWLMay.ImageChecked"), System.Drawing.Image)
        Me.BWLMay.ImageUnchecked = Nothing
        Me.BWLMay.LabelBackColor = System.Drawing.Color.Green
        Me.BWLMay.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLMay.LabelForeColor = System.Drawing.Color.White
        Me.BWLMay.LabelText = "May"
        Me.BWLMay.Location = New System.Drawing.Point(3, 85)
        Me.BWLMay.Name = "BWLMay"
        Me.BWLMay.Size = New System.Drawing.Size(80, 76)
        Me.BWLMay.StatusValue = False
        Me.BWLMay.TabIndex = 4
        Me.BWLMay.Tag = "5"
        '
        'BWLJun
        '
        Me.BWLJun.BackColor = System.Drawing.Color.Transparent
        Me.BWLJun.ButtonEnabled = True
        Me.BWLJun.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJun.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLJun.ButtonImage = Nothing
        Me.BWLJun.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLJun.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLJun.ButtonText = ""
        Me.BWLJun.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLJun.ImageChecked = CType(resources.GetObject("BWLJun.ImageChecked"), System.Drawing.Image)
        Me.BWLJun.ImageUnchecked = Nothing
        Me.BWLJun.LabelBackColor = System.Drawing.Color.Green
        Me.BWLJun.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJun.LabelForeColor = System.Drawing.Color.White
        Me.BWLJun.LabelText = "June"
        Me.BWLJun.Location = New System.Drawing.Point(89, 85)
        Me.BWLJun.Name = "BWLJun"
        Me.BWLJun.Size = New System.Drawing.Size(80, 76)
        Me.BWLJun.StatusValue = False
        Me.BWLJun.TabIndex = 5
        Me.BWLJun.Tag = "6"
        '
        'BWLJul
        '
        Me.BWLJul.BackColor = System.Drawing.Color.Transparent
        Me.BWLJul.ButtonEnabled = True
        Me.BWLJul.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJul.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLJul.ButtonImage = Nothing
        Me.BWLJul.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLJul.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLJul.ButtonText = ""
        Me.BWLJul.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLJul.ImageChecked = CType(resources.GetObject("BWLJul.ImageChecked"), System.Drawing.Image)
        Me.BWLJul.ImageUnchecked = Nothing
        Me.BWLJul.LabelBackColor = System.Drawing.Color.Green
        Me.BWLJul.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLJul.LabelForeColor = System.Drawing.Color.White
        Me.BWLJul.LabelText = "July"
        Me.BWLJul.Location = New System.Drawing.Point(175, 85)
        Me.BWLJul.Name = "BWLJul"
        Me.BWLJul.Size = New System.Drawing.Size(80, 76)
        Me.BWLJul.StatusValue = False
        Me.BWLJul.TabIndex = 6
        Me.BWLJul.Tag = "7"
        '
        'BWLAug
        '
        Me.BWLAug.BackColor = System.Drawing.Color.Transparent
        Me.BWLAug.ButtonEnabled = True
        Me.BWLAug.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLAug.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLAug.ButtonImage = Nothing
        Me.BWLAug.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLAug.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLAug.ButtonText = ""
        Me.BWLAug.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLAug.ImageChecked = CType(resources.GetObject("BWLAug.ImageChecked"), System.Drawing.Image)
        Me.BWLAug.ImageUnchecked = Nothing
        Me.BWLAug.LabelBackColor = System.Drawing.Color.Green
        Me.BWLAug.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLAug.LabelForeColor = System.Drawing.Color.White
        Me.BWLAug.LabelText = "August"
        Me.BWLAug.Location = New System.Drawing.Point(261, 85)
        Me.BWLAug.Name = "BWLAug"
        Me.BWLAug.Size = New System.Drawing.Size(80, 76)
        Me.BWLAug.StatusValue = False
        Me.BWLAug.TabIndex = 7
        Me.BWLAug.Tag = "8"
        '
        'BWLSept
        '
        Me.BWLSept.BackColor = System.Drawing.Color.Transparent
        Me.BWLSept.ButtonEnabled = True
        Me.BWLSept.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLSept.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLSept.ButtonImage = Nothing
        Me.BWLSept.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLSept.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLSept.ButtonText = ""
        Me.BWLSept.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLSept.ImageChecked = CType(resources.GetObject("BWLSept.ImageChecked"), System.Drawing.Image)
        Me.BWLSept.ImageUnchecked = Nothing
        Me.BWLSept.LabelBackColor = System.Drawing.Color.Green
        Me.BWLSept.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLSept.LabelForeColor = System.Drawing.Color.White
        Me.BWLSept.LabelText = "September"
        Me.BWLSept.Location = New System.Drawing.Point(3, 167)
        Me.BWLSept.Name = "BWLSept"
        Me.BWLSept.Size = New System.Drawing.Size(80, 76)
        Me.BWLSept.StatusValue = False
        Me.BWLSept.TabIndex = 8
        Me.BWLSept.Tag = "9"
        '
        'BWLOct
        '
        Me.BWLOct.BackColor = System.Drawing.Color.Transparent
        Me.BWLOct.ButtonEnabled = True
        Me.BWLOct.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLOct.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLOct.ButtonImage = Nothing
        Me.BWLOct.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLOct.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLOct.ButtonText = ""
        Me.BWLOct.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLOct.ImageChecked = CType(resources.GetObject("BWLOct.ImageChecked"), System.Drawing.Image)
        Me.BWLOct.ImageUnchecked = Nothing
        Me.BWLOct.LabelBackColor = System.Drawing.Color.Green
        Me.BWLOct.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLOct.LabelForeColor = System.Drawing.Color.White
        Me.BWLOct.LabelText = "October"
        Me.BWLOct.Location = New System.Drawing.Point(89, 167)
        Me.BWLOct.Name = "BWLOct"
        Me.BWLOct.Size = New System.Drawing.Size(80, 76)
        Me.BWLOct.StatusValue = False
        Me.BWLOct.TabIndex = 9
        Me.BWLOct.Tag = "10"
        '
        'BWLNov
        '
        Me.BWLNov.BackColor = System.Drawing.Color.Transparent
        Me.BWLNov.ButtonEnabled = True
        Me.BWLNov.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLNov.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLNov.ButtonImage = Nothing
        Me.BWLNov.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLNov.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLNov.ButtonText = ""
        Me.BWLNov.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLNov.ImageChecked = CType(resources.GetObject("BWLNov.ImageChecked"), System.Drawing.Image)
        Me.BWLNov.ImageUnchecked = Nothing
        Me.BWLNov.LabelBackColor = System.Drawing.Color.Green
        Me.BWLNov.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLNov.LabelForeColor = System.Drawing.Color.White
        Me.BWLNov.LabelText = "November"
        Me.BWLNov.Location = New System.Drawing.Point(175, 167)
        Me.BWLNov.Name = "BWLNov"
        Me.BWLNov.Size = New System.Drawing.Size(80, 76)
        Me.BWLNov.StatusValue = False
        Me.BWLNov.TabIndex = 10
        Me.BWLNov.Tag = "11"
        '
        'BWLDec
        '
        Me.BWLDec.BackColor = System.Drawing.Color.Transparent
        Me.BWLDec.ButtonEnabled = True
        Me.BWLDec.ButtonFont = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLDec.ButtonForeColor = System.Drawing.SystemColors.ControlText
        Me.BWLDec.ButtonImage = Nothing
        Me.BWLDec.ButtonImageAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.BWLDec.ButtonStyle = System.Windows.Forms.FlatStyle.Standard
        Me.BWLDec.ButtonText = ""
        Me.BWLDec.ButtonTextImageLayout = System.Windows.Forms.TextImageRelation.Overlay
        Me.BWLDec.ImageChecked = CType(resources.GetObject("BWLDec.ImageChecked"), System.Drawing.Image)
        Me.BWLDec.ImageUnchecked = Nothing
        Me.BWLDec.LabelBackColor = System.Drawing.Color.Green
        Me.BWLDec.LabelFont = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BWLDec.LabelForeColor = System.Drawing.Color.White
        Me.BWLDec.LabelText = "December"
        Me.BWLDec.Location = New System.Drawing.Point(261, 167)
        Me.BWLDec.Name = "BWLDec"
        Me.BWLDec.Size = New System.Drawing.Size(80, 76)
        Me.BWLDec.StatusValue = False
        Me.BWLDec.TabIndex = 11
        Me.BWLDec.Tag = "12"
        '
        'PanelFooter
        '
        Me.PanelFooter.Controls.Add(Me.btnClose)
        Me.PanelFooter.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanelFooter.Location = New System.Drawing.Point(0, 318)
        Me.PanelFooter.Name = "PanelFooter"
        Me.PanelFooter.Size = New System.Drawing.Size(347, 37)
        Me.PanelFooter.TabIndex = 2
        '
        'frmInterest_Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.CancelButton = Me.btnClose
        Me.ClientSize = New System.Drawing.Size(347, 355)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.PanelFooter)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(355, 382)
        Me.Name = "frmInterest_Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Accrual of Interest"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.PanelFooter.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnPreview As System.Windows.Forms.Button
    Friend WithEvents BtnAccrue As System.Windows.Forms.Button
    Friend WithEvents CboYear As System.Windows.Forms.ComboBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents BWLFeb As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLJan As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLMar As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLAprl As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLMay As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLJun As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLJul As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLAug As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLSept As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLOct As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLNov As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents BWLDec As ButtonWithLabel.ButtonWithLabel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents PanelFooter As System.Windows.Forms.Panel
End Class
