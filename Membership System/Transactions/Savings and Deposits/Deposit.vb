Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words
Public Class Deposit
    Private gcon As New Clsappconfiguration
    Private serialNo As String
#Region "Property"
    Private idNo As String
    Public Property GetIDNo() As String
        Get
            Return idNo
        End Get
        Set(ByVal value As String)
            idNo = value
        End Set
    End Property

    Private jvKeyID As String
    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property


    Private KeyJVno As String
    Public Property GetJVno() As String
        Get
            Return KeyJVno
        End Get
        Set(ByVal value As String)
            KeyJVno = value
        End Set
    End Property

    Private RefNo As String
    Public Property GetRefNo() As String
        Get
            Return RefNo
        End Get
        Set(ByVal value As String)
            RefNo = value
        End Set
    End Property

    Private RefNoMember As String
    Public Property GetRefNoForMember() As String
        Get
            Return RefNoMember
        End Get
        Set(ByVal value As String)
            RefNoMember = value
        End Set
    End Property

    'Private RefNo As String
    'Public Property GetRefNo() As String
    '    Get
    '        Return RefNo
    '    End Get
    '    Set(ByVal value As String)
    '        RefNo = value
    '    End Set
    'End Property
    Private Name As String
    Public Property GetName() As String
        Get
            Return Name
        End Get
        Set(ByVal value As String)
            Name = value
        End Set
    End Property
    Private SelectSTD As String
    Public Property GetSelectSTD() As String
        Get
            Return SelectSTD
        End Get
        Set(ByVal value As String)
            SelectSTD = value
        End Set
    End Property
    Private UniqEmp As String
    Public Property GetUniqEmp() As String
        Get
            Return UniqEmp
        End Get
        Set(ByVal value As String)
            UniqEmp = value
        End Set
    End Property
#End Region

    Private Sub Deposit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cbobanks.SelectedIndex = "0"
        LoadDepositDetails()
        txtamount.Clear()
        txtId.Text = GetIDNo()
        txtname.Text = GetName()
        'txtRefNo.Text = GetRefNo()
        Description()
        cboPaymethod.SelectedIndex = "0"
        Me.txtIntr.Clear()
        Me.txtcheck.Clear()
        Me.txtterm.Clear()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub
    Private Sub LoadDepositDetails()
        Dim BankLoad As String = "CIMS_m_Policies_BankSettings_BankList_Load"
        Try
            cbobanks.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, BankLoad).Tables(0).DefaultView
            cbobanks.DisplayMember = "Bank Name"
            cbobanks.ValueMember = "ID"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Description()
        'If SelectSTD = "Debit" Then
        '    'Me.txtDes.Text = "Debit -"
        '    Me.txtterm.Enabled = False
        '    Me.txtIntr.Enabled = False
        '    Me.txtIntr.BackColor = Color.Gainsboro
        '    Me.txtterm.BackColor = Color.Gainsboro
        '    Me.txtterm.BorderStyle = BorderStyle.None
        '    Me.txtIntr.BorderStyle = BorderStyle.None
        '    Me.btnDeposit.Enabled = True
        'Else
        '    'Me.txtDes.Text = "Credit -"
        '    Me.txtterm.Enabled = True
        '    Me.txtIntr.Enabled = True
        '    Me.txtIntr.BackColor = Color.White
        '    Me.txtterm.BackColor = Color.White
        '    Me.txtterm.BorderStyle = BorderStyle.Fixed3D
        '    Me.txtIntr.BorderStyle = BorderStyle.Fixed3D
        '    Me.btnDeposit.Enabled = False
        'End If
    End Sub
    Private Sub cboPaymethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymethod.SelectedIndexChanged
        If cboPaymethod.SelectedItem = "Cash" Then
            cbobanks.Enabled = False
            txtcheck.Enabled = False
        Else
            cbobanks.Enabled = True
            txtcheck.Enabled = True
        End If
    End Sub
    Private Sub DepositPay(ByVal Depdate As Date, ByVal descrip As String, ByVal UniqEmp As String, _
                            ByVal Depamnt As Decimal, ByVal paymthd As String, ByVal chckno As String, _
                            ByVal bankid As Integer, ByVal clear As Boolean, ByVal fcRefNo As String, ByVal fcDocNo As String, ByVal KeyAccount As String)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Dim originalGuid As New Guid(UniqEmp)
        Dim originalGuid1 As New Guid(KeyAccount)
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Deposit_Savings_Deposit", _
                New SqlParameter("@dtDepdate", Depdate), _
                New SqlParameter("@fcDes", descrip), _
                New SqlParameter("@Emp", originalGuid), _
                New SqlParameter("@fdDepAmnt", Depamnt), _
                New SqlParameter("@paymethod", paymthd), _
                New SqlParameter("@fcCheckNo", chckno), _
                New SqlParameter("@bankID", bankid), _
                New SqlParameter("@clear", clear), _
                New SqlParameter("@fcRefNo", fcRefNo), _
                New SqlParameter("@fcDocNumber", fcDocNo),
                New SqlParameter("@fxKey_Account", originalGuid1))
            trans.Commit()
            UpdateAccountInAccounting(fcDocNo, Date.Now)
            MessageBox.Show("Success!")
            gcon.sqlconn.Close()
            Me.Close()
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        End Try
    End Sub
    'Private Sub TimeDepositPay(ByVal Depdate As Date, ByVal descrip As String, ByVal UniqEmp As String, _
    '                        ByVal Depamnt As Decimal, ByVal paymthd As String, ByVal chckno As String, _
    '                        ByVal term As Integer, ByVal bankid As Integer, ByVal tdclear As Boolean)

    '    Me.txtterm.Clear()
    '    Me.txtcheck.Clear()
    '    gcon.sqlconn.Open()
    '    Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
    '    Try
    '        SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Deposit_Time_Deposit_Add", _
    '            New SqlParameter("@dtDepdate", Depdate), _
    '            New SqlParameter("@fcDes", descrip), _
    '            New SqlParameter("@Emp", UniqEmp), _
    '            New SqlParameter("@fdDepAmnt", Depamnt), _
    '            New SqlParameter("@paymethod", paymthd), _
    '            New SqlParameter("@fcCheckNo", chckno), _
    '            New SqlParameter("@term", term), _
    '            New SqlParameter("@bankID", bankid), _
    '            New SqlParameter("@tdClear", tdclear))

    '        trans.Commit()

    '        MessageBox.Show("Success!")
    '        Me.Close()

    '    Catch ex As Exception
    '        trans.Rollback()
    '        MessageBox.Show(ex.Message, "Deposit", MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    Finally
    '        gcon.sqlconn.Close()
    '    End Try
    'End Sub
    Private Sub btnDeposit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeposit.Click
        Try
            'Generate_JournalNo_Serial(dtDepdate.Value, "CC")
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to Apply Deposit?", "Confirm Deposit", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = Windows.Forms.DialogResult.OK Then
                'If SelectSTD = "Debit" Then
                If txtRefNo.Text = "" Then
                    MessageBox.Show("Please select Document Number!", "Deposit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    If Me.cboPaymethod.SelectedItem = "Cash" Then
                        DepositPay(dtDepdate.Value, txtD.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, cbobanks.SelectedValue, 1, RefNoMember, txtRefNo.Text, txtDes.Tag.ToString)
                        Generate_JournalNo_Serial(Date.Now, "DS")
                        AccountingEntry_Deposit(Date.Now, False, GetIDNo, txtD.Text, txtamount.Text, getusername, "OR", serialNo,
                        txtRefNo.Text, False, RefNoMember)
                    Else
                        DepositPay(dtDepdate.Value, txtD.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, cbobanks.SelectedValue, 0, RefNoMember, txtRefNo.Text, txtDes.Tag.ToString)
                        Generate_JournalNo_Serial(Date.Now, "DS")
                        AccountingEntry_Deposit(Date.Now, False, GetIDNo, txtD.Text, txtamount.Text, getusername, "OR", serialNo,
                                    txtRefNo.Text, False, RefNoMember)
                    End If
                End If

                'Accounting Entry Integration (Savings)
                'Call AccountingEntry_SavingsDeposit(txtId.Text, txtamount.Text, txtDes.Text, dtDepdate.Value, frmMain.username)
                'Call UpdateAccountInAccounting(txtRefNo.Text, Date.Now)
                'Else
                '    If Me.cboPaymethod.SelectedItem = "Cash" Then
                '        Call TimeDepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, txtterm.Text, cbobanks.SelectedValue, 1)
                '    Else
                '        Call TimeDepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, txtterm.Text, cbobanks.SelectedValue, 0)
                '    End If

                '    ' Accounting Entry Integration (Time)
                '    Call AccountingEntry_TimeDeposit(txtId.Text, txtamount.Text, txtDes.Text, dtDepdate.Value, frmMain.username)
                'End If
            ElseIf x = Windows.Forms.DialogResult.Cancel Then

            End If
        Catch ex As Exception
            'Throw ex
            MessageBox.Show(ex.Message, "Deposit", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub



    Private Sub UpdateAccountInAccounting(ByVal fcDocNumber As String, ByVal fdDateUsed As Date)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_Member_AccountRegister_UpdateAccount",
                                      New SqlParameter("@fcDocNumber", fcDocNumber),
                                      New SqlParameter("@fdDateUsed", fdDateUsed))
        Catch ex As Exception
            MessageBox.Show("Error! " + ex.ToString, "Update Document Number....", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub AccountingEntry_TimeDeposit(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
    '    Try
    '        SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_TimeDeposit_AccountingEntries_ToGenJournal", _
    '            New SqlParameter("@employeeNo", empNo), _
    '            New SqlParameter("@amountDeposited", amount), _
    '            New SqlParameter("@particulars", particulars), _
    '            New SqlParameter("@effectiveDate", effectiveDate), _
    '            New SqlParameter("@user", user))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub AccountingEntry_SavingsDeposit(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_SavingsDeposit_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountDeposited", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtamount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtamount.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtamount.Focus()
        End If
    End Sub

    Private Sub txtterm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtterm.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtterm.Focus()
        End If
    End Sub

    'Private Sub txtterm_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtterm.TextChanged
    '    If SelectSTD = "Time Deposit" Then
    '        If Me.txtterm.Text <> "" Then
    '            Me.btnDeposit.Enabled = True
    '        Else
    '            btnDeposit.Enabled = False
    '        End If
    '    Else
    '    End If
    'End Sub
    Private Sub txtIntr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIntr.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtIntr.Focus()
        End If
    End Sub

    'Private Sub txtIntr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIntr.TextChanged
    '    If SelectSTD = "Time Deposit" Then
    '        If Me.txtIntr.Text <> "" Then
    '            Me.btnDeposit.Enabled = True
    '        Else
    '            btnDeposit.Enabled = False
    '        End If
    '    Else
    '    End If
    'End SuB

#Region "SAVE ACCOUNTING ENTRY"
    Private Sub AccountingEntry_Deposit(ByVal fdTransDate As Date,
                                         ByVal fbAdjust As Boolean,
                                         ByVal fcEmployeeNo As String,
                                         ByVal fcMemo As String,
                                         ByVal fdTotAmt As Double,
                                         ByVal user As String,
                                         ByVal doctypeInitial As String,
                                         ByVal maxSerialNo As Integer,
                                         ByVal fiEntryNo As String,
                                         ByVal fbIsCancelled As Boolean,
                                         ByVal fcAccRef As String)
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "[CAS_t_GeneralJournal_Header_InsertUpdate]", _
                New SqlParameter("@fxKeyJVNo", GetJVKeyID), _
                New SqlParameter("@fdTransDate", fdTransDate), _
                New SqlParameter("@fbAdjust", fbAdjust), _
                New SqlParameter("@fcEmployeeNo", fcEmployeeNo), _
                New SqlParameter("@fcMemo", fcMemo), _
                New SqlParameter("@fdTotAmt", fdTotAmt), _
                New SqlParameter("@user", user), _
                New SqlParameter("@doctypeInitial", doctypeInitial), _
                New SqlParameter("@maxSerialNo", maxSerialNo), _
                New SqlParameter("@fiEntryNo", fiEntryNo), _
                New SqlParameter("@fbIsCancelled", fbIsCancelled), _
                New SqlParameter("@fcAccRef", fcAccRef))
            If Saving_and_Deposit_New.cbosavtd.Text = "Debit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtDes.Tag.ToString, txtamount.Text, 0, txtD.Text,
                                          " ", " ", GetJVKeyID, Date.Now)

                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, 0, txtamount.Text, txtD.Text,
                                          " ", " ", GetJVKeyID, Date.Now)
                'frmCreditDebit.GetAccount = Saving_and_Deposit_New.cbosavtd.Text
                'frmCreditDebit.txtRefNo.Text = Me.txtRefNo.Text
                'frmCreditDebit.txtamount.Text = Me.txtamount.Text
                'frmCreditDebit.GetjvKeyID = GetJVKeyID
                'frmCreditDebit.GetActive = True
                'frmCreditDebit.Show()
                Me.Close()
            ElseIf Saving_and_Deposit_New.cbosavtd.Text = "Credit" Then
                AccountingEntryItem_Deposit(txtRefNo.Text, txtDes.Tag.ToString, 0, txtamount.Text, txtD.Text,
                                        " ", " ", GetJVKeyID, Date.Now)

                AccountingEntryItem_Deposit(txtRefNo.Text, txtaccountname.Tag.ToString, txtamount.Text, 0, txtD.Text,
                                          " ", " ", GetJVKeyID, Date.Now)
                'frmCreditDebit.GetAccount = Saving_and_Deposit_New.cbosavtd.Text
                'frmCreditDebit.txtRefNo.Text = Me.txtRefNo.Text
                'frmCreditDebit.txtamount.Text = Me.txtamount.Text
                'frmCreditDebit.GetjvKeyID = GetJVKeyID
                'frmCreditDebit.GetActive = True
                'frmCreditDebit.Show()
                'Me.Close()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AccountingEntryItem_Deposit(ByVal fiEntryNo As String,
                                      ByVal fxKeyAccount As String,
                                      ByVal fdDebit As Double,
                                      ByVal fdCredit As Double,
                                      ByVal fcMemo As String,
                                      ByVal fcNote As String,
                                      ByVal fxKeyNameID As String,
                                      ByVal fk_tJVEntry As String,
                                      ByVal transDate As Date)
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If
            Dim guid As New Guid(fxKeyAccount)
            Dim guid1 As New Guid(jvKeyID)
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "[usp_t_tJVEntry_details_save]", _
                New SqlParameter("@fiEntryNo", fiEntryNo), _
                New SqlParameter("@fxKeyAccount", guid), _
                New SqlParameter("@fdDebit", fdDebit), _
                New SqlParameter("@fdCredit", fdCredit), _
                New SqlParameter("@fcMemo", fcMemo), _
                New SqlParameter("@fcNote", fcNote), _
                New SqlParameter("@fk_JVHeader", guid1), _
                New SqlParameter("@transDate", transDate))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As String
        Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialNo = rd("maxSerialNo").ToString
            Else
                serialNo = ""
            End If
            Return serialNo
        End Using
    End Function
#End Region

    Private Sub btnBrowseAccountNumber_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccountNumber.Click
        frmMasterfile_AccountNumber.GetID = 2
        frmMasterfile_AccountNumber.ShowDialog()
    End Sub

    Private Sub btnBrowseAccount_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAccount.Click
        frmMasterfile_Accounts.GetActive = 3
        frmMasterfile_Accounts.ShowDialog()
    End Sub
End Class