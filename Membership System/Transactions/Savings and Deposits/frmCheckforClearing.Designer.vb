<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckforClearing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.grdcheck = New System.Windows.Forms.DataGridView()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.grdcheck, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button3
        '
        Me.Button3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button3.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.Button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button3.Location = New System.Drawing.Point(531, 299)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(71, 30)
        Me.Button3.TabIndex = 60
        Me.Button3.Text = "Close"
        Me.Button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button3.UseVisualStyleBackColor = True
        '
        'btnApply
        '
        Me.btnApply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnApply.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnApply.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApply.Location = New System.Drawing.Point(451, 299)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(74, 30)
        Me.btnApply.TabIndex = 59
        Me.btnApply.Text = "Apply"
        Me.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'grdcheck
        '
        Me.grdcheck.AllowUserToAddRows = False
        Me.grdcheck.AllowUserToDeleteRows = False
        Me.grdcheck.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdcheck.BackgroundColor = System.Drawing.Color.White
        Me.grdcheck.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdcheck.Location = New System.Drawing.Point(0, 38)
        Me.grdcheck.Name = "grdcheck"
        Me.grdcheck.Size = New System.Drawing.Size(607, 255)
        Me.grdcheck.TabIndex = 61
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(607, 43)
        Me.PanePanel5.TabIndex = 57
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(154, 19)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Checks fo Clearing"
        '
        'frmCheckforClearing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(607, 332)
        Me.Controls.Add(Me.grdcheck)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.PanePanel5)
        Me.Name = "frmCheckforClearing"
        Me.Text = "Check for Clearing"
        CType(Me.grdcheck, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents grdcheck As System.Windows.Forms.DataGridView
End Class
