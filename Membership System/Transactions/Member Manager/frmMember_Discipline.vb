﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_Discipline

    Dim pkDiscipline As String
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim Files As String
    Dim ex As String
    'Dim FileDirectory As String
    Public fileFromMem As String
    Dim Filesfromdb As Byte()

#Region "Property"
    Public Property getpkDiscipline() As String
        Get
            Return pkDiscipline
        End Get
        Set(ByVal value As String)
            pkDiscipline = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            Dim openFileDialog1 As New OpenFileDialog()

            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = openFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                Files = Path.GetFileName(filenym)
                'FileDirectory = filenym

                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)
                txtAttachFiles.Text = filenym
            Else
                MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch
            Exit Sub
        End Try
    End Sub

    Private Sub AddEditDescipline(ByVal EmpNo As String, ByVal CaseHistory As String, ByVal Offense As String, ByVal DateofOffense As Date, _
                                ByVal Status As String, ByVal Penalty As String, ByVal DateResolved As Date, ByVal FileNames As String, _
                                ByVal Files As Byte(), ByVal FilePath As String, ByVal pk_Discipline As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Discipline_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@CaseHistory", CaseHistory), _
                                     New SqlParameter("@Offense", Offense), _
                                     New SqlParameter("@DateOfOffense", DateofOffense), _
                                     New SqlParameter("@Stat", Status), _
                                     New SqlParameter("@Penalty", Penalty), _
                                     New SqlParameter("@DateResolved", DateResolved), _
                                     New SqlParameter("@FileName", FileNames), _
                                     New SqlParameter("@AttachedFiles", Files), _
                                     New SqlParameter("@FileDirectory", FilePath), _
                                     New SqlParameter("@pk_Discipline", pk_Discipline))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Discipline")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtCaseHis.Text = "" Or txtOffense.Text = "" Or txtStatus.Text = "" Or dtOffense.Text = "" Or dtSolved.Text = "" Or txtPenalty.Text = "" Or txtAttachFiles.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Discipline Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditDescipline(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtCaseHis.Text.Trim, Me.txtOffense.Text.Trim, Me.dtOffense.Value,
                                 Me.txtStatus.Text.Trim, Me.txtPenalty.Text.Trim, Me.dtSolved.Value, Me.Files, Me.fileData, Me.txtAttachFiles.Text, "")
            Call frmMember_Master.GetmemberDiscipline(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetDiscipline_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = fileFromMem
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                Filesfromdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditDescipline(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtCaseHis.Text.Trim, Me.txtOffense.Text.Trim, Me.dtOffense.Value,
                                    Me.txtStatus.Text.Trim, Me.txtPenalty.Text.Trim, Me.dtSolved.Value, Me.fileFromMem, Me.Filesfromdb, Me.txtAttachFiles.Text, pkDiscipline)
            Call frmMember_Master.GetmemberDiscipline(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else

            Call AddEditDescipline(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtCaseHis.Text.Trim, Me.txtOffense.Text.Trim, Me.dtOffense.Value,
                                    Me.txtStatus.Text.Trim, Me.txtPenalty.Text.Trim, Me.dtSolved.Value, Me.Files, Me.fileData, Me.txtAttachFiles.Text, pkDiscipline)
            Call frmMember_Master.GetmemberDiscipline(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub
End Class