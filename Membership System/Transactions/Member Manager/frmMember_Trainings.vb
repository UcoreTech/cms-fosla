﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_Trainings

    Dim pkTrainings As String
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim FileNames As String
    Dim ex As String
    Public FilefromMem As String
    Dim filesFormdb As Byte()

#Region "Property"
    Public Property getpkTrainings() As String
        Get
            Return pkTrainings
        End Get
        Set(ByVal value As String)
            pkTrainings = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        'Try
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
        openFileDialog1.Title = "Select File"
        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            filenym = openFileDialog1.FileName
            fpath = Path.GetExtension(filenym)
            FileNames = Path.GetFileName(filenym)

            fileData = File.ReadAllBytes(filenym)
            ex = Path.GetExtension(LTrim(RTrim(filenym)))

            Dim ms As New MemoryStream(fileData, 0, fileData.Length)

            ms.Write(fileData, 0, fileData.Length)
            txtFiles.Text = filenym
        Else
            MessageBox.Show("User Cancelled!", "Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

    Private Sub AddEditTrainigs(ByVal EmpNo As String, ByVal Title As String, ByVal Place As String, ByVal OfferedBy As String, ByVal dtFrom As Date, ByVal dtTo As Date, _
                                ByVal Hours As String, ByVal Cost As String, ByVal Name As String, ByVal Files As Byte(), ByVal FilePath As String, ByVal pk_Trainings As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Trainings_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@TrainingTitle", Title), _
                                     New SqlParameter("@Place", Place), _
                                     New SqlParameter("@OfferedBy", OfferedBy), _
                                     New SqlParameter("@From", dtFrom), _
                                     New SqlParameter("@To", dtTo), _
                                     New SqlParameter("@Hours", Hours), _
                                     New SqlParameter("@Cost", Cost), _
                                     New SqlParameter("@FileName", Name), _
                                     New SqlParameter("@AttachedFiles", Files), _
                                     New SqlParameter("@FileDirectory", FilePath), _
                                     New SqlParameter("@pk_Trainings", pk_Trainings))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Training")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtTitle.Text = "" Or txtPlace.Text = "" Or txtOfferedBy.Text = "" Or dtFrom.Text = "" Or dtTo.Text = "" Or txtHrs.Text = "" Or txtCost.Text = "" Then
            MessageBox.Show("Complete Information is Needed", "Training Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtPlace.Text.Trim, Me.txtOfferedBy.Text.Trim,
                                 Me.dtFrom.Value, Me.dtTo.Value, Me.txtHrs.Text.Trim, Me.txtCost.Text.Trim, Me.FileNames, Me.fileData, Me.txtFiles.Text, "")
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetTrainings_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = FilefromMem
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                filesFormdb = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtPlace.Text.Trim, Me.txtOfferedBy.Text.Trim,
                         Me.dtFrom.Value, Me.dtTo.Value, Me.txtHrs.Text.Trim, Me.txtCost.Text.Trim, Me.FilefromMem, Me.filesFormdb, Me.txtFiles.Text, pkTrainings)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditTrainigs(frmMember_Master.txtEmployeeNo.Text.Trim, Me.txtTitle.Text.Trim, Me.txtPlace.Text.Trim, Me.txtOfferedBy.Text.Trim,
                         Me.dtFrom.Value, Me.dtTo.Value, Me.txtHrs.Text.Trim, Me.txtCost.Text.Trim, Me.FileNames, Me.fileData, Me.txtFiles.Text, pkTrainings)
            Call frmMember_Master.GetmemberTrainings(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub txtHrs_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtHrs.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub

    Private Sub txtCost_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtCost.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Asc(e.KeyChar) < 48 Or Asc(e.KeyChar) > 57 Then
                MessageBox.Show("Invalid Input! This field Allow 0-9 Only.", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error)
                e.Handled = True
            End If
        End If
    End Sub
End Class