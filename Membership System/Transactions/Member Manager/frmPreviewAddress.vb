﻿Public Class frmPreviewAddress
    'Module : Preview Client Address Using Google Map
    'Created By : Vincent Nacar
    '7/31/14
    Private Sub frmPreviewAddress_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub SearchAddress(ByVal address As String)
        Try
            WebFastx.Navigate("https://www.google.com.ph/maps?q=" + address)
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Please Check your Internet Connection!"
            frmMsgBox.ShowDialog()
            Exit Sub
            Me.Close()
        End Try
    End Sub


    Private Sub btnCLose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCLose.Click
        WebFastx.Refresh()
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If WebFastx.IsBusy Then
            PictureBox1.Visible = True
        Else
            PictureBox1.Visible = False
        End If
    End Sub
End Class