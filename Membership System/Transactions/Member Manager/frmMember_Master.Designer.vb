<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Master))
        Me.tabMember = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.txtBODResolution = New System.Windows.Forms.TextBox()
        Me.txtCedula = New System.Windows.Forms.TextBox()
        Me.txtCTCNo = New System.Windows.Forms.TextBox()
        Me.Label89 = New System.Windows.Forms.Label()
        Me.Label90 = New System.Windows.Forms.Label()
        Me.Label91 = New System.Windows.Forms.Label()
        Me.dtBoardApproval = New System.Windows.Forms.DateTimePicker()
        Me.Label58 = New System.Windows.Forms.Label()
        Me.txtHDMF = New System.Windows.Forms.TextBox()
        Me.txtPhilhealth = New System.Windows.Forms.TextBox()
        Me.txtSSS = New System.Windows.Forms.TextBox()
        Me.txtTin = New System.Windows.Forms.TextBox()
        Me.Label57 = New System.Windows.Forms.Label()
        Me.Label56 = New System.Windows.Forms.Label()
        Me.Label55 = New System.Windows.Forms.Label()
        Me.Label54 = New System.Windows.Forms.Label()
        Me.grpPayrollStatus = New System.Windows.Forms.GroupBox()
        Me.rdoNonExempt = New System.Windows.Forms.RadioButton()
        Me.rdoExempt = New System.Windows.Forms.RadioButton()
        Me.chkBereaveYes = New System.Windows.Forms.CheckBox()
        Me.chkBereaveNo = New System.Windows.Forms.CheckBox()
        Me.mem_WithdrawDate = New System.Windows.Forms.DateTimePicker()
        Me.mem_PaycontDate = New System.Windows.Forms.DateTimePicker()
        Me.chkNo = New System.Windows.Forms.CheckBox()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.txtpayrollcontriamount = New System.Windows.Forms.TextBox()
        Me.chkyes = New System.Windows.Forms.CheckBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtwithdrawal = New System.Windows.Forms.TextBox()
        Me.mem_MemberDate = New System.Windows.Forms.DateTimePicker()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.cboemp_status = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.Label84 = New System.Windows.Forms.Label()
        Me.Label83 = New System.Windows.Forms.Label()
        Me.btnContractRate = New System.Windows.Forms.Button()
        Me.cboClient = New System.Windows.Forms.ComboBox()
        Me.txtContractrate = New System.Windows.Forms.TextBox()
        Me.Label82 = New System.Windows.Forms.Label()
        Me.txtContractPrice = New System.Windows.Forms.TextBox()
        Me.Label61 = New System.Windows.Forms.Label()
        Me.txtEcola = New System.Windows.Forms.TextBox()
        Me.Label63 = New System.Windows.Forms.Label()
        Me.Label52 = New System.Windows.Forms.Label()
        Me.Label51 = New System.Windows.Forms.Label()
        Me.Label50 = New System.Windows.Forms.Label()
        Me.Label49 = New System.Windows.Forms.Label()
        Me.Label48 = New System.Windows.Forms.Label()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cbotaxcode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboPositionLevel = New System.Windows.Forms.ComboBox()
        Me.emp_company = New System.Windows.Forms.TextBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.emp_Ytenures = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.txtlocalofficenumber = New System.Windows.Forms.TextBox()
        Me.btnrestricted = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtofficenumber = New System.Windows.Forms.TextBox()
        Me.txtofficeadd = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.emp_Datehired = New System.Windows.Forms.DateTimePicker()
        Me.cbopayroll = New System.Windows.Forms.ComboBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.cborate = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboEmp_type = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.cbotitledesignation = New System.Windows.Forms.ComboBox()
        Me.txtbasicpay = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.TabPage20 = New System.Windows.Forms.TabPage()
        Me.btnADdReferral = New System.Windows.Forms.Button()
        Me.btnDeleteReferrals = New System.Windows.Forms.Button()
        Me.dgvPersonsReferred = New System.Windows.Forms.DataGridView()
        Me.Label87 = New System.Windows.Forms.Label()
        Me.PictureBox22 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.btnUpdateBA = New System.Windows.Forms.Button()
        Me.btnDeleteBA = New System.Windows.Forms.Button()
        Me.btnNewBA = New System.Windows.Forms.Button()
        Me.lvlBankInfo = New System.Windows.Forms.ListView()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnUpdateSInc = New System.Windows.Forms.Button()
        Me.btnDeleteSInc = New System.Windows.Forms.Button()
        Me.btnsaveSInc = New System.Windows.Forms.Button()
        Me.lvlSourceIncome = New System.Windows.Forms.ListView()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.BtnEditContact = New System.Windows.Forms.Button()
        Me.BtnDeleteContact = New System.Windows.Forms.Button()
        Me.btnaddContact = New System.Windows.Forms.Button()
        Me.lvlNearestRelatives = New System.Windows.Forms.ListView()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage21 = New System.Windows.Forms.TabPage()
        Me.btnNREdit = New System.Windows.Forms.Button()
        Me.btnNRDelete = New System.Windows.Forms.Button()
        Me.btnNRNew = New System.Windows.Forms.Button()
        Me.lvlRelative = New System.Windows.Forms.ListView()
        Me.Label88 = New System.Windows.Forms.Label()
        Me.PictureBox23 = New System.Windows.Forms.PictureBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.LvlEmployeeDependent = New System.Windows.Forms.ListView()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.emp_pic = New System.Windows.Forms.PictureBox()
        Me.temp_marks = New System.Windows.Forms.TextBox()
        Me.txtitemno = New System.Windows.Forms.TextBox()
        Me.txtremarks = New System.Windows.Forms.TextBox()
        Me.temp_add3 = New System.Windows.Forms.TextBox()
        Me.txtparent_ID = New System.Windows.Forms.TextBox()
        Me.txtmonthreqular = New System.Windows.Forms.TextBox()
        Me.txtkeysection = New System.Windows.Forms.TextBox()
        Me.temp_height = New System.Windows.Forms.TextBox()
        Me.txtdescsection = New System.Windows.Forms.TextBox()
        Me.txtsalarygrade = New System.Windows.Forms.TextBox()
        Me.txtdescdepartment = New System.Windows.Forms.TextBox()
        Me.temp_add2 = New System.Windows.Forms.TextBox()
        Me.txtdescdivision = New System.Windows.Forms.TextBox()
        Me.txtweight = New System.Windows.Forms.TextBox()
        Me.txtkeydepartment = New System.Windows.Forms.TextBox()
        Me.txtdateregular = New System.Windows.Forms.TextBox()
        Me.txtkeydivision = New System.Windows.Forms.TextBox()
        Me.Dateregular = New System.Windows.Forms.DateTimePicker()
        Me.txtkeycompany = New System.Windows.Forms.TextBox()
        Me.emp_extphone = New System.Windows.Forms.MaskedTextBox()
        Me.cbosalarygrade = New System.Windows.Forms.ComboBox()
        Me.temp_citizen = New System.Windows.Forms.TextBox()
        Me.temp_designation = New System.Windows.Forms.TextBox()
        Me.txtType_employee = New System.Windows.Forms.TextBox()
        Me.txtfxkeypositionlevel = New System.Windows.Forms.TextBox()
        Me.txtfullname = New System.Windows.Forms.TextBox()
        Me.TXTRECID = New System.Windows.Forms.TextBox()
        Me.cbodept = New System.Windows.Forms.TextBox()
        Me.Dateresigned = New System.Windows.Forms.DateTimePicker()
        Me.TXTKEYEMPLOYEEID = New System.Windows.Forms.TextBox()
        Me.btnemp_editdepdts = New System.Windows.Forms.Button()
        Me.btndelete_dep = New System.Windows.Forms.Button()
        Me.btnadd_dep = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.others = New System.Windows.Forms.TabPage()
        Me.btnEditMobile = New System.Windows.Forms.Button()
        Me.gridMobileNos = New System.Windows.Forms.DataGridView()
        Me.txtEloadingLimit = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtEloaderPIN = New System.Windows.Forms.TextBox()
        Me.chkShowPINChar = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnAddMobileNo = New System.Windows.Forms.Button()
        Me.chkIsEloader = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMobileNo = New System.Windows.Forms.MaskedTextBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.dgvemail = New System.Windows.Forms.DataGridView()
        Me.Label43 = New System.Windows.Forms.Label()
        Me.dgvphone = New System.Windows.Forms.DataGridView()
        Me.Label42 = New System.Windows.Forms.Label()
        Me.dgvcivil = New System.Windows.Forms.DataGridView()
        Me.Label41 = New System.Windows.Forms.Label()
        Me.dgvaddress = New System.Windows.Forms.DataGridView()
        Me.Label40 = New System.Windows.Forms.Label()
        Me.dgvlastname = New System.Windows.Forms.DataGridView()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.TabPage19 = New System.Windows.Forms.TabPage()
        Me.cboEmpHistory = New System.Windows.Forms.ComboBox()
        Me.Label81 = New System.Windows.Forms.Label()
        Me.PictureBox21 = New System.Windows.Forms.PictureBox()
        Me.dgvEmpHistory = New System.Windows.Forms.DataGridView()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.Label70 = New System.Windows.Forms.Label()
        Me.btnUpdateEI = New System.Windows.Forms.Button()
        Me.btnDeleteEI = New System.Windows.Forms.Button()
        Me.btnNewEI = New System.Windows.Forms.Button()
        Me.Label64 = New System.Windows.Forms.Label()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.lvlEducInfo = New System.Windows.Forms.ListView()
        Me.TabPage8 = New System.Windows.Forms.TabPage()
        Me.Label71 = New System.Windows.Forms.Label()
        Me.btnUpdateJD = New System.Windows.Forms.Button()
        Me.btnDeleteJD = New System.Windows.Forms.Button()
        Me.btnNewJD = New System.Windows.Forms.Button()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.lvlJobDesc = New System.Windows.Forms.ListView()
        Me.TabPage9 = New System.Windows.Forms.TabPage()
        Me.btnTDownload = New System.Windows.Forms.Button()
        Me.Label72 = New System.Windows.Forms.Label()
        Me.btnUpdateT = New System.Windows.Forms.Button()
        Me.btnDeleteT = New System.Windows.Forms.Button()
        Me.btnNewT = New System.Windows.Forms.Button()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.lvlTrainings = New System.Windows.Forms.ListView()
        Me.TabPage10 = New System.Windows.Forms.TabPage()
        Me.Label73 = New System.Windows.Forms.Label()
        Me.btnUpdateSkills = New System.Windows.Forms.Button()
        Me.btnDeleteSkills = New System.Windows.Forms.Button()
        Me.btnNewSkills = New System.Windows.Forms.Button()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.lvlSkills = New System.Windows.Forms.ListView()
        Me.TabPage11 = New System.Windows.Forms.TabPage()
        Me.Label74 = New System.Windows.Forms.Label()
        Me.btnUpdateAward = New System.Windows.Forms.Button()
        Me.btnDeleteAward = New System.Windows.Forms.Button()
        Me.btnNewAward = New System.Windows.Forms.Button()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.lvlAwards = New System.Windows.Forms.ListView()
        Me.TabPage12 = New System.Windows.Forms.TabPage()
        Me.btnPEDownload = New System.Windows.Forms.Button()
        Me.Label75 = New System.Windows.Forms.Label()
        Me.btnUpdatePE = New System.Windows.Forms.Button()
        Me.btnDeletePE = New System.Windows.Forms.Button()
        Me.btnNewPE = New System.Windows.Forms.Button()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.lvlPerfEval = New System.Windows.Forms.ListView()
        Me.TabPage13 = New System.Windows.Forms.TabPage()
        Me.btnDDownload = New System.Windows.Forms.Button()
        Me.Label76 = New System.Windows.Forms.Label()
        Me.btnUpdateDisc = New System.Windows.Forms.Button()
        Me.btnDeleteDisc = New System.Windows.Forms.Button()
        Me.btnNewDisc = New System.Windows.Forms.Button()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.lvlDiscipline = New System.Windows.Forms.ListView()
        Me.TabPage14 = New System.Windows.Forms.TabPage()
        Me.btnMedUpload = New System.Windows.Forms.Button()
        Me.Label77 = New System.Windows.Forms.Label()
        Me.btnUpdateMed = New System.Windows.Forms.Button()
        Me.btnDeleteMed = New System.Windows.Forms.Button()
        Me.btnNewMed = New System.Windows.Forms.Button()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.lvlMedical = New System.Windows.Forms.ListView()
        Me.TabPage16 = New System.Windows.Forms.TabPage()
        Me.btnPayrollHistory = New System.Windows.Forms.Button()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.lvlPayrollHistory = New System.Windows.Forms.ListView()
        Me.TabPage18 = New System.Windows.Forms.TabPage()
        Me.Label80 = New System.Windows.Forms.Label()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.lvlLoanHistory = New System.Windows.Forms.ListView()
        Me.TabPage15 = New System.Windows.Forms.TabPage()
        Me.cboLeave = New System.Windows.Forms.ComboBox()
        Me.Label78 = New System.Windows.Forms.Label()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.lvlLeave = New System.Windows.Forms.ListView()
        Me.Cbomem_Status = New System.Windows.Forms.ComboBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.txtMemberID = New System.Windows.Forms.TextBox()
        Me.temp_placebirth = New System.Windows.Forms.TextBox()
        Me.txtage1 = New System.Windows.Forms.TextBox()
        Me.lblage = New System.Windows.Forms.Label()
        Me.EMP_DATEbirth = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.MaskedTextBox5 = New System.Windows.Forms.MaskedTextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEmployeeNo = New System.Windows.Forms.TextBox()
        Me.temp_fname = New System.Windows.Forms.TextBox()
        Me.temp_lname = New System.Windows.Forms.TextBox()
        Me.temp_midname = New System.Windows.Forms.TextBox()
        Me.txtprovincialadd = New System.Windows.Forms.TextBox()
        Me.cboemp_gender = New System.Windows.Forms.ComboBox()
        Me.cboemp_civil = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtresidencephone = New System.Windows.Forms.MaskedTextBox()
        Me.txtEmailAddress = New System.Windows.Forms.TextBox()
        Me.txtorgchart = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.txtResidenceadd = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.txtDepartment2 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnSearchProvince = New System.Windows.Forms.Button()
        Me.btnSearchHome = New System.Windows.Forms.Button()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.txtpk_Employee = New System.Windows.Forms.TextBox()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label53 = New System.Windows.Forms.Label()
        Me.txtReferred = New System.Windows.Forms.TextBox()
        Me.OpenFileDialog2 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnMaxSignature = New System.Windows.Forms.Button()
        Me.btnMaxPhoto = New System.Windows.Forms.Button()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.btnBrowseSignature = New System.Windows.Forms.Button()
        Me.picEmpSignature = New System.Windows.Forms.PictureBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.btnBrowsePicture = New System.Windows.Forms.Button()
        Me.picEmpPhoto = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label45 = New System.Windows.Forms.Label()
        Me.Label46 = New System.Windows.Forms.Label()
        Me.Label47 = New System.Windows.Forms.Label()
        Me.cbofGroup = New System.Windows.Forms.ComboBox()
        Me.cbofType = New System.Windows.Forms.ComboBox()
        Me.cbofCategory = New System.Windows.Forms.ComboBox()
        Me.cboRank = New System.Windows.Forms.ComboBox()
        Me.txtCompanyChart = New System.Windows.Forms.TextBox()
        Me.cboSubgroup = New System.Windows.Forms.ComboBox()
        Me.Label60 = New System.Windows.Forms.Label()
        Me.txtCompanyName = New System.Windows.Forms.TextBox()
        Me.Label79 = New System.Windows.Forms.Label()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.btnemp_close = New System.Windows.Forms.Button()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btnemp_next = New System.Windows.Forms.Button()
        Me.btnemp_delete = New System.Windows.Forms.Button()
        Me.BTNSEARCH = New System.Windows.Forms.Button()
        Me.btnemp_edit = New System.Windows.Forms.Button()
        Me.btnemp_previous = New System.Windows.Forms.Button()
        Me.btnemp_add = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblTerminated = New System.Windows.Forms.Label()
        Me.lblemployee_name = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.tabMember.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.grpPayrollStatus.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage20.SuspendLayout()
        CType(Me.dgvPersonsReferred, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage21.SuspendLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.others.SuspendLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage6.SuspendLayout()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage19.SuspendLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvEmpHistory, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage7.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage8.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage9.SuspendLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage10.SuspendLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage11.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage12.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage13.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage14.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage16.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage18.SuspendLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage15.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel2.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabMember
        '
        Me.tabMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabMember.Controls.Add(Me.TabPage5)
        Me.tabMember.Controls.Add(Me.TabPage1)
        Me.tabMember.Controls.Add(Me.TabPage20)
        Me.tabMember.Controls.Add(Me.TabPage2)
        Me.tabMember.Controls.Add(Me.TabPage4)
        Me.tabMember.Controls.Add(Me.TabPage17)
        Me.tabMember.Controls.Add(Me.TabPage21)
        Me.tabMember.Controls.Add(Me.TabPage3)
        Me.tabMember.Controls.Add(Me.others)
        Me.tabMember.Controls.Add(Me.TabPage6)
        Me.tabMember.Controls.Add(Me.TabPage19)
        Me.tabMember.Controls.Add(Me.TabPage7)
        Me.tabMember.Controls.Add(Me.TabPage8)
        Me.tabMember.Controls.Add(Me.TabPage9)
        Me.tabMember.Controls.Add(Me.TabPage10)
        Me.tabMember.Controls.Add(Me.TabPage11)
        Me.tabMember.Controls.Add(Me.TabPage12)
        Me.tabMember.Controls.Add(Me.TabPage13)
        Me.tabMember.Controls.Add(Me.TabPage14)
        Me.tabMember.Controls.Add(Me.TabPage16)
        Me.tabMember.Controls.Add(Me.TabPage18)
        Me.tabMember.Controls.Add(Me.TabPage15)
        Me.tabMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabMember.ItemSize = New System.Drawing.Size(120, 18)
        Me.tabMember.Location = New System.Drawing.Point(7, 374)
        Me.tabMember.Name = "tabMember"
        Me.tabMember.SelectedIndex = 0
        Me.tabMember.Size = New System.Drawing.Size(1017, 216)
        Me.tabMember.TabIndex = 17
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage5.Controls.Add(Me.txtBODResolution)
        Me.TabPage5.Controls.Add(Me.txtCedula)
        Me.TabPage5.Controls.Add(Me.txtCTCNo)
        Me.TabPage5.Controls.Add(Me.Label89)
        Me.TabPage5.Controls.Add(Me.Label90)
        Me.TabPage5.Controls.Add(Me.Label91)
        Me.TabPage5.Controls.Add(Me.dtBoardApproval)
        Me.TabPage5.Controls.Add(Me.Label58)
        Me.TabPage5.Controls.Add(Me.txtHDMF)
        Me.TabPage5.Controls.Add(Me.txtPhilhealth)
        Me.TabPage5.Controls.Add(Me.txtSSS)
        Me.TabPage5.Controls.Add(Me.txtTin)
        Me.TabPage5.Controls.Add(Me.Label57)
        Me.TabPage5.Controls.Add(Me.Label56)
        Me.TabPage5.Controls.Add(Me.Label55)
        Me.TabPage5.Controls.Add(Me.Label54)
        Me.TabPage5.Controls.Add(Me.grpPayrollStatus)
        Me.TabPage5.Controls.Add(Me.chkBereaveYes)
        Me.TabPage5.Controls.Add(Me.chkBereaveNo)
        Me.TabPage5.Controls.Add(Me.mem_WithdrawDate)
        Me.TabPage5.Controls.Add(Me.mem_PaycontDate)
        Me.TabPage5.Controls.Add(Me.chkNo)
        Me.TabPage5.Controls.Add(Me.Label110)
        Me.TabPage5.Controls.Add(Me.txtpayrollcontriamount)
        Me.TabPage5.Controls.Add(Me.chkyes)
        Me.TabPage5.Controls.Add(Me.Label109)
        Me.TabPage5.Controls.Add(Me.Label37)
        Me.TabPage5.Controls.Add(Me.txtwithdrawal)
        Me.TabPage5.Controls.Add(Me.mem_MemberDate)
        Me.TabPage5.Controls.Add(Me.Label111)
        Me.TabPage5.Controls.Add(Me.Label108)
        Me.TabPage5.Controls.Add(Me.Label65)
        Me.TabPage5.Controls.Add(Me.Label105)
        Me.TabPage5.Controls.Add(Me.cboemp_status)
        Me.TabPage5.Controls.Add(Me.Label11)
        Me.TabPage5.Controls.Add(Me.PictureBox8)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Client Information"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'txtBODResolution
        '
        Me.txtBODResolution.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtBODResolution.Location = New System.Drawing.Point(850, 122)
        Me.txtBODResolution.Name = "txtBODResolution"
        Me.txtBODResolution.Size = New System.Drawing.Size(155, 21)
        Me.txtBODResolution.TabIndex = 135
        '
        'txtCedula
        '
        Me.txtCedula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCedula.Location = New System.Drawing.Point(850, 79)
        Me.txtCedula.Name = "txtCedula"
        Me.txtCedula.Size = New System.Drawing.Size(155, 21)
        Me.txtCedula.TabIndex = 134
        '
        'txtCTCNo
        '
        Me.txtCTCNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCTCNo.Location = New System.Drawing.Point(850, 37)
        Me.txtCTCNo.Name = "txtCTCNo"
        Me.txtCTCNo.Size = New System.Drawing.Size(155, 21)
        Me.txtCTCNo.TabIndex = 133
        '
        'Label89
        '
        Me.Label89.AutoSize = True
        Me.Label89.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label89.Location = New System.Drawing.Point(848, 103)
        Me.Label89.Name = "Label89"
        Me.Label89.Size = New System.Drawing.Size(37, 15)
        Me.Label89.TabIndex = 132
        Me.Label89.Text = "GSIS :"
        '
        'Label90
        '
        Me.Label90.AutoSize = True
        Me.Label90.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label90.Location = New System.Drawing.Point(848, 146)
        Me.Label90.Name = "Label90"
        Me.Label90.Size = New System.Drawing.Size(114, 15)
        Me.Label90.TabIndex = 131
        Me.Label90.Text = "BOD Resolution No."
        '
        'Label91
        '
        Me.Label91.AutoSize = True
        Me.Label91.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label91.Location = New System.Drawing.Point(848, 61)
        Me.Label91.Name = "Label91"
        Me.Label91.Size = New System.Drawing.Size(99, 15)
        Me.Label91.TabIndex = 130
        Me.Label91.Text = "CTC No. /Cedula:"
        '
        'dtBoardApproval
        '
        Me.dtBoardApproval.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtBoardApproval.Location = New System.Drawing.Point(672, 145)
        Me.dtBoardApproval.Name = "dtBoardApproval"
        Me.dtBoardApproval.Size = New System.Drawing.Size(155, 21)
        Me.dtBoardApproval.TabIndex = 129
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label58.Location = New System.Drawing.Point(567, 149)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(95, 15)
        Me.Label58.TabIndex = 128
        Me.Label58.Text = "Board Approval:"
        '
        'txtHDMF
        '
        Me.txtHDMF.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtHDMF.Location = New System.Drawing.Point(672, 117)
        Me.txtHDMF.Name = "txtHDMF"
        Me.txtHDMF.Size = New System.Drawing.Size(155, 21)
        Me.txtHDMF.TabIndex = 127
        '
        'txtPhilhealth
        '
        Me.txtPhilhealth.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtPhilhealth.Location = New System.Drawing.Point(672, 90)
        Me.txtPhilhealth.Name = "txtPhilhealth"
        Me.txtPhilhealth.Size = New System.Drawing.Size(155, 21)
        Me.txtPhilhealth.TabIndex = 126
        '
        'txtSSS
        '
        Me.txtSSS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtSSS.Location = New System.Drawing.Point(672, 63)
        Me.txtSSS.Name = "txtSSS"
        Me.txtSSS.Size = New System.Drawing.Size(155, 21)
        Me.txtSSS.TabIndex = 125
        '
        'txtTin
        '
        Me.txtTin.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTin.Location = New System.Drawing.Point(672, 36)
        Me.txtTin.Name = "txtTin"
        Me.txtTin.Size = New System.Drawing.Size(155, 21)
        Me.txtTin.TabIndex = 124
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label57.Location = New System.Drawing.Point(633, 65)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(29, 15)
        Me.Label57.TabIndex = 123
        Me.Label57.Text = "SSS:"
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label56.Location = New System.Drawing.Point(582, 92)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(80, 15)
        Me.Label56.TabIndex = 122
        Me.Label56.Text = "PHIL-HEALTH:"
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label55.Location = New System.Drawing.Point(619, 120)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(44, 15)
        Me.Label55.TabIndex = 121
        Me.Label55.Text = "HDMF:"
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold)
        Me.Label54.Location = New System.Drawing.Point(634, 41)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(32, 15)
        Me.Label54.TabIndex = 120
        Me.Label54.Text = "TIN: "
        '
        'grpPayrollStatus
        '
        Me.grpPayrollStatus.Controls.Add(Me.rdoNonExempt)
        Me.grpPayrollStatus.Controls.Add(Me.rdoExempt)
        Me.grpPayrollStatus.Location = New System.Drawing.Point(284, 96)
        Me.grpPayrollStatus.Name = "grpPayrollStatus"
        Me.grpPayrollStatus.Size = New System.Drawing.Size(267, 49)
        Me.grpPayrollStatus.TabIndex = 118
        Me.grpPayrollStatus.TabStop = False
        Me.grpPayrollStatus.Text = "Payroll Status"
        '
        'rdoNonExempt
        '
        Me.rdoNonExempt.AutoSize = True
        Me.rdoNonExempt.Location = New System.Drawing.Point(99, 20)
        Me.rdoNonExempt.Name = "rdoNonExempt"
        Me.rdoNonExempt.Size = New System.Drawing.Size(94, 19)
        Me.rdoNonExempt.TabIndex = 1
        Me.rdoNonExempt.Text = "Non-Exempt"
        Me.rdoNonExempt.UseVisualStyleBackColor = True
        '
        'rdoExempt
        '
        Me.rdoExempt.AutoSize = True
        Me.rdoExempt.Checked = True
        Me.rdoExempt.Location = New System.Drawing.Point(10, 20)
        Me.rdoExempt.Name = "rdoExempt"
        Me.rdoExempt.Size = New System.Drawing.Size(67, 19)
        Me.rdoExempt.TabIndex = 0
        Me.rdoExempt.TabStop = True
        Me.rdoExempt.Text = "Exempt"
        Me.rdoExempt.UseVisualStyleBackColor = True
        '
        'chkBereaveYes
        '
        Me.chkBereaveYes.AutoSize = True
        Me.chkBereaveYes.Location = New System.Drawing.Point(153, 126)
        Me.chkBereaveYes.Name = "chkBereaveYes"
        Me.chkBereaveYes.Size = New System.Drawing.Size(46, 19)
        Me.chkBereaveYes.TabIndex = 117
        Me.chkBereaveYes.Text = "Yes"
        Me.chkBereaveYes.UseVisualStyleBackColor = True
        '
        'chkBereaveNo
        '
        Me.chkBereaveNo.AutoSize = True
        Me.chkBereaveNo.Location = New System.Drawing.Point(211, 126)
        Me.chkBereaveNo.Name = "chkBereaveNo"
        Me.chkBereaveNo.Size = New System.Drawing.Size(42, 19)
        Me.chkBereaveNo.TabIndex = 116
        Me.chkBereaveNo.Text = "No"
        Me.chkBereaveNo.UseVisualStyleBackColor = True
        '
        'mem_WithdrawDate
        '
        Me.mem_WithdrawDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_WithdrawDate.Location = New System.Drawing.Point(549, 36)
        Me.mem_WithdrawDate.Name = "mem_WithdrawDate"
        Me.mem_WithdrawDate.Size = New System.Drawing.Size(22, 21)
        Me.mem_WithdrawDate.TabIndex = 110
        '
        'mem_PaycontDate
        '
        Me.mem_PaycontDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_PaycontDate.Location = New System.Drawing.Point(453, 148)
        Me.mem_PaycontDate.Name = "mem_PaycontDate"
        Me.mem_PaycontDate.Size = New System.Drawing.Size(104, 21)
        Me.mem_PaycontDate.TabIndex = 1
        Me.mem_PaycontDate.Visible = False
        '
        'chkNo
        '
        Me.chkNo.AutoSize = True
        Me.chkNo.Location = New System.Drawing.Point(211, 38)
        Me.chkNo.Name = "chkNo"
        Me.chkNo.Size = New System.Drawing.Size(42, 19)
        Me.chkNo.TabIndex = 1
        Me.chkNo.Text = "No"
        Me.chkNo.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(405, 148)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(33, 15)
        Me.Label110.TabIndex = 109
        Me.Label110.Text = "Date"
        Me.Label110.Visible = False
        '
        'txtpayrollcontriamount
        '
        Me.txtpayrollcontriamount.AcceptsTab = True
        Me.txtpayrollcontriamount.Location = New System.Drawing.Point(447, 63)
        Me.txtpayrollcontriamount.Name = "txtpayrollcontriamount"
        Me.txtpayrollcontriamount.Size = New System.Drawing.Size(104, 21)
        Me.txtpayrollcontriamount.TabIndex = 0
        Me.txtpayrollcontriamount.Text = "0.00"
        Me.txtpayrollcontriamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        Me.txtpayrollcontriamount.Visible = False
        '
        'chkyes
        '
        Me.chkyes.AutoSize = True
        Me.chkyes.Location = New System.Drawing.Point(152, 38)
        Me.chkyes.Name = "chkyes"
        Me.chkyes.Size = New System.Drawing.Size(46, 19)
        Me.chkyes.TabIndex = 0
        Me.chkyes.Text = "Yes"
        Me.chkyes.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(281, 64)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(129, 15)
        Me.Label109.TabIndex = 109
        Me.Label109.Text = "Contribution Amount:"
        Me.Label109.Visible = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(13, 40)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 15)
        Me.Label37.TabIndex = 115
        Me.Label37.Text = "Employed:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwithdrawal
        '
        Me.txtwithdrawal.Location = New System.Drawing.Point(447, 36)
        Me.txtwithdrawal.Name = "txtwithdrawal"
        Me.txtwithdrawal.Size = New System.Drawing.Size(104, 21)
        Me.txtwithdrawal.TabIndex = 6
        '
        'mem_MemberDate
        '
        Me.mem_MemberDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_MemberDate.Location = New System.Drawing.Point(152, 64)
        Me.mem_MemberDate.Name = "mem_MemberDate"
        Me.mem_MemberDate.Size = New System.Drawing.Size(116, 21)
        Me.mem_MemberDate.TabIndex = 5
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.Location = New System.Drawing.Point(281, 39)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(144, 15)
        Me.Label111.TabIndex = 109
        Me.Label111.Text = "Clients Withdrawal Date:"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.Location = New System.Drawing.Point(13, 65)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(110, 15)
        Me.Label108.TabIndex = 102
        Me.Label108.Text = "Membership Date:"
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label65.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label65.Location = New System.Drawing.Point(6, 1)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(122, 18)
        Me.Label65.TabIndex = 9
        Me.Label65.Text = "Client Information"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.Location = New System.Drawing.Point(14, 125)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(86, 15)
        Me.Label105.TabIndex = 102
        Me.Label105.Text = "Bereavement:"
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboemp_status
        '
        Me.cboemp_status.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_status.FormattingEnabled = True
        Me.cboemp_status.Location = New System.Drawing.Point(152, 91)
        Me.cboemp_status.Name = "cboemp_status"
        Me.cboemp_status.Size = New System.Drawing.Size(116, 23)
        Me.cboemp_status.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 94)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Employement Status:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox8.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(1086, 22)
        Me.PictureBox8.TabIndex = 119
        Me.PictureBox8.TabStop = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.Label84)
        Me.TabPage1.Controls.Add(Me.Label83)
        Me.TabPage1.Controls.Add(Me.btnContractRate)
        Me.TabPage1.Controls.Add(Me.cboClient)
        Me.TabPage1.Controls.Add(Me.txtContractrate)
        Me.TabPage1.Controls.Add(Me.Label82)
        Me.TabPage1.Controls.Add(Me.txtContractPrice)
        Me.TabPage1.Controls.Add(Me.Label61)
        Me.TabPage1.Controls.Add(Me.txtEcola)
        Me.TabPage1.Controls.Add(Me.Label63)
        Me.TabPage1.Controls.Add(Me.Label52)
        Me.TabPage1.Controls.Add(Me.Label51)
        Me.TabPage1.Controls.Add(Me.Label50)
        Me.TabPage1.Controls.Add(Me.Label49)
        Me.TabPage1.Controls.Add(Me.Label48)
        Me.TabPage1.Controls.Add(Me.cboPaycode)
        Me.TabPage1.Controls.Add(Me.Label38)
        Me.TabPage1.Controls.Add(Me.cbotaxcode)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.cboPositionLevel)
        Me.TabPage1.Controls.Add(Me.emp_company)
        Me.TabPage1.Controls.Add(Me.Label114)
        Me.TabPage1.Controls.Add(Me.emp_Ytenures)
        Me.TabPage1.Controls.Add(Me.Label100)
        Me.TabPage1.Controls.Add(Me.txtlocalofficenumber)
        Me.TabPage1.Controls.Add(Me.btnrestricted)
        Me.TabPage1.Controls.Add(Me.Label34)
        Me.TabPage1.Controls.Add(Me.txtofficenumber)
        Me.TabPage1.Controls.Add(Me.txtofficeadd)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.emp_Datehired)
        Me.TabPage1.Controls.Add(Me.cbopayroll)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.cborate)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.cboEmp_type)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label69)
        Me.TabPage1.Controls.Add(Me.cbotitledesignation)
        Me.TabPage1.Controls.Add(Me.txtbasicpay)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.PictureBox3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Employment Information"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'Label84
        '
        Me.Label84.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label84.AutoSize = True
        Me.Label84.ForeColor = System.Drawing.Color.Red
        Me.Label84.Location = New System.Drawing.Point(773, 129)
        Me.Label84.Name = "Label84"
        Me.Label84.Size = New System.Drawing.Size(12, 15)
        Me.Label84.TabIndex = 126
        Me.Label84.Text = "*"
        Me.Label84.Visible = False
        '
        'Label83
        '
        Me.Label83.AutoSize = True
        Me.Label83.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label83.Location = New System.Drawing.Point(435, 129)
        Me.Label83.Name = "Label83"
        Me.Label83.Size = New System.Drawing.Size(130, 15)
        Me.Label83.TabIndex = 47
        Me.Label83.Text = "Client Representative:"
        Me.Label83.Visible = False
        '
        'btnContractRate
        '
        Me.btnContractRate.Enabled = False
        Me.btnContractRate.Location = New System.Drawing.Point(777, 154)
        Me.btnContractRate.Name = "btnContractRate"
        Me.btnContractRate.Size = New System.Drawing.Size(45, 21)
        Me.btnContractRate.TabIndex = 125
        Me.btnContractRate.Text = "....."
        Me.btnContractRate.UseVisualStyleBackColor = True
        Me.btnContractRate.Visible = False
        '
        'cboClient
        '
        Me.cboClient.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboClient.BackColor = System.Drawing.Color.White
        Me.cboClient.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboClient.FormattingEnabled = True
        Me.cboClient.Location = New System.Drawing.Point(567, 126)
        Me.cboClient.Name = "cboClient"
        Me.cboClient.Size = New System.Drawing.Size(208, 23)
        Me.cboClient.TabIndex = 46
        Me.cboClient.Visible = False
        '
        'txtContractrate
        '
        Me.txtContractrate.Enabled = False
        Me.txtContractrate.Location = New System.Drawing.Point(637, 154)
        Me.txtContractrate.Name = "txtContractrate"
        Me.txtContractrate.Size = New System.Drawing.Size(138, 21)
        Me.txtContractrate.TabIndex = 124
        Me.txtContractrate.Visible = False
        '
        'Label82
        '
        Me.Label82.AutoSize = True
        Me.Label82.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label82.Location = New System.Drawing.Point(553, 156)
        Me.Label82.Name = "Label82"
        Me.Label82.Size = New System.Drawing.Size(82, 15)
        Me.Label82.TabIndex = 123
        Me.Label82.Text = "Contract Rate"
        Me.Label82.Visible = False
        '
        'txtContractPrice
        '
        Me.txtContractPrice.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtContractPrice.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtContractPrice.Location = New System.Drawing.Point(322, 154)
        Me.txtContractPrice.MaxLength = 255
        Me.txtContractPrice.Name = "txtContractPrice"
        Me.txtContractPrice.Size = New System.Drawing.Size(83, 21)
        Me.txtContractPrice.TabIndex = 122
        Me.txtContractPrice.Text = "0.00"
        Me.txtContractPrice.Visible = False
        '
        'Label61
        '
        Me.Label61.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label61.AutoSize = True
        Me.Label61.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label61.Location = New System.Drawing.Point(199, 155)
        Me.Label61.Name = "Label61"
        Me.Label61.Size = New System.Drawing.Size(117, 15)
        Me.Label61.TabIndex = 121
        Me.Label61.Text = "Basic Contract Price:"
        Me.Label61.Visible = False
        '
        'txtEcola
        '
        Me.txtEcola.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEcola.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEcola.Location = New System.Drawing.Point(464, 154)
        Me.txtEcola.MaxLength = 255
        Me.txtEcola.Name = "txtEcola"
        Me.txtEcola.Size = New System.Drawing.Size(83, 21)
        Me.txtEcola.TabIndex = 120
        Me.txtEcola.Text = "0.00"
        Me.txtEcola.Visible = False
        '
        'Label63
        '
        Me.Label63.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label63.AutoSize = True
        Me.Label63.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label63.Location = New System.Drawing.Point(411, 157)
        Me.Label63.Name = "Label63"
        Me.Label63.Size = New System.Drawing.Size(47, 15)
        Me.Label63.TabIndex = 119
        Me.Label63.Text = "ECOLA:"
        Me.Label63.Visible = False
        '
        'Label52
        '
        Me.Label52.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label52.AutoSize = True
        Me.Label52.ForeColor = System.Drawing.Color.Red
        Me.Label52.Location = New System.Drawing.Point(991, 92)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(12, 15)
        Me.Label52.TabIndex = 117
        Me.Label52.Text = "*"
        '
        'Label51
        '
        Me.Label51.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label51.AutoSize = True
        Me.Label51.ForeColor = System.Drawing.Color.Red
        Me.Label51.Location = New System.Drawing.Point(991, 64)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(12, 15)
        Me.Label51.TabIndex = 116
        Me.Label51.Text = "*"
        '
        'Label50
        '
        Me.Label50.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label50.AutoSize = True
        Me.Label50.ForeColor = System.Drawing.Color.Red
        Me.Label50.Location = New System.Drawing.Point(991, 39)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(12, 15)
        Me.Label50.TabIndex = 115
        Me.Label50.Text = "*"
        '
        'Label49
        '
        Me.Label49.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label49.AutoSize = True
        Me.Label49.ForeColor = System.Drawing.Color.Red
        Me.Label49.Location = New System.Drawing.Point(774, 110)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(12, 15)
        Me.Label49.TabIndex = 114
        Me.Label49.Text = "*"
        '
        'Label48
        '
        Me.Label48.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label48.AutoSize = True
        Me.Label48.ForeColor = System.Drawing.Color.Red
        Me.Label48.Location = New System.Drawing.Point(774, 62)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(12, 15)
        Me.Label48.TabIndex = 113
        Me.Label48.Text = "*"
        '
        'cboPaycode
        '
        Me.cboPaycode.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaycode.BackColor = System.Drawing.Color.White
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(567, 102)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(208, 23)
        Me.cboPaycode.TabIndex = 112
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(481, 103)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(61, 15)
        Me.Label38.TabIndex = 111
        Me.Label38.Text = "Pay Code:"
        '
        'cbotaxcode
        '
        Me.cbotaxcode.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotaxcode.Enabled = False
        Me.cbotaxcode.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbotaxcode.FormattingEnabled = True
        Me.cbotaxcode.Location = New System.Drawing.Point(871, 79)
        Me.cbotaxcode.Name = "cbotaxcode"
        Me.cbotaxcode.Size = New System.Drawing.Size(116, 23)
        Me.cbotaxcode.TabIndex = 107
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(791, 83)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Tax Code:"
        '
        'cboPositionLevel
        '
        Me.cboPositionLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPositionLevel.FormattingEnabled = True
        Me.cboPositionLevel.Location = New System.Drawing.Point(567, 76)
        Me.cboPositionLevel.Name = "cboPositionLevel"
        Me.cboPositionLevel.Size = New System.Drawing.Size(208, 23)
        Me.cboPositionLevel.TabIndex = 50
        '
        'emp_company
        '
        Me.emp_company.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_company.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_company.Location = New System.Drawing.Point(111, 28)
        Me.emp_company.Name = "emp_company"
        Me.emp_company.Size = New System.Drawing.Size(345, 21)
        Me.emp_company.TabIndex = 103
        '
        'Label114
        '
        Me.Label114.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label114.AutoSize = True
        Me.Label114.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label114.Location = New System.Drawing.Point(481, 80)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(87, 15)
        Me.Label114.TabIndex = 49
        Me.Label114.Text = "Position Level:"
        '
        'emp_Ytenures
        '
        Me.emp_Ytenures.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_Ytenures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_Ytenures.Location = New System.Drawing.Point(110, 125)
        Me.emp_Ytenures.Name = "emp_Ytenures"
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_Ytenures.Size = New System.Drawing.Size(220, 21)
        Me.emp_Ytenures.TabIndex = 106
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.Location = New System.Drawing.Point(3, 126)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(95, 15)
        Me.Label100.TabIndex = 45
        Me.Label100.Text = "Years of Tenure:"
        '
        'txtlocalofficenumber
        '
        Me.txtlocalofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtlocalofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlocalofficenumber.Location = New System.Drawing.Point(110, 101)
        Me.txtlocalofficenumber.MaxLength = 255
        Me.txtlocalofficenumber.Name = "txtlocalofficenumber"
        Me.txtlocalofficenumber.Size = New System.Drawing.Size(292, 21)
        Me.txtlocalofficenumber.TabIndex = 4
        '
        'btnrestricted
        '
        Me.btnrestricted.Enabled = False
        Me.btnrestricted.ForeColor = System.Drawing.Color.Red
        Me.btnrestricted.Location = New System.Drawing.Point(108, 150)
        Me.btnrestricted.Name = "btnrestricted"
        Me.btnrestricted.Size = New System.Drawing.Size(85, 25)
        Me.btnrestricted.TabIndex = 8
        Me.btnrestricted.Text = "Confidential"
        Me.btnrestricted.UseVisualStyleBackColor = True
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label34.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label34.Location = New System.Drawing.Point(6, 3)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(164, 18)
        Me.Label34.TabIndex = 19
        Me.Label34.Text = "Employment Information"
        '
        'txtofficenumber
        '
        Me.txtofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficenumber.Location = New System.Drawing.Point(110, 77)
        Me.txtofficenumber.MaxLength = 255
        Me.txtofficenumber.Name = "txtofficenumber"
        Me.txtofficenumber.Size = New System.Drawing.Size(292, 21)
        Me.txtofficenumber.TabIndex = 3
        '
        'txtofficeadd
        '
        Me.txtofficeadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficeadd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficeadd.Location = New System.Drawing.Point(111, 53)
        Me.txtofficeadd.MaxLength = 50
        Me.txtofficeadd.Name = "txtofficeadd"
        Me.txtofficeadd.Size = New System.Drawing.Size(345, 21)
        Me.txtofficeadd.TabIndex = 2
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(3, 104)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 15)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Local No.:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(3, 80)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(65, 15)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Office No.:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(4, 57)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(90, 15)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Office Address:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(4, 31)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 15)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Company:"
        '
        'emp_Datehired
        '
        Me.emp_Datehired.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_Datehired.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_Datehired.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.emp_Datehired.Location = New System.Drawing.Point(871, 104)
        Me.emp_Datehired.Name = "emp_Datehired"
        Me.emp_Datehired.Size = New System.Drawing.Size(116, 21)
        Me.emp_Datehired.TabIndex = 14
        Me.emp_Datehired.Value = New Date(2006, 5, 3, 0, 0, 0, 0)
        '
        'cbopayroll
        '
        Me.cbopayroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbopayroll.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbopayroll.FormattingEnabled = True
        Me.cbopayroll.Location = New System.Drawing.Point(871, 28)
        Me.cbopayroll.Name = "cbopayroll"
        Me.cbopayroll.Size = New System.Drawing.Size(116, 23)
        Me.cbopayroll.TabIndex = 11
        '
        'Label62
        '
        Me.Label62.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(791, 31)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(79, 15)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Payroll Code:"
        '
        'cborate
        '
        Me.cborate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cborate.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cborate.FormattingEnabled = True
        Me.cborate.Location = New System.Drawing.Point(871, 54)
        Me.cborate.Name = "cborate"
        Me.cborate.Size = New System.Drawing.Size(116, 23)
        Me.cborate.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(481, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Position Type:"
        '
        'cboEmp_type
        '
        Me.cboEmp_type.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboEmp_type.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboEmp_type.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmp_type.FormattingEnabled = True
        Me.cboEmp_type.Location = New System.Drawing.Point(567, 26)
        Me.cboEmp_type.Name = "cboEmp_type"
        Me.cboEmp_type.Size = New System.Drawing.Size(208, 23)
        Me.cboEmp_type.TabIndex = 5
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(481, 54)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 15)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Position Title:"
        '
        'Label69
        '
        Me.Label69.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(791, 57)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(65, 15)
        Me.Label69.TabIndex = 34
        Me.Label69.Text = "Rate Type:"
        '
        'cbotitledesignation
        '
        Me.cbotitledesignation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotitledesignation.FormattingEnabled = True
        Me.cbotitledesignation.Location = New System.Drawing.Point(567, 51)
        Me.cbotitledesignation.Name = "cbotitledesignation"
        Me.cbotitledesignation.Size = New System.Drawing.Size(208, 23)
        Me.cbotitledesignation.TabIndex = 6
        '
        'txtbasicpay
        '
        Me.txtbasicpay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbasicpay.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbasicpay.Location = New System.Drawing.Point(108, 152)
        Me.txtbasicpay.Name = "txtbasicpay"
        Me.txtbasicpay.Size = New System.Drawing.Size(85, 21)
        Me.txtbasicpay.TabIndex = 6
        Me.txtbasicpay.Text = "0.00"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(1, 156)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(59, 15)
        Me.Label59.TabIndex = 12
        Me.Label59.Text = "Basic Pay:"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(791, 107)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 15)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Date Hired:"
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox3.Location = New System.Drawing.Point(1, 1)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(1065, 22)
        Me.PictureBox3.TabIndex = 18
        Me.PictureBox3.TabStop = False
        '
        'TabPage20
        '
        Me.TabPage20.BackColor = System.Drawing.Color.White
        Me.TabPage20.Controls.Add(Me.btnADdReferral)
        Me.TabPage20.Controls.Add(Me.btnDeleteReferrals)
        Me.TabPage20.Controls.Add(Me.dgvPersonsReferred)
        Me.TabPage20.Controls.Add(Me.Label87)
        Me.TabPage20.Controls.Add(Me.PictureBox22)
        Me.TabPage20.Location = New System.Drawing.Point(4, 22)
        Me.TabPage20.Name = "TabPage20"
        Me.TabPage20.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage20.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage20.TabIndex = 34
        Me.TabPage20.Text = "Referrals"
        '
        'btnADdReferral
        '
        Me.btnADdReferral.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnADdReferral.Location = New System.Drawing.Point(711, 40)
        Me.btnADdReferral.Name = "btnADdReferral"
        Me.btnADdReferral.Size = New System.Drawing.Size(75, 23)
        Me.btnADdReferral.TabIndex = 32
        Me.btnADdReferral.Text = "Add"
        Me.btnADdReferral.UseVisualStyleBackColor = True
        '
        'btnDeleteReferrals
        '
        Me.btnDeleteReferrals.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDeleteReferrals.Location = New System.Drawing.Point(711, 69)
        Me.btnDeleteReferrals.Name = "btnDeleteReferrals"
        Me.btnDeleteReferrals.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteReferrals.TabIndex = 31
        Me.btnDeleteReferrals.Text = "Delete"
        Me.btnDeleteReferrals.UseVisualStyleBackColor = True
        '
        'dgvPersonsReferred
        '
        Me.dgvPersonsReferred.AllowUserToDeleteRows = False
        Me.dgvPersonsReferred.AllowUserToResizeColumns = False
        Me.dgvPersonsReferred.AllowUserToResizeRows = False
        Me.dgvPersonsReferred.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvPersonsReferred.BackgroundColor = System.Drawing.Color.White
        Me.dgvPersonsReferred.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPersonsReferred.Location = New System.Drawing.Point(9, 37)
        Me.dgvPersonsReferred.Name = "dgvPersonsReferred"
        Me.dgvPersonsReferred.ReadOnly = True
        Me.dgvPersonsReferred.Size = New System.Drawing.Size(695, 150)
        Me.dgvPersonsReferred.TabIndex = 30
        '
        'Label87
        '
        Me.Label87.AutoSize = True
        Me.Label87.BackColor = System.Drawing.Color.Gray
        Me.Label87.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label87.ForeColor = System.Drawing.Color.White
        Me.Label87.Location = New System.Drawing.Point(6, 10)
        Me.Label87.Name = "Label87"
        Me.Label87.Size = New System.Drawing.Size(122, 14)
        Me.Label87.TabIndex = 29
        Me.Label87.Text = "Persons Referred"
        '
        'PictureBox22
        '
        Me.PictureBox22.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox22.Dock = System.Windows.Forms.DockStyle.Top
        Me.PictureBox22.Location = New System.Drawing.Point(3, 3)
        Me.PictureBox22.Name = "PictureBox22"
        Me.PictureBox22.Size = New System.Drawing.Size(1003, 31)
        Me.PictureBox22.TabIndex = 28
        Me.PictureBox22.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.btnUpdateBA)
        Me.TabPage2.Controls.Add(Me.btnDeleteBA)
        Me.TabPage2.Controls.Add(Me.btnNewBA)
        Me.TabPage2.Controls.Add(Me.lvlBankInfo)
        Me.TabPage2.Controls.Add(Me.PictureBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage2.TabIndex = 18
        Me.TabPage2.Text = "Bank Information"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label35.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label35.Location = New System.Drawing.Point(6, 2)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(115, 18)
        Me.Label35.TabIndex = 26
        Me.Label35.Text = "Bank Information"
        '
        'btnUpdateBA
        '
        Me.btnUpdateBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateBA.Image = CType(resources.GetObject("btnUpdateBA.Image"), System.Drawing.Image)
        Me.btnUpdateBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateBA.Location = New System.Drawing.Point(781, 153)
        Me.btnUpdateBA.Name = "btnUpdateBA"
        Me.btnUpdateBA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnUpdateBA.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateBA.TabIndex = 24
        Me.btnUpdateBA.Text = "Edit"
        Me.btnUpdateBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateBA.UseVisualStyleBackColor = True
        Me.btnUpdateBA.Visible = False
        '
        'btnDeleteBA
        '
        Me.btnDeleteBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteBA.Image = CType(resources.GetObject("btnDeleteBA.Image"), System.Drawing.Image)
        Me.btnDeleteBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteBA.Location = New System.Drawing.Point(851, 153)
        Me.btnDeleteBA.Name = "btnDeleteBA"
        Me.btnDeleteBA.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteBA.TabIndex = 25
        Me.btnDeleteBA.Text = "Delete"
        Me.btnDeleteBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteBA.UseVisualStyleBackColor = True
        Me.btnDeleteBA.Visible = False
        '
        'btnNewBA
        '
        Me.btnNewBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewBA.Image = CType(resources.GetObject("btnNewBA.Image"), System.Drawing.Image)
        Me.btnNewBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewBA.Location = New System.Drawing.Point(710, 153)
        Me.btnNewBA.Name = "btnNewBA"
        Me.btnNewBA.Size = New System.Drawing.Size(68, 24)
        Me.btnNewBA.TabIndex = 23
        Me.btnNewBA.Text = "New"
        Me.btnNewBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewBA.UseVisualStyleBackColor = True
        Me.btnNewBA.Visible = False
        '
        'lvlBankInfo
        '
        Me.lvlBankInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlBankInfo.Location = New System.Drawing.Point(3, 29)
        Me.lvlBankInfo.Name = "lvlBankInfo"
        Me.lvlBankInfo.Size = New System.Drawing.Size(916, 118)
        Me.lvlBankInfo.TabIndex = 22
        Me.lvlBankInfo.UseCompatibleStateImageBehavior = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox4.Location = New System.Drawing.Point(-1, 1)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox4.TabIndex = 20
        Me.PictureBox4.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage4.Controls.Add(Me.btnUpdateSInc)
        Me.TabPage4.Controls.Add(Me.btnDeleteSInc)
        Me.TabPage4.Controls.Add(Me.btnsaveSInc)
        Me.TabPage4.Controls.Add(Me.lvlSourceIncome)
        Me.TabPage4.Controls.Add(Me.Label36)
        Me.TabPage4.Controls.Add(Me.PictureBox6)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage4.TabIndex = 19
        Me.TabPage4.Text = "Source of Income"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnUpdateSInc
        '
        Me.btnUpdateSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSInc.Image = CType(resources.GetObject("btnUpdateSInc.Image"), System.Drawing.Image)
        Me.btnUpdateSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSInc.Location = New System.Drawing.Point(781, 153)
        Me.btnUpdateSInc.Name = "btnUpdateSInc"
        Me.btnUpdateSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSInc.TabIndex = 28
        Me.btnUpdateSInc.Text = "Edit"
        Me.btnUpdateSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSInc.UseVisualStyleBackColor = True
        '
        'btnDeleteSInc
        '
        Me.btnDeleteSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSInc.Image = CType(resources.GetObject("btnDeleteSInc.Image"), System.Drawing.Image)
        Me.btnDeleteSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSInc.Location = New System.Drawing.Point(850, 153)
        Me.btnDeleteSInc.Name = "btnDeleteSInc"
        Me.btnDeleteSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSInc.TabIndex = 27
        Me.btnDeleteSInc.Text = "Delete"
        Me.btnDeleteSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSInc.UseVisualStyleBackColor = True
        '
        'btnsaveSInc
        '
        Me.btnsaveSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsaveSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsaveSInc.Image = CType(resources.GetObject("btnsaveSInc.Image"), System.Drawing.Image)
        Me.btnsaveSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsaveSInc.Location = New System.Drawing.Point(712, 153)
        Me.btnsaveSInc.Name = "btnsaveSInc"
        Me.btnsaveSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnsaveSInc.TabIndex = 26
        Me.btnsaveSInc.Text = "New"
        Me.btnsaveSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsaveSInc.UseVisualStyleBackColor = True
        '
        'lvlSourceIncome
        '
        Me.lvlSourceIncome.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSourceIncome.Location = New System.Drawing.Point(3, 29)
        Me.lvlSourceIncome.Name = "lvlSourceIncome"
        Me.lvlSourceIncome.Size = New System.Drawing.Size(914, 118)
        Me.lvlSourceIncome.TabIndex = 25
        Me.lvlSourceIncome.UseCompatibleStateImageBehavior = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label36.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label36.Location = New System.Drawing.Point(7, 2)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(115, 18)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Source of Income"
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox6.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(1027, 22)
        Me.PictureBox6.TabIndex = 23
        Me.PictureBox6.TabStop = False
        '
        'TabPage17
        '
        Me.TabPage17.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage17.Controls.Add(Me.BtnEditContact)
        Me.TabPage17.Controls.Add(Me.BtnDeleteContact)
        Me.TabPage17.Controls.Add(Me.btnaddContact)
        Me.TabPage17.Controls.Add(Me.lvlNearestRelatives)
        Me.TabPage17.Controls.Add(Me.Label98)
        Me.TabPage17.Controls.Add(Me.PictureBox1)
        Me.TabPage17.Location = New System.Drawing.Point(4, 22)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage17.TabIndex = 17
        Me.TabPage17.Text = "Beneficiary"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'BtnEditContact
        '
        Me.BtnEditContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnEditContact.Image = CType(resources.GetObject("BtnEditContact.Image"), System.Drawing.Image)
        Me.BtnEditContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnEditContact.Location = New System.Drawing.Point(778, 153)
        Me.BtnEditContact.Name = "BtnEditContact"
        Me.BtnEditContact.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BtnEditContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnEditContact.TabIndex = 20
        Me.BtnEditContact.Text = "Edit"
        Me.BtnEditContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnEditContact.UseVisualStyleBackColor = True
        '
        'BtnDeleteContact
        '
        Me.BtnDeleteContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnDeleteContact.Image = CType(resources.GetObject("BtnDeleteContact.Image"), System.Drawing.Image)
        Me.BtnDeleteContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeleteContact.Location = New System.Drawing.Point(847, 153)
        Me.BtnDeleteContact.Name = "BtnDeleteContact"
        Me.BtnDeleteContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnDeleteContact.TabIndex = 21
        Me.BtnDeleteContact.Text = "Delete"
        Me.BtnDeleteContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeleteContact.UseVisualStyleBackColor = True
        '
        'btnaddContact
        '
        Me.btnaddContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnaddContact.Image = CType(resources.GetObject("btnaddContact.Image"), System.Drawing.Image)
        Me.btnaddContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnaddContact.Location = New System.Drawing.Point(709, 153)
        Me.btnaddContact.Name = "btnaddContact"
        Me.btnaddContact.Size = New System.Drawing.Size(68, 24)
        Me.btnaddContact.TabIndex = 19
        Me.btnaddContact.Text = "New"
        Me.btnaddContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnaddContact.UseVisualStyleBackColor = True
        '
        'lvlNearestRelatives
        '
        Me.lvlNearestRelatives.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlNearestRelatives.Location = New System.Drawing.Point(3, 29)
        Me.lvlNearestRelatives.Name = "lvlNearestRelatives"
        Me.lvlNearestRelatives.Size = New System.Drawing.Size(911, 118)
        Me.lvlNearestRelatives.TabIndex = 4
        Me.lvlNearestRelatives.UseCompatibleStateImageBehavior = False
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label98.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label98.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label98.Location = New System.Drawing.Point(5, 2)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(77, 18)
        Me.Label98.TabIndex = 3
        Me.Label98.Text = "Beneficiary"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(8, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(1049, 21)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPage21
        '
        Me.TabPage21.BackColor = System.Drawing.Color.White
        Me.TabPage21.Controls.Add(Me.btnNREdit)
        Me.TabPage21.Controls.Add(Me.btnNRDelete)
        Me.TabPage21.Controls.Add(Me.btnNRNew)
        Me.TabPage21.Controls.Add(Me.lvlRelative)
        Me.TabPage21.Controls.Add(Me.Label88)
        Me.TabPage21.Controls.Add(Me.PictureBox23)
        Me.TabPage21.Location = New System.Drawing.Point(4, 22)
        Me.TabPage21.Name = "TabPage21"
        Me.TabPage21.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage21.TabIndex = 35
        Me.TabPage21.Text = "Nearest Relative"
        '
        'btnNREdit
        '
        Me.btnNREdit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNREdit.Image = CType(resources.GetObject("btnNREdit.Image"), System.Drawing.Image)
        Me.btnNREdit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNREdit.Location = New System.Drawing.Point(776, 153)
        Me.btnNREdit.Name = "btnNREdit"
        Me.btnNREdit.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnNREdit.Size = New System.Drawing.Size(68, 24)
        Me.btnNREdit.TabIndex = 26
        Me.btnNREdit.Text = "Edit"
        Me.btnNREdit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNREdit.UseVisualStyleBackColor = True
        '
        'btnNRDelete
        '
        Me.btnNRDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNRDelete.Image = CType(resources.GetObject("btnNRDelete.Image"), System.Drawing.Image)
        Me.btnNRDelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNRDelete.Location = New System.Drawing.Point(845, 153)
        Me.btnNRDelete.Name = "btnNRDelete"
        Me.btnNRDelete.Size = New System.Drawing.Size(68, 24)
        Me.btnNRDelete.TabIndex = 27
        Me.btnNRDelete.Text = "Delete"
        Me.btnNRDelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNRDelete.UseVisualStyleBackColor = True
        '
        'btnNRNew
        '
        Me.btnNRNew.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNRNew.Image = CType(resources.GetObject("btnNRNew.Image"), System.Drawing.Image)
        Me.btnNRNew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNRNew.Location = New System.Drawing.Point(707, 153)
        Me.btnNRNew.Name = "btnNRNew"
        Me.btnNRNew.Size = New System.Drawing.Size(68, 24)
        Me.btnNRNew.TabIndex = 25
        Me.btnNRNew.Text = "New"
        Me.btnNRNew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNRNew.UseVisualStyleBackColor = True
        '
        'lvlRelative
        '
        Me.lvlRelative.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlRelative.Location = New System.Drawing.Point(1, 29)
        Me.lvlRelative.Name = "lvlRelative"
        Me.lvlRelative.Size = New System.Drawing.Size(911, 118)
        Me.lvlRelative.TabIndex = 24
        Me.lvlRelative.UseCompatibleStateImageBehavior = False
        '
        'Label88
        '
        Me.Label88.AutoSize = True
        Me.Label88.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label88.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label88.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label88.Location = New System.Drawing.Point(3, 2)
        Me.Label88.Name = "Label88"
        Me.Label88.Size = New System.Drawing.Size(111, 18)
        Me.Label88.TabIndex = 23
        Me.Label88.Text = "Nearest Relative"
        '
        'PictureBox23
        '
        Me.PictureBox23.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox23.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox23.Location = New System.Drawing.Point(6, 0)
        Me.PictureBox23.Name = "PictureBox23"
        Me.PictureBox23.Size = New System.Drawing.Size(1049, 21)
        Me.PictureBox23.TabIndex = 22
        Me.PictureBox23.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage3.Controls.Add(Me.LvlEmployeeDependent)
        Me.TabPage3.Controls.Add(Me.Label68)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.btnemp_editdepdts)
        Me.TabPage3.Controls.Add(Me.btndelete_dep)
        Me.TabPage3.Controls.Add(Me.btnadd_dep)
        Me.TabPage3.Controls.Add(Me.PictureBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Dependents"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'LvlEmployeeDependent
        '
        Me.LvlEmployeeDependent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LvlEmployeeDependent.Location = New System.Drawing.Point(9, 22)
        Me.LvlEmployeeDependent.Name = "LvlEmployeeDependent"
        Me.LvlEmployeeDependent.Size = New System.Drawing.Size(910, 124)
        Me.LvlEmployeeDependent.TabIndex = 68
        Me.LvlEmployeeDependent.UseCompatibleStateImageBehavior = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label68.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label68.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label68.Location = New System.Drawing.Point(3, 0)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(149, 18)
        Me.Label68.TabIndex = 8
        Me.Label68.Text = "Employee Dependents"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.emp_pic)
        Me.GroupBox3.Controls.Add(Me.temp_marks)
        Me.GroupBox3.Controls.Add(Me.txtitemno)
        Me.GroupBox3.Controls.Add(Me.txtremarks)
        Me.GroupBox3.Controls.Add(Me.temp_add3)
        Me.GroupBox3.Controls.Add(Me.txtparent_ID)
        Me.GroupBox3.Controls.Add(Me.txtmonthreqular)
        Me.GroupBox3.Controls.Add(Me.txtkeysection)
        Me.GroupBox3.Controls.Add(Me.temp_height)
        Me.GroupBox3.Controls.Add(Me.txtdescsection)
        Me.GroupBox3.Controls.Add(Me.txtsalarygrade)
        Me.GroupBox3.Controls.Add(Me.txtdescdepartment)
        Me.GroupBox3.Controls.Add(Me.temp_add2)
        Me.GroupBox3.Controls.Add(Me.txtdescdivision)
        Me.GroupBox3.Controls.Add(Me.txtweight)
        Me.GroupBox3.Controls.Add(Me.txtkeydepartment)
        Me.GroupBox3.Controls.Add(Me.txtdateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeydivision)
        Me.GroupBox3.Controls.Add(Me.Dateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeycompany)
        Me.GroupBox3.Controls.Add(Me.emp_extphone)
        Me.GroupBox3.Controls.Add(Me.cbosalarygrade)
        Me.GroupBox3.Controls.Add(Me.temp_citizen)
        Me.GroupBox3.Controls.Add(Me.temp_designation)
        Me.GroupBox3.Controls.Add(Me.txtType_employee)
        Me.GroupBox3.Controls.Add(Me.txtfxkeypositionlevel)
        Me.GroupBox3.Controls.Add(Me.txtfullname)
        Me.GroupBox3.Controls.Add(Me.TXTRECID)
        Me.GroupBox3.Controls.Add(Me.cbodept)
        Me.GroupBox3.Controls.Add(Me.Dateresigned)
        Me.GroupBox3.Controls.Add(Me.TXTKEYEMPLOYEEID)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(423, 22)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(242, 159)
        Me.GroupBox3.TabIndex = 67
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Omitted items in Personnel New Version"
        Me.GroupBox3.Visible = False
        '
        'emp_pic
        '
        Me.emp_pic.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar
        Me.emp_pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_pic.Location = New System.Drawing.Point(96, 92)
        Me.emp_pic.Name = "emp_pic"
        Me.emp_pic.Size = New System.Drawing.Size(126, 124)
        Me.emp_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.emp_pic.TabIndex = 0
        Me.emp_pic.TabStop = False
        '
        'temp_marks
        '
        Me.temp_marks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_marks.Location = New System.Drawing.Point(104, 182)
        Me.temp_marks.MaxLength = 50
        Me.temp_marks.Name = "temp_marks"
        Me.temp_marks.Size = New System.Drawing.Size(137, 21)
        Me.temp_marks.TabIndex = 9
        '
        'txtitemno
        '
        Me.txtitemno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtitemno.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtitemno.Location = New System.Drawing.Point(57, 37)
        Me.txtitemno.Name = "txtitemno"
        Me.txtitemno.Size = New System.Drawing.Size(35, 21)
        Me.txtitemno.TabIndex = 19
        '
        'txtremarks
        '
        Me.txtremarks.Location = New System.Drawing.Point(66, 208)
        Me.txtremarks.Multiline = True
        Me.txtremarks.Name = "txtremarks"
        Me.txtremarks.Size = New System.Drawing.Size(39, 22)
        Me.txtremarks.TabIndex = 43
        '
        'temp_add3
        '
        Me.temp_add3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add3.Location = New System.Drawing.Point(104, 208)
        Me.temp_add3.MaxLength = 255
        Me.temp_add3.Name = "temp_add3"
        Me.temp_add3.Size = New System.Drawing.Size(156, 21)
        Me.temp_add3.TabIndex = 5
        '
        'txtparent_ID
        '
        Me.txtparent_ID.Location = New System.Drawing.Point(166, 183)
        Me.txtparent_ID.Name = "txtparent_ID"
        Me.txtparent_ID.Size = New System.Drawing.Size(28, 21)
        Me.txtparent_ID.TabIndex = 97
        Me.txtparent_ID.Visible = False
        '
        'txtmonthreqular
        '
        Me.txtmonthreqular.Location = New System.Drawing.Point(125, 60)
        Me.txtmonthreqular.MaxLength = 3
        Me.txtmonthreqular.Name = "txtmonthreqular"
        Me.txtmonthreqular.Size = New System.Drawing.Size(39, 21)
        Me.txtmonthreqular.TabIndex = 15
        '
        'txtkeysection
        '
        Me.txtkeysection.Location = New System.Drawing.Point(132, 183)
        Me.txtkeysection.Name = "txtkeysection"
        Me.txtkeysection.Size = New System.Drawing.Size(28, 21)
        Me.txtkeysection.TabIndex = 96
        Me.txtkeysection.Visible = False
        '
        'temp_height
        '
        Me.temp_height.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_height.Location = New System.Drawing.Point(78, 62)
        Me.temp_height.MaxLength = 10
        Me.temp_height.Name = "temp_height"
        Me.temp_height.Size = New System.Drawing.Size(51, 21)
        Me.temp_height.TabIndex = 7
        '
        'txtdescsection
        '
        Me.txtdescsection.Location = New System.Drawing.Point(166, 159)
        Me.txtdescsection.Name = "txtdescsection"
        Me.txtdescsection.Size = New System.Drawing.Size(59, 21)
        Me.txtdescsection.TabIndex = 95
        Me.txtdescsection.Visible = False
        '
        'txtsalarygrade
        '
        Me.txtsalarygrade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtsalarygrade.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsalarygrade.Location = New System.Drawing.Point(183, 32)
        Me.txtsalarygrade.Name = "txtsalarygrade"
        Me.txtsalarygrade.Size = New System.Drawing.Size(39, 21)
        Me.txtsalarygrade.TabIndex = 47
        Me.txtsalarygrade.Visible = False
        '
        'txtdescdepartment
        '
        Me.txtdescdepartment.Location = New System.Drawing.Point(166, 136)
        Me.txtdescdepartment.Name = "txtdescdepartment"
        Me.txtdescdepartment.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdepartment.TabIndex = 94
        Me.txtdescdepartment.Visible = False
        '
        'temp_add2
        '
        Me.temp_add2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add2.Location = New System.Drawing.Point(84, 133)
        Me.temp_add2.Name = "temp_add2"
        Me.temp_add2.Size = New System.Drawing.Size(25, 21)
        Me.temp_add2.TabIndex = 10
        Me.temp_add2.Visible = False
        '
        'txtdescdivision
        '
        Me.txtdescdivision.Location = New System.Drawing.Point(166, 113)
        Me.txtdescdivision.Name = "txtdescdivision"
        Me.txtdescdivision.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdivision.TabIndex = 93
        Me.txtdescdivision.Visible = False
        '
        'txtweight
        '
        Me.txtweight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtweight.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtweight.Location = New System.Drawing.Point(202, 134)
        Me.txtweight.MaxLength = 10
        Me.txtweight.Name = "txtweight"
        Me.txtweight.Size = New System.Drawing.Size(32, 21)
        Me.txtweight.TabIndex = 8
        '
        'txtkeydepartment
        '
        Me.txtkeydepartment.Location = New System.Drawing.Point(132, 159)
        Me.txtkeydepartment.Name = "txtkeydepartment"
        Me.txtkeydepartment.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydepartment.TabIndex = 92
        Me.txtkeydepartment.Visible = False
        '
        'txtdateregular
        '
        Me.txtdateregular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdateregular.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdateregular.Location = New System.Drawing.Point(9, 91)
        Me.txtdateregular.Name = "txtdateregular"
        Me.txtdateregular.Size = New System.Drawing.Size(69, 21)
        Me.txtdateregular.TabIndex = 57
        Me.txtdateregular.Visible = False
        '
        'txtkeydivision
        '
        Me.txtkeydivision.Location = New System.Drawing.Point(132, 136)
        Me.txtkeydivision.Name = "txtkeydivision"
        Me.txtkeydivision.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydivision.TabIndex = 91
        Me.txtkeydivision.Visible = False
        '
        'Dateregular
        '
        Me.Dateregular.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateregular.Location = New System.Drawing.Point(84, 91)
        Me.Dateregular.Name = "Dateregular"
        Me.Dateregular.Size = New System.Drawing.Size(21, 21)
        Me.Dateregular.TabIndex = 49
        Me.Dateregular.Value = New Date(2006, 7, 17, 0, 0, 0, 0)
        Me.Dateregular.Visible = False
        '
        'txtkeycompany
        '
        Me.txtkeycompany.Location = New System.Drawing.Point(132, 113)
        Me.txtkeycompany.Name = "txtkeycompany"
        Me.txtkeycompany.Size = New System.Drawing.Size(28, 21)
        Me.txtkeycompany.TabIndex = 90
        Me.txtkeycompany.Visible = False
        '
        'emp_extphone
        '
        Me.emp_extphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_extphone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_extphone.Location = New System.Drawing.Point(191, 89)
        Me.emp_extphone.Name = "emp_extphone"
        Me.emp_extphone.Size = New System.Drawing.Size(45, 21)
        Me.emp_extphone.TabIndex = 7
        '
        'cbosalarygrade
        '
        Me.cbosalarygrade.FormattingEnabled = True
        Me.cbosalarygrade.Location = New System.Drawing.Point(84, 86)
        Me.cbosalarygrade.Name = "cbosalarygrade"
        Me.cbosalarygrade.Size = New System.Drawing.Size(156, 23)
        Me.cbosalarygrade.TabIndex = 7
        '
        'temp_citizen
        '
        Me.temp_citizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_citizen.Location = New System.Drawing.Point(97, 110)
        Me.temp_citizen.MaxLength = 50
        Me.temp_citizen.Name = "temp_citizen"
        Me.temp_citizen.Size = New System.Drawing.Size(137, 21)
        Me.temp_citizen.TabIndex = 6
        '
        'temp_designation
        '
        Me.temp_designation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_designation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_designation.Location = New System.Drawing.Point(67, 95)
        Me.temp_designation.Name = "temp_designation"
        Me.temp_designation.Size = New System.Drawing.Size(25, 21)
        Me.temp_designation.TabIndex = 16
        Me.temp_designation.Visible = False
        '
        'txtType_employee
        '
        Me.txtType_employee.Location = New System.Drawing.Point(118, 53)
        Me.txtType_employee.Name = "txtType_employee"
        Me.txtType_employee.Size = New System.Drawing.Size(42, 21)
        Me.txtType_employee.TabIndex = 80
        Me.txtType_employee.Visible = False
        '
        'txtfxkeypositionlevel
        '
        Me.txtfxkeypositionlevel.Location = New System.Drawing.Point(118, 76)
        Me.txtfxkeypositionlevel.Name = "txtfxkeypositionlevel"
        Me.txtfxkeypositionlevel.Size = New System.Drawing.Size(42, 21)
        Me.txtfxkeypositionlevel.TabIndex = 81
        Me.txtfxkeypositionlevel.Visible = False
        '
        'txtfullname
        '
        Me.txtfullname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtfullname.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfullname.Location = New System.Drawing.Point(99, 95)
        Me.txtfullname.MaxLength = 50
        Me.txtfullname.Name = "txtfullname"
        Me.txtfullname.Size = New System.Drawing.Size(22, 21)
        Me.txtfullname.TabIndex = 62
        Me.txtfullname.Visible = False
        '
        'TXTRECID
        '
        Me.TXTRECID.Location = New System.Drawing.Point(145, 65)
        Me.TXTRECID.Name = "TXTRECID"
        Me.TXTRECID.Size = New System.Drawing.Size(72, 21)
        Me.TXTRECID.TabIndex = 52
        Me.TXTRECID.Visible = False
        '
        'cbodept
        '
        Me.cbodept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cbodept.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbodept.Location = New System.Drawing.Point(100, 123)
        Me.cbodept.MaxLength = 50
        Me.cbodept.Name = "cbodept"
        Me.cbodept.Size = New System.Drawing.Size(22, 21)
        Me.cbodept.TabIndex = 72
        Me.cbodept.Visible = False
        '
        'Dateresigned
        '
        Me.Dateresigned.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dateresigned.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateresigned.Location = New System.Drawing.Point(67, 122)
        Me.Dateresigned.Name = "Dateresigned"
        Me.Dateresigned.Size = New System.Drawing.Size(28, 21)
        Me.Dateresigned.TabIndex = 58
        Me.Dateresigned.Value = New Date(2006, 5, 30, 0, 0, 0, 0)
        Me.Dateresigned.Visible = False
        '
        'TXTKEYEMPLOYEEID
        '
        Me.TXTKEYEMPLOYEEID.Location = New System.Drawing.Point(145, 89)
        Me.TXTKEYEMPLOYEEID.Name = "TXTKEYEMPLOYEEID"
        Me.TXTKEYEMPLOYEEID.Size = New System.Drawing.Size(72, 21)
        Me.TXTKEYEMPLOYEEID.TabIndex = 51
        Me.TXTKEYEMPLOYEEID.Visible = False
        '
        'btnemp_editdepdts
        '
        Me.btnemp_editdepdts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_editdepdts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_editdepdts.Image = CType(resources.GetObject("btnemp_editdepdts.Image"), System.Drawing.Image)
        Me.btnemp_editdepdts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_editdepdts.Location = New System.Drawing.Point(783, 152)
        Me.btnemp_editdepdts.Name = "btnemp_editdepdts"
        Me.btnemp_editdepdts.Size = New System.Drawing.Size(68, 24)
        Me.btnemp_editdepdts.TabIndex = 10
        Me.btnemp_editdepdts.Text = "Edit"
        Me.btnemp_editdepdts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_editdepdts.UseVisualStyleBackColor = True
        '
        'btndelete_dep
        '
        Me.btndelete_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btndelete_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete_dep.Image = CType(resources.GetObject("btndelete_dep.Image"), System.Drawing.Image)
        Me.btndelete_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete_dep.Location = New System.Drawing.Point(851, 152)
        Me.btndelete_dep.Name = "btndelete_dep"
        Me.btndelete_dep.Size = New System.Drawing.Size(68, 24)
        Me.btndelete_dep.TabIndex = 5
        Me.btndelete_dep.Text = "Delete"
        Me.btndelete_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete_dep.UseVisualStyleBackColor = True
        '
        'btnadd_dep
        '
        Me.btnadd_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnadd_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd_dep.Image = CType(resources.GetObject("btnadd_dep.Image"), System.Drawing.Image)
        Me.btnadd_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnadd_dep.Location = New System.Drawing.Point(715, 152)
        Me.btnadd_dep.Name = "btnadd_dep"
        Me.btnadd_dep.Size = New System.Drawing.Size(68, 24)
        Me.btnadd_dep.TabIndex = 3
        Me.btnadd_dep.Text = "New"
        Me.btnadd_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnadd_dep.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox5.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(1050, 18)
        Me.PictureBox5.TabIndex = 1
        Me.PictureBox5.TabStop = False
        '
        'others
        '
        Me.others.Controls.Add(Me.btnEditMobile)
        Me.others.Controls.Add(Me.gridMobileNos)
        Me.others.Controls.Add(Me.txtEloadingLimit)
        Me.others.Controls.Add(Me.Label31)
        Me.others.Controls.Add(Me.txtEloaderPIN)
        Me.others.Controls.Add(Me.chkShowPINChar)
        Me.others.Controls.Add(Me.Label24)
        Me.others.Controls.Add(Me.Label15)
        Me.others.Controls.Add(Me.btnAddMobileNo)
        Me.others.Controls.Add(Me.chkIsEloader)
        Me.others.Controls.Add(Me.Label4)
        Me.others.Controls.Add(Me.txtMobileNo)
        Me.others.Controls.Add(Me.PictureBox9)
        Me.others.Controls.Add(Me.PictureBox7)
        Me.others.Location = New System.Drawing.Point(4, 22)
        Me.others.Name = "others"
        Me.others.Padding = New System.Windows.Forms.Padding(3)
        Me.others.Size = New System.Drawing.Size(1009, 190)
        Me.others.TabIndex = 20
        Me.others.Text = "Others"
        Me.others.UseVisualStyleBackColor = True
        '
        'btnEditMobile
        '
        Me.btnEditMobile.Location = New System.Drawing.Point(423, 72)
        Me.btnEditMobile.Name = "btnEditMobile"
        Me.btnEditMobile.Size = New System.Drawing.Size(43, 21)
        Me.btnEditMobile.TabIndex = 45
        Me.btnEditMobile.Text = "Edit"
        Me.btnEditMobile.UseVisualStyleBackColor = True
        '
        'gridMobileNos
        '
        Me.gridMobileNos.AllowUserToAddRows = False
        Me.gridMobileNos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridMobileNos.BackgroundColor = System.Drawing.Color.White
        Me.gridMobileNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridMobileNos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.gridMobileNos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMobileNos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.gridMobileNos.Location = New System.Drawing.Point(203, 45)
        Me.gridMobileNos.Name = "gridMobileNos"
        Me.gridMobileNos.Size = New System.Drawing.Size(214, 125)
        Me.gridMobileNos.TabIndex = 44
        '
        'txtEloadingLimit
        '
        Me.txtEloadingLimit.Location = New System.Drawing.Point(571, 73)
        Me.txtEloadingLimit.Name = "txtEloadingLimit"
        Me.txtEloadingLimit.Size = New System.Drawing.Size(115, 21)
        Me.txtEloadingLimit.TabIndex = 43
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(484, 78)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(87, 15)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Eloading Limit:"
        '
        'txtEloaderPIN
        '
        Me.txtEloaderPIN.Location = New System.Drawing.Point(571, 45)
        Me.txtEloaderPIN.MaxLength = 4
        Me.txtEloaderPIN.Name = "txtEloaderPIN"
        Me.txtEloaderPIN.Size = New System.Drawing.Size(115, 21)
        Me.txtEloaderPIN.TabIndex = 40
        Me.txtEloaderPIN.UseSystemPasswordChar = True
        '
        'chkShowPINChar
        '
        Me.chkShowPINChar.AutoSize = True
        Me.chkShowPINChar.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPINChar.Location = New System.Drawing.Point(692, 49)
        Me.chkShowPINChar.Name = "chkShowPINChar"
        Me.chkShowPINChar.Size = New System.Drawing.Size(95, 17)
        Me.chkShowPINChar.TabIndex = 39
        Me.chkShowPINChar.Text = "Show PIN Char."
        Me.chkShowPINChar.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.Transparent
        Me.Label24.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(7, 29)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(101, 15)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "E-Loading Service"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(484, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 15)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "PIN:"
        '
        'btnAddMobileNo
        '
        Me.btnAddMobileNo.Location = New System.Drawing.Point(423, 45)
        Me.btnAddMobileNo.Name = "btnAddMobileNo"
        Me.btnAddMobileNo.Size = New System.Drawing.Size(43, 21)
        Me.btnAddMobileNo.TabIndex = 27
        Me.btnAddMobileNo.Text = "Add"
        Me.btnAddMobileNo.UseVisualStyleBackColor = True
        '
        'chkIsEloader
        '
        Me.chkIsEloader.AutoSize = True
        Me.chkIsEloader.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsEloader.Location = New System.Drawing.Point(571, 98)
        Me.chkIsEloader.Name = "chkIsEloader"
        Me.chkIsEloader.Size = New System.Drawing.Size(131, 19)
        Me.chkIsEloader.TabIndex = 35
        Me.chkIsEloader.Text = "Registered Member"
        Me.chkIsEloader.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(5, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 18)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Apps Registrations"
        '
        'txtMobileNo
        '
        Me.txtMobileNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.Location = New System.Drawing.Point(487, 134)
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.ReadOnly = True
        Me.txtMobileNo.Size = New System.Drawing.Size(199, 21)
        Me.txtMobileNo.TabIndex = 13
        Me.txtMobileNo.Visible = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox9.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox9.Location = New System.Drawing.Point(0, 1)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(996, 19)
        Me.PictureBox9.TabIndex = 33
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.WindowsApplication2.My.Resources.Resources.img_eload
        Me.PictureBox7.Location = New System.Drawing.Point(9, 48)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(188, 124)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.White
        Me.TabPage6.Controls.Add(Me.dgvemail)
        Me.TabPage6.Controls.Add(Me.Label43)
        Me.TabPage6.Controls.Add(Me.dgvphone)
        Me.TabPage6.Controls.Add(Me.Label42)
        Me.TabPage6.Controls.Add(Me.dgvcivil)
        Me.TabPage6.Controls.Add(Me.Label41)
        Me.TabPage6.Controls.Add(Me.dgvaddress)
        Me.TabPage6.Controls.Add(Me.Label40)
        Me.TabPage6.Controls.Add(Me.dgvlastname)
        Me.TabPage6.Controls.Add(Me.Label39)
        Me.TabPage6.Location = New System.Drawing.Point(4, 22)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage6.TabIndex = 21
        Me.TabPage6.Text = "Information History"
        '
        'dgvemail
        '
        Me.dgvemail.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvemail.BackgroundColor = System.Drawing.Color.White
        Me.dgvemail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvemail.Location = New System.Drawing.Point(603, 28)
        Me.dgvemail.Name = "dgvemail"
        Me.dgvemail.ReadOnly = True
        Me.dgvemail.RowHeadersVisible = False
        Me.dgvemail.Size = New System.Drawing.Size(302, 68)
        Me.dgvemail.TabIndex = 9
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label43.Location = New System.Drawing.Point(600, 12)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(92, 13)
        Me.Label43.TabIndex = 8
        Me.Label43.Text = "Previous Email:"
        '
        'dgvphone
        '
        Me.dgvphone.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvphone.BackgroundColor = System.Drawing.Color.White
        Me.dgvphone.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvphone.Location = New System.Drawing.Point(396, 28)
        Me.dgvphone.Name = "dgvphone"
        Me.dgvphone.ReadOnly = True
        Me.dgvphone.RowHeadersVisible = False
        Me.dgvphone.Size = New System.Drawing.Size(198, 68)
        Me.dgvphone.TabIndex = 7
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label42.Location = New System.Drawing.Point(396, 12)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(158, 13)
        Me.Label42.TabIndex = 6
        Me.Label42.Text = "Previous Residence Phone:"
        '
        'dgvcivil
        '
        Me.dgvcivil.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvcivil.BackgroundColor = System.Drawing.Color.White
        Me.dgvcivil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvcivil.Location = New System.Drawing.Point(235, 28)
        Me.dgvcivil.Name = "dgvcivil"
        Me.dgvcivil.ReadOnly = True
        Me.dgvcivil.RowHeadersVisible = False
        Me.dgvcivil.Size = New System.Drawing.Size(155, 68)
        Me.dgvcivil.TabIndex = 5
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label41.Location = New System.Drawing.Point(232, 12)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(128, 13)
        Me.Label41.TabIndex = 4
        Me.Label41.Text = "Previous Civil Status :"
        '
        'dgvaddress
        '
        Me.dgvaddress.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvaddress.BackgroundColor = System.Drawing.Color.White
        Me.dgvaddress.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvaddress.Location = New System.Drawing.Point(18, 115)
        Me.dgvaddress.Name = "dgvaddress"
        Me.dgvaddress.ReadOnly = True
        Me.dgvaddress.RowHeadersVisible = False
        Me.dgvaddress.Size = New System.Drawing.Size(674, 58)
        Me.dgvaddress.TabIndex = 3
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label40.Location = New System.Drawing.Point(15, 99)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(111, 13)
        Me.Label40.TabIndex = 2
        Me.Label40.Text = "Previous Address :"
        '
        'dgvlastname
        '
        Me.dgvlastname.AllowUserToAddRows = False
        Me.dgvlastname.AllowUserToDeleteRows = False
        Me.dgvlastname.AllowUserToResizeColumns = False
        Me.dgvlastname.AllowUserToResizeRows = False
        Me.dgvlastname.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvlastname.BackgroundColor = System.Drawing.Color.White
        Me.dgvlastname.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvlastname.Location = New System.Drawing.Point(18, 28)
        Me.dgvlastname.Name = "dgvlastname"
        Me.dgvlastname.ReadOnly = True
        Me.dgvlastname.RowHeadersVisible = False
        Me.dgvlastname.Size = New System.Drawing.Size(208, 68)
        Me.dgvlastname.TabIndex = 1
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(15, 12)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(121, 13)
        Me.Label39.TabIndex = 0
        Me.Label39.Text = "Previous Lastname :"
        '
        'TabPage19
        '
        Me.TabPage19.BackColor = System.Drawing.Color.White
        Me.TabPage19.Controls.Add(Me.cboEmpHistory)
        Me.TabPage19.Controls.Add(Me.Label81)
        Me.TabPage19.Controls.Add(Me.PictureBox21)
        Me.TabPage19.Controls.Add(Me.dgvEmpHistory)
        Me.TabPage19.Location = New System.Drawing.Point(4, 22)
        Me.TabPage19.Name = "TabPage19"
        Me.TabPage19.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage19.TabIndex = 33
        Me.TabPage19.Text = "Employment History"
        '
        'cboEmpHistory
        '
        Me.cboEmpHistory.FormattingEnabled = True
        Me.cboEmpHistory.Items.AddRange(New Object() {"Position History", "Basic Rate History", "Contract Rate History", "Company History"})
        Me.cboEmpHistory.Location = New System.Drawing.Point(7, 26)
        Me.cboEmpHistory.Name = "cboEmpHistory"
        Me.cboEmpHistory.Size = New System.Drawing.Size(200, 23)
        Me.cboEmpHistory.TabIndex = 37
        '
        'Label81
        '
        Me.Label81.AutoSize = True
        Me.Label81.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label81.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label81.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label81.Location = New System.Drawing.Point(4, 1)
        Me.Label81.Name = "Label81"
        Me.Label81.Size = New System.Drawing.Size(164, 18)
        Me.Label81.TabIndex = 28
        Me.Label81.Text = "Employment Information"
        '
        'PictureBox21
        '
        Me.PictureBox21.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox21.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox21.Location = New System.Drawing.Point(-3, 0)
        Me.PictureBox21.Name = "PictureBox21"
        Me.PictureBox21.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox21.TabIndex = 27
        Me.PictureBox21.TabStop = False
        '
        'dgvEmpHistory
        '
        Me.dgvEmpHistory.AllowUserToAddRows = False
        Me.dgvEmpHistory.AllowUserToDeleteRows = False
        Me.dgvEmpHistory.AllowUserToResizeColumns = False
        Me.dgvEmpHistory.AllowUserToResizeRows = False
        Me.dgvEmpHistory.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvEmpHistory.BackgroundColor = System.Drawing.Color.White
        Me.dgvEmpHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvEmpHistory.Location = New System.Drawing.Point(3, 54)
        Me.dgvEmpHistory.Name = "dgvEmpHistory"
        Me.dgvEmpHistory.ReadOnly = True
        Me.dgvEmpHistory.RowHeadersVisible = False
        Me.dgvEmpHistory.Size = New System.Drawing.Size(899, 132)
        Me.dgvEmpHistory.TabIndex = 11
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.White
        Me.TabPage7.Controls.Add(Me.Label70)
        Me.TabPage7.Controls.Add(Me.btnUpdateEI)
        Me.TabPage7.Controls.Add(Me.btnDeleteEI)
        Me.TabPage7.Controls.Add(Me.btnNewEI)
        Me.TabPage7.Controls.Add(Me.Label64)
        Me.TabPage7.Controls.Add(Me.PictureBox10)
        Me.TabPage7.Controls.Add(Me.lvlEducInfo)
        Me.TabPage7.Location = New System.Drawing.Point(4, 22)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage7.TabIndex = 22
        Me.TabPage7.Text = "Educational Infomation"
        '
        'Label70
        '
        Me.Label70.AutoSize = True
        Me.Label70.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label70.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label70.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label70.Location = New System.Drawing.Point(6, 2)
        Me.Label70.Name = "Label70"
        Me.Label70.Size = New System.Drawing.Size(157, 18)
        Me.Label70.TabIndex = 29
        Me.Label70.Text = "Educational Information"
        '
        'btnUpdateEI
        '
        Me.btnUpdateEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateEI.Image = CType(resources.GetObject("btnUpdateEI.Image"), System.Drawing.Image)
        Me.btnUpdateEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateEI.Location = New System.Drawing.Point(782, 148)
        Me.btnUpdateEI.Name = "btnUpdateEI"
        Me.btnUpdateEI.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateEI.TabIndex = 28
        Me.btnUpdateEI.Text = "Edit"
        Me.btnUpdateEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateEI.UseVisualStyleBackColor = True
        '
        'btnDeleteEI
        '
        Me.btnDeleteEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteEI.Image = CType(resources.GetObject("btnDeleteEI.Image"), System.Drawing.Image)
        Me.btnDeleteEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteEI.Location = New System.Drawing.Point(850, 148)
        Me.btnDeleteEI.Name = "btnDeleteEI"
        Me.btnDeleteEI.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteEI.TabIndex = 27
        Me.btnDeleteEI.Text = "Delete"
        Me.btnDeleteEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteEI.UseVisualStyleBackColor = True
        '
        'btnNewEI
        '
        Me.btnNewEI.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewEI.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewEI.Image = CType(resources.GetObject("btnNewEI.Image"), System.Drawing.Image)
        Me.btnNewEI.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewEI.Location = New System.Drawing.Point(714, 148)
        Me.btnNewEI.Name = "btnNewEI"
        Me.btnNewEI.Size = New System.Drawing.Size(68, 24)
        Me.btnNewEI.TabIndex = 26
        Me.btnNewEI.Text = "New"
        Me.btnNewEI.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewEI.UseVisualStyleBackColor = True
        '
        'Label64
        '
        Me.Label64.AutoSize = True
        Me.Label64.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label64.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label64.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label64.Location = New System.Drawing.Point(3, 2)
        Me.Label64.Name = "Label64"
        Me.Label64.Size = New System.Drawing.Size(0, 18)
        Me.Label64.TabIndex = 25
        '
        'PictureBox10
        '
        Me.PictureBox10.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox10.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox10.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox10.TabIndex = 24
        Me.PictureBox10.TabStop = False
        '
        'lvlEducInfo
        '
        Me.lvlEducInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlEducInfo.Location = New System.Drawing.Point(5, 25)
        Me.lvlEducInfo.Name = "lvlEducInfo"
        Me.lvlEducInfo.Size = New System.Drawing.Size(913, 117)
        Me.lvlEducInfo.TabIndex = 23
        Me.lvlEducInfo.UseCompatibleStateImageBehavior = False
        '
        'TabPage8
        '
        Me.TabPage8.BackColor = System.Drawing.Color.White
        Me.TabPage8.Controls.Add(Me.Label71)
        Me.TabPage8.Controls.Add(Me.btnUpdateJD)
        Me.TabPage8.Controls.Add(Me.btnDeleteJD)
        Me.TabPage8.Controls.Add(Me.btnNewJD)
        Me.TabPage8.Controls.Add(Me.PictureBox11)
        Me.TabPage8.Controls.Add(Me.lvlJobDesc)
        Me.TabPage8.Location = New System.Drawing.Point(4, 22)
        Me.TabPage8.Name = "TabPage8"
        Me.TabPage8.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage8.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage8.TabIndex = 23
        Me.TabPage8.Text = "Job Description"
        '
        'Label71
        '
        Me.Label71.AutoSize = True
        Me.Label71.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label71.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label71.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label71.Location = New System.Drawing.Point(4, 3)
        Me.Label71.Name = "Label71"
        Me.Label71.Size = New System.Drawing.Size(103, 18)
        Me.Label71.TabIndex = 35
        Me.Label71.Text = "Job Description"
        '
        'btnUpdateJD
        '
        Me.btnUpdateJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateJD.Image = CType(resources.GetObject("btnUpdateJD.Image"), System.Drawing.Image)
        Me.btnUpdateJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateJD.Location = New System.Drawing.Point(780, 149)
        Me.btnUpdateJD.Name = "btnUpdateJD"
        Me.btnUpdateJD.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateJD.TabIndex = 34
        Me.btnUpdateJD.Text = "Edit"
        Me.btnUpdateJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateJD.UseVisualStyleBackColor = True
        '
        'btnDeleteJD
        '
        Me.btnDeleteJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteJD.Image = CType(resources.GetObject("btnDeleteJD.Image"), System.Drawing.Image)
        Me.btnDeleteJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteJD.Location = New System.Drawing.Point(848, 149)
        Me.btnDeleteJD.Name = "btnDeleteJD"
        Me.btnDeleteJD.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteJD.TabIndex = 33
        Me.btnDeleteJD.Text = "Delete"
        Me.btnDeleteJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteJD.UseVisualStyleBackColor = True
        '
        'btnNewJD
        '
        Me.btnNewJD.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewJD.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewJD.Image = CType(resources.GetObject("btnNewJD.Image"), System.Drawing.Image)
        Me.btnNewJD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewJD.Location = New System.Drawing.Point(712, 149)
        Me.btnNewJD.Name = "btnNewJD"
        Me.btnNewJD.Size = New System.Drawing.Size(68, 24)
        Me.btnNewJD.TabIndex = 32
        Me.btnNewJD.Text = "New"
        Me.btnNewJD.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewJD.UseVisualStyleBackColor = True
        '
        'PictureBox11
        '
        Me.PictureBox11.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox11.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox11.Location = New System.Drawing.Point(-2, 1)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox11.TabIndex = 31
        Me.PictureBox11.TabStop = False
        '
        'lvlJobDesc
        '
        Me.lvlJobDesc.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlJobDesc.Location = New System.Drawing.Point(3, 26)
        Me.lvlJobDesc.Name = "lvlJobDesc"
        Me.lvlJobDesc.Size = New System.Drawing.Size(913, 117)
        Me.lvlJobDesc.TabIndex = 30
        Me.lvlJobDesc.UseCompatibleStateImageBehavior = False
        '
        'TabPage9
        '
        Me.TabPage9.BackColor = System.Drawing.Color.White
        Me.TabPage9.Controls.Add(Me.btnTDownload)
        Me.TabPage9.Controls.Add(Me.Label72)
        Me.TabPage9.Controls.Add(Me.btnUpdateT)
        Me.TabPage9.Controls.Add(Me.btnDeleteT)
        Me.TabPage9.Controls.Add(Me.btnNewT)
        Me.TabPage9.Controls.Add(Me.PictureBox12)
        Me.TabPage9.Controls.Add(Me.lvlTrainings)
        Me.TabPage9.Location = New System.Drawing.Point(4, 22)
        Me.TabPage9.Name = "TabPage9"
        Me.TabPage9.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage9.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage9.TabIndex = 24
        Me.TabPage9.Text = "Trainings"
        '
        'btnTDownload
        '
        Me.btnTDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnTDownload.Enabled = False
        Me.btnTDownload.Image = CType(resources.GetObject("btnTDownload.Image"), System.Drawing.Image)
        Me.btnTDownload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.btnTDownload.Location = New System.Drawing.Point(837, 149)
        Me.btnTDownload.Name = "btnTDownload"
        Me.btnTDownload.Size = New System.Drawing.Size(78, 24)
        Me.btnTDownload.TabIndex = 37
        Me.btnTDownload.Text = "Upload"
        Me.btnTDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnTDownload.UseVisualStyleBackColor = True
        '
        'Label72
        '
        Me.Label72.AutoSize = True
        Me.Label72.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label72.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label72.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label72.Location = New System.Drawing.Point(3, 3)
        Me.Label72.Name = "Label72"
        Me.Label72.Size = New System.Drawing.Size(63, 18)
        Me.Label72.TabIndex = 35
        Me.Label72.Text = "Trainings"
        '
        'btnUpdateT
        '
        Me.btnUpdateT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateT.Image = CType(resources.GetObject("btnUpdateT.Image"), System.Drawing.Image)
        Me.btnUpdateT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateT.Location = New System.Drawing.Point(687, 149)
        Me.btnUpdateT.Name = "btnUpdateT"
        Me.btnUpdateT.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateT.TabIndex = 34
        Me.btnUpdateT.Text = "Edit"
        Me.btnUpdateT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateT.UseVisualStyleBackColor = True
        '
        'btnDeleteT
        '
        Me.btnDeleteT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteT.Image = CType(resources.GetObject("btnDeleteT.Image"), System.Drawing.Image)
        Me.btnDeleteT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteT.Location = New System.Drawing.Point(755, 149)
        Me.btnDeleteT.Name = "btnDeleteT"
        Me.btnDeleteT.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteT.TabIndex = 33
        Me.btnDeleteT.Text = "Delete"
        Me.btnDeleteT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteT.UseVisualStyleBackColor = True
        '
        'btnNewT
        '
        Me.btnNewT.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewT.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewT.Image = CType(resources.GetObject("btnNewT.Image"), System.Drawing.Image)
        Me.btnNewT.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewT.Location = New System.Drawing.Point(619, 149)
        Me.btnNewT.Name = "btnNewT"
        Me.btnNewT.Size = New System.Drawing.Size(68, 24)
        Me.btnNewT.TabIndex = 32
        Me.btnNewT.Text = "New"
        Me.btnNewT.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewT.UseVisualStyleBackColor = True
        '
        'PictureBox12
        '
        Me.PictureBox12.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox12.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox12.Location = New System.Drawing.Point(-3, 1)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox12.TabIndex = 31
        Me.PictureBox12.TabStop = False
        '
        'lvlTrainings
        '
        Me.lvlTrainings.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlTrainings.Location = New System.Drawing.Point(2, 26)
        Me.lvlTrainings.Name = "lvlTrainings"
        Me.lvlTrainings.Size = New System.Drawing.Size(913, 117)
        Me.lvlTrainings.TabIndex = 30
        Me.lvlTrainings.UseCompatibleStateImageBehavior = False
        '
        'TabPage10
        '
        Me.TabPage10.BackColor = System.Drawing.Color.White
        Me.TabPage10.Controls.Add(Me.Label73)
        Me.TabPage10.Controls.Add(Me.btnUpdateSkills)
        Me.TabPage10.Controls.Add(Me.btnDeleteSkills)
        Me.TabPage10.Controls.Add(Me.btnNewSkills)
        Me.TabPage10.Controls.Add(Me.PictureBox13)
        Me.TabPage10.Controls.Add(Me.lvlSkills)
        Me.TabPage10.Location = New System.Drawing.Point(4, 22)
        Me.TabPage10.Name = "TabPage10"
        Me.TabPage10.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage10.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage10.TabIndex = 25
        Me.TabPage10.Text = "Skills"
        '
        'Label73
        '
        Me.Label73.AutoSize = True
        Me.Label73.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label73.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label73.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label73.Location = New System.Drawing.Point(3, 3)
        Me.Label73.Name = "Label73"
        Me.Label73.Size = New System.Drawing.Size(40, 18)
        Me.Label73.TabIndex = 35
        Me.Label73.Text = "Skills"
        '
        'btnUpdateSkills
        '
        Me.btnUpdateSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSkills.Image = CType(resources.GetObject("btnUpdateSkills.Image"), System.Drawing.Image)
        Me.btnUpdateSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSkills.Location = New System.Drawing.Point(779, 149)
        Me.btnUpdateSkills.Name = "btnUpdateSkills"
        Me.btnUpdateSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSkills.TabIndex = 34
        Me.btnUpdateSkills.Text = "Edit"
        Me.btnUpdateSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSkills.UseVisualStyleBackColor = True
        '
        'btnDeleteSkills
        '
        Me.btnDeleteSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSkills.Image = CType(resources.GetObject("btnDeleteSkills.Image"), System.Drawing.Image)
        Me.btnDeleteSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSkills.Location = New System.Drawing.Point(847, 149)
        Me.btnDeleteSkills.Name = "btnDeleteSkills"
        Me.btnDeleteSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSkills.TabIndex = 33
        Me.btnDeleteSkills.Text = "Delete"
        Me.btnDeleteSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSkills.UseVisualStyleBackColor = True
        '
        'btnNewSkills
        '
        Me.btnNewSkills.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewSkills.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewSkills.Image = CType(resources.GetObject("btnNewSkills.Image"), System.Drawing.Image)
        Me.btnNewSkills.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewSkills.Location = New System.Drawing.Point(711, 149)
        Me.btnNewSkills.Name = "btnNewSkills"
        Me.btnNewSkills.Size = New System.Drawing.Size(68, 24)
        Me.btnNewSkills.TabIndex = 32
        Me.btnNewSkills.Text = "New"
        Me.btnNewSkills.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewSkills.UseVisualStyleBackColor = True
        '
        'PictureBox13
        '
        Me.PictureBox13.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox13.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox13.Location = New System.Drawing.Point(-3, 1)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox13.TabIndex = 31
        Me.PictureBox13.TabStop = False
        '
        'lvlSkills
        '
        Me.lvlSkills.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSkills.Location = New System.Drawing.Point(2, 26)
        Me.lvlSkills.Name = "lvlSkills"
        Me.lvlSkills.Size = New System.Drawing.Size(913, 117)
        Me.lvlSkills.TabIndex = 30
        Me.lvlSkills.UseCompatibleStateImageBehavior = False
        '
        'TabPage11
        '
        Me.TabPage11.BackColor = System.Drawing.Color.White
        Me.TabPage11.Controls.Add(Me.Label74)
        Me.TabPage11.Controls.Add(Me.btnUpdateAward)
        Me.TabPage11.Controls.Add(Me.btnDeleteAward)
        Me.TabPage11.Controls.Add(Me.btnNewAward)
        Me.TabPage11.Controls.Add(Me.PictureBox14)
        Me.TabPage11.Controls.Add(Me.lvlAwards)
        Me.TabPage11.Location = New System.Drawing.Point(4, 22)
        Me.TabPage11.Name = "TabPage11"
        Me.TabPage11.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage11.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage11.TabIndex = 26
        Me.TabPage11.Text = "Awards"
        '
        'Label74
        '
        Me.Label74.AutoSize = True
        Me.Label74.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label74.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label74.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label74.Location = New System.Drawing.Point(3, 3)
        Me.Label74.Name = "Label74"
        Me.Label74.Size = New System.Drawing.Size(54, 18)
        Me.Label74.TabIndex = 35
        Me.Label74.Text = "Awards"
        '
        'btnUpdateAward
        '
        Me.btnUpdateAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateAward.Image = CType(resources.GetObject("btnUpdateAward.Image"), System.Drawing.Image)
        Me.btnUpdateAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateAward.Location = New System.Drawing.Point(779, 149)
        Me.btnUpdateAward.Name = "btnUpdateAward"
        Me.btnUpdateAward.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateAward.TabIndex = 34
        Me.btnUpdateAward.Text = "Edit"
        Me.btnUpdateAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateAward.UseVisualStyleBackColor = True
        '
        'btnDeleteAward
        '
        Me.btnDeleteAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteAward.Image = CType(resources.GetObject("btnDeleteAward.Image"), System.Drawing.Image)
        Me.btnDeleteAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteAward.Location = New System.Drawing.Point(847, 149)
        Me.btnDeleteAward.Name = "btnDeleteAward"
        Me.btnDeleteAward.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteAward.TabIndex = 33
        Me.btnDeleteAward.Text = "Delete"
        Me.btnDeleteAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteAward.UseVisualStyleBackColor = True
        '
        'btnNewAward
        '
        Me.btnNewAward.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewAward.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewAward.Image = CType(resources.GetObject("btnNewAward.Image"), System.Drawing.Image)
        Me.btnNewAward.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewAward.Location = New System.Drawing.Point(711, 149)
        Me.btnNewAward.Name = "btnNewAward"
        Me.btnNewAward.Size = New System.Drawing.Size(68, 24)
        Me.btnNewAward.TabIndex = 32
        Me.btnNewAward.Text = "New"
        Me.btnNewAward.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewAward.UseVisualStyleBackColor = True
        '
        'PictureBox14
        '
        Me.PictureBox14.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox14.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox14.Location = New System.Drawing.Point(-3, 1)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox14.TabIndex = 31
        Me.PictureBox14.TabStop = False
        '
        'lvlAwards
        '
        Me.lvlAwards.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlAwards.Location = New System.Drawing.Point(2, 26)
        Me.lvlAwards.Name = "lvlAwards"
        Me.lvlAwards.Size = New System.Drawing.Size(913, 117)
        Me.lvlAwards.TabIndex = 30
        Me.lvlAwards.UseCompatibleStateImageBehavior = False
        '
        'TabPage12
        '
        Me.TabPage12.BackColor = System.Drawing.Color.White
        Me.TabPage12.Controls.Add(Me.btnPEDownload)
        Me.TabPage12.Controls.Add(Me.Label75)
        Me.TabPage12.Controls.Add(Me.btnUpdatePE)
        Me.TabPage12.Controls.Add(Me.btnDeletePE)
        Me.TabPage12.Controls.Add(Me.btnNewPE)
        Me.TabPage12.Controls.Add(Me.PictureBox15)
        Me.TabPage12.Controls.Add(Me.lvlPerfEval)
        Me.TabPage12.Location = New System.Drawing.Point(4, 22)
        Me.TabPage12.Name = "TabPage12"
        Me.TabPage12.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage12.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage12.TabIndex = 27
        Me.TabPage12.Text = "Perf. Evaluation"
        '
        'btnPEDownload
        '
        Me.btnPEDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPEDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnPEDownload.Enabled = False
        Me.btnPEDownload.Image = CType(resources.GetObject("btnPEDownload.Image"), System.Drawing.Image)
        Me.btnPEDownload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.btnPEDownload.Location = New System.Drawing.Point(836, 150)
        Me.btnPEDownload.Name = "btnPEDownload"
        Me.btnPEDownload.Size = New System.Drawing.Size(79, 24)
        Me.btnPEDownload.TabIndex = 36
        Me.btnPEDownload.Text = "Upload"
        Me.btnPEDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPEDownload.UseVisualStyleBackColor = True
        '
        'Label75
        '
        Me.Label75.AutoSize = True
        Me.Label75.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label75.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label75.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label75.Location = New System.Drawing.Point(3, 4)
        Me.Label75.Name = "Label75"
        Me.Label75.Size = New System.Drawing.Size(156, 18)
        Me.Label75.TabIndex = 35
        Me.Label75.Text = "Performance Evaluation"
        '
        'btnUpdatePE
        '
        Me.btnUpdatePE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdatePE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdatePE.Image = CType(resources.GetObject("btnUpdatePE.Image"), System.Drawing.Image)
        Me.btnUpdatePE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdatePE.Location = New System.Drawing.Point(688, 150)
        Me.btnUpdatePE.Name = "btnUpdatePE"
        Me.btnUpdatePE.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdatePE.TabIndex = 34
        Me.btnUpdatePE.Text = "Edit"
        Me.btnUpdatePE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdatePE.UseVisualStyleBackColor = True
        '
        'btnDeletePE
        '
        Me.btnDeletePE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeletePE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeletePE.Image = CType(resources.GetObject("btnDeletePE.Image"), System.Drawing.Image)
        Me.btnDeletePE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeletePE.Location = New System.Drawing.Point(756, 150)
        Me.btnDeletePE.Name = "btnDeletePE"
        Me.btnDeletePE.Size = New System.Drawing.Size(68, 24)
        Me.btnDeletePE.TabIndex = 33
        Me.btnDeletePE.Text = "Delete"
        Me.btnDeletePE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeletePE.UseVisualStyleBackColor = True
        '
        'btnNewPE
        '
        Me.btnNewPE.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewPE.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewPE.Image = CType(resources.GetObject("btnNewPE.Image"), System.Drawing.Image)
        Me.btnNewPE.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewPE.Location = New System.Drawing.Point(620, 150)
        Me.btnNewPE.Name = "btnNewPE"
        Me.btnNewPE.Size = New System.Drawing.Size(68, 24)
        Me.btnNewPE.TabIndex = 32
        Me.btnNewPE.Text = "New"
        Me.btnNewPE.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewPE.UseVisualStyleBackColor = True
        '
        'PictureBox15
        '
        Me.PictureBox15.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox15.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox15.Location = New System.Drawing.Point(-3, 2)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox15.TabIndex = 31
        Me.PictureBox15.TabStop = False
        '
        'lvlPerfEval
        '
        Me.lvlPerfEval.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlPerfEval.Location = New System.Drawing.Point(2, 27)
        Me.lvlPerfEval.Name = "lvlPerfEval"
        Me.lvlPerfEval.Size = New System.Drawing.Size(913, 117)
        Me.lvlPerfEval.TabIndex = 30
        Me.lvlPerfEval.UseCompatibleStateImageBehavior = False
        '
        'TabPage13
        '
        Me.TabPage13.BackColor = System.Drawing.Color.White
        Me.TabPage13.Controls.Add(Me.btnDDownload)
        Me.TabPage13.Controls.Add(Me.Label76)
        Me.TabPage13.Controls.Add(Me.btnUpdateDisc)
        Me.TabPage13.Controls.Add(Me.btnDeleteDisc)
        Me.TabPage13.Controls.Add(Me.btnNewDisc)
        Me.TabPage13.Controls.Add(Me.PictureBox16)
        Me.TabPage13.Controls.Add(Me.lvlDiscipline)
        Me.TabPage13.Location = New System.Drawing.Point(4, 22)
        Me.TabPage13.Name = "TabPage13"
        Me.TabPage13.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage13.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage13.TabIndex = 28
        Me.TabPage13.Text = "Discipline"
        '
        'btnDDownload
        '
        Me.btnDDownload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDDownload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDDownload.Enabled = False
        Me.btnDDownload.Image = CType(resources.GetObject("btnDDownload.Image"), System.Drawing.Image)
        Me.btnDDownload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.btnDDownload.Location = New System.Drawing.Point(837, 156)
        Me.btnDDownload.Name = "btnDDownload"
        Me.btnDDownload.Size = New System.Drawing.Size(78, 24)
        Me.btnDDownload.TabIndex = 37
        Me.btnDDownload.Text = "Upload"
        Me.btnDDownload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDDownload.UseVisualStyleBackColor = True
        '
        'Label76
        '
        Me.Label76.AutoSize = True
        Me.Label76.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label76.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label76.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label76.Location = New System.Drawing.Point(3, 4)
        Me.Label76.Name = "Label76"
        Me.Label76.Size = New System.Drawing.Size(118, 18)
        Me.Label76.TabIndex = 35
        Me.Label76.Text = "Disciplinary Cases"
        '
        'btnUpdateDisc
        '
        Me.btnUpdateDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateDisc.Image = CType(resources.GetObject("btnUpdateDisc.Image"), System.Drawing.Image)
        Me.btnUpdateDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateDisc.Location = New System.Drawing.Point(690, 156)
        Me.btnUpdateDisc.Name = "btnUpdateDisc"
        Me.btnUpdateDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateDisc.TabIndex = 34
        Me.btnUpdateDisc.Text = "Edit"
        Me.btnUpdateDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateDisc.UseVisualStyleBackColor = True
        '
        'btnDeleteDisc
        '
        Me.btnDeleteDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteDisc.Image = CType(resources.GetObject("btnDeleteDisc.Image"), System.Drawing.Image)
        Me.btnDeleteDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteDisc.Location = New System.Drawing.Point(758, 156)
        Me.btnDeleteDisc.Name = "btnDeleteDisc"
        Me.btnDeleteDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteDisc.TabIndex = 33
        Me.btnDeleteDisc.Text = "Delete"
        Me.btnDeleteDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteDisc.UseVisualStyleBackColor = True
        '
        'btnNewDisc
        '
        Me.btnNewDisc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewDisc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewDisc.Image = CType(resources.GetObject("btnNewDisc.Image"), System.Drawing.Image)
        Me.btnNewDisc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewDisc.Location = New System.Drawing.Point(622, 156)
        Me.btnNewDisc.Name = "btnNewDisc"
        Me.btnNewDisc.Size = New System.Drawing.Size(68, 24)
        Me.btnNewDisc.TabIndex = 32
        Me.btnNewDisc.Text = "New"
        Me.btnNewDisc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewDisc.UseVisualStyleBackColor = True
        '
        'PictureBox16
        '
        Me.PictureBox16.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox16.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox16.Location = New System.Drawing.Point(-3, 2)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox16.TabIndex = 31
        Me.PictureBox16.TabStop = False
        '
        'lvlDiscipline
        '
        Me.lvlDiscipline.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlDiscipline.Location = New System.Drawing.Point(2, 27)
        Me.lvlDiscipline.Name = "lvlDiscipline"
        Me.lvlDiscipline.Size = New System.Drawing.Size(913, 123)
        Me.lvlDiscipline.TabIndex = 30
        Me.lvlDiscipline.UseCompatibleStateImageBehavior = False
        '
        'TabPage14
        '
        Me.TabPage14.BackColor = System.Drawing.Color.White
        Me.TabPage14.Controls.Add(Me.btnMedUpload)
        Me.TabPage14.Controls.Add(Me.Label77)
        Me.TabPage14.Controls.Add(Me.btnUpdateMed)
        Me.TabPage14.Controls.Add(Me.btnDeleteMed)
        Me.TabPage14.Controls.Add(Me.btnNewMed)
        Me.TabPage14.Controls.Add(Me.PictureBox17)
        Me.TabPage14.Controls.Add(Me.lvlMedical)
        Me.TabPage14.Location = New System.Drawing.Point(4, 22)
        Me.TabPage14.Name = "TabPage14"
        Me.TabPage14.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage14.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage14.TabIndex = 29
        Me.TabPage14.Text = "Medical"
        '
        'btnMedUpload
        '
        Me.btnMedUpload.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMedUpload.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMedUpload.Enabled = False
        Me.btnMedUpload.Image = CType(resources.GetObject("btnMedUpload.Image"), System.Drawing.Image)
        Me.btnMedUpload.ImageAlign = System.Drawing.ContentAlignment.BottomLeft
        Me.btnMedUpload.Location = New System.Drawing.Point(836, 156)
        Me.btnMedUpload.Name = "btnMedUpload"
        Me.btnMedUpload.Size = New System.Drawing.Size(79, 24)
        Me.btnMedUpload.TabIndex = 37
        Me.btnMedUpload.Text = "Upload"
        Me.btnMedUpload.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnMedUpload.UseVisualStyleBackColor = True
        '
        'Label77
        '
        Me.Label77.AutoSize = True
        Me.Label77.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label77.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label77.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label77.Location = New System.Drawing.Point(3, 4)
        Me.Label77.Name = "Label77"
        Me.Label77.Size = New System.Drawing.Size(58, 18)
        Me.Label77.TabIndex = 35
        Me.Label77.Text = "Medical"
        '
        'btnUpdateMed
        '
        Me.btnUpdateMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateMed.Image = CType(resources.GetObject("btnUpdateMed.Image"), System.Drawing.Image)
        Me.btnUpdateMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateMed.Location = New System.Drawing.Point(690, 156)
        Me.btnUpdateMed.Name = "btnUpdateMed"
        Me.btnUpdateMed.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateMed.TabIndex = 34
        Me.btnUpdateMed.Text = "Edit"
        Me.btnUpdateMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateMed.UseVisualStyleBackColor = True
        '
        'btnDeleteMed
        '
        Me.btnDeleteMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteMed.Image = CType(resources.GetObject("btnDeleteMed.Image"), System.Drawing.Image)
        Me.btnDeleteMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteMed.Location = New System.Drawing.Point(758, 156)
        Me.btnDeleteMed.Name = "btnDeleteMed"
        Me.btnDeleteMed.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteMed.TabIndex = 33
        Me.btnDeleteMed.Text = "Delete"
        Me.btnDeleteMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteMed.UseVisualStyleBackColor = True
        '
        'btnNewMed
        '
        Me.btnNewMed.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewMed.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNewMed.Image = CType(resources.GetObject("btnNewMed.Image"), System.Drawing.Image)
        Me.btnNewMed.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewMed.Location = New System.Drawing.Point(622, 156)
        Me.btnNewMed.Name = "btnNewMed"
        Me.btnNewMed.Size = New System.Drawing.Size(68, 24)
        Me.btnNewMed.TabIndex = 32
        Me.btnNewMed.Text = "New"
        Me.btnNewMed.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewMed.UseVisualStyleBackColor = True
        '
        'PictureBox17
        '
        Me.PictureBox17.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox17.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox17.Location = New System.Drawing.Point(-3, 2)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox17.TabIndex = 31
        Me.PictureBox17.TabStop = False
        '
        'lvlMedical
        '
        Me.lvlMedical.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlMedical.Location = New System.Drawing.Point(2, 27)
        Me.lvlMedical.Name = "lvlMedical"
        Me.lvlMedical.Size = New System.Drawing.Size(913, 125)
        Me.lvlMedical.TabIndex = 30
        Me.lvlMedical.UseCompatibleStateImageBehavior = False
        '
        'TabPage16
        '
        Me.TabPage16.BackColor = System.Drawing.Color.White
        Me.TabPage16.Controls.Add(Me.btnPayrollHistory)
        Me.TabPage16.Controls.Add(Me.PictureBox19)
        Me.TabPage16.Controls.Add(Me.lvlPayrollHistory)
        Me.TabPage16.Location = New System.Drawing.Point(4, 22)
        Me.TabPage16.Name = "TabPage16"
        Me.TabPage16.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage16.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage16.TabIndex = 31
        Me.TabPage16.Text = "Payroll History"
        '
        'btnPayrollHistory
        '
        Me.btnPayrollHistory.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None
        Me.btnPayrollHistory.Location = New System.Drawing.Point(3, 1)
        Me.btnPayrollHistory.Name = "btnPayrollHistory"
        Me.btnPayrollHistory.Size = New System.Drawing.Size(133, 23)
        Me.btnPayrollHistory.TabIndex = 40
        Me.btnPayrollHistory.Text = "Payroll History"
        Me.btnPayrollHistory.UseVisualStyleBackColor = True
        '
        'PictureBox19
        '
        Me.PictureBox19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox19.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox19.Location = New System.Drawing.Point(-2, 2)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox19.TabIndex = 38
        Me.PictureBox19.TabStop = False
        '
        'lvlPayrollHistory
        '
        Me.lvlPayrollHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlPayrollHistory.Location = New System.Drawing.Point(3, 32)
        Me.lvlPayrollHistory.Name = "lvlPayrollHistory"
        Me.lvlPayrollHistory.Size = New System.Drawing.Size(913, 152)
        Me.lvlPayrollHistory.TabIndex = 37
        Me.lvlPayrollHistory.UseCompatibleStateImageBehavior = False
        '
        'TabPage18
        '
        Me.TabPage18.BackColor = System.Drawing.Color.White
        Me.TabPage18.Controls.Add(Me.Label80)
        Me.TabPage18.Controls.Add(Me.PictureBox20)
        Me.TabPage18.Controls.Add(Me.lvlLoanHistory)
        Me.TabPage18.Location = New System.Drawing.Point(4, 22)
        Me.TabPage18.Name = "TabPage18"
        Me.TabPage18.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage18.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage18.TabIndex = 32
        Me.TabPage18.Text = "Loan History"
        '
        'Label80
        '
        Me.Label80.AutoSize = True
        Me.Label80.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label80.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label80.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label80.Location = New System.Drawing.Point(8, 4)
        Me.Label80.Name = "Label80"
        Me.Label80.Size = New System.Drawing.Size(84, 18)
        Me.Label80.TabIndex = 42
        Me.Label80.Text = "Loan History"
        '
        'PictureBox20
        '
        Me.PictureBox20.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox20.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox20.Location = New System.Drawing.Point(-3, 2)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox20.TabIndex = 41
        Me.PictureBox20.TabStop = False
        '
        'lvlLoanHistory
        '
        Me.lvlLoanHistory.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlLoanHistory.Location = New System.Drawing.Point(3, 30)
        Me.lvlLoanHistory.Name = "lvlLoanHistory"
        Me.lvlLoanHistory.Size = New System.Drawing.Size(913, 154)
        Me.lvlLoanHistory.TabIndex = 40
        Me.lvlLoanHistory.UseCompatibleStateImageBehavior = False
        '
        'TabPage15
        '
        Me.TabPage15.BackColor = System.Drawing.Color.White
        Me.TabPage15.Controls.Add(Me.cboLeave)
        Me.TabPage15.Controls.Add(Me.Label78)
        Me.TabPage15.Controls.Add(Me.PictureBox18)
        Me.TabPage15.Controls.Add(Me.lvlLeave)
        Me.TabPage15.Location = New System.Drawing.Point(4, 22)
        Me.TabPage15.Name = "TabPage15"
        Me.TabPage15.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage15.Size = New System.Drawing.Size(1009, 190)
        Me.TabPage15.TabIndex = 30
        Me.TabPage15.Text = "Leave Ledger"
        '
        'cboLeave
        '
        Me.cboLeave.FormattingEnabled = True
        Me.cboLeave.Items.AddRange(New Object() {"All"})
        Me.cboLeave.Location = New System.Drawing.Point(3, 28)
        Me.cboLeave.Name = "cboLeave"
        Me.cboLeave.Size = New System.Drawing.Size(200, 23)
        Me.cboLeave.TabIndex = 36
        '
        'Label78
        '
        Me.Label78.AutoSize = True
        Me.Label78.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label78.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold)
        Me.Label78.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label78.Location = New System.Drawing.Point(3, 4)
        Me.Label78.Name = "Label78"
        Me.Label78.Size = New System.Drawing.Size(89, 18)
        Me.Label78.TabIndex = 35
        Me.Label78.Text = "Leave Ledger"
        '
        'PictureBox18
        '
        Me.PictureBox18.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox18.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox18.Location = New System.Drawing.Point(-3, 2)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(1071, 22)
        Me.PictureBox18.TabIndex = 31
        Me.PictureBox18.TabStop = False
        '
        'lvlLeave
        '
        Me.lvlLeave.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlLeave.Location = New System.Drawing.Point(2, 55)
        Me.lvlLeave.Name = "lvlLeave"
        Me.lvlLeave.Size = New System.Drawing.Size(913, 128)
        Me.lvlLeave.TabIndex = 30
        Me.lvlLeave.UseCompatibleStateImageBehavior = False
        '
        'Cbomem_Status
        '
        Me.Cbomem_Status.FormattingEnabled = True
        Me.Cbomem_Status.Location = New System.Drawing.Point(366, 89)
        Me.Cbomem_Status.Name = "Cbomem_Status"
        Me.Cbomem_Status.Size = New System.Drawing.Size(153, 21)
        Me.Cbomem_Status.TabIndex = 2
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.BackColor = System.Drawing.Color.Transparent
        Me.Label107.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(283, 92)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(80, 15)
        Me.Label107.TabIndex = 102
        Me.Label107.Text = "Client Status:"
        '
        'txtMemberID
        '
        Me.txtMemberID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemberID.BackColor = System.Drawing.SystemColors.Window
        Me.txtMemberID.Location = New System.Drawing.Point(894, 90)
        Me.txtMemberID.MaxLength = 30
        Me.txtMemberID.Name = "txtMemberID"
        Me.txtMemberID.ReadOnly = True
        Me.txtMemberID.Size = New System.Drawing.Size(131, 20)
        Me.txtMemberID.TabIndex = 2
        '
        'temp_placebirth
        '
        Me.temp_placebirth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_placebirth.Location = New System.Drawing.Point(115, 114)
        Me.temp_placebirth.MaxLength = 255
        Me.temp_placebirth.Name = "temp_placebirth"
        Me.temp_placebirth.Size = New System.Drawing.Size(661, 20)
        Me.temp_placebirth.TabIndex = 15
        '
        'txtage1
        '
        Me.txtage1.BackColor = System.Drawing.SystemColors.Window
        Me.txtage1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtage1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtage1.Location = New System.Drawing.Point(304, 80)
        Me.txtage1.MaxLength = 3
        Me.txtage1.Name = "txtage1"
        Me.txtage1.ReadOnly = True
        Me.txtage1.Size = New System.Drawing.Size(25, 14)
        Me.txtage1.TabIndex = 45
        Me.txtage1.Text = "0"
        '
        'lblage
        '
        Me.lblage.AutoSize = True
        Me.lblage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblage.Location = New System.Drawing.Point(278, 80)
        Me.lblage.Name = "lblage"
        Me.lblage.Size = New System.Drawing.Size(30, 14)
        Me.lblage.TabIndex = 44
        Me.lblage.Text = "Age "
        '
        'EMP_DATEbirth
        '
        Me.EMP_DATEbirth.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EMP_DATEbirth.Location = New System.Drawing.Point(115, 76)
        Me.EMP_DATEbirth.Name = "EMP_DATEbirth"
        Me.EMP_DATEbirth.Size = New System.Drawing.Size(150, 20)
        Me.EMP_DATEbirth.TabIndex = 10
        Me.EMP_DATEbirth.Value = New Date(2006, 3, 17, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(3, 81)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(81, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Date of Birth:"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(92, 145)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(156, 20)
        Me.TextBox15.TabIndex = 11
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(92, 119)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(156, 20)
        Me.TextBox16.TabIndex = 10
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(92, 93)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(156, 20)
        Me.TextBox17.TabIndex = 9
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(92, 67)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(156, 20)
        Me.TextBox18.TabIndex = 8
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(92, 41)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(156, 20)
        Me.TextBox19.TabIndex = 7
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 152)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(54, 13)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Address 3"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(17, 126)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(54, 13)
        Me.Label26.TabIndex = 5
        Me.Label26.Text = "Address 2"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(17, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(54, 13)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Address 1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Religion"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(70, 13)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Place of Birth"
        '
        'MaskedTextBox5
        '
        Me.MaskedTextBox5.Location = New System.Drawing.Point(92, 15)
        Me.MaskedTextBox5.Mask = "LLLL,00,0000"
        Me.MaskedTextBox5.Name = "MaskedTextBox5"
        Me.MaskedTextBox5.Size = New System.Drawing.Size(79, 20)
        Me.MaskedTextBox5.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 22)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Date of Birth"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.BackColor = System.Drawing.Color.Transparent
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(803, 67)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(79, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Client ID No.:"
        '
        'txtEmployeeNo
        '
        Me.txtEmployeeNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmployeeNo.BackColor = System.Drawing.SystemColors.Window
        Me.txtEmployeeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployeeNo.Enabled = False
        Me.txtEmployeeNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeNo.Location = New System.Drawing.Point(894, 65)
        Me.txtEmployeeNo.MaxLength = 50
        Me.txtEmployeeNo.Name = "txtEmployeeNo"
        Me.txtEmployeeNo.Size = New System.Drawing.Size(130, 21)
        Me.txtEmployeeNo.TabIndex = 1
        '
        'temp_fname
        '
        Me.temp_fname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_fname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_fname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_fname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_fname.Enabled = False
        Me.temp_fname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_fname.Location = New System.Drawing.Point(68, 2)
        Me.temp_fname.MaxLength = 50
        Me.temp_fname.Name = "temp_fname"
        Me.temp_fname.Size = New System.Drawing.Size(285, 25)
        Me.temp_fname.TabIndex = 0
        '
        'temp_lname
        '
        Me.temp_lname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_lname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_lname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_lname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_lname.Enabled = False
        Me.temp_lname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_lname.Location = New System.Drawing.Point(67, 2)
        Me.temp_lname.MaxLength = 50
        Me.temp_lname.Name = "temp_lname"
        Me.temp_lname.Size = New System.Drawing.Size(265, 25)
        Me.temp_lname.TabIndex = 4
        '
        'temp_midname
        '
        Me.temp_midname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_midname.BackColor = System.Drawing.SystemColors.Window
        Me.temp_midname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_midname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_midname.Enabled = False
        Me.temp_midname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_midname.Location = New System.Drawing.Point(78, 2)
        Me.temp_midname.MaxLength = 50
        Me.temp_midname.Name = "temp_midname"
        Me.temp_midname.Size = New System.Drawing.Size(222, 25)
        Me.temp_midname.TabIndex = 6
        '
        'txtprovincialadd
        '
        Me.txtprovincialadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtprovincialadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtprovincialadd.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprovincialadd.Location = New System.Drawing.Point(115, 161)
        Me.txtprovincialadd.MaxLength = 1000
        Me.txtprovincialadd.Name = "txtprovincialadd"
        Me.txtprovincialadd.Size = New System.Drawing.Size(622, 21)
        Me.txtprovincialadd.TabIndex = 17
        '
        'cboemp_gender
        '
        Me.cboemp_gender.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_gender.BackColor = System.Drawing.Color.White
        Me.cboemp_gender.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_gender.FormattingEnabled = True
        Me.cboemp_gender.Items.AddRange(New Object() {"F", "M"})
        Me.cboemp_gender.Location = New System.Drawing.Point(115, 22)
        Me.cboemp_gender.Name = "cboemp_gender"
        Me.cboemp_gender.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_gender.TabIndex = 3
        '
        'cboemp_civil
        '
        Me.cboemp_civil.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_civil.BackColor = System.Drawing.Color.White
        Me.cboemp_civil.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_civil.FormattingEnabled = True
        Me.cboemp_civil.Location = New System.Drawing.Point(115, 49)
        Me.cboemp_civil.Name = "cboemp_civil"
        Me.cboemp_civil.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_civil.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "First Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 14)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Last Name"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 7)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Middle Name"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 15)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Gender:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Civil Status:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(110, 15)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Provincial Address:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.BackColor = System.Drawing.Color.Transparent
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(19, 67)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(80, 15)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Organization:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(3, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(75, 15)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Contact No.:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(3, 54)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(40, 15)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Email:"
        '
        'txtresidencephone
        '
        Me.txtresidencephone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtresidencephone.BackColor = System.Drawing.Color.White
        Me.txtresidencephone.Enabled = False
        Me.txtresidencephone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtresidencephone.Location = New System.Drawing.Point(111, 27)
        Me.txtresidencephone.Name = "txtresidencephone"
        Me.txtresidencephone.Size = New System.Drawing.Size(100, 21)
        Me.txtresidencephone.TabIndex = 12
        '
        'txtEmailAddress
        '
        Me.txtEmailAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmailAddress.BackColor = System.Drawing.Color.White
        Me.txtEmailAddress.Enabled = False
        Me.txtEmailAddress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailAddress.Location = New System.Drawing.Point(111, 50)
        Me.txtEmailAddress.MaxLength = 50
        Me.txtEmailAddress.Name = "txtEmailAddress"
        Me.txtEmailAddress.Size = New System.Drawing.Size(201, 21)
        Me.txtEmailAddress.TabIndex = 14
        '
        'txtorgchart
        '
        Me.txtorgchart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtorgchart.BackColor = System.Drawing.SystemColors.Window
        Me.txtorgchart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtorgchart.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtorgchart.Enabled = False
        Me.txtorgchart.Location = New System.Drawing.Point(112, 65)
        Me.txtorgchart.Name = "txtorgchart"
        Me.txtorgchart.Size = New System.Drawing.Size(655, 20)
        Me.txtorgchart.TabIndex = 0
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(3, 142)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(91, 15)
        Me.Label85.TabIndex = 74
        Me.Label85.Text = "Home Address:"
        '
        'txtResidenceadd
        '
        Me.txtResidenceadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResidenceadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResidenceadd.Location = New System.Drawing.Point(115, 138)
        Me.txtResidenceadd.Name = "txtResidenceadd"
        Me.txtResidenceadd.Size = New System.Drawing.Size(622, 20)
        Me.txtResidenceadd.TabIndex = 16
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.BackColor = System.Drawing.Color.Transparent
        Me.Label86.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.Location = New System.Drawing.Point(19, 92)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(75, 15)
        Me.Label86.TabIndex = 76
        Me.Label86.Text = "Rank / Item:"
        '
        'txtDepartment2
        '
        Me.txtDepartment2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartment2.Location = New System.Drawing.Point(758, 90)
        Me.txtDepartment2.Name = "txtDepartment2"
        Me.txtDepartment2.Size = New System.Drawing.Size(162, 20)
        Me.txtDepartment2.TabIndex = 13
        Me.txtDepartment2.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.GroupBox1.Controls.Add(Me.SplitContainer2)
        Me.GroupBox1.Location = New System.Drawing.Point(20, 110)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(1012, 40)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(1, 7)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.temp_lname)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label6)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer2.Size = New System.Drawing.Size(1010, 31)
        Me.SplitContainer2.SplitterDistance = 337
        Me.SplitContainer2.TabIndex = 46
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(669, 31)
        Me.Panel1.TabIndex = 0
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.temp_fname)
        Me.SplitContainer3.Panel1.Controls.Add(Me.Label5)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.temp_midname)
        Me.SplitContainer3.Panel2.Controls.Add(Me.Label7)
        Me.SplitContainer3.Size = New System.Drawing.Size(669, 31)
        Me.SplitContainer3.SplitterDistance = 359
        Me.SplitContainer3.TabIndex = 0
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.Black
        Me.Label66.Location = New System.Drawing.Point(3, 0)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(140, 14)
        Me.Label66.TabIndex = 11
        Me.Label66.Text = "PERSONAL INFORMATION"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(3, 118)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(69, 15)
        Me.Label67.TabIndex = 102
        Me.Label67.Text = "Birth Place:"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.BackColor = System.Drawing.Color.Transparent
        Me.Panel2.Controls.Add(Me.btnSearchProvince)
        Me.Panel2.Controls.Add(Me.btnSearchHome)
        Me.Panel2.Controls.Add(Me.Label33)
        Me.Panel2.Controls.Add(Me.Label67)
        Me.Panel2.Controls.Add(Me.temp_placebirth)
        Me.Panel2.Controls.Add(Me.txtprovincialadd)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label85)
        Me.Panel2.Controls.Add(Me.txtResidenceadd)
        Me.Panel2.Location = New System.Drawing.Point(14, 181)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(783, 186)
        Me.Panel2.TabIndex = 14
        '
        'btnSearchProvince
        '
        Me.btnSearchProvince.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchProvince.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSearchProvince.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchProvince.Location = New System.Drawing.Point(741, 161)
        Me.btnSearchProvince.Name = "btnSearchProvince"
        Me.btnSearchProvince.Size = New System.Drawing.Size(35, 19)
        Me.btnSearchProvince.TabIndex = 109
        Me.btnSearchProvince.UseVisualStyleBackColor = True
        '
        'btnSearchHome
        '
        Me.btnSearchHome.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSearchHome.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.viewmag_
        Me.btnSearchHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnSearchHome.Location = New System.Drawing.Point(741, 134)
        Me.btnSearchHome.Name = "btnSearchHome"
        Me.btnSearchHome.Size = New System.Drawing.Size(35, 23)
        Me.btnSearchHome.TabIndex = 108
        Me.btnSearchHome.UseVisualStyleBackColor = True
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label33.Location = New System.Drawing.Point(467, 86)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(63, 15)
        Me.Label33.TabIndex = 107
        Me.Label33.Text = "Signature:"
        '
        'txtpk_Employee
        '
        Me.txtpk_Employee.Location = New System.Drawing.Point(111, 3)
        Me.txtpk_Employee.Name = "txtpk_Employee"
        Me.txtpk_Employee.Size = New System.Drawing.Size(139, 20)
        Me.txtpk_Employee.TabIndex = 11
        Me.txtpk_Employee.Visible = False
        '
        'Label112
        '
        Me.Label112.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label112.AutoSize = True
        Me.Label112.BackColor = System.Drawing.Color.Transparent
        Me.Label112.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label112.Location = New System.Drawing.Point(802, 91)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(76, 15)
        Me.Label112.TabIndex = 107
        Me.Label112.Text = "Member ID :"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(14, 181)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.Transparent
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label53)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label66)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtReferred)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtage1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblage)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EMP_DATEbirth)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label19)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_gender)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_civil)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtpk_Employee)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtresidencephone)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtEmailAddress)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label14)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label17)
        Me.SplitContainer1.Size = New System.Drawing.Size(765, 111)
        Me.SplitContainer1.SplitterDistance = 391
        Me.SplitContainer1.TabIndex = 103
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.BackColor = System.Drawing.Color.Transparent
        Me.Label53.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label53.Location = New System.Drawing.Point(270, 94)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(72, 14)
        Me.Label53.TabIndex = 122
        Me.Label53.Text = "Referred By :"
        Me.Label53.Visible = False
        '
        'txtReferred
        '
        Me.txtReferred.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtReferred.BackColor = System.Drawing.SystemColors.Window
        Me.txtReferred.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtReferred.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtReferred.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtReferred.Location = New System.Drawing.Point(348, 90)
        Me.txtReferred.MaxLength = 50
        Me.txtReferred.Name = "txtReferred"
        Me.txtReferred.Size = New System.Drawing.Size(413, 21)
        Me.txtReferred.TabIndex = 121
        Me.txtReferred.Visible = False
        '
        'OpenFileDialog2
        '
        Me.OpenFileDialog2.FileName = "OpenFileDialog2"
        '
        'Panel3
        '
        Me.Panel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel3.BackColor = System.Drawing.Color.Transparent
        Me.Panel3.Controls.Add(Me.GroupBox2)
        Me.Panel3.Location = New System.Drawing.Point(802, 154)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(227, 213)
        Me.Panel3.TabIndex = 108
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnMaxSignature)
        Me.GroupBox2.Controls.Add(Me.btnMaxPhoto)
        Me.GroupBox2.Controls.Add(Me.Label32)
        Me.GroupBox2.Controls.Add(Me.btnBrowseSignature)
        Me.GroupBox2.Controls.Add(Me.picEmpSignature)
        Me.GroupBox2.Controls.Add(Me.Label16)
        Me.GroupBox2.Controls.Add(Me.btnBrowsePicture)
        Me.GroupBox2.Controls.Add(Me.picEmpPhoto)
        Me.GroupBox2.Location = New System.Drawing.Point(2, 2)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(224, 207)
        Me.GroupBox2.TabIndex = 127
        Me.GroupBox2.TabStop = False
        '
        'btnMaxSignature
        '
        Me.btnMaxSignature.Location = New System.Drawing.Point(189, 118)
        Me.btnMaxSignature.Name = "btnMaxSignature"
        Me.btnMaxSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxSignature.TabIndex = 128
        Me.btnMaxSignature.Text = "[*]"
        Me.btnMaxSignature.UseVisualStyleBackColor = True
        '
        'btnMaxPhoto
        '
        Me.btnMaxPhoto.Location = New System.Drawing.Point(191, 11)
        Me.btnMaxPhoto.Name = "btnMaxPhoto"
        Me.btnMaxPhoto.Size = New System.Drawing.Size(29, 23)
        Me.btnMaxPhoto.TabIndex = 127
        Me.btnMaxPhoto.Text = "[* ]"
        Me.btnMaxPhoto.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(6, 9)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(44, 15)
        Me.Label32.TabIndex = 126
        Me.Label32.Text = "Photo:"
        '
        'btnBrowseSignature
        '
        Me.btnBrowseSignature.Location = New System.Drawing.Point(189, 183)
        Me.btnBrowseSignature.Name = "btnBrowseSignature"
        Me.btnBrowseSignature.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowseSignature.TabIndex = 125
        Me.btnBrowseSignature.Text = "..."
        Me.btnBrowseSignature.UseVisualStyleBackColor = True
        Me.btnBrowseSignature.Visible = False
        '
        'picEmpSignature
        '
        Me.picEmpSignature.BackColor = System.Drawing.Color.DarkGray
        Me.picEmpSignature.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpSignature.Image = CType(resources.GetObject("picEmpSignature.Image"), System.Drawing.Image)
        Me.picEmpSignature.Location = New System.Drawing.Point(65, 115)
        Me.picEmpSignature.Name = "picEmpSignature"
        Me.picEmpSignature.Size = New System.Drawing.Size(120, 91)
        Me.picEmpSignature.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpSignature.TabIndex = 124
        Me.picEmpSignature.TabStop = False
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(3, 109)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(63, 15)
        Me.Label16.TabIndex = 121
        Me.Label16.Text = "Signature:"
        '
        'btnBrowsePicture
        '
        Me.btnBrowsePicture.Location = New System.Drawing.Point(189, 89)
        Me.btnBrowsePicture.Name = "btnBrowsePicture"
        Me.btnBrowsePicture.Size = New System.Drawing.Size(29, 23)
        Me.btnBrowsePicture.TabIndex = 123
        Me.btnBrowsePicture.Text = "..."
        Me.btnBrowsePicture.UseVisualStyleBackColor = True
        Me.btnBrowsePicture.Visible = False
        '
        'picEmpPhoto
        '
        Me.picEmpPhoto.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picEmpPhoto.Image = Global.WindowsApplication2.My.Resources.Resources.photo
        Me.picEmpPhoto.Location = New System.Drawing.Point(65, 9)
        Me.picEmpPhoto.Name = "picEmpPhoto"
        Me.picEmpPhoto.Size = New System.Drawing.Size(120, 100)
        Me.picEmpPhoto.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picEmpPhoto.TabIndex = 122
        Me.picEmpPhoto.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(772, 65)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 20)
        Me.PictureBox2.TabIndex = 45
        Me.PictureBox2.TabStop = False
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.BackColor = System.Drawing.Color.Transparent
        Me.Label45.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label45.Location = New System.Drawing.Point(19, 40)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(43, 14)
        Me.Label45.TabIndex = 109
        Me.Label45.Text = "Group :"
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.BackColor = System.Drawing.Color.Transparent
        Me.Label46.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label46.Location = New System.Drawing.Point(542, 40)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(57, 14)
        Me.Label46.TabIndex = 110
        Me.Label46.Text = "Category :"
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.BackColor = System.Drawing.Color.Transparent
        Me.Label47.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label47.Location = New System.Drawing.Point(784, 40)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(37, 14)
        Me.Label47.TabIndex = 111
        Me.Label47.Text = "Type :"
        '
        'cbofGroup
        '
        Me.cbofGroup.Enabled = False
        Me.cbofGroup.FormattingEnabled = True
        Me.cbofGroup.Location = New System.Drawing.Point(112, 37)
        Me.cbofGroup.Name = "cbofGroup"
        Me.cbofGroup.Size = New System.Drawing.Size(167, 21)
        Me.cbofGroup.TabIndex = 112
        '
        'cbofType
        '
        Me.cbofType.Enabled = False
        Me.cbofType.FormattingEnabled = True
        Me.cbofType.Location = New System.Drawing.Point(828, 37)
        Me.cbofType.Name = "cbofType"
        Me.cbofType.Size = New System.Drawing.Size(167, 21)
        Me.cbofType.TabIndex = 113
        '
        'cbofCategory
        '
        Me.cbofCategory.Enabled = False
        Me.cbofCategory.FormattingEnabled = True
        Me.cbofCategory.Location = New System.Drawing.Point(612, 37)
        Me.cbofCategory.Name = "cbofCategory"
        Me.cbofCategory.Size = New System.Drawing.Size(167, 21)
        Me.cbofCategory.TabIndex = 114
        '
        'cboRank
        '
        Me.cboRank.Enabled = False
        Me.cboRank.FormattingEnabled = True
        Me.cboRank.Location = New System.Drawing.Point(112, 88)
        Me.cboRank.Name = "cboRank"
        Me.cboRank.Size = New System.Drawing.Size(165, 21)
        Me.cboRank.TabIndex = 116
        '
        'txtCompanyChart
        '
        Me.txtCompanyChart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCompanyChart.Enabled = False
        Me.txtCompanyChart.Location = New System.Drawing.Point(112, 65)
        Me.txtCompanyChart.Name = "txtCompanyChart"
        Me.txtCompanyChart.Size = New System.Drawing.Size(655, 20)
        Me.txtCompanyChart.TabIndex = 117
        Me.txtCompanyChart.Visible = False
        '
        'cboSubgroup
        '
        Me.cboSubgroup.Enabled = False
        Me.cboSubgroup.FormattingEnabled = True
        Me.cboSubgroup.Location = New System.Drawing.Point(366, 37)
        Me.cboSubgroup.Name = "cboSubgroup"
        Me.cboSubgroup.Size = New System.Drawing.Size(167, 21)
        Me.cboSubgroup.TabIndex = 119
        '
        'Label60
        '
        Me.Label60.AutoSize = True
        Me.Label60.BackColor = System.Drawing.Color.Transparent
        Me.Label60.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label60.Location = New System.Drawing.Point(283, 40)
        Me.Label60.Name = "Label60"
        Me.Label60.Size = New System.Drawing.Size(62, 14)
        Me.Label60.TabIndex = 118
        Me.Label60.Text = "Sub-Group:"
        '
        'txtCompanyName
        '
        Me.txtCompanyName.BackColor = System.Drawing.SystemColors.Window
        Me.txtCompanyName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtCompanyName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCompanyName.Enabled = False
        Me.txtCompanyName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCompanyName.Location = New System.Drawing.Point(113, 154)
        Me.txtCompanyName.Name = "txtCompanyName"
        Me.txtCompanyName.Size = New System.Drawing.Size(655, 20)
        Me.txtCompanyName.TabIndex = 7
        '
        'Label79
        '
        Me.Label79.AutoSize = True
        Me.Label79.BackColor = System.Drawing.Color.Transparent
        Me.Label79.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label79.Location = New System.Drawing.Point(13, 156)
        Me.Label79.Name = "Label79"
        Me.Label79.Size = New System.Drawing.Size(98, 15)
        Me.Label79.TabIndex = 120
        Me.Label79.Text = "Company Name:"
        '
        'btnemp_close
        '
        Me.btnemp_close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_close.BackColor = System.Drawing.Color.White
        Me.btnemp_close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnemp_close.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnemp_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_close.Location = New System.Drawing.Point(958, 3)
        Me.btnemp_close.Name = "btnemp_close"
        Me.btnemp_close.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_close.TabIndex = 7
        Me.btnemp_close.Text = "Close"
        Me.btnemp_close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_close.UseVisualStyleBackColor = False
        '
        'PanePanel2
        '
        Me.PanePanel2.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.btnExport)
        Me.PanePanel2.Controls.Add(Me.btnemp_close)
        Me.PanePanel2.Controls.Add(Me.btnemp_next)
        Me.PanePanel2.Controls.Add(Me.btnemp_delete)
        Me.PanePanel2.Controls.Add(Me.BTNSEARCH)
        Me.PanePanel2.Controls.Add(Me.btnemp_edit)
        Me.PanePanel2.Controls.Add(Me.btnemp_previous)
        Me.PanePanel2.Controls.Add(Me.btnemp_add)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel2.Location = New System.Drawing.Point(0, 597)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(1028, 35)
        Me.PanePanel2.TabIndex = 83
        '
        'btnExport
        '
        Me.btnExport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnExport.BackColor = System.Drawing.Color.White
        Me.btnExport.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnExport.Location = New System.Drawing.Point(218, 4)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(66, 28)
        Me.btnExport.TabIndex = 8
        Me.btnExport.Text = "Export"
        Me.btnExport.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnExport.UseVisualStyleBackColor = False
        '
        'btnemp_next
        '
        Me.btnemp_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_next.Location = New System.Drawing.Point(147, 3)
        Me.btnemp_next.Name = "btnemp_next"
        Me.btnemp_next.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_next.TabIndex = 3
        Me.btnemp_next.Text = ">>"
        Me.btnemp_next.UseVisualStyleBackColor = True
        '
        'btnemp_delete
        '
        Me.btnemp_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_delete.BackColor = System.Drawing.Color.White
        Me.btnemp_delete.Image = CType(resources.GetObject("btnemp_delete.Image"), System.Drawing.Image)
        Me.btnemp_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_delete.Location = New System.Drawing.Point(891, 3)
        Me.btnemp_delete.Name = "btnemp_delete"
        Me.btnemp_delete.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_delete.TabIndex = 6
        Me.btnemp_delete.Text = "Delete"
        Me.btnemp_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_delete.UseVisualStyleBackColor = False
        '
        'BTNSEARCH
        '
        Me.BTNSEARCH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTNSEARCH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTNSEARCH.Image = CType(resources.GetObject("BTNSEARCH.Image"), System.Drawing.Image)
        Me.BTNSEARCH.Location = New System.Drawing.Point(80, 3)
        Me.BTNSEARCH.Name = "BTNSEARCH"
        Me.BTNSEARCH.Size = New System.Drawing.Size(66, 28)
        Me.BTNSEARCH.TabIndex = 2
        Me.BTNSEARCH.UseVisualStyleBackColor = True
        '
        'btnemp_edit
        '
        Me.btnemp_edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_edit.BackColor = System.Drawing.Color.White
        Me.btnemp_edit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_edit.Image = CType(resources.GetObject("btnemp_edit.Image"), System.Drawing.Image)
        Me.btnemp_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_edit.Location = New System.Drawing.Point(824, 3)
        Me.btnemp_edit.Name = "btnemp_edit"
        Me.btnemp_edit.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_edit.TabIndex = 5
        Me.btnemp_edit.Text = "Edit"
        Me.btnemp_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_edit.UseVisualStyleBackColor = False
        '
        'btnemp_previous
        '
        Me.btnemp_previous.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_previous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_previous.Location = New System.Drawing.Point(13, 3)
        Me.btnemp_previous.Name = "btnemp_previous"
        Me.btnemp_previous.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_previous.TabIndex = 1
        Me.btnemp_previous.Text = "<<"
        Me.btnemp_previous.UseVisualStyleBackColor = True
        '
        'btnemp_add
        '
        Me.btnemp_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_add.BackColor = System.Drawing.Color.White
        Me.btnemp_add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_add.Image = CType(resources.GetObject("btnemp_add.Image"), System.Drawing.Image)
        Me.btnemp_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_add.Location = New System.Drawing.Point(757, 3)
        Me.btnemp_add.Name = "btnemp_add"
        Me.btnemp_add.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_add.TabIndex = 4
        Me.btnemp_add.Text = "New"
        Me.btnemp_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_add.UseVisualStyleBackColor = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblTerminated)
        Me.PanePanel1.Controls.Add(Me.lblemployee_name)
        Me.PanePanel1.Controls.Add(Me.Label44)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(1028, 25)
        Me.PanePanel1.TabIndex = 82
        '
        'lblTerminated
        '
        Me.lblTerminated.AutoSize = True
        Me.lblTerminated.BackColor = System.Drawing.Color.Transparent
        Me.lblTerminated.Font = New System.Drawing.Font("Consolas", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTerminated.ForeColor = System.Drawing.Color.Red
        Me.lblTerminated.Location = New System.Drawing.Point(890, 0)
        Me.lblTerminated.Name = "lblTerminated"
        Me.lblTerminated.Size = New System.Drawing.Size(0, 22)
        Me.lblTerminated.TabIndex = 15
        '
        'lblemployee_name
        '
        Me.lblemployee_name.AutoSize = True
        Me.lblemployee_name.BackColor = System.Drawing.Color.Transparent
        Me.lblemployee_name.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblemployee_name.ForeColor = System.Drawing.Color.Black
        Me.lblemployee_name.Location = New System.Drawing.Point(3, 3)
        Me.lblemployee_name.Name = "lblemployee_name"
        Me.lblemployee_name.Size = New System.Drawing.Size(29, 19)
        Me.lblemployee_name.TabIndex = 13
        Me.lblemployee_name.Text = "....."
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label44.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.Red
        Me.Label44.Location = New System.Drawing.Point(643, -3)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(0, 22)
        Me.Label44.TabIndex = 14
        Me.Label44.Visible = False
        '
        'frmMember_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CancelButton = Me.btnemp_close
        Me.ClientSize = New System.Drawing.Size(1028, 632)
        Me.Controls.Add(Me.txtCompanyName)
        Me.Controls.Add(Me.Label79)
        Me.Controls.Add(Me.cboSubgroup)
        Me.Controls.Add(Me.Label60)
        Me.Controls.Add(Me.txtCompanyChart)
        Me.Controls.Add(Me.cboRank)
        Me.Controls.Add(Me.cbofCategory)
        Me.Controls.Add(Me.cbofType)
        Me.Controls.Add(Me.cbofGroup)
        Me.Controls.Add(Me.Label47)
        Me.Controls.Add(Me.Label46)
        Me.Controls.Add(Me.Label45)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label112)
        Me.Controls.Add(Me.txtMemberID)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Cbomem_Status)
        Me.Controls.Add(Me.Label107)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.txtDepartment2)
        Me.Controls.Add(Me.Label86)
        Me.Controls.Add(Me.txtEmployeeNo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tabMember)
        Me.Controls.Add(Me.txtorgchart)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.DoubleBuffered = True
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(781, 581)
        Me.Name = "frmMember_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Existing Client File"
        Me.tabMember.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        Me.grpPayrollStatus.ResumeLayout(False)
        Me.grpPayrollStatus.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage20.ResumeLayout(False)
        Me.TabPage20.PerformLayout()
        CType(Me.dgvPersonsReferred, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage21.ResumeLayout(False)
        Me.TabPage21.PerformLayout()
        CType(Me.PictureBox23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.others.ResumeLayout(False)
        Me.others.PerformLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        CType(Me.dgvemail, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvphone, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvcivil, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvaddress, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvlastname, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage19.ResumeLayout(False)
        Me.TabPage19.PerformLayout()
        CType(Me.PictureBox21, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvEmpHistory, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage8.ResumeLayout(False)
        Me.TabPage8.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage9.ResumeLayout(False)
        Me.TabPage9.PerformLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage10.ResumeLayout(False)
        Me.TabPage10.PerformLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage11.ResumeLayout(False)
        Me.TabPage11.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage12.ResumeLayout(False)
        Me.TabPage12.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage13.ResumeLayout(False)
        Me.TabPage13.PerformLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage14.ResumeLayout(False)
        Me.TabPage14.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage16.ResumeLayout(False)
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage18.ResumeLayout(False)
        Me.TabPage18.PerformLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage15.ResumeLayout(False)
        Me.TabPage15.PerformLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.Panel2.PerformLayout()
        Me.SplitContainer3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.picEmpSignature, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picEmpPhoto, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMember As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents temp_placebirth As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents temp_add3 As System.Windows.Forms.TextBox
    Friend WithEvents temp_add2 As System.Windows.Forms.TextBox
    Friend WithEvents txtofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtofficeadd As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox5 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents temp_marks As System.Windows.Forms.TextBox
    Friend WithEvents temp_height As System.Windows.Forms.TextBox
    Friend WithEvents temp_citizen As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents btndelete_dep As System.Windows.Forms.Button
    Friend WithEvents btnadd_dep As System.Windows.Forms.Button
    Friend WithEvents btnemp_previous As System.Windows.Forms.Button
    Friend WithEvents btnemp_next As System.Windows.Forms.Button
    Friend WithEvents btnemp_add As System.Windows.Forms.Button
    Friend WithEvents btnemp_edit As System.Windows.Forms.Button
    Friend WithEvents btnemp_delete As System.Windows.Forms.Button
    Friend WithEvents txtbasicpay As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cbopayroll As System.Windows.Forms.ComboBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents cborate As System.Windows.Forms.ComboBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents EMP_DATEbirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents Address2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnemp_editdepdts As System.Windows.Forms.Button
    Friend WithEvents emp_pic As System.Windows.Forms.PictureBox
    Friend WithEvents BTNSEARCH As System.Windows.Forms.Button
    Friend WithEvents temp_designation As System.Windows.Forms.TextBox
    Friend WithEvents txtEmailAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtresidencephone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents emp_Datehired As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboemp_civil As System.Windows.Forms.ComboBox
    Friend WithEvents cboemp_gender As System.Windows.Forms.ComboBox
    Friend WithEvents txtprovincialadd As System.Windows.Forms.TextBox
    Friend WithEvents temp_midname As System.Windows.Forms.TextBox
    Friend WithEvents temp_lname As System.Windows.Forms.TextBox
    Friend WithEvents temp_fname As System.Windows.Forms.TextBox
    Friend WithEvents txtEmployeeNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboEmp_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblemployee_name As System.Windows.Forms.Label
    Friend WithEvents txtlocalofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtweight As System.Windows.Forms.TextBox
    Friend WithEvents txtremarks As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents cbosalarygrade As System.Windows.Forms.ComboBox
    Friend WithEvents btnrestricted As System.Windows.Forms.Button
    Friend WithEvents TXTKEYEMPLOYEEID As System.Windows.Forms.TextBox
    Friend WithEvents TXTRECID As System.Windows.Forms.TextBox
    Friend WithEvents cbotitledesignation As System.Windows.Forms.ComboBox
    Friend WithEvents Dateresigned As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtorgchart As System.Windows.Forms.TextBox
    Friend WithEvents txtfullname As System.Windows.Forms.TextBox
    Friend WithEvents lblage As System.Windows.Forms.Label
    Friend WithEvents txtage1 As System.Windows.Forms.TextBox
    Friend WithEvents cbodept As System.Windows.Forms.TextBox
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents txtResidenceadd As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartment2 As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtType_employee As System.Windows.Forms.TextBox
    Friend WithEvents txtfxkeypositionlevel As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents txtkeycompany As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydivision As System.Windows.Forms.TextBox
    Friend WithEvents txtdescsection As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdivision As System.Windows.Forms.TextBox
    Friend WithEvents txtkeysection As System.Windows.Forms.TextBox
    Friend WithEvents txtparent_ID As System.Windows.Forms.TextBox
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents lvlNearestRelatives As System.Windows.Forms.ListView
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BtnEditContact As System.Windows.Forms.Button
    Friend WithEvents BtnDeleteContact As System.Windows.Forms.Button
    Friend WithEvents btnaddContact As System.Windows.Forms.Button
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents txtMemberID As System.Windows.Forms.TextBox
    Friend WithEvents emp_company As System.Windows.Forms.TextBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Cbomem_Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents emp_Ytenures As System.Windows.Forms.TextBox
    Friend WithEvents cboPositionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents cbotaxcode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlBankInfo As System.Windows.Forms.ListView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lvlSourceIncome As System.Windows.Forms.ListView
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents txtpk_Employee As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateBA As System.Windows.Forms.Button
    Friend WithEvents btnDeleteBA As System.Windows.Forms.Button
    Friend WithEvents btnNewBA As System.Windows.Forms.Button
    Friend WithEvents btnUpdateSInc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSInc As System.Windows.Forms.Button
    Friend WithEvents btnsaveSInc As System.Windows.Forms.Button
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtitemno As System.Windows.Forms.TextBox
    Friend WithEvents txtmonthreqular As System.Windows.Forms.TextBox
    Friend WithEvents txtsalarygrade As System.Windows.Forms.TextBox
    Friend WithEvents txtdateregular As System.Windows.Forms.TextBox
    Friend WithEvents Dateregular As System.Windows.Forms.DateTimePicker
    Friend WithEvents emp_extphone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnAddMobileNo As System.Windows.Forms.Button
    Friend WithEvents others As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkIsEloader As System.Windows.Forms.CheckBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkShowPINChar As System.Windows.Forms.CheckBox
    Friend WithEvents txtEloaderPIN As System.Windows.Forms.TextBox
    Friend WithEvents txtEloadingLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents gridMobileNos As System.Windows.Forms.DataGridView
    Friend WithEvents txtMobileNo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnEditMobile As System.Windows.Forms.Button
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents OpenFileDialog2 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents btnBrowseSignature As System.Windows.Forms.Button
    Friend WithEvents picEmpSignature As System.Windows.Forms.PictureBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnBrowsePicture As System.Windows.Forms.Button
    Friend WithEvents picEmpPhoto As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents dgvemail As System.Windows.Forms.DataGridView
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents dgvphone As System.Windows.Forms.DataGridView
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents dgvcivil As System.Windows.Forms.DataGridView
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents dgvaddress As System.Windows.Forms.DataGridView
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents dgvlastname As System.Windows.Forms.DataGridView
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents cbofCategory As System.Windows.Forms.ComboBox
    Friend WithEvents cbofType As System.Windows.Forms.ComboBox
    Friend WithEvents cbofGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents btnMaxPhoto As System.Windows.Forms.Button
    Friend WithEvents btnMaxSignature As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents grpPayrollStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rdoNonExempt As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExempt As System.Windows.Forms.RadioButton
    Friend WithEvents chkBereaveYes As System.Windows.Forms.CheckBox
    Friend WithEvents chkBereaveNo As System.Windows.Forms.CheckBox
    Friend WithEvents mem_WithdrawDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mem_PaycontDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkNo As System.Windows.Forms.CheckBox
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents txtpayrollcontriamount As System.Windows.Forms.TextBox
    Friend WithEvents chkyes As System.Windows.Forms.CheckBox
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents txtwithdrawal As System.Windows.Forms.TextBox
    Friend WithEvents mem_MemberDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents cboemp_status As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents cboRank As System.Windows.Forms.ComboBox
    Friend WithEvents txtCompanyChart As System.Windows.Forms.TextBox
    Friend WithEvents LvlEmployeeDependent As System.Windows.Forms.ListView
    Friend WithEvents txtHDMF As System.Windows.Forms.TextBox
    Friend WithEvents txtPhilhealth As System.Windows.Forms.TextBox
    Friend WithEvents txtSSS As System.Windows.Forms.TextBox
    Friend WithEvents txtTin As System.Windows.Forms.TextBox
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents dtBoardApproval As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents btnSearchProvince As System.Windows.Forms.Button
    Friend WithEvents btnSearchHome As System.Windows.Forms.Button
    Friend WithEvents cboSubgroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label60 As System.Windows.Forms.Label
    Friend WithEvents txtEcola As System.Windows.Forms.TextBox
    Friend WithEvents Label63 As System.Windows.Forms.Label
    Friend WithEvents txtContractPrice As System.Windows.Forms.TextBox
    Friend WithEvents Label61 As System.Windows.Forms.Label
    Friend WithEvents btnemp_close As System.Windows.Forms.Button
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents btnUpdateEI As System.Windows.Forms.Button
    Friend WithEvents btnDeleteEI As System.Windows.Forms.Button
    Friend WithEvents btnNewEI As System.Windows.Forms.Button
    Friend WithEvents Label64 As System.Windows.Forms.Label
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlEducInfo As System.Windows.Forms.ListView
    Friend WithEvents TabPage8 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage9 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage10 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage11 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage12 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage13 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage14 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage15 As System.Windows.Forms.TabPage
    Friend WithEvents Label70 As System.Windows.Forms.Label
    Friend WithEvents Label71 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateJD As System.Windows.Forms.Button
    Friend WithEvents btnDeleteJD As System.Windows.Forms.Button
    Friend WithEvents btnNewJD As System.Windows.Forms.Button
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlJobDesc As System.Windows.Forms.ListView
    Friend WithEvents Label72 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateT As System.Windows.Forms.Button
    Friend WithEvents btnDeleteT As System.Windows.Forms.Button
    Friend WithEvents btnNewT As System.Windows.Forms.Button
    Friend WithEvents PictureBox12 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlTrainings As System.Windows.Forms.ListView
    Friend WithEvents Label73 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateSkills As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSkills As System.Windows.Forms.Button
    Friend WithEvents btnNewSkills As System.Windows.Forms.Button
    Friend WithEvents PictureBox13 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlSkills As System.Windows.Forms.ListView
    Friend WithEvents Label74 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateAward As System.Windows.Forms.Button
    Friend WithEvents btnDeleteAward As System.Windows.Forms.Button
    Friend WithEvents btnNewAward As System.Windows.Forms.Button
    Friend WithEvents PictureBox14 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlAwards As System.Windows.Forms.ListView
    Friend WithEvents Label75 As System.Windows.Forms.Label
    Friend WithEvents btnUpdatePE As System.Windows.Forms.Button
    Friend WithEvents btnDeletePE As System.Windows.Forms.Button
    Friend WithEvents btnNewPE As System.Windows.Forms.Button
    Friend WithEvents PictureBox15 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlPerfEval As System.Windows.Forms.ListView
    Friend WithEvents Label76 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateDisc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteDisc As System.Windows.Forms.Button
    Friend WithEvents btnNewDisc As System.Windows.Forms.Button
    Friend WithEvents PictureBox16 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlDiscipline As System.Windows.Forms.ListView
    Friend WithEvents Label77 As System.Windows.Forms.Label
    Friend WithEvents btnUpdateMed As System.Windows.Forms.Button
    Friend WithEvents btnDeleteMed As System.Windows.Forms.Button
    Friend WithEvents btnNewMed As System.Windows.Forms.Button
    Friend WithEvents PictureBox17 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlMedical As System.Windows.Forms.ListView
    Friend WithEvents Label78 As System.Windows.Forms.Label
    Friend WithEvents PictureBox18 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlLeave As System.Windows.Forms.ListView
    Friend WithEvents cboLeave As System.Windows.Forms.ComboBox
    Friend WithEvents TabPage16 As System.Windows.Forms.TabPage
    Friend WithEvents btnPayrollHistory As System.Windows.Forms.Button
    Friend WithEvents PictureBox19 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlPayrollHistory As System.Windows.Forms.ListView
    Friend WithEvents TabPage18 As System.Windows.Forms.TabPage
    Friend WithEvents Label80 As System.Windows.Forms.Label
    Friend WithEvents PictureBox20 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlLoanHistory As System.Windows.Forms.ListView
    Friend WithEvents txtCompanyName As System.Windows.Forms.TextBox
    Friend WithEvents Label79 As System.Windows.Forms.Label
    Friend WithEvents btnPEDownload As System.Windows.Forms.Button
    Friend WithEvents btnTDownload As System.Windows.Forms.Button
    Friend WithEvents btnDDownload As System.Windows.Forms.Button
    Friend WithEvents SaveFileDialog1 As System.Windows.Forms.SaveFileDialog
    Friend WithEvents btnMedUpload As System.Windows.Forms.Button
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents TabPage19 As System.Windows.Forms.TabPage
    Friend WithEvents dgvEmpHistory As System.Windows.Forms.DataGridView
    Friend WithEvents Label81 As System.Windows.Forms.Label
    Friend WithEvents PictureBox21 As System.Windows.Forms.PictureBox
    Friend WithEvents cboEmpHistory As System.Windows.Forms.ComboBox
    Friend WithEvents btnContractRate As System.Windows.Forms.Button
    Friend WithEvents txtContractrate As System.Windows.Forms.TextBox
    Friend WithEvents Label82 As System.Windows.Forms.Label
    Friend WithEvents Label83 As System.Windows.Forms.Label
    Friend WithEvents cboClient As System.Windows.Forms.ComboBox
    Friend WithEvents Label84 As System.Windows.Forms.Label
    Friend WithEvents TabPage20 As System.Windows.Forms.TabPage
    Friend WithEvents Label87 As System.Windows.Forms.Label
    Friend WithEvents PictureBox22 As System.Windows.Forms.PictureBox
    Friend WithEvents dgvPersonsReferred As System.Windows.Forms.DataGridView
    Friend WithEvents btnADdReferral As System.Windows.Forms.Button
    Friend WithEvents btnDeleteReferrals As System.Windows.Forms.Button
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents txtReferred As System.Windows.Forms.TextBox
    Friend WithEvents btnExport As System.Windows.Forms.Button
    Friend WithEvents TabPage21 As System.Windows.Forms.TabPage
    Friend WithEvents btnNREdit As System.Windows.Forms.Button
    Friend WithEvents btnNRDelete As System.Windows.Forms.Button
    Friend WithEvents btnNRNew As System.Windows.Forms.Button
    Friend WithEvents lvlRelative As System.Windows.Forms.ListView
    Friend WithEvents Label88 As System.Windows.Forms.Label
    Friend WithEvents PictureBox23 As System.Windows.Forms.PictureBox
    Friend WithEvents lblTerminated As System.Windows.Forms.Label
    Friend WithEvents txtBODResolution As System.Windows.Forms.TextBox
    Friend WithEvents txtCedula As System.Windows.Forms.TextBox
    Friend WithEvents txtCTCNo As System.Windows.Forms.TextBox
    Friend WithEvents Label89 As System.Windows.Forms.Label
    Friend WithEvents Label90 As System.Windows.Forms.Label
    Friend WithEvents Label91 As System.Windows.Forms.Label
End Class
