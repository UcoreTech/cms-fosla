﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmMember_Skills

    Public pkSkills As String

#Region "Property"
    Public Property getpkSkills() As String
        Get
            Return pkSkills
        End Get
        Set(ByVal value As String)
            pkSkills = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditSkills(ByVal EmpNo As String, ByVal Skills As String, ByVal Remarks As String, ByVal pk_Skills As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Skills_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@Skills", Skills), _
                                     New SqlParameter("@Remark", Remarks), _
                                     New SqlParameter("@pk_Skills", pk_Skills))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Skills")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtRemarks.Text = "" Or txtSkills.Text = "" Then
            MessageBox.Show("Complete Infomation is Needed", "Skills Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditSkills(frmMember_Master.txtEmployeeNo.Text, txtSkills.Text, txtRemarks.Text, "")
            Call frmMember_Master.GetmemberSkills(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Call AddEditSkills(frmMember_Master.txtEmployeeNo.Text, txtSkills.Text, txtRemarks.Text, pkSkills)
        Call frmMember_Master.GetmemberSkills(frmMember_Master.txtEmployeeNo.Text)
        Me.Close()
    End Sub
End Class