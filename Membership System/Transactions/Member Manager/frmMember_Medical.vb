﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Public Class frmMember_Medical

    Public pkMedicals As String
    Dim openFileDialog1 As New OpenFileDialog()
    Dim fpath As String
    Dim filenym As String
    Dim fileData As Byte()
    Dim ex As String
    Dim afiles As String
    'Public FileDirectory As String
    Dim files As Byte()
    Public filefromMem As String

#Region "Property"
    Public Property getpkMedicals() As String
        Get
            Return pkMedicals
        End Get
        Set(ByVal value As String)
            pkMedicals = value
        End Set
    End Property
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub AddEditMedicals(ByVal EmpNo As String, ByVal Cases As String, ByVal Status As String, ByVal Remarks As String, ByVal Fnames As String, ByVal Files As Byte(), ByVal DirectoryName As String, ByVal pk_Medicals As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Member_Medical_AddEdit", _
                                     New SqlParameter("@employeeNo", EmpNo), _
                                     New SqlParameter("@Cases", Cases), _
                                     New SqlParameter("@Status", Status), _
                                     New SqlParameter("@Remarks", Remarks), _
                                     New SqlParameter("@FileName", Fnames), _
                                     New SqlParameter("@AttachedFiles", Files), _
                                     New SqlParameter("@FileDirectory", DirectoryName), _
                                     New SqlParameter("@pk_Medicals", pk_Medicals))
            trans.Commit()
            MessageBox.Show("Record has been Saved", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Save Medical")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If txtCases.Text = "" Or txtStatus.Text = "" Or txtRemarks.Text = "" Then
            MessageBox.Show("Complete Infomation is Needed", " Medical Information", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, txtCases.Text, txtStatus.Text, txtRemarks.Text, Me.afiles, Me.fileData, Me.txtAttachFiles.Text, "")
            Call frmMember_Master.GetmemberMedicals(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        If fileData Is Nothing Then
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("MSS_MembersInfo_GetMedicals_AttachedFiles", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@EmployeeNo", SqlDbType.VarChar, 256).Value = frmMember_Master.txtEmployeeNo.Text
            cmd.Parameters.Add("@FileName", SqlDbType.VarChar, 100).Value = filefromMem
            Using myreader As SqlDataReader = cmd.ExecuteReader
                myreader.Read()
                files = DirectCast(myreader("fcAttachedFiles"), Byte())
            End Using
            Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, txtCases.Text, txtStatus.Text, txtRemarks.Text, Me.filefromMem, Me.files, Me.txtAttachFiles.Text, pkMedicals)
            Call frmMember_Master.GetmemberMedicals(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        Else
            Call AddEditMedicals(frmMember_Master.txtEmployeeNo.Text, txtCases.Text, txtStatus.Text, txtRemarks.Text, Me.afiles, Me.fileData, Me.txtAttachFiles.Text, pkMedicals)
            Call frmMember_Master.GetmemberMedicals(frmMember_Master.txtEmployeeNo.Text)
            Me.Close()
        End If
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            openFileDialog1.Filter = "Text Files (*.txt)|*.txt|PDF Files (*.pdf)|*.pdf|Word Documents (*.docx)|*.docx|Excel Worksheets (*.xlsx)|*.xlsx|PowerPoint Presentations (*.pptx)|*.pptx|Word Documents 97-2003 (*.doc)|*.docx|Excel Worksheets 93-2003 (*.xls)|*.xls|PowerPoint Presentations (*.ppt)|*.ppt" & "|Office Files|*.docx;*.xlsx;*.pptx;*.doc;*.xls;*.ppt" & "|Images|*.jpg;*.png;*.gif"
            openFileDialog1.Title = "Select File"
            openFileDialog1.CheckFileExists = True
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                filenym = openFileDialog1.FileName
                fpath = Path.GetExtension(filenym)
                afiles = Path.GetFileName(filenym)

                fileData = File.ReadAllBytes(filenym)
                ex = Path.GetExtension(LTrim(RTrim(filenym)))

                Dim ms As New MemoryStream(fileData, 0, fileData.Length)

                ms.Write(fileData, 0, fileData.Length)
                txtAttachFiles.Text = filenym
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class