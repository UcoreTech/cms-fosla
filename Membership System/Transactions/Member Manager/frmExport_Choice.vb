﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmExport_Choice

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Public ReportMode As String = ""

#Region "Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
#End Region

    Private Sub chkIndividual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIndividual.CheckedChanged
        If chkIndividual.Checked = True Then
            txtID.ReadOnly = False
            chkAll.Checked = False
            btnExport.Enabled = True
            chkAll.Enabled = False
        Else
            txtID.ReadOnly = True
            chkAll.Enabled = True
            btnExport.Enabled = False
        End If
    End Sub

    Private Sub frmExport_Choice_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If frmMember_Master.txtEmployeeNo.Text <> "" Then
            txtID.Text = frmMember_Master.txtEmployeeNo.Text
            chkAll.Checked = False
            txtID.ReadOnly = False
            btnExport.Enabled = True
            chkIndividual.Checked = True
        Else
            chkAll.Checked = False
            txtID.ReadOnly = True
            btnExport.Enabled = False
            chkIndividual.Checked = False
        End If
    End Sub

    Private Sub chkAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAll.CheckedChanged
        If chkAll.Checked = True Then
            chkIndividual.Checked = False
            btnExport.Enabled = True
            chkIndividual.Enabled = False
        Else
            chkIndividual.Enabled = True
            btnExport.Enabled = False
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Public Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Member_AllData.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@fcEmployeeNo", txtID.Text)
            rptsummary.SetParameterValue("@EmployeeNo", txtID.Text, "Analog_BankSubreport.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", txtID.Text, "Analog_BenefeciarySubreport.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", txtID.Text, "Analog_Relatives.rpt")
            rptsummary.SetParameterValue("@EmployeeNo", txtID.Text, "Analog_DependentsSubreport.rpt")
            frmReport_Export.crvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Public Sub LoadReport_All()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\Member_AllData_AllEmp.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            frmReport_Export.crvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            If chkIndividual.Checked = True Then
                If txtID.Text = "" Then
                    MessageBox.Show("ID must Provide", "Export", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                Else
                    LoadReport()
                End If
            ElseIf chkAll.Checked = True Then
                LoadReport_All()
            End If
            picLoading.Visible = True
            frmReport_Export.crvRpt.ExportReport()
            picLoading.Visible = False
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub
End Class