﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmTermination

    Private Sub frmTermination_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        lblTermination.Text = "Termination"
        LoadEmployee()
    End Sub

    Private Sub LoadEmployee()
        Try
            Dim gcon As New Clsappconfiguration
            gcon.sqlconn.Open()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_MemberList_Termination_Analog")

            dgvEmployee.DataSource = ds.Tables(0)
            dgvEmployee.Columns(2).Visible = False
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadEmployee_Filter(ByVal Search As String)
        Try
            Dim gcon As New Clsappconfiguration
            gcon.sqlconn.Open()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_MemberFilter_Termination_Analog",
                                          New SqlParameter("@fcEmployeeNo", Search))

            dgvEmployee.DataSource = ds.Tables(0)
            dgvEmployee.Columns(2).Visible = False
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        LoadEmployee_Filter(txtSearch.Text)
    End Sub

    Private Sub dgvEmployee_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvEmployee.DoubleClick
        txtEmpId.Text = Me.dgvEmployee.CurrentRow.Cells(0).Value.ToString
        txtFullname.Text = Me.dgvEmployee.CurrentRow.Cells(1).Value.ToString
        txtDateHired.Text = Me.dgvEmployee.CurrentRow.Cells(2).Value.ToString
        dtDate.Enabled = True
        btnTerminate.Enabled = True
        btnPreview.Enabled = True
    End Sub

    Private Sub Close_Account(ByVal ID As String)
        Try
            Dim gcon As New Clsappconfiguration
            gcon.sqlconn.Open()
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_AccountClose_Analog",
                                          New SqlParameter("@fcEmployeeNo", ID),
                                          New SqlParameter("@fbClosed", 1))
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ClearTxt()
        txtDateHired.Text = ""
        txtEmpId.Text = ""
        txtFullname.Text = ""
        txtSearch.Text = ""
        btnTerminate.Enabled = False
        btnPreview.Enabled = False
    End Sub

    Private Sub btnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPreview.Click
        'frmReport_Termination.EmployeeName = Me.dgvEmployee.CurrentRow.Cells(1).Value.ToString
        'frmReport_Termination.ShowDialog()
    End Sub

    Private Sub btnEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEntry.Click
        frmTermination_Entry.ShowDialog()
    End Sub

    Private Sub LoadEntry(ByVal ID As String, ByVal dDate As Date)
        Try
            Dim gcon As New Clsappconfiguration
            gcon.sqlconn.Open()
            Dim ds As DataSet
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to Terminate this Member?", "Confirm Termination", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = System.Windows.Forms.DialogResult.OK Then
                ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_Membership_Termination_Analog",
                                              New SqlParameter("@fcEmployeeNo", ID),
                                              New SqlParameter("@dtTermination", dDate),
                                              New SqlParameter("@fbTerminated", 1))
                frmTermination_Entry.ShowDialog()
                picLoading.Visible = False
                Close_Account(txtEmpId.Text)
                MessageBox.Show("Employee Terminated", "Terminated", MessageBoxButtons.OK, MessageBoxIcon.Information)
                LoadEmployee()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
            btnEntry.Enabled = True
            txtEmpId.Text = ""
            txtSearch.Text = ""
            txtFullname.Text = ""
            txtDateHired.Text = ""
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnTerminates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTerminates.Click
        picLoading.Visible = False
        LoadEntry(txtEmpId.Text, dtDate.Text)
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        ClearTxt()
        Me.Close()
    End Sub
End Class