﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmTermination_Entry

    Private gcon As New Clsappconfiguration
    Dim i As Integer = 0
    Public totDebit As Double = 0
    Public totcredit As Double = 0
    Public fxkey As String
    Dim xtr As Integer = 0
    Dim DocType As String

    Private jvKeyID As String

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Sub frmTermination_Entry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        i = 0
        cboDoctype.Text = "Select Type"
        txtParticulars.Text = ""
        totDebit = 0
        totcredit = 0
        txtDebit.Text = "0.00"
        txtCredit.Text = "0.00"
        LoadUser()
        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")
        GenerateEntry()
        LoadAccountDetails(frmTermination.txtEmpId.Text)
    End Sub

    Private Sub GenerateEntry()
        Dim IDNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim LoanNo As New DataGridViewTextBoxColumn
        Dim AcctRef As New DataGridViewTextBoxColumn
        Dim Code As New DataGridViewTextBoxColumn
        Dim AccountTitle As New DataGridViewTextBoxColumn
        Dim Debit As New DataGridViewTextBoxColumn
        Dim Credit As New DataGridViewTextBoxColumn

        With IDNo
            .HeaderText = "ID No"
            .Name = "IDNo"
            .DataPropertyName = "IDNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Client Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With LoanNo
            .HeaderText = "Loan No."
            .Name = "LoanNo"
            .DataPropertyName = "LoanNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctRef
            .HeaderText = "Acct Ref."
            .Name = "AcctRef"
            .DataPropertyName = "AcctRef"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Code
            .HeaderText = "Code"
            .Name = "Code"
            .DataPropertyName = "Code"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AccountTitle
            .HeaderText = "Account Title"
            .Name = "AccountTitle"
            .DataPropertyName = "AccountTitle"
            .ReadOnly = True
        End With
        With Debit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With Credit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With dgvEntry
            .Columns.Clear()
            .Columns.Add(IDNo)
            .Columns.Add(ClientName)
            .Columns.Add(LoanNo)
            .Columns.Add(AcctRef)
            .Columns.Add(Code)
            .Columns.Add(AccountTitle)
            .Columns.Add(Debit)
            .Columns.Add(Credit)
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With
    End Sub

    Private Sub LoadAccountDetails(ByVal EmpNo As String)
        Try
            Dim idno As String = ""
            Dim clientname As String = ""
            Dim xloanno As String = ""
            Dim rd1 As SqlDataReader
            Dim rd2 As SqlDataReader
            Dim rd3 As SqlDataReader

            rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "CIMS_MembersCreditAccount_Analog",
                                          New SqlParameter("@employeeno", EmpNo))

            rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "CIMS_MembersDebitAccount_Analog",
                                       New SqlParameter("@employeeno", EmpNo))

            rd3 = SqlHelper.ExecuteReader(gcon.cnstring, "CIMS_MembersLoan_Termination_Analog",
                                       New SqlParameter("@IDNo", EmpNo))
            With dgvEntry
                While rd1.Read
                    idno = rd1("fcEmployeeNo").ToString
                    clientname = rd1("EmpName").ToString

                    .Rows.Add()
                    .Item("IDNo", i).Value = idno
                    .Item("ClientName", i).Value = clientname
                    .Item("AcctRef", i).Value = rd1("RefNo").ToString
                    .Item("Code", i).Value = rd1("AccountCode").ToString
                    .Item("AccountTitle", i).Value = rd1("AccountName").ToString
                    .Item("Debit", i).Value = rd1("Balance").ToString
                    .Item("Credit", i).Value = "0.00"
                    totDebit = totDebit + CDec(.Item("Debit", i).Value)
                    txtDebit.Text = Format(Val(totDebit), "###,##0.00")
                    i += 1
                End While

                If clientname = "" And idno = "" Then
                    While rd2.Read
                        idno = rd2("fcEmployeeNo").ToString
                        clientname = rd2("EmpName").ToString

                        .Rows.Add()
                        .Item("IDNo", i).Value = idno
                        .Item("ClientName", i).Value = clientname
                        .Item("AcctRef", i).Value = rd2("RefNo").ToString
                        .Item("Code", i).Value = rd2("AccountCode").ToString
                        .Item("AccountTitle", i).Value = rd2("AccountName").ToString
                        .Item("Debit", i).Value = "0.00"
                        .Item("Credit", i).Value = rd2("Balance").ToString
                        totcredit = totcredit + Val(.Item("Credit", i).Value)
                        txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                        i += 1
                    End While
                Else
                    While rd2.Read
                        .Rows.Add()
                        .Item("IDNo", i).Value = idno
                        .Item("ClientName", i).Value = clientname
                        .Item("AcctRef", i).Value = rd2("RefNo").ToString
                        .Item("Code", i).Value = rd2("AccountCode").ToString
                        .Item("AccountTitle", i).Value = rd2("AccountName").ToString
                        .Item("Debit", i).Value = "0.00"
                        .Item("Credit", i).Value = rd2("Balance").ToString
                        totcredit = totcredit + Val(.Item("Credit", i).Value)
                        txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                        i += 1
                    End While
                End If

                If clientname = "" And idno = "" Then
                    While rd3.Read
                        idno = rd3("fcEmployeeNo").ToString
                        clientname = rd3("Fullname").ToString
                        xloanno = rd3("fnLoanNo").ToString

                        .Rows.Add()
                        .Item("IDNo", i).Value = idno
                        .Item("ClientName", i).Value = clientname
                        .Item("LoanNo", i).Value = xloanno
                        .Item("Code", i).Value = rd3("Code").ToString
                        .Item("AccountTitle", i).Value = rd3("Account Title").ToString
                        .Item("Debit", i).Value = "0.00"
                        .Item("Credit", i).Value = rd3("Balance").ToString
                        totcredit = totcredit + Val(.Item("Credit", i).Value)
                        txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                        i += 1
                    End While
                Else
                    While rd3.Read
                        xloanno = rd3("fnLoanNo").ToString

                        .Rows.Add()
                        .Item("IDNo", i).Value = EmpNo
                        .Item("ClientName", i).Value = clientname
                        .Item("LoanNo", i).Value = xloanno
                        .Item("Code", i).Value = rd3("Code").ToString
                        .Item("AccountTitle", i).Value = rd3("Account Title").ToString
                        .Item("Debit", i).Value = "0.00"
                        .Item("Credit", i).Value = rd3("Balance").ToString
                        totcredit = totcredit + Val(.Item("Credit", i).Value)
                        txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                        i += 1
                    End While
                End If
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadUser()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Cashier",
                                      New SqlParameter("@username", mod_UsersAccessibility.xUser))
        While rd.Read
            txtCreatedby.Text = rd("email").ToString
        End While
    End Sub

    Private Sub SaveEntry()
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If

            Dim MyGuid As Guid = New Guid(GetJVKeyID)
            fxkey = MyGuid.ToString

            Dim maxSerialNo As Integer = 0
            Dim dctype As String = ""
            If cboDoctype.Text = "CHECK VOUCHER" Then
                maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CHV")
                dctype = "CHV"
            ElseIf cboDoctype.Text = "CASH VOUCHER" Then
                maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CV")
                dctype = "CV"
            ElseIf cboDoctype.Text = "JOURNAL VOUCHER" Then
                maxSerialNo = Generate_JournalNo_Serial(Date.Now, "JV")
                dctype = "JV"
            ElseIf cboDoctype.Text = "ACCOUNTS PAYABLE VOUCHER" Then
                maxSerialNo = Generate_JournalNo_Serial(Date.Now, "APV")
                dctype = "APV"

            End If

            SqlHelper.ExecuteNonQuery(gcon.cnstring, "CAS_t_GeneralJournal_Header_InsertUpdate_Analog",
                                  New SqlParameter("@fxKeyJVNo", MyGuid),
                                  New SqlParameter("@Company", frmMain.lblCoopName.Text),
                                  New SqlParameter("@fiEntryNO", ""),
                                  New SqlParameter("@fnJournalNo", maxSerialNo),
                                  New SqlParameter("@fcEmployeeNo", frmTermination.txtEmpId.Text),
                                  New SqlParameter("@fcFullname", frmTermination.txtFullname.Text),
                                  New SqlParameter("@fcMemo", txtParticulars.Text),
                                  New SqlParameter("@fdDebit", txtDebit.Text),
                                  New SqlParameter("@fdCredit", txtCredit.Text),
                                  New SqlParameter("@fuCreatedby", txtCreatedby.Text),
                                  New SqlParameter("@fdDateCreated", txtDate.Text),
                                  New SqlParameter("@fbIsDeleted", False),
                                  New SqlParameter("@fdPosted", False),
                                  New SqlParameter("@fkDocType", dctype),
                                  New SqlParameter("@fbCancelled", False))

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialno As Integer
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialno = rd("maxSerialNo").ToString
            Else
                serialno = ""
            End If
            Return serialno
        End Using
    End Function

    Private Sub SaveDetails()
        Try
            Dim MyGuid As Guid = New Guid(fxkey)
            For xtr = 0 To dgvEntry.RowCount - 1

                With dgvEntry
                    Dim accounttitle As String = IIf(IsDBNull(.Item("Code", xtr).Value), Nothing, .Item("Code", xtr).Value)
                    Dim empno As String = .Item("IDNo", xtr).Value
                    Dim fullname As String = .Item("ClientName", xtr).Value
                    Dim loan As String = .Item("LoanNo", xtr).Value
                    Dim acctref As String = .Item("AcctRef", xtr).Value
                    Dim debit As Double = .Item("Debit", xtr).Value
                    Dim credit As Double = .Item("Credit", xtr).Value


                    SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_t_tJVEntry_details_save_Analog",
                                              New SqlParameter("@Company", frmMain.lblCoopName.Text),
                                              New SqlParameter("@fdDebit", debit),
                                              New SqlParameter("@fdCredit", credit),
                                              New SqlParameter("@fcMemo", txtParticulars.Text),
                                              New SqlParameter("@transDate", txtDate.Text),
                                              New SqlParameter("@EmployeeNo", empno),
                                              New SqlParameter("@fcFullname", fullname),
                                              New SqlParameter("@fcLoanRef", loan),
                                              New SqlParameter("@fcAccountRef", acctref),
                                              New SqlParameter("@fcCode", accounttitle),
                                              New SqlParameter("@fxKey", System.Guid.NewGuid),
                                              New SqlParameter("@fiEntryNo", ""),
                                              New SqlParameter("@fk_JVHeader", MyGuid))
                End With
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Exit Sub
        End Try
        xtr = 0
    End Sub

    Private Sub cboDoctype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDoctype.SelectedIndexChanged
        Select Case cboDoctype.Text
            Case "CHECK VOUCHER"
                DocType = "CHV"
            Case "CASH VOUCHER"
                DocType = "CV"
            Case "JOURNAL VOUCHER"
                DocType = "JV"
            Case "ACCOUNTS PAYABLE VOUCHER"
                DocType = "APV"
        End Select
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        i = 0
        dgvEntry.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub btnSaveEntry_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveEntry.Click
        If cboDoctype.Text = "Select Type" Then
            MessageBox.Show("Select Document Type", "Select Document Type First!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            SaveEntry()
            SaveDetails()
            MessageBox.Show("Save Information", "Successfully Save", MessageBoxButtons.OK, MessageBoxIcon.Information)

            GetJVKeyID = ""
            'GenerateEntry()
            txtDebit.Text = "0.00"
            txtCredit.Text = "0.00"
            cboDoctype.Text = ""
            cboDoctype.Text = "Select Type"

            i = 0
            dgvEntry.Columns.Clear()
            Me.Close()
        End If
    End Sub
End Class