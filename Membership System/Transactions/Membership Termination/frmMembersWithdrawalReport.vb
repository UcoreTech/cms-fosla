﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Security.Principal.WindowsIdentity


Public Class frmMembersWithdrawalReport
    Public Shared Termination_mode As String
    Public Shared Termination_effectiveDate As Date

    Private gcon As New Clsappconfiguration
    Private rptsummary As New ReportDocument
    Private journalForm As frmMembersWithdrawal_JournalEntries
    Private progress As New frmMembershipTermination_Progress
    Public EditedInterestRefund As String = ""

#Region "Properties"


    Private mode As String
    Public Property SetMode() As String
        Get
            Return mode
        End Get
        Set(ByVal value As String)
            mode = value
        End Set
    End Property


    Private terminationDate As Date
    Public Property SetTerminationDate() As Date
        Get
            Return terminationDate
        End Get
        Set(ByVal value As Date)
            terminationDate = value
        End Set
    End Property


    Private employeeNo As String
    Public Property GetSetEmployeeNo() As String
        Get
            Return employeeNo
        End Get
        Set(ByVal value As String)
            employeeNo = value
        End Set
    End Property
    Private effectiveDate As Date
    Public Property GetSetEffectiveDate() As Date
        Get
            Return effectiveDate
        End Get
        Set(ByVal value As Date)
            effectiveDate = value
        End Set
    End Property
    Private user As String
    Public Property GetSetUser() As String
        Get
            Return user
        End Get
        Set(ByVal value As String)
            user = value
        End Set
    End Property

    Private balance As Decimal
    Public Property GetSetBalance() As Decimal
        Get
            Return balance
        End Get
        Set(ByVal value As Decimal)
            balance = value
        End Set
    End Property

#End Region

#Region "Events"
    Private Sub frmMembersWithdrawalReport_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        rptsummary.Close()
    End Sub

    Private Sub frmMembersWithdrawalReport_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        bgwMembersTermination.RunWorkerAsync()
    End Sub
#End Region

#Region "Background Tasks"
    Private Sub bgwMembersTermination_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwMembersTermination.DoWork
        Call InitializeReport()
        Call GetNetBalanceFromTermination()
    End Sub

    Private Sub bgwMembersTermination_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwMembersTermination.RunWorkerCompleted
        Call LoadReportToViewer()
        Call HideLoadingStatus()
        btnEditInterest.Enabled = True
        'btnWithdraw.Enabled = CheckIfWithdrawable()
    End Sub
#End Region

#Region "Sub-Routines"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function

    Private Function CheckIfWithdrawable() As Boolean
        Dim xAmount As Decimal
        Dim sSqlCmd As String = "CIMS_Reports_Member_Withdrawal '" & GetSetEmployeeNo() & "', '" & GetSetEffectiveDate() & "' ,'" & GetSetUser() & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    xAmount += rd.Item("Debit")
                    xAmount -= rd.Item("Credit")
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at CheckIfWithdrawable in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        If xAmount < 0 Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub InitializeReport()
        rptsummary.Load(Application.StartupPath & "\LoanReport\MembersWithdrawal.rpt")
        rptsummary.Refresh()
        Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
        rptsummary.SetParameterValue("@employeeNo", GetSetEmployeeNo())
        rptsummary.SetParameterValue("@effectiveDate", GetSetEffectiveDate())
        rptsummary.SetParameterValue("@user", GetSetUser())
        rptsummary.SetParameterValue("@EditedIntrstRfund", EditedInterestRefund)
    End Sub

    Private Sub GetNetBalanceFromTermination()
        Dim balance As Decimal
        Dim xSQLHelper As New NewSQLHelper

        Using rd As SqlDataReader = xSQLHelper.ExecuteReader("CIMS_Reports_Member_Withdrawal", _
                            New SqlParameter("@employeeNo", GetSetEmployeeNo()), _
                            New SqlParameter("@effectiveDate", GetSetEffectiveDate()), _
                            New SqlParameter("@user", GetSetUser()), _
                            New SqlParameter("@EditedIntrstRfund", EditedInterestRefund))
            If rd.Read() Then


                If rd.Item("Balance") IsNot DBNull.Value Then
                    GetSetBalance() = rd.Item("Balance")
                End If

            End If
        End Using

    End Sub

    Private Sub ChangeMembershipStatus()

    End Sub

    Private Sub HideLoadingStatus()
        loadingLabel.Visible = False
    End Sub

    Private Sub LoadReportToViewer()
        Me.crvRpt.Visible = True
        Me.crvRpt.ReportSource = rptsummary
    End Sub

    Private Function getWithdrawalAmount(ByVal EmployeeNo As String) As Decimal
        Dim sSqlCmd As String = "SELECT	SUM(ISNULL(fdDepositAmount,0)) - SUM(ISNULL(fdWithdrawalAmount,0)) AS Amount " & _
                                "FROM dbo.CIMS_t_Member_Contributions AS t1 " & _
                                "INNER JOIN dbo.CIMS_m_Member AS t2 " & _
                                "ON t1.fk_Employee = t2.pk_Employee " & _
                                "WHERE t2.fcEmployeeNo = '" & EmployeeNo & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                If rd.HasRows = True Then
                    rd.Read()
                    Return rd.Item(0)
                Else
                    Return 0
                End If
            End Using
        Catch ex As Exception
            MsgBox("Error at getWithdrawalAmount in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            Return 0
        End Try
    End Function

    Private Function getTotalPayment(ByVal LoanNo As String) As Decimal
        Dim sSqlCmd As String = "select sum(t1.fnPaymentAmount) as Amount from dbo.CIMS_t_Loans_Payment t1 " & _
                                "inner join dbo.CIMS_t_Member_Loans t2 " & _
                                "on t1.fk_Member_Loan = t2.pk_Members_Loan " & _
                                "where t2.fnLoanNo = '" & LoanNo & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item("Amount")
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at getTotalPayment in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Function

    Private Sub Withdraw(ByVal EmployeeNo As String, ByVal EffectiveDate As Date)
        Dim sSqlCmd As String
        '######### Contribution #############
        sSqlCmd = "CIMS_Membership_Withdrawal_Contribution " & _
                    "@employeeNo = '" & EmployeeNo & "', " & _
                    "@date = '" & EffectiveDate & "', " & _
                    "@description = 'Members Withdrawal - " & EffectiveDate.Date.ToString & "', " & _
                    "@withdraw = '" & getWithdrawalAmount(EmployeeNo) & "'"

        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at Withdraw in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        '######### Loan Balance #############
        sSqlCmd = "select t1.pk_Members_Loan, t1.fnLoanNo, t1.fdBalance from dbo.View_1_LoanMaster_Current t1 " & _
                    "inner join dbo.CIMS_m_Member t2 " & _
                    "on t1.pk_Employee = t2.pk_Employee " & _
                    "where t2.fcEmployeeNo = '" & EmployeeNo & "'"
        Try
            Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd2.Read
                    sSqlCmd = "CIMS_Membership_Withdrawal_LoanBalance " & _
                                "@loanNo = '" & rd2.Item("fnLoanNo").ToString & "', " & _
                                "@date = '" & EffectiveDate & "', " & _
                                "@description = 'Members Withdrawal - " & EffectiveDate.Date.ToString & "', " & _
                                "@fnPaymentAmount = '" & rd2.Item("fdBalance").ToString & "', " & _
                                "@pk_Member_Loan = '" & rd2.Item("pk_Members_Loan").ToString & "'"
                    Try
                        SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
                    Catch ex As Exception
                        MsgBox("Error at Withdraw in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                    End Try

                    sSqlCmd = "update dbo.CIMS_t_Member_Loans " & _
                                "set fdBalance = 0, " & _
                                "fdAmountPaid = '" & getTotalPayment(rd2.Item("fnLoanNo").ToString) & "' " & _
                                "where pk_Members_Loan = '" & rd2.Item("pk_Members_Loan").ToString & "'"
                    Try
                        SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
                    Catch ex As Exception
                        MsgBox("Error at Withdraw in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                    End Try
                End While
            End Using
            ChangeEmployeeStatus(EmployeeNo)
            MsgBox("Withdrawal successful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted!")
        Catch ex As Exception
            MsgBox("Error at Withdraw in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

    End Sub

    Private Function GetKeyResigningStatus() As String
        Dim sSqlCmd As String = "Select pk_MembershipStatus from dbo.CIMS_m_Membership_Status where fcStatusDesc = 'Resigning'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    Return rd.Item("pk_MembershipStatus").ToString
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at GetKeyResigningStatus in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            Return Nothing
        End Try
    End Function

    Private Sub ChangeEmployeeStatus(ByVal EmployeeNo As String)
        Dim sSqlCmd As String = "update dbo.CIMS_m_Member " & _
                                "set fk_MembershipStatus = '" & GetKeyResigningStatus() & "' " & _
                                "where fcEmployeeNo = '" & EmployeeNo & "'"
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at ChangeEmployeeStatus in frmMembersWithdrawalReport module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub
#End Region

    Private Sub ToolStrip_Journal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStrip_Journal.Click
        '  If the instance still exists... (ie. it's Not Nothing)
        If Not IsNothing(journalForm) Then
            '  and if it hasn't been disposed yet
            If Not journalForm.IsDisposed Then
                '  then it must already be instantiated - maybe it's
                '  minimized or hidden behind other forms ?
                journalForm.WindowState = FormWindowState.Normal  ' Optional
                journalForm.BringToFront()  '  Optional
            Else
                '  else it has already been disposed, so you can
                '  instantiate a new form and show it
                journalForm = New frmMembersWithdrawal_JournalEntries()
                Call LoadParameters(journalForm)
                journalForm.Show()
            End If
        Else
            '  else the form = nothing, so you can safely
            '  instantiate a new form and show it
            journalForm = New frmMembersWithdrawal_JournalEntries()
            Call LoadParameters(journalForm)
            journalForm.Show()
        End If
    End Sub

    Private Sub LoadParameters(ByVal journalForm As frmMembersWithdrawal_JournalEntries)
        journalForm.GetSetEmployeeNo() = Me.GetSetEmployeeNo()
        journalForm.GetSetEffectiveDate() = Me.GetSetEffectiveDate()
        journalForm.GetSetUser() = Me.GetSetUser()
        journalForm.EditedInterestRefnd = EditedInterestRefund
    End Sub

    Private Sub toolStrip_Post_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolStrip_Post.Click
        Try
            If MessageBox.Show("Membership will be terminated from this Member. Do you want to proceed?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                Termination_mode = ""
                Dim dateTermination As New frmMembershipTermination_DateEntry
                dateTermination.ShowDialog()

                If Termination_mode = "Set" Then
                    progress.Show()
                    bgwProcessTermination.RunWorkerAsync()
                End If


            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Membership Termination", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ProcessMembershipTermination(ByVal employeeNo As String, ByVal mode As String)
        Try
            Select Case mode
                Case "Positive"
                    Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_InterestRefundPerEmployeeNo", _
                        New SqlParameter("@employeeNo", employeeNo), _
                        New SqlParameter("@EditedIntrstRfund", EditedInterestRefund))
                        Dim interestRefund As Decimal
                        Dim netBalance As Decimal
                        Dim loanNo As String
                        Dim username As String = frmLogin.Username.Text
                        Dim effectiveDate As Date = Termination_effectiveDate

                        '1. Record Accounting Entries To Bills
                        Call RecordAccountingEntries_ToBills(employeeNo, effectiveDate, username)

                        While rd.Read()
                            interestRefund = CDec(rd.Item("intRefund"))
                            netBalance = CDec(rd.Item("Balance"))
                            loanNo = rd.Item("loanNo").ToString()

                            '2. Deduct Loan Payments from Contribution
                            Call RecordContributionWithdrawal(employeeNo, effectiveDate, netBalance, loanNo)

                            '3. Insert Payment to Loan Based on Payment
                            Call InsertPaymentToLoan(loanNo, netBalance, effectiveDate, interestRefund)

                        End While

                        '4. Change Membership Status
                        Call ChangeMemberStatus(employeeNo)

                        '5. Remaining Capital Share/Contribution to be withdrawn
                        Call WithdrawRemainingCapitalShare(employeeNo, effectiveDate)

                    End Using

                Case "Negative"
                    '1. Determine Loan Priority
                    Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_InterestRefundPerEmployeeNo_WithPriority", _
                            New SqlParameter("@employeeNo", employeeNo), _
                            New SqlParameter("@EditedIntrstRfund", EditedInterestRefund))
                        Dim interestRefund As Decimal
                        Dim netBalance As Decimal
                        Dim loanNo As String
                        Dim username As String = frmLogin.Username.Text
                        Dim effectiveDate As Date = Termination_effectiveDate

                        '2. Record Accounting Entries To General Journal
                        Call RecordAccountingEntriesToGeneralJournal(employeeNo, effectiveDate, username)

                        While rd.Read()
                            interestRefund = CDec(rd.Item("intRefund"))
                            netBalance = CDec(rd.Item("Balance"))
                            loanNo = rd.Item("loanNo").ToString()
                            '3. Deduct Loan Payments from Contribution sorted by priorities
                            '4. Post Remaining Receivables to Other Current Receivables
                            Call RecordLoanPaymentFromCapitalShare(employeeNo, effectiveDate, netBalance, interestRefund, loanNo)
                        End While
                    End Using

                    If MsgBox("The member currently owes the coop with the amount of " & GetSetBalance().ToString("#,##0.00") & "." & vbCr & "Do you still want to tagged him/her as resigning?", MsgBoxStyle.YesNo + MsgBoxStyle.Question, "Membership status change") = MsgBoxResult.Yes Then
                        Call ChangeMemberStatus(employeeNo)
                    End If
            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RecordLoanPaymentFromCapitalShare(ByVal employeeNo As String, ByVal effectiveDate As Date, ByVal loanNetBalance As Decimal, ByVal intRefund As Decimal, ByVal loanNo As Integer)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_RecordPaymentToLoan_withPriority", _
                    New SqlParameter("@employeeNo", employeeNo), _
                    New SqlParameter("@date", effectiveDate), _
                    New SqlParameter("@loanNetBalance", loanNetBalance), _
                    New SqlParameter("@interestRefund", intRefund), _
                    New SqlParameter("@loanNo", loanNo))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RecordAccountingEntriesToGeneralJournal(ByVal employeeNo As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_Termination_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", employeeNo), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user), _
                New SqlParameter("@EditedIntrstRfund", EditedInterestRefund))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub WithdrawRemainingCapitalShare(ByVal employeeNo As String, ByVal effectiveDate As Date)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_WithdrawRemainingBalance", _
                New SqlParameter("@employeeNo", employeeNo), _
                New SqlParameter("@date", effectiveDate))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RecordAccountingEntries_ToBills(ByVal employeeNo As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_Termination_AccountingEntries_ToBills", _
                New SqlParameter("@employeeNo", employeeNo), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user), _
                New SqlParameter("@EditedIntrstRfund", EditedInterestRefund))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RecordContributionWithdrawal(ByVal employeeNo As String, ByVal terminationDate As Date, ByVal paymentAmount As Decimal, ByVal loanNo As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_WithdrawFromContribution", _
                            New SqlParameter("@employeeNo", employeeNo), _
                            New SqlParameter("@date", terminationDate), _
                            New SqlParameter("@withdrawal", paymentAmount), _
                            New SqlParameter("@loanNo", loanNo))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub InsertPaymentToLoan(ByVal loanNo As String, ByVal paymentAmount As Decimal, ByVal dateTerminated As Date, ByVal interestRefund As Decimal)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_RecordPaymentToLoan", _
                    New SqlParameter("@loanNo", loanNo), _
                    New SqlParameter("@paymentAmount", paymentAmount), _
                    New SqlParameter("@date", dateTerminated), _
                    New SqlParameter("@interestRefund", interestRefund))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ChangeMemberStatus(ByVal employeeNo As String)
        Try
            ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Membership_ChangeStatusToInactive", _
                    New SqlParameter("@employeeNo", employeeNo))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub bgwProcessTermination_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwProcessTermination.DoWork
        If GetSetBalance() > 0 Then

            Call ProcessMembershipTermination(GetSetEmployeeNo(), "Positive")

            MessageBox.Show("Membership Successfully Terminated from Cooperative.", "Membership Termination", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else

            Call ProcessMembershipTermination(GetSetEmployeeNo(), "Negative")
            MessageBox.Show("Membership Successfully Terminated from Cooperative.", "Membership Termination", MessageBoxButtons.OK, MessageBoxIcon.Information)

        End If
    End Sub

    Private Sub bgwProcessTermination_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwProcessTermination.RunWorkerCompleted
        progress.Close()
        Me.Close()
    End Sub

    Private Sub btnEditInterest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditInterest.Click
        Dim xForm As New frmChangeInterestRefund
        xForm.EmpNo = GetSetEmployeeNo()
        xForm.xDate = GetSetEffectiveDate()
        xForm.ParentForm = Me

        If xForm.ShowDialog() = Windows.Forms.DialogResult.OK Then
            btnEditInterest.Enabled = False
            bgwMembersTermination.RunWorkerAsync()
        End If

    End Sub

    Private Sub crvRpt_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles crvRpt.Load

    End Sub
End Class