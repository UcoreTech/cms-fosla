Public Class frmMembershipTermination_DateEntry
    Private Sub BtnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.DialogResult = DialogResult.Cancel

        frmMembersWithdrawalReport.SetMode = "Cancelled"
        Close()
    End Sub

    Private Sub btnSet_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSet.Click
        Me.DialogResult = DialogResult.OK


        frmMembersWithdrawalReport.Termination_mode = "Set"
        frmMembersWithdrawalReport.Termination_effectiveDate = dteTerminationDate.Value

        Close()
    End Sub

    Private Sub frmMembershipTermination_DateEntry_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dteTerminationDate.Value = frmMembersWithdrawalReport.Termination_effectiveDate
    End Sub
End Class