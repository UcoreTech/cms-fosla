Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words
Public Class Deposit
    Private gcon As New Clsappconfiguration
#Region "Property"
    Private idNo As String
    Public Property GetIDNo() As String
        Get
            Return idNo
        End Get
        Set(ByVal value As String)
            idNo = value
        End Set
    End Property
    Private Name As String
    Public Property GetName() As String
        Get
            Return Name
        End Get
        Set(ByVal value As String)
            Name = value
        End Set
    End Property
    Private SelectSTD As String
    Public Property GetSelectSTD() As String
        Get
            Return SelectSTD
        End Get
        Set(ByVal value As String)
            SelectSTD = value
        End Set
    End Property
    Private UniqEmp As String
    Public Property GetUniqEmp() As String
        Get
            Return UniqEmp
        End Get
        Set(ByVal value As String)
            UniqEmp = value
        End Set
    End Property
#End Region
    Private Sub Deposit_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'cbobanks.SelectedIndex = "0"
        LoadDepositDetails()
        txtamount.Clear()
        txtId.Text = GetIDNo()
        txtname.Text = GetName()
        Description()
        cboPaymethod.SelectedIndex = "0"
        Me.txtIntr.Clear()
        Me.txtcheck.Clear()
        Me.txtterm.Clear()
    End Sub
    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Close()
    End Sub
    Private Sub LoadDepositDetails()
        Dim BankLoad As String = "CIMS_m_Policies_BankSettings_BankList_Load"
        Try
            cbobanks.DataSource = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.Text, BankLoad).Tables(0).DefaultView
            cbobanks.DisplayMember = "Bank Name"
            cbobanks.ValueMember = "ID"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub Description()
        If SelectSTD = "Savings" Then
            Me.txtDes.Text = "Savings Deposit -"
            Me.txtterm.Enabled = False
            Me.txtIntr.Enabled = False
            Me.txtIntr.BackColor = Color.Gainsboro
            Me.txtterm.BackColor = Color.Gainsboro
            Me.txtterm.BorderStyle = BorderStyle.None
            Me.txtIntr.BorderStyle = BorderStyle.None
            Me.btnDeposit.Enabled = True
        Else
            Me.txtDes.Text = "Time Deposit -"
            Me.txtterm.Enabled = True
            Me.txtIntr.Enabled = True
            Me.txtIntr.BackColor = Color.White
            Me.txtterm.BackColor = Color.White
            Me.txtterm.BorderStyle = BorderStyle.Fixed3D
            Me.txtIntr.BorderStyle = BorderStyle.Fixed3D
            Me.btnDeposit.Enabled = False

        End If
    End Sub
    Private Sub cboPaymethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboPaymethod.SelectedIndexChanged
        If cboPaymethod.SelectedItem = "Cash" Then
            cbobanks.Enabled = False
            txtcheck.Enabled = False
        Else
            cbobanks.Enabled = True
            txtcheck.Enabled = True
        End If
    End Sub
    Private Sub DepositPay(ByVal Depdate As Date, ByVal descrip As String, ByVal UniqEmp As String, _
                            ByVal Depamnt As Decimal, ByVal paymthd As String, ByVal chckno As String, _
                            ByVal bankid As Integer, ByVal clear As Boolean)
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Deposit_Savings_Deposit", _
                New SqlParameter("@dtDepdate", Depdate), _
                New SqlParameter("@fcDes", descrip), _
                New SqlParameter("@Emp", UniqEmp), _
                New SqlParameter("@fdDepAmnt", Depamnt), _
                New SqlParameter("@paymethod", paymthd), _
                New SqlParameter("@fcCheckNo", chckno), _
                New SqlParameter("@bankID", bankid), _
                New SqlParameter("@clear", clear))

            trans.Commit()

            MessageBox.Show("Success!")
            Me.Close()
        Catch ex As Exception
            trans.Rollback()
            Throw ex
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub TimeDepositPay(ByVal Depdate As Date, ByVal descrip As String, ByVal UniqEmp As String, _
                            ByVal Depamnt As Decimal, ByVal paymthd As String, ByVal chckno As String, _
                            ByVal term As Integer, ByVal bankid As Integer, ByVal tdclear As Boolean)

        Me.txtterm.Clear()
        Me.txtcheck.Clear()
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_m_Deposit_Time_Deposit_Add", _
                New SqlParameter("@dtDepdate", Depdate), _
                New SqlParameter("@fcDes", descrip), _
                New SqlParameter("@Emp", UniqEmp), _
                New SqlParameter("@fdDepAmnt", Depamnt), _
                New SqlParameter("@paymethod", paymthd), _
                New SqlParameter("@fcCheckNo", chckno), _
                New SqlParameter("@term", term), _
                New SqlParameter("@bankID", bankid), _
                New SqlParameter("@tdClear", tdclear))

            trans.Commit()

            MessageBox.Show("Success!")
            Me.Close()

        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Deposit", MessageBoxButtons.OK, MessageBoxIcon.Error)
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnDeposit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeposit.Click
        Try
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to Apply Deposit?", "Confirm Deposit", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
            If x = Windows.Forms.DialogResult.OK Then

                If SelectSTD = "Savings" Then
                    If Me.cboPaymethod.SelectedItem = "Cash" Then
                        Call DepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, cbobanks.SelectedValue, 1)
                    Else
                        Call DepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, cbobanks.SelectedValue, 0)
                    End If

                    ' Accounting Entry Integration (Savings)
                    Call AccountingEntry_SavingsDeposit(txtId.Text, txtamount.Text, txtDes.Text, dtDepdate.Value, frmMain.username)
                Else
                    If Me.cboPaymethod.SelectedItem = "Cash" Then
                        Call TimeDepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, txtterm.Text, cbobanks.SelectedValue, 1)
                    Else
                        Call TimeDepositPay(dtDepdate.Value, txtDes.Text, GetUniqEmp, txtamount.Text, cboPaymethod.SelectedItem, txtcheck.Text, txtterm.Text, cbobanks.SelectedValue, 0)
                    End If

                    ' Accounting Entry Integration (Time)
                    Call AccountingEntry_TimeDeposit(txtId.Text, txtamount.Text, txtDes.Text, dtDepdate.Value, frmMain.username)
                End If
            ElseIf x = Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Savings Deposit", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AccountingEntry_TimeDeposit(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_TimeDeposit_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountDeposited", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AccountingEntry_SavingsDeposit(ByVal empNo As String, ByVal amount As Decimal, ByVal particulars As String, ByVal effectiveDate As Date, ByVal user As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_SavingsDeposit_AccountingEntries_ToGenJournal", _
                New SqlParameter("@employeeNo", empNo), _
                New SqlParameter("@amountDeposited", amount), _
                New SqlParameter("@particulars", particulars), _
                New SqlParameter("@effectiveDate", effectiveDate), _
                New SqlParameter("@user", user))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub txtamount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtamount.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtamount.Focus()
        End If
    End Sub
    Private Sub txtterm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtterm.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtterm.Focus()
        End If
    End Sub
    Private Sub txtterm_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtterm.TextChanged
        If SelectSTD = "Time Deposit" Then
            If Me.txtterm.Text <> "" Then
                Me.btnDeposit.Enabled = True
            Else
                btnDeposit.Enabled = False
            End If
        Else
        End If
    End Sub
    Private Sub txtIntr_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtIntr.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtIntr.Focus()
        End If
    End Sub
    Private Sub txtIntr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtIntr.TextChanged
        If SelectSTD = "Time Deposit" Then
            If Me.txtIntr.Text <> "" Then
                Me.btnDeposit.Enabled = True
            Else
                btnDeposit.Enabled = False
            End If
        Else
        End If
    End Sub
End Class