<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBankSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBankSettings))
        Me.tabbankset = New System.Windows.Forms.TabControl()
        Me.Savings = New System.Windows.Forms.TabPage()
        Me.cboCashAcc = New System.Windows.Forms.ComboBox()
        Me.cboIntAcc = New System.Windows.Forms.ComboBox()
        Me.cboSavingAccount = New System.Windows.Forms.ComboBox()
        Me.txtMaintainingBal = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txtint = New System.Windows.Forms.TextBox()
        Me.cboperiod = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.rdbAuto = New System.Windows.Forms.RadioButton()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.TimeDeposit = New System.Windows.Forms.TabPage()
        Me.cbotdcash = New System.Windows.Forms.ComboBox()
        Me.cbotdint = New System.Windows.Forms.ComboBox()
        Me.cbotdsav = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtperiodtd = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtinttd = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.rdbtdauto = New System.Windows.Forms.RadioButton()
        Me.RadioButton4 = New System.Windows.Forms.RadioButton()
        Me.Banks = New System.Windows.Forms.TabPage()
        Me.grdBank = New System.Windows.Forms.DataGridView()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtBank = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnupdate = New System.Windows.Forms.Button()
        Me.btnsave = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnapply = New System.Windows.Forms.Button()
        Me.btndelete = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.tabbankset.SuspendLayout()
        Me.Savings.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TimeDeposit.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.Banks.SuspendLayout()
        CType(Me.grdBank, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabbankset
        '
        Me.tabbankset.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabbankset.Controls.Add(Me.Savings)
        Me.tabbankset.Controls.Add(Me.TimeDeposit)
        Me.tabbankset.Controls.Add(Me.Banks)
        Me.tabbankset.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabbankset.Location = New System.Drawing.Point(0, 30)
        Me.tabbankset.Name = "tabbankset"
        Me.tabbankset.SelectedIndex = 0
        Me.tabbankset.Size = New System.Drawing.Size(545, 229)
        Me.tabbankset.TabIndex = 55
        '
        'Savings
        '
        Me.Savings.Controls.Add(Me.cboCashAcc)
        Me.Savings.Controls.Add(Me.cboIntAcc)
        Me.Savings.Controls.Add(Me.cboSavingAccount)
        Me.Savings.Controls.Add(Me.txtMaintainingBal)
        Me.Savings.Controls.Add(Me.Label7)
        Me.Savings.Controls.Add(Me.Label6)
        Me.Savings.Controls.Add(Me.Label5)
        Me.Savings.Controls.Add(Me.Label4)
        Me.Savings.Controls.Add(Me.GroupBox1)
        Me.Savings.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Savings.Location = New System.Drawing.Point(4, 25)
        Me.Savings.Name = "Savings"
        Me.Savings.Padding = New System.Windows.Forms.Padding(3)
        Me.Savings.Size = New System.Drawing.Size(537, 200)
        Me.Savings.TabIndex = 0
        Me.Savings.Text = "Savings"
        Me.Savings.UseVisualStyleBackColor = True
        '
        'cboCashAcc
        '
        Me.cboCashAcc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboCashAcc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCashAcc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCashAcc.FormattingEnabled = True
        Me.cboCashAcc.Location = New System.Drawing.Point(11, 171)
        Me.cboCashAcc.Name = "cboCashAcc"
        Me.cboCashAcc.Size = New System.Drawing.Size(234, 21)
        Me.cboCashAcc.TabIndex = 11
        '
        'cboIntAcc
        '
        Me.cboIntAcc.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboIntAcc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIntAcc.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboIntAcc.FormattingEnabled = True
        Me.cboIntAcc.Location = New System.Drawing.Point(11, 130)
        Me.cboIntAcc.Name = "cboIntAcc"
        Me.cboIntAcc.Size = New System.Drawing.Size(234, 21)
        Me.cboIntAcc.TabIndex = 10
        '
        'cboSavingAccount
        '
        Me.cboSavingAccount.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboSavingAccount.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboSavingAccount.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboSavingAccount.FormattingEnabled = True
        Me.cboSavingAccount.Location = New System.Drawing.Point(11, 84)
        Me.cboSavingAccount.Name = "cboSavingAccount"
        Me.cboSavingAccount.Size = New System.Drawing.Size(234, 21)
        Me.cboSavingAccount.TabIndex = 6
        '
        'txtMaintainingBal
        '
        Me.txtMaintainingBal.Location = New System.Drawing.Point(11, 40)
        Me.txtMaintainingBal.Name = "txtMaintainingBal"
        Me.txtMaintainingBal.Size = New System.Drawing.Size(234, 23)
        Me.txtMaintainingBal.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(8, 152)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(112, 16)
        Me.Label7.TabIndex = 9
        Me.Label7.Text = "Cash Account:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 111)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(135, 16)
        Me.Label6.TabIndex = 8
        Me.Label6.Text = "Interest Account:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(133, 16)
        Me.Label5.TabIndex = 7
        Me.Label5.Text = "Savings Account:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 23)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(155, 16)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "Maintaning Balance:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.txtint)
        Me.GroupBox1.Controls.Add(Me.cboperiod)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.rdbAuto)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Location = New System.Drawing.Point(267, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(267, 223)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Interest Computation"
        '
        'txtint
        '
        Me.txtint.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtint.Location = New System.Drawing.Point(112, 92)
        Me.txtint.Name = "txtint"
        Me.txtint.Size = New System.Drawing.Size(152, 23)
        Me.txtint.TabIndex = 5
        '
        'cboperiod
        '
        Me.cboperiod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboperiod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboperiod.FormattingEnabled = True
        Me.cboperiod.Items.AddRange(New Object() {"Monthly", "Quarterly", "Annually"})
        Me.cboperiod.Location = New System.Drawing.Point(113, 130)
        Me.cboperiod.Name = "cboperiod"
        Me.cboperiod.Size = New System.Drawing.Size(151, 24)
        Me.cboperiod.TabIndex = 4
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(34, 133)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 16)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Period:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(34, 95)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Interest:"
        '
        'rdbAuto
        '
        Me.rdbAuto.AutoSize = True
        Me.rdbAuto.Location = New System.Drawing.Point(8, 43)
        Me.rdbAuto.Name = "rdbAuto"
        Me.rdbAuto.Size = New System.Drawing.Size(101, 20)
        Me.rdbAuto.TabIndex = 1
        Me.rdbAuto.Text = "Automatic"
        Me.rdbAuto.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(8, 23)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(78, 20)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.Text = "Manual"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'TimeDeposit
        '
        Me.TimeDeposit.Controls.Add(Me.cbotdcash)
        Me.TimeDeposit.Controls.Add(Me.cbotdint)
        Me.TimeDeposit.Controls.Add(Me.cbotdsav)
        Me.TimeDeposit.Controls.Add(Me.Label10)
        Me.TimeDeposit.Controls.Add(Me.Label11)
        Me.TimeDeposit.Controls.Add(Me.Label12)
        Me.TimeDeposit.Controls.Add(Me.GroupBox2)
        Me.TimeDeposit.Location = New System.Drawing.Point(4, 25)
        Me.TimeDeposit.Name = "TimeDeposit"
        Me.TimeDeposit.Padding = New System.Windows.Forms.Padding(3)
        Me.TimeDeposit.Size = New System.Drawing.Size(537, 200)
        Me.TimeDeposit.TabIndex = 1
        Me.TimeDeposit.Text = "Time Deposit"
        Me.TimeDeposit.UseVisualStyleBackColor = True
        '
        'cbotdcash
        '
        Me.cbotdcash.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotdcash.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbotdcash.FormattingEnabled = True
        Me.cbotdcash.Location = New System.Drawing.Point(12, 130)
        Me.cbotdcash.Name = "cbotdcash"
        Me.cbotdcash.Size = New System.Drawing.Size(234, 24)
        Me.cbotdcash.TabIndex = 19
        '
        'cbotdint
        '
        Me.cbotdint.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotdint.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbotdint.FormattingEnabled = True
        Me.cbotdint.Location = New System.Drawing.Point(12, 86)
        Me.cbotdint.Name = "cbotdint"
        Me.cbotdint.Size = New System.Drawing.Size(234, 24)
        Me.cbotdint.TabIndex = 18
        '
        'cbotdsav
        '
        Me.cbotdsav.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotdsav.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbotdsav.FormattingEnabled = True
        Me.cbotdsav.Location = New System.Drawing.Point(12, 40)
        Me.cbotdsav.Name = "cbotdsav"
        Me.cbotdsav.Size = New System.Drawing.Size(234, 24)
        Me.cbotdsav.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(9, 113)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(112, 16)
        Me.Label10.TabIndex = 15
        Me.Label10.Text = "Cash Account:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(9, 67)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(135, 16)
        Me.Label11.TabIndex = 14
        Me.Label11.Text = "Interest Account:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(9, 22)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(170, 16)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Time Deposit Account:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Controls.Add(Me.txtperiodtd)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.txtinttd)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.rdbtdauto)
        Me.GroupBox2.Controls.Add(Me.RadioButton4)
        Me.GroupBox2.Location = New System.Drawing.Point(267, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(275, 205)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Interest Computation"
        '
        'txtperiodtd
        '
        Me.txtperiodtd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtperiodtd.Location = New System.Drawing.Point(113, 76)
        Me.txtperiodtd.Name = "txtperiodtd"
        Me.txtperiodtd.Size = New System.Drawing.Size(151, 23)
        Me.txtperiodtd.TabIndex = 11
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(48, 79)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 16)
        Me.Label8.TabIndex = 10
        Me.Label8.Text = "Period:"
        '
        'txtinttd
        '
        Me.txtinttd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtinttd.Location = New System.Drawing.Point(113, 110)
        Me.txtinttd.Name = "txtinttd"
        Me.txtinttd.Size = New System.Drawing.Size(151, 23)
        Me.txtinttd.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(35, 113)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(72, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Interest:"
        '
        'rdbtdauto
        '
        Me.rdbtdauto.AutoSize = True
        Me.rdbtdauto.Location = New System.Drawing.Point(8, 43)
        Me.rdbtdauto.Name = "rdbtdauto"
        Me.rdbtdauto.Size = New System.Drawing.Size(101, 20)
        Me.rdbtdauto.TabIndex = 3
        Me.rdbtdauto.Text = "Automatic"
        Me.rdbtdauto.UseVisualStyleBackColor = True
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(8, 23)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(78, 20)
        Me.RadioButton4.TabIndex = 2
        Me.RadioButton4.Text = "Manual"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'Banks
        '
        Me.Banks.Controls.Add(Me.grdBank)
        Me.Banks.Controls.Add(Me.Label13)
        Me.Banks.Controls.Add(Me.txtBank)
        Me.Banks.Location = New System.Drawing.Point(4, 25)
        Me.Banks.Name = "Banks"
        Me.Banks.Size = New System.Drawing.Size(537, 200)
        Me.Banks.TabIndex = 2
        Me.Banks.Text = "Banks"
        Me.Banks.UseVisualStyleBackColor = True
        '
        'grdBank
        '
        Me.grdBank.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grdBank.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight
        Me.grdBank.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdBank.Location = New System.Drawing.Point(3, 3)
        Me.grdBank.Name = "grdBank"
        Me.grdBank.ReadOnly = True
        Me.grdBank.Size = New System.Drawing.Size(531, 191)
        Me.grdBank.TabIndex = 70
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(6, 203)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(49, 16)
        Me.Label13.TabIndex = 69
        Me.Label13.Text = "Bank:"
        '
        'txtBank
        '
        Me.txtBank.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtBank.Location = New System.Drawing.Point(56, 200)
        Me.txtBank.Name = "txtBank"
        Me.txtBank.Size = New System.Drawing.Size(384, 23)
        Me.txtBank.TabIndex = 68
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(470, 260)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 30)
        Me.btnClose.TabIndex = 66
        Me.btnClose.Text = "Cancel"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        Me.btnClose.Visible = False
        '
        'btnupdate
        '
        Me.btnupdate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnupdate.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnupdate.Image = Global.WindowsApplication2.My.Resources.Resources.edit1
        Me.btnupdate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnupdate.Location = New System.Drawing.Point(314, 260)
        Me.btnupdate.Name = "btnupdate"
        Me.btnupdate.Size = New System.Drawing.Size(74, 30)
        Me.btnupdate.TabIndex = 64
        Me.btnupdate.Text = "Edit"
        Me.btnupdate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnupdate.UseVisualStyleBackColor = True
        Me.btnupdate.Visible = False
        '
        'btnsave
        '
        Me.btnsave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsave.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnsave.Image = Global.WindowsApplication2.My.Resources.Resources.new3
        Me.btnsave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsave.Location = New System.Drawing.Point(234, 260)
        Me.btnsave.Name = "btnsave"
        Me.btnsave.Size = New System.Drawing.Size(74, 30)
        Me.btnsave.TabIndex = 62
        Me.btnsave.Text = "New"
        Me.btnsave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsave.UseVisualStyleBackColor = True
        Me.btnsave.Visible = False
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(474, 260)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(71, 30)
        Me.btnCancel.TabIndex = 58
        Me.btnCancel.Text = "Close"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnapply
        '
        Me.btnapply.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnapply.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnapply.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnapply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnapply.Location = New System.Drawing.Point(394, 260)
        Me.btnapply.Name = "btnapply"
        Me.btnapply.Size = New System.Drawing.Size(74, 30)
        Me.btnapply.TabIndex = 56
        Me.btnapply.Text = "Apply"
        Me.btnapply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnapply.UseVisualStyleBackColor = True
        '
        'btndelete
        '
        Me.btndelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btndelete.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btndelete.Image = CType(resources.GetObject("btndelete.Image"), System.Drawing.Image)
        Me.btndelete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete.Location = New System.Drawing.Point(394, 260)
        Me.btndelete.Name = "btndelete"
        Me.btndelete.Size = New System.Drawing.Size(74, 30)
        Me.btndelete.TabIndex = 67
        Me.btndelete.Text = "Delete"
        Me.btndelete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete.UseVisualStyleBackColor = True
        Me.btndelete.Visible = False
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(546, 30)
        Me.PanePanel5.TabIndex = 54
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 5)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 19)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Bank Settings"
        '
        'frmBankSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(546, 292)
        Me.Controls.Add(Me.btndelete)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnCancel)
        Me.Controls.Add(Me.btnupdate)
        Me.Controls.Add(Me.btnapply)
        Me.Controls.Add(Me.PanePanel5)
        Me.Controls.Add(Me.btnsave)
        Me.Controls.Add(Me.tabbankset)
        Me.Name = "frmBankSettings"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmBankSettings"
        Me.tabbankset.ResumeLayout(False)
        Me.Savings.ResumeLayout(False)
        Me.Savings.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.TimeDeposit.ResumeLayout(False)
        Me.TimeDeposit.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Banks.ResumeLayout(False)
        Me.Banks.PerformLayout()
        CType(Me.grdBank, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tabbankset As System.Windows.Forms.TabControl
    Friend WithEvents Savings As System.Windows.Forms.TabPage
    Friend WithEvents TimeDeposit As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents rdbAuto As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtint As System.Windows.Forms.TextBox
    Friend WithEvents cboperiod As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboSavingAccount As System.Windows.Forms.ComboBox
    Friend WithEvents txtMaintainingBal As System.Windows.Forms.TextBox
    Friend WithEvents btnapply As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents cbotdsav As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtinttd As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents rdbtdauto As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents Banks As System.Windows.Forms.TabPage
    Friend WithEvents btnupdate As System.Windows.Forms.Button
    Friend WithEvents btnsave As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents cboCashAcc As System.Windows.Forms.ComboBox
    Friend WithEvents cboIntAcc As System.Windows.Forms.ComboBox
    Friend WithEvents cbotdcash As System.Windows.Forms.ComboBox
    Friend WithEvents cbotdint As System.Windows.Forms.ComboBox
    Friend WithEvents txtBank As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents grdBank As System.Windows.Forms.DataGridView
    Friend WithEvents btndelete As System.Windows.Forms.Button
    Friend WithEvents txtperiodtd As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
End Class
