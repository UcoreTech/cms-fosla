<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCapitalShare
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCapitalShare))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtSearchLastname = New System.Windows.Forms.TextBox()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.grdContribution = New System.Windows.Forms.DataGridView()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.lstMembers = New System.Windows.Forms.ListView()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.txtSearchnumber = New System.Windows.Forms.TextBox()
        Me.lstNumber = New System.Windows.Forms.ListBox()
        Me.optName = New System.Windows.Forms.RadioButton()
        Me.lstNames = New System.Windows.Forms.ListBox()
        Me.optNumber = New System.Windows.Forms.RadioButton()
        Me.loadingPic = New System.Windows.Forms.PictureBox()
        Me.crvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.bgwCapitalShare = New System.ComponentModel.BackgroundWorker()
        Me.GroupBox3.SuspendLayout()
        CType(Me.grdContribution, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel5.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtSearchLastname)
        Me.GroupBox3.Controls.Add(Me.cmdSearch)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Top
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(265, 50)
        Me.GroupBox3.TabIndex = 203
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Search Option"
        '
        'txtSearchLastname
        '
        Me.txtSearchLastname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchLastname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSearchLastname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchLastname.Location = New System.Drawing.Point(6, 18)
        Me.txtSearchLastname.Name = "txtSearchLastname"
        Me.txtSearchLastname.Size = New System.Drawing.Size(179, 20)
        Me.txtSearchLastname.TabIndex = 207
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearch.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSearch.Location = New System.Drawing.Point(191, 15)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(68, 23)
        Me.cmdSearch.TabIndex = 205
        Me.cmdSearch.Text = "Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'grdContribution
        '
        Me.grdContribution.AllowUserToAddRows = False
        Me.grdContribution.AllowUserToDeleteRows = False
        Me.grdContribution.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.grdContribution.BackgroundColor = System.Drawing.SystemColors.Window
        Me.grdContribution.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdContribution.GridColor = System.Drawing.Color.Black
        Me.grdContribution.Location = New System.Drawing.Point(3, 2)
        Me.grdContribution.Name = "grdContribution"
        Me.grdContribution.ReadOnly = True
        Me.grdContribution.Size = New System.Drawing.Size(646, 443)
        Me.grdContribution.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.exit17
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(581, 450)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(68, 26)
        Me.btnClose.TabIndex = 211
        Me.btnClose.Text = "Close"
        Me.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'lstMembers
        '
        Me.lstMembers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lstMembers.Location = New System.Drawing.Point(0, 50)
        Me.lstMembers.Name = "lstMembers"
        Me.lstMembers.Size = New System.Drawing.Size(265, 429)
        Me.lstMembers.TabIndex = 0
        Me.lstMembers.UseCompatibleStateImageBehavior = False
        '
        'PanePanel5
        '
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label19)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(921, 26)
        Me.PanePanel5.TabIndex = 53
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(3, 3)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(111, 19)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Contribution"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 26)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtSearchnumber)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lstNumber)
        Me.SplitContainer1.Panel1.Controls.Add(Me.optName)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lstNames)
        Me.SplitContainer1.Panel1.Controls.Add(Me.optNumber)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lstMembers)
        Me.SplitContainer1.Panel1.Controls.Add(Me.GroupBox3)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.loadingPic)
        Me.SplitContainer1.Panel2.Controls.Add(Me.crvRpt)
        Me.SplitContainer1.Panel2.Controls.Add(Me.grdContribution)
        Me.SplitContainer1.Panel2.Controls.Add(Me.btnClose)
        Me.SplitContainer1.Size = New System.Drawing.Size(921, 479)
        Me.SplitContainer1.SplitterDistance = 265
        Me.SplitContainer1.TabIndex = 213
        '
        'txtSearchnumber
        '
        Me.txtSearchnumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchnumber.Location = New System.Drawing.Point(61, 193)
        Me.txtSearchnumber.Name = "txtSearchnumber"
        Me.txtSearchnumber.Size = New System.Drawing.Size(137, 20)
        Me.txtSearchnumber.TabIndex = 206
        Me.txtSearchnumber.Visible = False
        '
        'lstNumber
        '
        Me.lstNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNumber.FormattingEnabled = True
        Me.lstNumber.Location = New System.Drawing.Point(61, 125)
        Me.lstNumber.Name = "lstNumber"
        Me.lstNumber.Size = New System.Drawing.Size(137, 28)
        Me.lstNumber.TabIndex = 208
        Me.lstNumber.Visible = False
        '
        'optName
        '
        Me.optName.AutoSize = True
        Me.optName.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optName.Location = New System.Drawing.Point(138, 228)
        Me.optName.Name = "optName"
        Me.optName.Size = New System.Drawing.Size(85, 17)
        Me.optName.TabIndex = 49
        Me.optName.TabStop = True
        Me.optName.Text = "Last Name"
        Me.optName.UseVisualStyleBackColor = True
        Me.optName.Visible = False
        '
        'lstNames
        '
        Me.lstNames.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lstNames.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstNames.FormattingEnabled = True
        Me.lstNames.Location = New System.Drawing.Point(61, 159)
        Me.lstNames.Name = "lstNames"
        Me.lstNames.Size = New System.Drawing.Size(137, 28)
        Me.lstNames.TabIndex = 209
        Me.lstNames.Visible = False
        '
        'optNumber
        '
        Me.optNumber.AutoSize = True
        Me.optNumber.Checked = True
        Me.optNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.optNumber.Location = New System.Drawing.Point(64, 228)
        Me.optNumber.Name = "optNumber"
        Me.optNumber.Size = New System.Drawing.Size(68, 17)
        Me.optNumber.TabIndex = 48
        Me.optNumber.TabStop = True
        Me.optNumber.Text = "Number"
        Me.optNumber.UseVisualStyleBackColor = True
        Me.optNumber.Visible = False
        '
        'loadingPic
        '
        Me.loadingPic.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.loadingPic.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.loadingPic.Location = New System.Drawing.Point(262, 254)
        Me.loadingPic.Name = "loadingPic"
        Me.loadingPic.Size = New System.Drawing.Size(133, 77)
        Me.loadingPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.loadingPic.TabIndex = 213
        Me.loadingPic.TabStop = False
        Me.loadingPic.Visible = False
        '
        'crvRpt
        '
        Me.crvRpt.ActiveViewIndex = -1
        Me.crvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.crvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvRpt.Location = New System.Drawing.Point(0, 0)
        Me.crvRpt.Name = "crvRpt"
        Me.crvRpt.ShowGroupTreeButton = False
        Me.crvRpt.ShowParameterPanelButton = False
        Me.crvRpt.ShowRefreshButton = False
        Me.crvRpt.Size = New System.Drawing.Size(652, 479)
        Me.crvRpt.TabIndex = 212
        Me.crvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        '
        'bgwCapitalShare
        '
        '
        'frmCapitalShare
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(921, 505)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.PanePanel5)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmCapitalShare"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Contributions"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.grdContribution, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.loadingPic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents txtSearchLastname As System.Windows.Forms.TextBox
    Friend WithEvents grdContribution As System.Windows.Forms.DataGridView
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents lstMembers As System.Windows.Forms.ListView
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents txtSearchnumber As System.Windows.Forms.TextBox
    Friend WithEvents lstNumber As System.Windows.Forms.ListBox
    Friend WithEvents optName As System.Windows.Forms.RadioButton
    Friend WithEvents lstNames As System.Windows.Forms.ListBox
    Friend WithEvents optNumber As System.Windows.Forms.RadioButton
    Friend WithEvents crvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents loadingPic As System.Windows.Forms.PictureBox
    Friend WithEvents bgwCapitalShare As System.ComponentModel.BackgroundWorker
End Class
