﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCapitalShare_Deposit_and_Withdrawal
    Dim newid As String
    Public xcase As String
    Public Sub newguidid()
        newid = Guid.NewGuid.ToString
    End Sub
    Private Sub frmCapitalShare_Deposit_and_Withdrawal_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        If txttotalContri.Text = "" Then
            txttotalContri.Text = 0.0
        Else
            Dim item As Double = txttotalContri.Text
            txttotalContri.Text = item.ToString("n2")
        End If
        
        LoadBankAcnt()
        ComboBox1.SelectedIndex = -1
        btnDeposit.Text = "...."
        MTcboBackAcnt.Visible = False
        Label11.Visible = False
        txtAmount.Text = "0.00"

        If frmOverpayment.chkCapShare.Checked = True Then

            Me.xcase = "Loan Overpmt Dep"
            Me.ComboBox1.SelectedIndex = 0
            Me.txtAmount.Text = frmOverpayment.grdOverpmt.CurrentRow.Cells("Amount").Value.ToString()
        Else
            Me.xcase = ""

        End If


    End Sub
    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click

        If xcase = "Loan Overpmt Dep" Then
            frmOverpayment.Close()
            frmOverpayment.Show()
            Me.Close()
        Else
            Me.Close()
        End If

    End Sub
    Private Sub LoadBankAcnt()
        Dim gcon As New Clsappconfiguration
        Dim DTBankAcnt As New DataTable("BankAcnt")
        Dim DR As DataRow
        Dim sSqlCmd As String = "CIMS_Payments_BankAccounts_Load"
        DTBankAcnt.Columns.Add("acnt_name", System.Type.GetType("System.String"))
        DTBankAcnt.Columns.Add("acnt_id", System.Type.GetType("System.String"))
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DR = DTBankAcnt.NewRow
                    DR("acnt_name") = rd.Item("acnt_name").ToString
                    DR("acnt_id") = rd.Item("acnt_id").ToString
                    DTBankAcnt.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadBankAcnt in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        Finally
            gcon.sqlconn.Close()
        End Try
        MTcboBackAcnt.Items.Clear()
        MTcboBackAcnt.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MTcboBackAcnt.SourceDataString = New String(1) {"acnt_name", "acnt_id"}
        MTcboBackAcnt.SourceDataTable = DTBankAcnt
    End Sub

    Private Sub txtAmount_KeyPress(sender As Object, e As System.Windows.Forms.KeyPressEventArgs) Handles txtAmount.KeyPress
        Try
            If ("0123456789.".IndexOf(e.KeyChar) = -1) Then
                If e.KeyChar <> Convert.ToChar(Keys.Back) Then
                    e.Handled = True
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub txtAmount_Leave(sender As Object, e As System.EventArgs) Handles txtAmount.Leave

        If txtAmount.Text = "" Then
            txtAmount.Text = "0.00"
        Else

            Dim item As Double = txtAmount.Text
            txtAmount.Text = item.ToString("n2")
        End If
    End Sub

    Private Sub UpdateStatus()
        newguidid()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CapitalShare_Deposit", _
                                      New SqlParameter("@fdTotAmt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@fdTransDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@fullname", lblMemberName.Text), _
                                      New SqlParameter("@user", frmMain.username.ToString), _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@employeeNo", lblMemberName.Tag))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub UpdateJV_ITEM()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CapitalShare_DepositItem", _
                                      New SqlParameter("@fdTotAmt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@fdTransDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@fullname", lblMemberName.Text), _
                                      New SqlParameter("@employeeNO", lblMemberName.Tag), _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@accnt", MTcboBackAcnt.SelectedItem.Col2.ToString))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub Update_OverPmt_IsCredited(ByVal pk_Overpayment As String, ByVal IsCredit As Boolean)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Overpayment_CreditToLoan", _
                                        New SqlParameter("@pk_Overpayment", pk_Overpayment), _
                                        New SqlParameter("@fbIsCredited", IsCredit))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()

        End Try
    End Sub
    Private Sub btnDeposit_Click(sender As System.Object, e As System.EventArgs) Handles btnDeposit.Click
        If Me.xcase = "Loan Overpmt Dep" Then
            Dim x As DialogResult = MessageBox.Show("Are you sure you want to do this transaction?", "QUESTION", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
            If x = Windows.Forms.DialogResult.OK Then
                Call UpdateStatus()
                Call UpdateJV_ITEM()
                Call Update_OverPmt_IsCredited(frmOverpayment.Get_pkOverpmt(), frmOverpayment.chkCapShare.Checked)
                Call updateContributionDebit()

                MessageBox.Show("Transaction has been Applied", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("Transaction Cancelled", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        ElseIf Me.xcase = "" Then
            If btnDeposit.Text = "...." Then
                MessageBox.Show("Please select a transaction", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
            ElseIf btnDeposit.Text = "Deposit" Then
                If MTcboBackAcnt.SelectedIndex = -1 Then
                    MessageBox.Show("Select Account", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                Else
                    If CDec(txtAmount.Text) = 0.0 Then
                        MessageBox.Show("Please Input the amount of transaction", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk)
                    Else
                        Dim x As DialogResult = MessageBox.Show("Are you sure you want to do this transaction?", "QUESTION", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                        If x = Windows.Forms.DialogResult.OK Then
                            Call UpdateStatus()
                            Call UpdateJV_ITEM()
                            Call updateContributionDebit()
                            Call loadcurrentBal()
                            Call conv2Dec()
                            MessageBox.Show("Transaction has been Applied", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            MessageBox.Show("Transaction Cancelled", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        End If
                    End If
                End If
            Else
                Dim x As DialogResult = MessageBox.Show("Are you sure you want to do this transaction?", "QUESTION", MessageBoxButtons.OKCancel, MessageBoxIcon.Question)
                If x = Windows.Forms.DialogResult.OK Then
                    If checkingBal(CDec(txtAmount.Text)) = True Then
                        Update_Bills()
                        Update_Bills_Expences()
                        updateContributionwithdrawal()
                        loadcurrentBal()
                        conv2Dec()
                        MessageBox.Show("Transaction has been Applied", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        MessageBox.Show("Insuficient Balance", "INFORMATION", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                Else
                    MessageBox.Show("Transaction Cancelled", "Notification", MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If
            End If
        End If

    End Sub
    Private Sub Update_Bills()
        newguidid()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CapitalShare_Withdraw", _
                                      New SqlParameter("@Amnt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@transDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@FullName", lblMemberName.Text), _
                                      New SqlParameter("@EmpNO", lblMemberName.Tag), _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@user", frmMain.username.ToString))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles ComboBox1.SelectedIndexChanged
        If ComboBox1.SelectedIndex = 0 Then
            btnDeposit.Text = "Deposit"
            MTcboBackAcnt.Visible = True
            Label11.Visible = True
            MTcboBackAcnt.Text = ""
        ElseIf ComboBox1.SelectedIndex = 1 Then
            btnDeposit.Text = "Withdrawal"
            MTcboBackAcnt.Visible = False
            Label11.Visible = False
        End If
    End Sub
    Private Function checkingBal(ByVal amnt As Decimal) As Boolean
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        Dim load As String = "select fcEmployeeNo,cast((sum(fdDepositAmount) - sum(fdWithdrawalAmount)) as decimal(18,2))[Total Contribution] from dbo.CIMS_m_Member a inner join dbo.CIMS_t_Member_Contributions b on fk_Employee =  pk_employee WHERE fcEmployeeNo =" & "'" & lblMemberName.Tag & "'  group by fcEmployeeNo"
        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, load)
        Try
            While rd.Read
                If amnt > rd.Item(1) Then
                    Return False
                Else
                    Return True
                End If
            End While
            rd.Close()
        Catch ex As Exception
        Finally
            gcon.sqlconn.Close()
        End Try
    End Function
    Private Sub Update_Bills_Expences()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CapitalShare_Withdraw_BIllsExpences", _
                                      New SqlParameter("@Amnt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@transDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@FullName", lblMemberName.Text), _
                                      New SqlParameter("@EmpNO", lblMemberName.Tag), _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@user", frmMain.username.ToString))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub updateContributionDebit()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateContribution_Insert", _
                                      New SqlParameter("@Amnt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@transDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@fullname", lblMemberName.Text), _
                                      New SqlParameter("@EmpNO", lblMemberName.Tag), _
                                      New SqlParameter("@AccntCode", MTcboBackAcnt.SelectedItem.Col2.ToString))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub updateContributionwithdrawal()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "UpdateContribution_Insert_Withdraw", _
                                      New SqlParameter("@Amnt", CDec(txtAmount.Text)), _
                                      New SqlParameter("@transDate", DateTimePicker1.Value.Date), _
                                      New SqlParameter("@fullname", lblMemberName.Text), _
                                      New SqlParameter("@EmpNO", lblMemberName.Tag))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
    Private Sub loadcurrentBal()
        Dim gcon As New Clsappconfiguration
        Dim rd As SqlDataReader
        Dim load As String = "select fcEmployeeNo,fcLastName +', '+ fcFirstName +' '+  fcMiddleName [full name], isnull((sum(fdDepositAmount) - sum(fdWithdrawalAmount)),0.00)[Total Contribution] from dbo.CIMS_m_Member a left outer join dbo.CIMS_t_Member_Contributions b on fk_Employee =  pk_employee WHERE fcEmployeeNo =" & "'" & lblMemberName.Tag & "'  group by fcEmployeeNo,fcLastName,fcFirstName,fcMiddleName"
        rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, load)
        Try
            While rd.Read
                txttotalContri.Text = rd.Item(2).ToString

            End While
            rd.Close()
        Catch ex As Exception
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
    Private Sub conv2Dec()
        If txttotalContri.Text = "" Then
            txttotalContri.Text = 0.0
        Else
            Dim item As Double = txttotalContri.Text
            txttotalContri.Text = item.ToString("n2")
        End If
    End Sub
End Class
