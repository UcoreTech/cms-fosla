Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmListofRqts

#Region "Public Variables"
    Public empNo As String
#End Region

#Region "Public Property"
    Public Property getempNumber() As String
        Get
            Return empNo
        End Get
        Set(ByVal value As String)
            empNo = value
        End Set
    End Property
#End Region

#Region "View Requirements Per Applicant"
    Private Sub ViewRequirementsforMembership()
        Dim gcon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("CIMS_Applicant_Retrieve_MembershipReqts", gcon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        gcon.sqlconn.Open()

        Try
            ad.SelectCommand = cmd
            ad.Fill(ds, "Cims_m_Applicant_Reqts")

            Me.grdMembershipRqts.DataSource = ds
            Me.grdMembershipRqts.DataMember = "Cims_m_Applicant_Reqts"
            Me.grdMembershipRqts.Columns(0).Visible = False
            Me.grdMembershipRqts.Columns(1).Width = 190

            cmd.ExecuteNonQuery()
            gcon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "View Requirements for Memebership")
        End Try
    End Sub
#End Region

#Region "Get Selected Requirements"
    Private Sub InsertUpdateSelectUpdates(ByVal empNo As String, ByVal fk_Rqts As String)
        Dim gcon As New Clsappconfiguration
        gcon.sqlconn.Open()
        Dim trans As SqlTransaction = gcon.sqlconn.BeginTransaction()

        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Applicants_AddEdit_MembershipReqts", _
                                      New SqlParameter("@empNo", empNo), _
                                      New SqlParameter("@pk_MembershipRqts", fk_Rqts), _
                                      New SqlParameter("@fbActive", 1))

            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Insert/Update Requirement")
        Finally
            gcon.sqlconn.Close()
        End Try
    End Sub
#End Region

    Private Sub cmdCancel_MembershipRqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCancel_MembershipRqts.Click
        Me.Close()
    End Sub

    Private Sub frmListofRqts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call ViewRequirementsforMembership()
    End Sub

    Private Sub cmdUpdate_MembershipRqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdUpdate_MembershipRqts.Click

        getempNumber() = frmNewMembersProcessing.GrdApplicantInfo.CurrentRow.Cells(0).Value

        Dim i As Integer
        Dim guid As String
        For i = 0 To Me.grdMembershipRqts.Rows.Count - 1
            If Me.grdMembershipRqts.Item(2, i).Value = True Then
                guid = Me.grdMembershipRqts.Item(0, i).Value
                Call InsertUpdateSelectUpdates(empNo, Me.grdMembershipRqts.Item(0, i).Value)
            End If
        Next
        Me.Close()
    End Sub
End Class