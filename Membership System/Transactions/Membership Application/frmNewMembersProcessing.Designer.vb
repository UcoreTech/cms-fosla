<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewMembersProcessing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstExistingLoans = New System.Windows.Forms.ListView()
        Me.cmdSearch = New System.Windows.Forms.Button()
        Me.txtSearchLastname = New System.Windows.Forms.TextBox()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.ListView2 = New System.Windows.Forms.ListView()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lstApplicants = New System.Windows.Forms.ListView()
        Me.GrdApplicantInfo = New System.Windows.Forms.DataGridView()
        Me.btnPrint_membDetails = New System.Windows.Forms.Button()
        Me.cmdCheck_rqts = New System.Windows.Forms.Button()
        Me.btnView_rqts = New System.Windows.Forms.Button()
        Me.btnClose_memberpocessing = New System.Windows.Forms.Button()
        Me.PanePanel3 = New WindowsApplication2.PanePanel()
        Me.lblWhatdouwant = New System.Windows.Forms.Label()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblMemberDetails = New System.Windows.Forms.Label()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.GrdApplicantInfo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel3.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'lstExistingLoans
        '
        Me.lstExistingLoans.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstExistingLoans.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lstExistingLoans.Location = New System.Drawing.Point(6, 32)
        Me.lstExistingLoans.Name = "lstExistingLoans"
        Me.lstExistingLoans.Size = New System.Drawing.Size(240, 531)
        Me.lstExistingLoans.TabIndex = 0
        Me.lstExistingLoans.UseCompatibleStateImageBehavior = False
        '
        'cmdSearch
        '
        Me.cmdSearch.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdSearch.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSearch.Location = New System.Drawing.Point(192, 5)
        Me.cmdSearch.Name = "cmdSearch"
        Me.cmdSearch.Size = New System.Drawing.Size(55, 23)
        Me.cmdSearch.TabIndex = 42
        Me.cmdSearch.Text = "Search"
        Me.cmdSearch.UseVisualStyleBackColor = True
        '
        'txtSearchLastname
        '
        Me.txtSearchLastname.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtSearchLastname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtSearchLastname.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtSearchLastname.Location = New System.Drawing.Point(6, 6)
        Me.txtSearchLastname.Name = "txtSearchLastname"
        Me.txtSearchLastname.Size = New System.Drawing.Size(182, 20)
        Me.txtSearchLastname.TabIndex = 48
        '
        'ListView1
        '
        Me.ListView1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView1.Location = New System.Drawing.Point(6, 32)
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(240, 531)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(192, 5)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(55, 23)
        Me.Button1.TabIndex = 42
        Me.Button1.Text = "Search"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox1.Location = New System.Drawing.Point(6, 6)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(182, 20)
        Me.TextBox1.TabIndex = 48
        '
        'ListView2
        '
        Me.ListView2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListView2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ListView2.Location = New System.Drawing.Point(6, 32)
        Me.ListView2.Name = "ListView2"
        Me.ListView2.Size = New System.Drawing.Size(240, 531)
        Me.ListView2.TabIndex = 0
        Me.ListView2.UseCompatibleStateImageBehavior = False
        '
        'Button2
        '
        Me.Button2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(192, 5)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(55, 23)
        Me.Button2.TabIndex = 42
        Me.Button2.Text = "Search"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.TextBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TextBox2.Location = New System.Drawing.Point(6, 6)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(182, 20)
        Me.TextBox2.TabIndex = 48
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.lstApplicants)
        Me.Panel1.Location = New System.Drawing.Point(5, 34)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(473, 454)
        Me.Panel1.TabIndex = 53
        '
        'lstApplicants
        '
        Me.lstApplicants.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lstApplicants.Location = New System.Drawing.Point(11, 4)
        Me.lstApplicants.Name = "lstApplicants"
        Me.lstApplicants.Size = New System.Drawing.Size(460, 444)
        Me.lstApplicants.TabIndex = 0
        Me.lstApplicants.UseCompatibleStateImageBehavior = False
        '
        'GrdApplicantInfo
        '
        Me.GrdApplicantInfo.AllowUserToDeleteRows = False
        Me.GrdApplicantInfo.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GrdApplicantInfo.BackgroundColor = System.Drawing.Color.White
        Me.GrdApplicantInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GrdApplicantInfo.Location = New System.Drawing.Point(485, 38)
        Me.GrdApplicantInfo.Name = "GrdApplicantInfo"
        Me.GrdApplicantInfo.ReadOnly = True
        Me.GrdApplicantInfo.Size = New System.Drawing.Size(629, 381)
        Me.GrdApplicantInfo.TabIndex = 55
        '
        'btnPrint_membDetails
        '
        Me.btnPrint_membDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrint_membDetails.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPrint_membDetails.Location = New System.Drawing.Point(826, 457)
        Me.btnPrint_membDetails.Name = "btnPrint_membDetails"
        Me.btnPrint_membDetails.Size = New System.Drawing.Size(154, 31)
        Me.btnPrint_membDetails.TabIndex = 67
        Me.btnPrint_membDetails.Text = "Print Member Details"
        Me.btnPrint_membDetails.UseVisualStyleBackColor = True
        '
        'cmdCheck_rqts
        '
        Me.cmdCheck_rqts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cmdCheck_rqts.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCheck_rqts.Location = New System.Drawing.Point(484, 457)
        Me.cmdCheck_rqts.Name = "cmdCheck_rqts"
        Me.cmdCheck_rqts.Size = New System.Drawing.Size(145, 30)
        Me.cmdCheck_rqts.TabIndex = 63
        Me.cmdCheck_rqts.Text = "View Requirements"
        Me.cmdCheck_rqts.UseVisualStyleBackColor = True
        '
        'btnView_rqts
        '
        Me.btnView_rqts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnView_rqts.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnView_rqts.Location = New System.Drawing.Point(635, 457)
        Me.btnView_rqts.Name = "btnView_rqts"
        Me.btnView_rqts.Size = New System.Drawing.Size(185, 30)
        Me.btnView_rqts.TabIndex = 65
        Me.btnView_rqts.Text = "Requirements for Approval"
        Me.btnView_rqts.UseVisualStyleBackColor = True
        '
        'btnClose_memberpocessing
        '
        Me.btnClose_memberpocessing.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose_memberpocessing.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose_memberpocessing.Location = New System.Drawing.Point(1039, 457)
        Me.btnClose_memberpocessing.Name = "btnClose_memberpocessing"
        Me.btnClose_memberpocessing.Size = New System.Drawing.Size(75, 29)
        Me.btnClose_memberpocessing.TabIndex = 66
        Me.btnClose_memberpocessing.Text = "Close"
        Me.btnClose_memberpocessing.UseVisualStyleBackColor = True
        '
        'PanePanel3
        '
        Me.PanePanel3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel3.Controls.Add(Me.lblWhatdouwant)
        Me.PanePanel3.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel3.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel3.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel3.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel3.Location = New System.Drawing.Point(485, 423)
        Me.PanePanel3.Name = "PanePanel3"
        Me.PanePanel3.Size = New System.Drawing.Size(629, 25)
        Me.PanePanel3.TabIndex = 64
        '
        'lblWhatdouwant
        '
        Me.lblWhatdouwant.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWhatdouwant.AutoSize = True
        Me.lblWhatdouwant.BackColor = System.Drawing.Color.Transparent
        Me.lblWhatdouwant.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblWhatdouwant.ForeColor = System.Drawing.Color.White
        Me.lblWhatdouwant.Location = New System.Drawing.Point(9, 1)
        Me.lblWhatdouwant.Name = "lblWhatdouwant"
        Me.lblWhatdouwant.Size = New System.Drawing.Size(208, 23)
        Me.lblWhatdouwant.TabIndex = 25
        Me.lblWhatdouwant.Text = "What do you want to do?"
        '
        'PanePanel1
        '
        Me.PanePanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblMemberDetails)
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(485, 6)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(629, 26)
        Me.PanePanel1.TabIndex = 54
        '
        'lblMemberDetails
        '
        Me.lblMemberDetails.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblMemberDetails.AutoSize = True
        Me.lblMemberDetails.BackColor = System.Drawing.Color.Transparent
        Me.lblMemberDetails.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMemberDetails.ForeColor = System.Drawing.Color.White
        Me.lblMemberDetails.Location = New System.Drawing.Point(9, 2)
        Me.lblMemberDetails.Name = "lblMemberDetails"
        Me.lblMemberDetails.Size = New System.Drawing.Size(176, 23)
        Me.lblMemberDetails.TabIndex = 20
        Me.lblMemberDetails.Text = "Member Information"
        '
        'PanePanel5
        '
        Me.PanePanel5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label19)
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(5, 6)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(474, 26)
        Me.PanePanel5.TabIndex = 52
        '
        'Label19
        '
        Me.Label19.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.Color.Transparent
        Me.Label19.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.White
        Me.Label19.Location = New System.Drawing.Point(6, 1)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(87, 23)
        Me.Label19.TabIndex = 14
        Me.Label19.Text = "Members"
        '
        'frmNewMembersProcessing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1116, 491)
        Me.Controls.Add(Me.PanePanel3)
        Me.Controls.Add(Me.btnPrint_membDetails)
        Me.Controls.Add(Me.cmdCheck_rqts)
        Me.Controls.Add(Me.btnView_rqts)
        Me.Controls.Add(Me.btnClose_memberpocessing)
        Me.Controls.Add(Me.GrdApplicantInfo)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.PanePanel5)
        Me.Name = "frmNewMembersProcessing"
        Me.Text = "New Members Processing"
        Me.Panel1.ResumeLayout(False)
        CType(Me.GrdApplicantInfo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel3.ResumeLayout(False)
        Me.PanePanel3.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstExistingLoans As System.Windows.Forms.ListView
    Friend WithEvents cmdSearch As System.Windows.Forms.Button
    Friend WithEvents txtSearchLastname As System.Windows.Forms.TextBox
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents ListView2 As System.Windows.Forms.ListView
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents lblMemberDetails As System.Windows.Forms.Label
    Friend WithEvents GrdApplicantInfo As System.Windows.Forms.DataGridView
    Friend WithEvents btnPrint_membDetails As System.Windows.Forms.Button
    Friend WithEvents cmdCheck_rqts As System.Windows.Forms.Button
    Friend WithEvents PanePanel3 As WindowsApplication2.PanePanel
    Friend WithEvents lblWhatdouwant As System.Windows.Forms.Label
    Friend WithEvents btnView_rqts As System.Windows.Forms.Button
    Friend WithEvents btnClose_memberpocessing As System.Windows.Forms.Button
    Friend WithEvents lstApplicants As System.Windows.Forms.ListView
End Class
