Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.Security

Public Class frmNewMembersProcessing

#Region "Public Variables"
    Private gcon As New Clsappconfiguration
    Private Empinfo As String

    Private editMode As Boolean = False
#End Region
#Region "Public Property"
    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
#End Region

#Region "View Applicant Info Using Employee Number"
    Public Sub ViewApplicantInfo(ByVal Empinfo As String)

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_ApplicantsInfo_GetInfo", _
                                          New SqlParameter("@employeeNo", Empinfo))

            With GrdApplicantInfo
                .DataSource = ds.Tables(0)
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

#Region "Load Applicant number"
    Private Sub Load_MemberNumber()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Applicant_GetApplicantList")
        While rd.Read
            lstApplicants.Items.Add(rd.Item("Employee No")).ToString()
        End While
        rd.Close()
        gCon.sqlconn.Close()

    End Sub
#End Region
#Region "Load Applicant fullname"
    Private Sub Load_MemberName()
        Dim gCon As New Clsappconfiguration
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Applicant_GetApplicantList")
        With Me.lstApplicants
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("IDnumber", 70, HorizontalAlignment.Left)
            .Columns.Add("Applicant's Fullname", 200, HorizontalAlignment.Left)
            Try
                While rd.Read
                    With .Items.Add(rd.Item(0))
                        .SubItems.Add(rd.Item(1))
                    End With
                End While
                rd.Close()
                gCon.sqlconn.Close()
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Load Applicant list")
            End Try
        End With
    End Sub
#End Region

    Private Sub frmNewMembersProcessing_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call Load_MemberName()
    End Sub

    Private Sub btnClose_memberpocessing_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose_memberpocessing.Click
        Me.Close()
    End Sub

    Private Sub lstApplicants_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lstApplicants.Click
        getEmpinfo() = Me.lstApplicants.SelectedItems(0).Text
        frmRqtsforApproval.getEmpinfo() = Me.lstApplicants.SelectedItems(0).Text
        Call ViewApplicantInfo(Me.lstApplicants.SelectedItems(0).Text)
        frmListofRqts.lblEmpNo.Text = Me.lstApplicants.SelectedItems(0).Text
    End Sub

    Private Sub cmdCheck_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdCheck_rqts.Click
        frmListofRqts.getempNumber = Me.lstApplicants.SelectedItems(0).Text
        frmListofRqts.Show()
    End Sub

    Private Sub btnView_rqts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView_rqts.Click
        frmRqtsforApproval.getEmpinfo() = Empinfo
        frmRqtsforApproval.Show()
    End Sub

    '==============================================================================================================================
    '===== NOTE: add to Ms.Bel's Code: ============================================================================================
    '==============================================================================================================================
    Private Sub RegisterMembersToWebsite(ByVal employeeNo As String)
        'Get Initial Security Credentials
        Dim password As String = ""
        Dim email As String = ""
        Dim securityQuestion As String = ""
        Dim securityAnswer As String = ""

        Using rd As SqlDataReader = ModifiedSqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_GetMemberSecurityCredentials", _
                New SqlParameter("@employeeNo", employeeNo))
            If rd.Read() Then
                password = rd.Item(0).ToString
                email = rd.Item(1).ToString
                securityQuestion = rd.Item(2).ToString
                securityAnswer = rd.Item(3).ToString
            End If
        End Using

        Dim status As MembershipCreateStatus
        Membership.CreateUser(employeeNo, password, email, securityQuestion, securityAnswer, True, status)
    End Sub

End Class