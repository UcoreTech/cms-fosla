Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient
IMports System.Text.RegularExpressions

Public Class frmLoan_Policy_PositionType
    Private gCon As New Clsappconfiguration()

    Private pkPolicyPositionType As String
    
    Private Sub frmLoan_Policy_PositionType_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call LoadPositionType()
        Call LoadPolicyPositionType()
        Call FormatPolicyGrid()
        Call ApplyDefaultButtons()
    End Sub

    Private Sub txtLowerLimit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtLowerLimit.KeyPress
        DecimalValidation(txtLowerLimit, e)
    End Sub

    Private Sub txtUpperLimit_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtUpperLimit.KeyPress
        DecimalValidation(txtUpperLimit, e)
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim policyID As String = System.Guid.NewGuid.ToString()
            Dim positionTypeID As String = cboPositionType.SelectedValue.ToString()
            Dim lowerLimit As Decimal = Decimal.Parse(txtLowerLimit.Text)
            Dim upperLimit As Decimal = Decimal.Parse(txtUpperLimit.Text)

            Call InsertUpdatePolicy(policyID, positionTypeID, lowerLimit, upperLimit)
            Call ApplyDefaultButtons()
        Catch ex As FormatException
            MessageBox.Show("You have entered an invalid Entry. Please re-check before proceeding.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
            txtLowerLimit.Focus()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
            Call ApplyDefaultButtons()
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        If gridPolicyPosition.Rows.Count <> 0 Then
            If gridPolicyPosition.CurrentRow.Cells("pkPolicies_PositionType").Value IsNot Nothing Then
                Try
                    Dim policyID As String = gridPolicyPosition.CurrentRow.Cells("pkPolicies_PositionType").Value.ToString()
                    Dim positionTypeID As String = cboPositionType.SelectedValue.ToString()
                    Dim lowerLimit As Decimal = Decimal.Parse(txtLowerLimit.Text)
                    Dim upperLimit As Decimal = Decimal.Parse(txtUpperLimit.Text)

                    Call InsertUpdatePolicy(policyID, positionTypeID, lowerLimit, upperLimit)
                    Call ApplyDefaultButtons()
                Catch ex As FormatException
                    MessageBox.Show("You have entered an invalid Entry. Please re-check before proceeding.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    txtLowerLimit.Focus()
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                    Call ApplyDefaultButtons()
                End Try
            Else
                MessageBox.Show("Please select the policy by clicking on the table before proceeding.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Question)
            End If
        End If
    End Sub



    Private Sub txtLowerLimit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLowerLimit.TextChanged
        If txtLowerLimit.Text = "" Then
            txtLowerLimit.Text = "0"
        End If
    End Sub

    Private Sub txtUpperLimit_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtUpperLimit.TextChanged
        If txtUpperLimit.Text = "" Then
            txtUpperLimit.Text = "0"
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Call ApplySaveMode()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Call ApplyDefaultButtons()
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Close()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If gridPolicyPosition.Rows.Count <> 0 Then
            Dim policyID As String = gridPolicyPosition.CurrentRow.Cells("pkPolicies_PositionType").Value.ToString()
            If policyID <> "" Then
                Call ApplyEditMode()
            Else
                MessageBox.Show("There's no selected policy to edit. Please select first.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Else
            MessageBox.Show("There is nothing to edit.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub gridPolicyPosition_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridPolicyPosition.CellClick
        Try
            cboPositionType.SelectedValue = gridPolicyPosition.CurrentRow.Cells("fk_PositionType").Value.ToString()
            txtUpperLimit.Text = gridPolicyPosition.CurrentRow.Cells("fdUpperLimit").Value.ToString()
            txtLowerLimit.Text = gridPolicyPosition.CurrentRow.Cells("fdLowerLimit").Value.ToString()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btndelete.Click
        If gridPolicyPosition.Rows.Count <> 0 Then
            Dim policyID As String = gridPolicyPosition.CurrentRow.Cells("pkPolicies_PositionType").Value.ToString()
            If policyID <> "" Then
                If MessageBox.Show("Are you sure you want to delete the selected policy", "Policy Position Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    Call DeletePolicy(policyID)
                End If
            Else
                MessageBox.Show("In order to delete you must select first a policy.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("There is nothing to delete.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub LoadPositionType()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Masterfile_PositionType")

        cboPositionType.DataSource = ds.Tables(0)
        cboPositionType.ValueMember = "pk_PositionType"
        cboPositionType.DisplayMember = "PositionTypeDesc"
    End Sub

    Private Sub LoadPolicyPositionType()
        Dim ds As New DataSet
        ds = SqlHelper.ExecuteDataset(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Policy_PositionType_Load")
        gridPolicyPosition.DataSource() = ds.Tables(0)
    End Sub

    Private Sub FormatPolicyGrid()
        With gridPolicyPosition
            .Columns("pkPolicies_PositionType").Visible = False
            .Columns("fk_PositionType").Visible = False

            .Columns("fdUpperLimit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("fdLowerLimit").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns("fdUpperLimit").DefaultCellStyle.Format = "n2"
            .Columns("fdLowerLimit").DefaultCellStyle.Format = "n2"

            .Columns("fdUpperLimit").HeaderText = "Upper Limit"
            .Columns("fdLowerLimit").HeaderText = "Lower Limit"
            .Columns("PositionTypeDesc").HeaderText = "Position Type"

        End With
    End Sub

    Private Sub InsertUpdatePolicy(ByVal policyID As String, ByVal positionTypeID As String, ByVal lowerLimit As Decimal, ByVal upperLimit As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Policy_PositionType_AddEdit", _
                    New SqlParameter("@pkPolicies_PositionType", policyID), _
                    New SqlParameter("@fk_PositionType", positionTypeID), _
                    New SqlParameter("@fdUpperLimit", upperLimit), _
                    New SqlParameter("@fdLowerLimit", lowerLimit))
            Call LoadPolicyPositionType()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DeletePolicy(ByVal policyID As String)
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Policy_PositionType_Delete", _
                New SqlParameter("@pkPolicies_PositionType", policyID))
            Call LoadPolicyPositionType()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ApplyDefaultButtons()
        cboPositionType.Enabled = False
        txtUpperLimit.Enabled = False
        txtLowerLimit.Enabled = False

        btnNew.Visible = True
        btnEdit.Visible = True
        btndelete.Visible = True
        btnclose.Visible = True

        btnUpdate.Visible = False
        btnSave.Visible = False
        btnCancel.Visible = False
    End Sub

    Private Sub ApplySaveMode()
        cboPositionType.Enabled = True
        txtUpperLimit.Enabled = True
        txtLowerLimit.Enabled = True

        txtUpperLimit.Text = ""
        txtLowerLimit.Text = ""

        cboPositionType.Focus()

        btnNew.Visible = False
        btnEdit.Visible = False
        btndelete.Visible = False
        btnclose.Visible = False

        btnUpdate.Visible = False
        btnSave.Visible = True
        btnCancel.Visible = True
    End Sub

    Private Sub ApplyEditMode()
        cboPositionType.Enabled = True
        txtUpperLimit.Enabled = True
        txtLowerLimit.Enabled = True

        txtLowerLimit.Text = gridPolicyPosition.CurrentRow.Cells("fdLowerLimit").Value
        txtUpperLimit.Text = gridPolicyPosition.CurrentRow.Cells("fdUpperLimit").Value
        cboPositionType.SelectedValue = gridPolicyPosition.CurrentRow.Cells("fk_PositionType").Value

        cboPositionType.Focus()

        btnNew.Visible = False
        btnEdit.Visible = False
        btndelete.Visible = False
        btnclose.Visible = False

        btnUpdate.Visible = True
        btnSave.Visible = False
        btnCancel.Visible = True
    End Sub

    Private Sub DecimalValidation(ByVal txtbox As TextBox, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim rex As Regex = New Regex("^[0-9]*[.]{0,1}[0-9]{0,1}$")

        If (Char.IsDigit(e.KeyChar) Or e.KeyChar.ToString() = "." Or e.KeyChar = CChar(ChrW(Keys.Back))) Then
            If (txtbox.Text.Trim() <> "") Then
                If (rex.IsMatch(txtbox.Text) = False And e.KeyChar <> CChar(ChrW(Keys.Back))) Then
                    e.Handled = True
                    MessageBox.Show("You are Not Allowed To Enter More then 2 Decimal!!")
                Else
                End If
            End If
        Else
            e.Handled = True
        End If
    End Sub


End Class