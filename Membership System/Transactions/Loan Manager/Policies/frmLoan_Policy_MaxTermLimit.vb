Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_Policy_MaxTermLimit
#Region "Variables"
    Private policyid As String
    Private maxtermid As String
#End Region
#Region "Property"
    Private Property getpolicyid() As String
        Get
            Return policyid
        End Get
        Set(ByVal value As String)
            policyid = value
        End Set
    End Property
    Private Property getmaxtermid() As String
        Get
            Return maxtermid
        End Get
        Set(ByVal value As String)
            maxtermid = value
        End Set
    End Property
#End Region
#Region "Function"
    Public Function Decimals(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 and '.' only.", MsgBoxStyle.Exclamation, "Invalid")
            Return False
        Else
            Return True
        End If
    End Function
#End Region
#Region "ClearText"
    Private Sub Cleartext()

        Me.txtTermfrom.Text = "0.0"
        Me.txtTermTo.Text = "0.0"
        Me.txtLower.Text = "0.00"
        Me.txtUpper.Text = "0.00"
    End Sub
#End Region
#Region "Policy Code without param"
    Private Sub Policycode()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Policies_Select")
        Try
            While rd.Read
                Me.cboloantype.Items.Add(rd.Item(0))
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Policycode")
        End Try

    End Sub
#End Region
#Region "Policy Code with param"
    Private Sub Policy(ByVal shareid As Integer)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_LoanType_Policies_SelectParam", _
                                     New SqlParameter("@fnPolicyCode", shareid))
        Try
            While rd.Read
                getpolicyid() = rd.Item(0)
            End While
            rd.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "PolicyID")
        End Try
    End Sub
#End Region
#Region "Add/Edit Max Terms"
    Private Sub AddEditMaxTerms(ByVal pk_PolicyMaxTerm As String, ByVal fk_LoanPolicies As String, ByVal fdTermFrom As Decimal, _
                                ByVal fdTermTo As Decimal, ByVal fdLowerLimit As Decimal, ByVal fdUpperLimit As Decimal)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Policy_MaxTermLimit_AddEdit", _
                                      New SqlParameter("@pk_PolicyMaxTerm", pk_PolicyMaxTerm), _
                                      New SqlParameter("@fk_LoanPolicies", fk_LoanPolicies), _
                                      New SqlParameter("@fdTermFrom", fdTermFrom), _
                                      New SqlParameter("@fdTermTo", fdTermTo), _
                                      New SqlParameter("@fdLowerLimit", fdLowerLimit), _
                                      New SqlParameter("@fdUpperLimit", fdUpperLimit))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "AddEditMaxTerms")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region
#Region "Delete"
    Private Sub Delete(ByVal maxid As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_Policy_MaxTermLimit_Delete", _
                                      New SqlParameter("@pk_PolicyMaxTerm", maxid))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Delete")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub
#End Region
#Region "View Max Terms"
    Private Sub ViewMaxTerms()
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Policies_MaxTerm_select")
        Try
            With Me.lvlMaxTerm
                .Clear()
                .View = View.Details
                .FullRowSelect = True
                .GridLines = True
                .Columns.Add("Max Term Code", 70, HorizontalAlignment.Left)
                .Columns.Add("Policy Description", 200, HorizontalAlignment.Left)
                .Columns.Add("Term From", 100, HorizontalAlignment.Right)
                .Columns.Add("Term To", 100, HorizontalAlignment.Right)
                .Columns.Add("Lower Limit", 100, HorizontalAlignment.Right)
                .Columns.Add("Upper Limit", 100, HorizontalAlignment.Right)

                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .SubItems.Add(rd.Item(1))
                            .SubItems.Add(rd.Item(2))
                            .SubItems.Add(rd.Item(3))
                            .SubItems.Add(Format(rd.Item(4), "#,##0.00"))
                            .SubItems.Add(Format(rd.Item(5), "#,##0.00"))
                            .SubItems.Add(rd.Item(6))
                            .SubItems.Add(rd.Item(7))
                            .SubItems.Add(rd.Item(8))
                        End With
                    End While
                    rd.Close()
                    mycon.sqlconn.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "ViewCapitalShare")
                End Try
            End With
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "View max share limits with param"
    Private Sub viewmaxshareparam(ByVal code As String)
        Dim mycon As New Clsappconfiguration
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_m_Policies_MaxTerm_param", _
                                     New SqlParameter("@fscode", code))
        Try
            With Me.lvlMaxTerm
                .Clear()
                .View = View.Details
                .FullRowSelect = True
                .GridLines = True
                .Columns.Add("Max Term Code", 70, HorizontalAlignment.Left)
                .Columns.Add("Policy Description", 200, HorizontalAlignment.Left)
                .Columns.Add("Term From", 100, HorizontalAlignment.Right)
                .Columns.Add("Term To", 100, HorizontalAlignment.Right)
                .Columns.Add("Lower Limit", 100, HorizontalAlignment.Right)
                .Columns.Add("Upper Limit", 100, HorizontalAlignment.Right)

                Try
                    While rd.Read
                        With .Items.Add(rd.Item(0))
                            .SubItems.Add(rd.Item(1))
                            .SubItems.Add(rd.Item(2))
                            .SubItems.Add(rd.Item(3))
                            .SubItems.Add(Format(rd.Item(4), "#,##0.00"))
                            .SubItems.Add(Format(rd.Item(5), "#,##0.00"))
                            .SubItems.Add(rd.Item(6))
                            .SubItems.Add(rd.Item(7))
                            .SubItems.Add(rd.Item(8))
                        End With
                    End While
                    rd.Close()
                    mycon.sqlconn.Close()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "ViewCapitalShare")
                End Try
            End With
        Catch ex As Exception

        End Try
    End Sub

#End Region
    Private Sub frmMaxtermLimitMaster_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.cboloantype.Text = frmLoan_Policy_Master.txtcode.Text
        If Me.cboloantype.Text = "" Then
            Call Policycode()
            Call ViewMaxTerms()
        Else
            Me.cboloantype.Text = frmLoan_Policy_Master.txtcode.Text
            Me.txttemp.Text = frmLoan_Policy_Master.txtcode.Text
            Call viewmaxshareparam(policyid)
            Call Policycode()
        End If
    End Sub

    Private Sub cboloantype_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboloantype.SelectedIndexChanged

    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Try
            If btnsave.Text = "New" Then
                Call Cleartext()
                'Call Policycode()
                btndelete.Visible = False
                btnupdate.Visible = False
                btnsave.Text = "Save"
                btnsave.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 4)
            ElseIf btnsave.Text = "Save" Then
                If Me.cboloantype.Text <> "" Then
                    Call AddEditMaxTerms("", policyid, Me.txtTermfrom.Text, Me.txtTermTo.Text, Me.txtLower.Text, Me.txtUpper.Text)
                    Call viewmaxshareparam(policyid)
                    btnsave.Text = "New"
                    btnsave.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(218, 4)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Policy is needed.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If
            Else
                MessageBox.Show("Empty field found! ", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If

        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.cboloantype.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(6).Text
                Me.txtTermfrom.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(2).Text
                Me.txtTermTo.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(3).Text
                Me.txtLower.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(4).Text
                Me.txtUpper.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(5).Text
                getmaxtermid() = Me.lvlMaxTerm.SelectedItems(0).SubItems(7).Text
                getpolicyid() = Me.lvlMaxTerm.SelectedItems(0).SubItems(8).Text

                Me.btnsave.Enabled = False
                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(141, 4)
                btndelete.Visible = False

            ElseIf btnupdate.Text = "Update" Then
                Call AddEditMaxTerms(maxtermid, policyid, Me.txtTermfrom.Text, Me.txtTermTo.Text, Me.txtLower.Text, Me.txtUpper.Text)
                Call viewmaxshareparam(policyid)
                Call Cleartext()
                btndelete.Visible = True
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(218, 4)
                btnclose.Text = "Close"
                Me.btnsave.Enabled = True
                Me.btndelete.Visible = True

            Else
                MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Try
            getmaxtermid() = Me.lvlMaxTerm.SelectedItems(0).SubItems(7).Text
            Dim x As New DialogResult
            x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            If x = System.Windows.Forms.DialogResult.OK Then
                Call Delete(maxtermid)
                Call viewmaxshareparam(policyid)
                Call Cleartext()
            ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            btnsave.Text = "New"
            btnsave.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(218, 4)
            btndelete.Visible = True
            btnupdate.Visible = True
            btnsave.Enabled = True
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub txtTermfrom_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTermfrom.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub

    Private Sub txtTermTo_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTermTo.KeyPress
        e.Handled = Not Decimals(e.KeyChar)
    End Sub
#Region "click/keypress/keydown"
    Private Sub lvlMaxtermkeypresskeydown()
        Try
            Me.txtTermfrom.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(2).Text
            Me.txtTermTo.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(3).Text
            Me.txtLower.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(4).Text
            Me.txtUpper.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(5).Text
            Me.cboloantype.Text = Me.lvlMaxTerm.SelectedItems(0).SubItems(6).Text
            getmaxtermid() = Me.lvlMaxTerm.SelectedItems(0).SubItems(7).Text
            getpolicyid() = Me.lvlMaxTerm.SelectedItems(0).SubItems(8).Text
        Catch ex As Exception
            MessageBox.Show(ex.Message, "lvlMaxtermkeypresskeydown")
        End Try
    End Sub
#End Region
    Private Sub lvlMaxTerm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvlMaxTerm.Click
        Call lvlMaxtermkeypresskeydown()
    End Sub
    Private Sub lvlMaxTerm_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlMaxTerm.KeyDown
        Call lvlMaxtermkeypresskeydown()
    End Sub
    Private Sub lvlMaxTerm_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles lvlMaxTerm.KeyUp
        Call lvlMaxtermkeypresskeydown()
    End Sub


    Private Sub cboloantype_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboloantype.TextChanged
        Call Policy(Me.cboloantype.Text)
        'Call viewmaxshareparam(policyid)
    End Sub

    Private Sub txttemp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txttemp.TextChanged
        'Call Policy(Me.cboloantype.Text)
        Call viewmaxshareparam(policyid)
    End Sub

    Private Sub lvlMaxTerm_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lvlMaxTerm.SelectedIndexChanged

    End Sub
End Class