﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmCheckLoanHistory

    Private con As New Clsappconfiguration
    Public loanNo As String
    Dim mIndex As Integer

    Private Sub frmCheckLoanHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadCol()
        LoadDetails(loanNo)
        _LoadDeductions_withEgg()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadDetails(ByVal loanno As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_GEtDetails_LoanRestructure",
                                                              New SqlParameter("@LoanNo", loanno))
            While rd.Read
                Me.Text = "Loan Balance of : " + rd(0).ToString
                lblName.Text = rd(1).ToString
                lblLoanType.Text = rd(2).ToString
                txtTerms.Text = rd(3).ToString
                txtPrincipal.Text = Format(CDec(rd(4)), "##,##0.00").ToString
                txtAdvInterest.Text = Format(CDec(rd(5)), "##,##0.00").ToString
                txtBalance.Text = Format(CDec(rd(6)), "##,##0.00").ToString
                txtNoofPaid.Text = rd(7).ToString
                txtNoofUnpaid.Text = rd(8).ToString
                txtRate.Text = rd(9).ToString
                txtAmtPaid.Text = Format(CDec(rd(10)), "##,##0.00").ToString
            End While
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _LoadDeductions_withEgg()
        Try
            mIndex = 0
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_ReportGenerate_Loantype_Charges",
                                                               New SqlParameter("@LoanNo", loanNo))
            While rd.Read
                With dgvDeductions
                    .Rows.Add()
                    .Item("acctref", mIndex).Value = rd(0)
                    .Item("acctcode", mIndex).Value = rd(1)
                    .Item("accttitle", mIndex).Value = rd(2)
                    .Item("amount", mIndex).Value = Format(CDec(rd(3).ToString), "##,##0.00")
                    mIndex += 1
                End With
            End While
            _TotalDeductions()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _TotalDeductions()
        Dim tot As Decimal = 0
        For i As Integer = 0 To dgvDeductions.RowCount - 1
            With dgvDeductions
                tot = tot + CDec(.Item("amount", i).Value.ToString)
            End With
        Next
        txtTotDeductions.Text = Format(tot, "##,##0.00")
    End Sub

#Region "Prep col"

    Private Sub _LoadCol()
        dgvDeductions.DataSource = Nothing
        dgvDeductions.Rows.Clear()
        dgvDeductions.Columns.Clear()
        Dim acctref As New DataGridViewTextBoxColumn
        Dim acctcode As New DataGridViewTextBoxColumn
        Dim accttitle As New DataGridViewTextBoxColumn
        Dim amount As New DataGridViewTextBoxColumn
        With acctref
            .Name = "acctref"
            .HeaderText = "Acct. Ref"
            .Width = 100
        End With
        With acctcode
            .Name = "acctcode"
            .HeaderText = "Code"
            .Width = 100
        End With
        With accttitle
            .Name = "accttitle"
            .HeaderText = "Acct. Title"
            .Width = 200
        End With
        With amount
            .Name = "amount"
            .HeaderText = "Amount"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Width = 100
        End With
        With dgvDeductions
            .Columns.Add(acctref)
            .Columns.Add(acctcode)
            .Columns.Add(accttitle)
            .Columns.Add(amount)
        End With
    End Sub

#End Region

    Private Sub btnRenew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRenew.Click
        If MsgBox("Renew this loan?", vbYesNo, "Confirmation") = vbYes Then
            frmExistingLoan.GetExistingLoan_Details()
            frmLoan_ApplicationMain.txtPrincipalAmount.Text = txtBalance.Text
            Me.Close()
        Else
            Exit Sub
        End If
    End Sub

End Class