﻿Imports System.Data
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmAddCheck

    Private con As New Clsappconfiguration

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub


    Private Sub frmAddCheck_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Me.LoadBanks()
    End Sub

    Private Sub LoadBanks()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "loanForRelease_SelectBank")
            With cboBank
                .DataSource = ds.Tables(0)
                .ValueMember = "Pk_BankID"
                .DisplayMember = "Bank"
            End With
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadCheck(ByVal bankid As String, ByVal search As String)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "loanForRelease_SelectCheck",
                                                         New SqlParameter("@BankID", bankid),
                                                         New SqlParameter("@Search", search))
            dgvChecks.DataSource = ds.Tables(0)
            dgvChecks.Columns(0).SortMode = DataGridViewColumnSortMode.NotSortable
            dgvChecks.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        Catch ex As Exception
        End Try
    End Sub

    Private Sub cboBank_SelectedValueChanged(sender As System.Object, e As System.EventArgs) Handles cboBank.SelectedValueChanged
        Me.LoadCheck(cboBank.SelectedValue.ToString(), txtSearch.Text)
    End Sub

    Private Sub txtSearch_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtSearch.TextChanged
        Me.LoadCheck(cboBank.SelectedValue.ToString(), txtSearch.Text)
    End Sub

    Private Sub dgvChecks_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvChecks.CellClick
        'Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub dgvChecks_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvChecks.CellDoubleClick
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

End Class