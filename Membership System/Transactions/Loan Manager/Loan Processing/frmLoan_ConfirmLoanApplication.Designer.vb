<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_ConfirmLoanApplication
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_ConfirmLoanApplication))
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.lblwhatdouwant = New System.Windows.Forms.Label()
        Me.picwhatdouwant = New System.Windows.Forms.PictureBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        CType(Me.picwhatdouwant, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.Location = New System.Drawing.Point(25, 89)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(232, 32)
        Me.cmdCancel.TabIndex = 17
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSubmit.Location = New System.Drawing.Point(25, 45)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(232, 33)
        Me.cmdSubmit.TabIndex = 16
        Me.cmdSubmit.Text = "Submit for Credit Committee Approval"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'lblwhatdouwant
        '
        Me.lblwhatdouwant.AutoSize = True
        Me.lblwhatdouwant.BackColor = System.Drawing.Color.Transparent
        Me.lblwhatdouwant.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblwhatdouwant.ForeColor = System.Drawing.Color.White
        Me.lblwhatdouwant.Location = New System.Drawing.Point(32, 6)
        Me.lblwhatdouwant.Name = "lblwhatdouwant"
        Me.lblwhatdouwant.Size = New System.Drawing.Size(213, 19)
        Me.lblwhatdouwant.TabIndex = 15
        Me.lblwhatdouwant.Text = "What do you want to do?"
        '
        'picwhatdouwant
        '
        Me.picwhatdouwant.BackColor = System.Drawing.Color.LimeGreen
        Me.picwhatdouwant.Location = New System.Drawing.Point(0, -1)
        Me.picwhatdouwant.Name = "picwhatdouwant"
        Me.picwhatdouwant.Size = New System.Drawing.Size(289, 27)
        Me.picwhatdouwant.TabIndex = 14
        Me.picwhatdouwant.TabStop = False
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblwhatdouwant)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(283, 32)
        Me.PanePanel1.TabIndex = 21
        '
        'frmLoan_ConfirmLoanApplication
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(283, 139)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.picwhatdouwant)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(291, 166)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(291, 166)
        Me.Name = "frmLoan_ConfirmLoanApplication"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Confirm"
        CType(Me.picwhatdouwant, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdSubmit As System.Windows.Forms.Button
    Friend WithEvents lblwhatdouwant As System.Windows.Forms.Label
    Friend WithEvents picwhatdouwant As System.Windows.Forms.PictureBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
End Class
