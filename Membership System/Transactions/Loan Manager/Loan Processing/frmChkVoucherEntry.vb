﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmChkVoucherEntry
    'Coded by : Vincent Nacar 
    Private gcon As New Clsappconfiguration
    Public LoanNo As String = ""
    Public empno As String
    Public fxkey As String
    Public xCtr As Integer
    Public totDebit As Double = 0
    Public totcredit As Double = 0
    Public keyresult As String = ""

    Dim i As Integer = 0

    Private Sub frmChkVoucherEntry_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cbDocNum.Text = ""
        txtParticulars.Text = ""
        totDebit = 0
        totcredit = 0
        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")
        LoadUser()
        'LoadCheck()
        GenerateLoanPaymentEntry()
        _Start_LoadEntry() '4/20/15
    End Sub

    Private Sub _Start_LoadEntry()
        If _LoadCustomEntry(LoanNo).HasRows Then
            dgvEntry.Rows.Clear()
            Dim rd As SqlDataReader = _LoadCustomEntry(LoanNo)
            While rd.Read
                With dgvEntry
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0).ToString
                    .Item("ClientName", i).Value = rd(1).ToString
                    .Item("LoanNo", i).Value = rd(2).ToString
                    .Item("AcctRef", i).Value = rd(3).ToString
                    .Item("Code", i).Value = rd(4).ToString
                    .Item("AccountTitle", i).Value = rd(5).ToString
                    .Item("Debit", i).Value = rd(6).ToString
                    .Item("Credit", i).Value = rd(7).ToString
                    i += 1
                End With
            End While
        Else
            LoadDetails()
            _LoadRebateLoan()
            _LoadRebateAmt()
        End If
    End Sub

#Region "Check if this loan have a customized entry"

    Private Function _LoadCustomEntry(ByVal loanNo As String) As SqlDataReader
        Try
            Return SqlHelper.ExecuteReader(gcon.cnstring, "_tblLoanEntry_Retrieve",
                                            New SqlParameter("@fkLoanNo", loanNo))
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region

    Private Sub GenerateLoanPaymentEntry()
        Dim btnselect As New DataGridViewCheckBoxColumn
        Dim IDNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim LoanNo As New DataGridViewTextBoxColumn
        Dim AcctRef As New DataGridViewTextBoxColumn
        Dim Code As New DataGridViewTextBoxColumn
        Dim AccountTitle As New DataGridViewTextBoxColumn
        Dim Debit As New DataGridViewTextBoxColumn
        Dim Credit As New DataGridViewTextBoxColumn

        With btnselect
            .Name = "btnselect"
            .HeaderText = "Select"
            .Width = 60
        End With
        With IDNo
            .HeaderText = "ID No"
            .Name = "IDNo"
            .DataPropertyName = "IDNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Client Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With LoanNo
            .HeaderText = "Loan No."
            .Name = "LoanNo"
            .DataPropertyName = "LoanNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctRef
            .HeaderText = "Acct Ref."
            .Name = "AcctRef"
            .DataPropertyName = "AcctRef"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Code
            .HeaderText = "Code"
            .Name = "Code"
            .DataPropertyName = "Code"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AccountTitle
            .HeaderText = "Account Title"
            .Name = "AccountTitle"
            .DataPropertyName = "AccountTitle"
            .ReadOnly = True
        End With
        With Debit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "Debit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            '.ReadOnly = True
        End With
        With Credit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "Credit"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            ' .ReadOnly = True
        End With
        With dgvEntry
            .Columns.Clear()
            .Columns.Add(btnselect)
            .Columns.Add(IDNo)
            .Columns.Add(ClientName)
            .Columns.Add(LoanNo)
            .Columns.Add(AcctRef)
            .Columns.Add(Code)
            .Columns.Add(AccountTitle)
            .Columns.Add(Debit)
            .Columns.Add(Credit)
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
        End With

    End Sub

    Private Sub LoadDetails()
        Dim rd1 As SqlDataReader
        Dim rd2 As SqlDataReader

        Dim idno As String = ""
        Dim clientname As String = ""
        Dim xloanno As String = ""

        If frmLoan_Approval.txtEmployeeNumber.Text = "" Then
            rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval",
                   New SqlParameter("@empNo", empno),
                   New SqlParameter("@LoanNo", LoanNo))
        Else
            rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval",
                   New SqlParameter("@empNo", frmLoan_Approval.txtEmployeeNumber.Text),
                   New SqlParameter("@LoanNo", LoanNo))
        End If

        'rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval_Details",
        '                               New SqlParameter("@LoanNo", LoanNo))
        rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "_Generate_Loantype_Charges",
                               New SqlParameter("@LoanNo", LoanNo))
        With dgvEntry
            While rd1.Read
                .Rows.Add()
                'added 8/15/2014 Vince
                idno = rd1("ID No.").ToString
                clientname = rd1("Name").ToString
                xloanno = rd1("Loan Ref.").ToString

                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = xloanno
                .Item("Code", i).Value = rd1("Code").ToString
                .Item("AccountTitle", i).Value = rd1("Account Title").ToString
                .Item("Debit", i).Value = rd1("Debit").ToString
                .Item("Credit", i).Value = "0.00"

                'totDebit = Val(.Item("Debit", i).Value)
                'txtDebit.Text = Format(Val(txtDebit.Text) + totDebit, "##,##0.00")
                i += 1
            End While
            While rd2.Read
                .Rows.Add()
                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = xloanno
                .Item("AcctRef", i).Value = rd2("Acct. Ref").ToString
                .Item("Code", i).Value = rd2("Code").ToString
                .Item("AccountTitle", i).Value = rd2("Description").ToString
                .Item("Debit", i).Value = rd2("Debit").ToString
                .Item("Credit", i).Value = rd2("Credit").ToString

                'totcredit = totcredit + Val(.Item("Credit", i).Value)
                'totDebit = totDebit + Val(.Item("Debit", i).Value)

                'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                'txtDebit.Text = Format(Val(totDebit), "##,##0.00")
                i += 1
            End While
        End With

        'txtNetproceeds.Text = Format((totDebit - totcredit), "##,##0.00")
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        txtDebit.Text = ""
        txtCredit.Text = ""
        GetJVKeyID = ""
        keyresult = "cancelled"
        i = 0
        dgvEntry.Columns.Clear()
        Me.Close()
    End Sub

    Private Sub LoadUser()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Cashier",
                                      New SqlParameter("@username", mod_UsersAccessibility.xUser))
        While rd.Read
            txtCreatedby.Text = rd("email").ToString
            'txtCashier.Text = rd("Fullname")
        End While
    End Sub

    Private Sub SaveEntry()

        If GetJVKeyID() = "" Then
            GetJVKeyID() = System.Guid.NewGuid.ToString()
        End If

        Dim MyGuid As Guid = New Guid(GetJVKeyID)
        fxkey = MyGuid.ToString

        '### Added by Vince 10/7/14
        Dim maxSerialNo As Integer = 0
        Dim dctype As String = ""
        If cboDoctype.Text = "CHECK VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CHV")
            dctype = "CHV"
        End If
        If cboDoctype.Text = "CASH VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "CV")
            dctype = "CV"
        End If
        If cboDoctype.Text = "JOURNAL VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "JV")
            dctype = "JV"
        End If
        If cboDoctype.Text = "ACCOUNTS PAYABLE VOUCHER" Then
            maxSerialNo = Generate_JournalNo_Serial(Date.Now, "APV")
            dctype = "APV"
        End If
        'Dim empno = dgvPaymentEntry.SelectedRows(0).Cells(0).Value.ToString

        SqlHelper.ExecuteNonQuery(gcon.cnstring, "CAS_t_GeneralJournal_Header_InsertUpdate_CMS",
                              New SqlParameter("@fxKeyJVNo", MyGuid),
                              New SqlParameter("@fdTransDate", txtDate.Text),
                              New SqlParameter("@fbAdjust", False),
                              New SqlParameter("@fcEmployeeNo", " "),
                              New SqlParameter("@fcMemo", " "),
                              New SqlParameter("@fdTotAmt", txtDebit.Text),
                              New SqlParameter("@user", txtCreatedby.Text),
                              New SqlParameter("@doctypeInitial", dctype),
                              New SqlParameter("@maxSerialNo", maxSerialNo),
                              New SqlParameter("@fiEntryNo", cbDocNum.Text),
                              New SqlParameter("@fbIsCancelled", False))

    End Sub

    Private jvKeyID As String

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialno As Integer
        ' Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialno = rd("maxSerialNo").ToString
            Else
                serialno = ""
            End If
            Return serialno
        End Using
    End Function

    Private Sub SaveDetails()
        Try
            Dim MyGuid As Guid = New Guid(fxkey)

            For xCtr = 0 To dgvEntry.RowCount - 1

                With dgvEntry

                    Dim accounttitle As String = IIf(IsDBNull(.Item("Code", xCtr).Value), Nothing, .Item("Code", xCtr).Value)
                    Dim empno As String = .Item("IDNo", xCtr).Value
                    Dim loan As String = .Item("LoanNo", xCtr).Value
                    Dim acctref As String = .Item("AcctRef", xCtr).Value
                    Dim debit As Double = .Item("Debit", xCtr).Value
                    Dim credit As Double = .Item("Credit", xCtr).Value

                    SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_t_tJVEntry_details_save",
                             New SqlParameter("@fiEntryNo", cbDocNum.Text),
                             New SqlParameter("@AccountCode", accounttitle),
                             New SqlParameter("@fdDebit", debit),
                             New SqlParameter("@fdCredit", credit),
                             New SqlParameter("@fcMemo", txtParticulars.Text),
                             New SqlParameter("@EmployeeNo", empno),
                             New SqlParameter("@fbBillable", False),
                             New SqlParameter("@fxKey", System.Guid.NewGuid),
                             New SqlParameter("@fk_JVHeader", MyGuid),
                             New SqlParameter("@transDate", txtDate.Text),
                             New SqlParameter("@fcLoanRef", loan),
                             New SqlParameter("@fcAccRef", acctref))

                End With

            Next
        Catch ex As Exception
            ' MessageBox.Show(ex.ToString)
            MsgBox(ex.ToString)
            'MessageBox.Show("Error! Duplicate ID of Accounts. Please ask Administrator.", "Oops")
            Exit Sub
        End Try

        xCtr = 0
    End Sub

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", cbDocNum.Text),
                                   New SqlParameter("@DateUsed", txtDate.Text))
    End Sub

    'Private Sub LoadCheck()
    '    cbDocNum.Items.Clear()
    '    Dim rd As SqlDataReader
    '    rd = SqlHelper.ExecuteReader(gcon.cnstring, "_LoadCHV")
    '    While rd.Read
    '        With cbDocNum
    '            .Items.Add(rd("fcDocNumber"))
    '        End With
    '    End While
    'End Sub

#Region "Apply Pay Calendar Schedule Date in Amortization 1-30-15"

    Private Sub _Apply_AmortDate_usingPayCalendar()
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Amortization_ApplyDate_UsingPayCalendar",
                                       New SqlParameter("@LoanNo", LoanNo),
                                       New SqlParameter("@EmpNo", frmLoan_Approval.txtEmployeeNumber.Text))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function is_withPaycode(ByVal empno As String) As Boolean ' apply this in Save Button
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_Check_Member_withPaycode",
                                                               New SqlParameter("@empno", frmLoan_Approval.txtEmployeeNumber.Text))
            If rd.HasRows Then
                While rd.Read
                    If rd(0) = 1 Then
                        Return True
                    Else
                        Return False
                    End If
                End While
            Else
                Return False
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

#End Region


    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If MessageBox.Show("Are you sure?", "Saving Document", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                If cbDocNum.Text = "" Then
                    MsgBox("Please Select Document No.!", vbInformation, "Oops!")
                    Exit Sub
                Else
                    SaveEntry()
                    SaveDetails()
                    saveCheck()
                    UpdateDocNumber()


                    frmMsgBox.txtMessage.Text = "Data Successfully Saved!"
                    frmMsgBox.ShowDialog()

                    GetJVKeyID = ""
                    'GenerateLoanPaymentEntry()
                    txtDebit.Text = ""
                    txtCredit.Text = ""

                    Me.keyresult = "Ok"
                    frmLoan_Approval.btnApprove.Enabled = False
                    frmLoan_Approval.btnDisapprove.Enabled = False
                    i = 0
                    'dgvEntry.Columns.Clear()
                    btnSave.Enabled = False
                    btnOutstandingLoan.Enabled = False
                    btnAddItem.Enabled = False
                    btnDelete.Enabled = False
                    'Me.Close()
                End If
            End If
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnOutstandingLoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOutstandingLoan.Click
        Dim frm As New frmOutstandingLoan
        frm.SelectLoan_byID(dgvEntry.Item("IDNo", 0).Value)
        frm.StartPosition = FormStartPosition.CenterScreen
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            With frm.dgvList.SelectedRows(0)
                Me.AddOutstandingLoans(.Cells(0).Value, .Cells(1).Value, .Cells(2).Value, .Cells(3).Value, .Cells(4).Value, .Cells(5).Value)
            End With
        End If
    End Sub

    Public Sub AddOutstandingLoans(ByVal id As String, ByVal clientname As String, ByVal loanref As String,
                                   ByVal code As String, ByVal accttitle As String, ByVal balance As String)
        Try
            With dgvEntry
                .Rows.Add()
                .Item("IDNo", i).Value = id
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = loanref
                .Item("Code", i).Value = code
                .Item("AccountTitle", i).Value = accttitle
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = balance
                'totcredit = totcredit + Val(.Item("Credit", i).Value)
                'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End With
            RecomputeNetproceed()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub RecomputeNetproceed()
        Dim subtotcredit As Decimal
        Dim subtotdebit As Decimal

        For x As Integer = 0 To dgvEntry.RowCount - 1
            With dgvEntry
                subtotcredit = subtotcredit + CDec(.Item("Credit", x).Value)
                subtotdebit = subtotdebit + CDec(.Item("Debit", x).Value)
                .Item("Debit", x).Value = Format(CDec(.Item("Debit", x).Value), "##,##0.00")
                .Item("Credit", x).Value = Format(CDec(.Item("Credit", x).Value), "##,##0.00")
            End With
        Next
        txtDebit.Text = Format(subtotdebit, "##,##0.00")
        txtCredit.Text = Format(subtotcredit, "##,##0.00")
        txtNetproceeds.Text = Format(CDec(txtDebit.Text) - CDec(txtCredit.Text), "##,##0.00")
    End Sub

    Private Sub dgvEntry_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvEntry.CellValueChanged
        RecomputeNetproceed()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        For Each row As DataGridViewRow In dgvEntry.Rows
            With dgvEntry
                If .Item("btnselect", row.Index).Value = True Then
                    .Rows.Remove(row)
                    i -= 1
                End If
            End With
        Next
        RecomputeNetproceed()
    End Sub

    Private Sub btnCheckNo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCheckNo.Click
        Dim frm As New frmBrowseCheck
        If cboDoctype.Text = "" Then
            MsgBox("Please Select Document Type to continue.", vbInformation, "Message")
            Exit Sub
        Else
            If cboDoctype.Text = "CHECK VOUCHER" Then
                frm.doctype = "CHV"
            End If
            If cboDoctype.Text = "CASH VOUCHER" Then
                frm.doctype = "CV"
            End If
            If cboDoctype.Text = "JOURNAL VOUCHER" Then
                frm.doctype = "JV"
            End If
            If cboDoctype.Text = "ACCOUNTS PAYABLE VOUCHER" Then
                frm.doctype = "APV"
            End If
            frm.StartPosition = FormStartPosition.CenterScreen
            If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
                Me.cbDocNum.Text = frm.dgvList.SelectedRows(0).Cells(0).Value.ToString
            End If
        End If
    End Sub

    Private Sub btnAddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddItem.Click
        Dim frm As New frmAddItem
        frm.xmode = ""
        frm.StartPosition = FormStartPosition.CenterScreen
        If frm.ShowDialog = Windows.Forms.DialogResult.OK Then
            If frm.cboViewSelected = "Chart of Accounts" Then
                Me.AddItems(" ", " ", " ", " ", frm.dgvItems.CurrentRow.Cells(0).Value.ToString, frm.dgvItems.CurrentRow.Cells(1).Value.ToString, "0.00")
            End If
            If frm.cboViewSelected = "Account Register" Then
                Me.AddItems(frm.dgvItems.CurrentRow.Cells(0).Value.ToString, frm.dgvItems.CurrentRow.Cells(1).Value.ToString, " ", frm.dgvItems.CurrentRow.Cells(2).Value.ToString, frm.dgvItems.CurrentRow.Cells(3).Value.ToString, frm.dgvItems.CurrentRow.Cells(4).Value.ToString, "0.00")
            End If
        End If
    End Sub

    Public Sub AddItems(ByVal id As String, ByVal clientname As String, ByVal loanref As String, ByVal acctref As String,
                               ByVal code As String, ByVal accttitle As String, ByVal balance As String)
        Try
            With dgvEntry
                .Rows.Add()
                .Item("IDNo", i).Value = id
                .Item("ClientName", i).Value = clientname
                .Item("LoanNo", i).Value = loanref
                .Item("AcctRef", i).Value = acctref
                .Item("Code", i).Value = code
                .Item("AccountTitle", i).Value = accttitle
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = balance
                'totcredit = totcredit + Val(.Item("Credit", i).Value)
                'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End With
            'RecomputeNetproceed()
        Catch ex As Exception
            Throw
        End Try
    End Sub

#Region "Rebates 12-9-14 Vince"

    Private Sub _LoadRebateLoan()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_REBATE_GenerateEntry",
                                                               New SqlParameter("@LoanNo", dgvEntry.Item("LoanNo", 0).Value.ToString))
            With dgvEntry
                While rd.Read
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0)
                    .Item("ClientName", i).Value = rd(1)
                    .Item("LoanNo", i).Value = rd(2)
                    .Item("AcctRef", i).Value = " "
                    .Item("Code", i).Value = rd(3)
                    .Item("AccountTitle", i).Value = rd(4)
                    .Item("Debit", i).Value = "0.00"
                    .Item("Credit", i).Value = rd(5)
                    'totcredit = totcredit + Val(.Item("Credit", i).Value)
                    'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                    i += 1
                End While
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _LoadRebateAmt()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_REBATE_GenerateEntry2",
                                                               New SqlParameter("@LoanNo", dgvEntry.Item("LoanNo", 0).Value.ToString))
            With dgvEntry
                While rd.Read
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0)
                    .Item("ClientName", i).Value = rd(1)
                    .Item("LoanNo", i).Value = rd(2)
                    .Item("AcctRef", i).Value = " "
                    .Item("Code", i).Value = rd(3)
                    .Item("AccountTitle", i).Value = rd(4)
                    .Item("Debit", i).Value = rd(5)
                    .Item("Credit", i).Value = "0.00"
                    'totcredit = totcredit + Val(.Item("Credit", i).Value)
                    'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                    i += 1
                End While
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

    Dim bankID As String = ""

    Private Sub btnAddCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCheck.Click
        If cbDocNum.Text = "" Then
            MessageBox.Show("Please select document number to continue.", "...")
            Exit Sub
        End If
        Dim frm As New frmAddCheck
        If frm.ShowDialog = DialogResult.OK Then
            Me.bankID = frm.cboBank.SelectedValue.ToString
            With dgvCheck
                .Rows.Clear()
                .Rows.Add(frm.cboBank.Text, frm.dgvChecks.SelectedRows(0).Cells(0).Value.ToString, txtDate.Text, txtNetproceeds.Text,
                          frmMain.lblCoopName.Text, Me.getPayee(dgvEntry.Rows(0).Cells(1).Value.ToString))
            End With
        End If
    End Sub

    Private Sub btnRemoveCheck_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveCheck.Click
        dgvCheck.Rows.Clear()
    End Sub

    Private Sub cboDoctype_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cboDoctype.SelectedIndexChanged
        If cboDoctype.Text = "CHECK VOUCHER" Then
            btnAddCheck.Visible = True
            btnRemoveCheck.Visible = True
            dgvCheck.Visible = True
        Else
            dgvCheck.Rows.Clear()
            btnAddCheck.Visible = False
            btnRemoveCheck.Visible = False
            dgvCheck.Visible = False
        End If
        Me.cbDocNum.Text = ""
    End Sub

    Private Sub txtNetproceeds_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtNetproceeds.TextChanged
        If dgvCheck.Rows.Count <> 0 Then
            dgvCheck.Rows(0).Cells(3).Value = txtNetproceeds.Text
        End If
    End Sub

    Private Sub saveCheck()
        Try
            If dgvCheck.Rows.Count <> 0 Then
                With dgvCheck.Rows(0)
                    SqlHelper.ExecuteNonQuery(gcon.cnstring, "loanForRelease_InsertCheck",
                                          New SqlParameter("@FX_CheckNumber", .Cells(1).Value.ToString),
                                          New SqlParameter("@FK_BankId", Me.bankID),
                                          New SqlParameter("@fiEntryNo", cbDocNum.Text),
                                          New SqlParameter("@fbIsUsed", True),
                                          New SqlParameter("@fcPayee", .Cells(5).Value.ToString),
                                          New SqlParameter("@fnAmount", CDec(.Cells(3).Value)),
                                          New SqlParameter("@fbIsCancelled", False),
                                          New SqlParameter("@fdCheckDate", .Cells(2).Value.ToString),
                                          New SqlParameter("@fcMaker", .Cells(4).Value.ToString))
                End With
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Function getPayee(ByVal idno As String)
        Dim xrd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "loanForRelease_SelectPayee",
                                                           New SqlParameter("@IDNO", idno))
        If xrd.HasRows Then
            While xrd.Read
                Return xrd(0).ToString
            End While
        End If
    End Function

    Private Sub btnPrintVoucher_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintVoucher.Click
        For xCtr = 0 To dgvEntry.RowCount - 1
            frmLoanVoucher_Report.xLoanNo = dgvEntry.Item("LoanNo", xCtr).Value
        Next
        frmLoanVoucher_Report.ShowDialog()
    End Sub
End Class