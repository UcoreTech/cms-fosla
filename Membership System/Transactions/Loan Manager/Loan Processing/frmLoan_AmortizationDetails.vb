﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmLoan_AmortizationDetails

    Private gcon As New Clsappconfiguration()

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmLoan_AmortizationDetails_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public Sub LoadAmortDetails(ByVal LoanAmt As String, ByVal interest As String, ByVal terms As String, ByVal datestart As String, ByVal mode As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_ShowAmortization_Computation",
                                       New SqlParameter("@Loan", LoanAmt),
                                       New SqlParameter("@InterestRate", interest),
                                       New SqlParameter("@PeriodInMonths", terms),
                                       New SqlParameter("@PaymentStartDate", datestart),
                                       New SqlParameter("@ComputeType", mode))
        dgvList.DataSource = ds.Tables(0)
    End Sub
End Class