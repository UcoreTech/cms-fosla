﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmIntMethodSetting

    Private con As New Clsappconfiguration

    Private Sub frmIntMethodSetting_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _colPrep()
        _rowPrep()
        _LoadDetails()
    End Sub

    Private Sub _rowPrep()
        Dim yr As Integer = 0
        For i As Integer = 0 To 12 - 1
            With dgvList
                .Rows.Add()
                yr += 1
                .Item("yr", i).Value = yr
                .Item("sInterest", i).Value = "0.00"
            End With
        Next
    End Sub
    Private Sub _colPrep()
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        dgvList.Rows.Clear()
        Dim yr As New DataGridViewTextBoxColumn
        Dim sAddon As New DataGridViewCheckBoxColumn
        Dim sDeduct As New DataGridViewCheckBoxColumn
        Dim sDim As New DataGridViewCheckBoxColumn
        Dim sActive As New DataGridViewCheckBoxColumn
        Dim sInterest As New DataGridViewTextBoxColumn
        With yr
            .Name = "yr"
            .HeaderText = "Year"
        End With
        With sAddon
            .Name = "sAddon"
            .HeaderText = "Add-On"
        End With
        With sDeduct
            .Name = "sDeduct"
            .HeaderText = "Deducted"
        End With
        With sDim
            .Name = "sDim"
            .HeaderText = "Diminishing"
        End With
        With sActive
            .Name = "sActive"
            .HeaderText = "Active"
        End With
        With sInterest
            .Name = "sInterest"
            .HeaderText = "Interest Rate (Monthly)"
        End With
        With dgvList
            .Columns.Add(yr)
            .Columns.Add(sAddon)
            .Columns.Add(sDeduct)
            .Columns.Add(sDim)
            .Columns.Add(sActive)
            .Columns.Add(sInterest)
        End With
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub dgvList_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellClick
        _LoadColor()
    End Sub

    Private Sub _LoadColor()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                If .Rows(i).Cells(4).Value = True Then
                    .Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                Else
                    .Rows(i).DefaultCellStyle.BackColor = Color.White
                End If
            End With
        Next
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    _InsertUpdate(.Item("yr", i).Value, .Item("sAddon", i).Value, .Item("sDeduct", i).Value, .Item("sDim", i).Value, .Item("sActive", i).Value, .Item("sInterest", i).Value)
                End With
            Next
            MsgBox("Item Saved.", vbInformation, "Success")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _InsertUpdate(ByVal yr As String, ByVal addon As Boolean, ByVal deduct As Boolean, ByVal dims As Boolean, ByVal actv As Boolean, ByVal interest As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_IntMethod_InsertUpdate",
                                       New SqlParameter("@fdYear", yr),
                                       New SqlParameter("@fbAddOn", addon),
                                       New SqlParameter("@fbDeduct", deduct),
                                       New SqlParameter("@fbDim", dims),
                                       New SqlParameter("@fbActive", actv),
                                       New SqlParameter("@fdInterest", interest))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _LoadDetails()
        Try
            Dim x As Integer = 0
            With dgvList
                Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_IntMethod_Load")
                While rd.Read
                    .Rows.Add()
                    .Item("yr", x).Value = rd(0)
                    .Item("sAddon", x).Value = rd(1)
                    .Item("sDeduct", x).Value = rd(2)
                    .Item("sDim", x).Value = rd(3)
                    .Item("sActive", x).Value = rd(4)
                    .Item("sInterest", x).Value = rd(5)
                    x += 1
                End While
            End With
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvList_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        _LoadColor()
    End Sub

End Class