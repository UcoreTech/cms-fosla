﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmApplyPenalty

    Private con As New Clsappconfiguration

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmApplyPenalty_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Public LoanRef As String = ""
    Public Sub LoadDefaultAccount(ByVal loantype As String)
        Try
            dgvList.Rows.Clear()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_Penalty_LoadAccount",
                                                              New SqlParameter("@LoanType", loantype))
            If rd.HasRows Then
                While rd.Read
                    dgvList.Rows.Add(LoanRef, rd(0), rd(1), "0.00")
                End While
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        SaveSetting()
    End Sub

    Private Sub SaveSetting()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Penalty_SaveSetting",
                                      New SqlParameter("@fnLoanNo", dgvList.CurrentRow.Cells(0).Value.ToString),
                                      New SqlParameter("@fdAmount", dgvList.CurrentRow.Cells(3).Value.ToString))
            frmLoan_ApplicationMain.btnApplyPenalty.Text = "Penalty : " + Format(CDec(dgvList.CurrentRow.Cells(3).Value.ToString), "##,##0.00")
            frmLoan_ApplicationMain.btnApplyPenalty.ForeColor = Color.Red
            Me.Close()
        Catch ex As Exception
        End Try
    End Sub
End Class