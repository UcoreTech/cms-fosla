﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class fmrLoan_Comaker

    Private mycon As New Clsappconfiguration


    Private Sub fmrLoan_Comaker_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SearchClients("")
    End Sub

    Private Sub SearchClients(ByVal search As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(mycon.cnstring, "_filter_Clients_Comaker",
                                       New SqlParameter("@Search", search))
        dgvList.DataSource = ds.Tables(0)
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        SearchClients(txtSearch.Text)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
        Dim empno As String = dgvList.SelectedRows(0).Cells(0).Value.ToString
        LoadReports(empno)
    End Sub

    Private Sub LoadReports(ByVal empno As String)
        frmPrint.LoadREport(empno)
        frmPrint.StartPosition = FormStartPosition.CenterScreen
        frmPrint.WindowState = FormWindowState.Maximized
        frmPrint.ShowDialog()
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        Dim ctr As Integer
        Dim xf As New frmLoan_ApplicationMain
        ctr = xf.xCtr
        With xf.dgvComakerList
            .Item("EmpNo", ctr).Value = dgvList.SelectedRows(0).Cells(0).Value
            .Item("ClientName", ctr).Value = dgvList.SelectedRows(0).Cells(1).Value
            .Item("DocNo", ctr).Value = dgvList.SelectedRows(0).Cells(2).Value
            .Item("AcctName", ctr).Value = dgvList.SelectedRows(0).Cells(3).Value
            .Item("Status", ctr).Value = dgvList.SelectedRows(0).Cells(4).Value
            .Item("Amount", ctr).Value = "0.00"
        End With

        xf.xCtr += 1
        Me.Close()
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        Dim ctr As Integer
        Dim xf As New frmLoan_ApplicationMain
        ctr = xf.xCtr
        With xf.dgvComakerList
            .Item("EmpNo", ctr).Value = ""
            .Item("ClientName", ctr).Value = dgvList.SelectedRows(0).Cells(1).Value
            .Item("DocNo", ctr).Value = dgvList.SelectedRows(0).Cells(2).Value
            .Item("AcctName", ctr).Value = dgvList.SelectedRows(0).Cells(3).Value
            .Item("Status", ctr).Value = dgvList.SelectedRows(0).Cells(4).Value
            .Item("Amount", ctr).Value = "0.00"
        End With

        xf.xCtr += 1
        Me.Close()
    End Sub
End Class