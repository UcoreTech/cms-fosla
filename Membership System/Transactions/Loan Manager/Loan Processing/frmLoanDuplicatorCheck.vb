Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Public Class frmLoanDuplicatorCheck
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Dim strCon As String = "Data Source=CHARL\SQLEXPRESS2K5;Initial Catalog=Coop_Membership_Test;Integrated Security=True"
        'Dim strSQL As String = "CIMS_Admin_GetMemberLoans"
        'Dim dataAdapter As New SqlClient.SqlDataAdapter(strSQL, strCon)
        'Dim table As New DataTable
        'dataAdapter.Fill(table)
        'GrdDupLoan.DataSource = table

        Call ViewLoanSummary()
        Call ViewLoanSummary2()
        If GrdDupLoan.Rows.Count = 0 Then
            btndelete.Enabled = False
        Else : btndelete.Enabled = True
        End If
        If DataGridView1.Rows.Count = 0 Then
            btnsave.Enabled = False
        Else : btnsave.Enabled = True
        End If
    End Sub
#Region "Get Loan Number based on EmpID"

    Public Sub ViewLoanSummary()

        Try
            Dim gcon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_GetMemberLoansForDeletion")
            With GrdDupLoan
                .DataSource = ds.Tables(0)
                '        .Columns(29).Visible = False
                '        .Columns(30).Visible = False
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

    Private Sub DeleteLoan(ByVal Loannum As String)
        Dim config As New Clsappconfiguration
        config.sqlconn.Open()
        Dim trans As SqlTransaction = config.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_t_Membership_Loans_Delete", _
                                      New SqlParameter("@fnLoan", Loannum))
            trans.Commit()
            MessageBox.Show("Loans have been successfully deleted.", "Duplicated Loans", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "DeleteLoan")
        Finally
            config.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        Dim x As New DialogResult

        x = MessageBox.Show("Are you sure you want to permanently delete this record? This is irreversible, Do you want to proceed?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        Try
            If x = System.Windows.Forms.DialogResult.OK Then
                Call DeleteLoan(Me.GrdDupLoan.CurrentRow.Cells(1).Value.ToString())
                Call ViewLoanSummary()
                Call ViewLoanSummary2()
            Else : Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Me.Show()
                Call ViewLoanSummary()
                Call ViewLoanSummary2()
            End If

        Catch ex As Exception
        End Try

        If GrdDupLoan.Rows.Count = 0 Then
            btndelete.Enabled = False
        Else : btndelete.Enabled = True
        End If
        If DataGridView1.Rows.Count = 0 Then
            btnsave.Enabled = False
        Else : btnsave.Enabled = True
        End If

        ' Call frmLoan_Processing.LoadAllLoanStatus(frmLoan_Processing.ListStatus.SelectedItems(0).Text)

        'Try
        '    Dim x As New DialogResult
        '    x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        '    If x = System.Windows.Forms.DialogResult.OK Then
        '        Call DeleteLoan(Me.GrdDupLoan.CurrentRow.Cells(1).Value)
        '        Call DeleteLoan(Me.GrdDupLoan.CurrentRow.Cells(1).Value)
        '    ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        '    End If
        'Catch ex As Exception
        'End Try
        'Call DeleteLoan(Me.GrdDupLoan.CurrentRow.Cells(1).Value)
        ''Dim strCon As String = "Data Source=CHARL\SQLEXPRESS2K5;Initial Catalog=Coop_Membership_Test;Integrated Security=True"
        ''Dim strSQL As String = "CIMS_Admin_GetMemberLoans"
        ''Dim dataAdapter As New SqlClient.SqlDataAdapter(strSQL, strCon)
        ''Dim table As New DataTable
        ''dataAdapter.Fill(table)
        ''GrdDupLoan.DataSource = table
    End Sub
#Region "Expired Loan"
    Public Sub ViewLoanSummary2()
        Try
            Dim gcon As New Clsappconfiguration
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "expiredloanview")
            With DataGridView1
                .DataSource = ds.Tables(0)
                '        .Columns(29).Visible = False
                '        .Columns(30).Visible = Fals
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub UpdateLoan(ByVal Update As String)
        Dim config As New Clsappconfiguration
        config.sqlconn.Open()
        Dim trans As SqlTransaction = config.sqlconn.BeginTransaction()
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Masterfiles_LoanValidity_UpdateLoanStatus", _
                                      New SqlParameter("@LoanNo", Update))
            trans.Commit()

            MessageBox.Show("Loans have been successfully validated.", "Expired Loans", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Update")
        Finally
            config.sqlconn.Close()
        End Try
    End Sub
    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click
        Dim x As New DialogResult

        x = MessageBox.Show("Are you sure you want to update this Loan? This is irreversible, Do you want to proceed?", "Confirm Update", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        Try
            If x = System.Windows.Forms.DialogResult.OK Then
                Call UpdateLoan(Me.DataGridView1.CurrentRow.Cells(1).Value.ToString())
                Call ViewLoanSummary()
                Call ViewLoanSummary2()
            Else : Me.DialogResult = Windows.Forms.DialogResult.Cancel
                Me.Show()
                Call ViewLoanSummary()
                Call ViewLoanSummary2()
            End If

        Catch ex As Exception
        End Try

        If GrdDupLoan.Rows.Count = 0 Then
            btndelete.Enabled = False
        Else : btndelete.Enabled = True
        End If
        If DataGridView1.Rows.Count = 0 Then
            btnsave.Enabled = False
        Else : btnsave.Enabled = True
        End If


    End Sub
#End Region

End Class