<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmloan_Penalties
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblSteptwo = New System.Windows.Forms.Label()
        Me.GridPenalties = New System.Windows.Forms.DataGridView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        CType(Me.GridPenalties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblSteptwo
        '
        Me.lblSteptwo.AutoSize = True
        Me.lblSteptwo.BackColor = System.Drawing.Color.Transparent
        Me.lblSteptwo.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblSteptwo.ForeColor = System.Drawing.Color.White
        Me.lblSteptwo.Location = New System.Drawing.Point(3, 5)
        Me.lblSteptwo.Name = "lblSteptwo"
        Me.lblSteptwo.Size = New System.Drawing.Size(116, 19)
        Me.lblSteptwo.TabIndex = 25
        Me.lblSteptwo.Text = "loan Penalties"
        '
        'GridPenalties
        '
        Me.GridPenalties.AllowUserToAddRows = False
        Me.GridPenalties.AllowUserToDeleteRows = False
        Me.GridPenalties.BackgroundColor = System.Drawing.Color.White
        Me.GridPenalties.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridPenalties.Location = New System.Drawing.Point(1, 31)
        Me.GridPenalties.Name = "GridPenalties"
        Me.GridPenalties.ReadOnly = True
        Me.GridPenalties.Size = New System.Drawing.Size(557, 152)
        Me.GridPenalties.TabIndex = 26
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblSteptwo)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(559, 30)
        Me.PanePanel1.TabIndex = 27
        '
        'frmloan_Penalties
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(559, 184)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.GridPenalties)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmloan_Penalties"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Penalties"
        CType(Me.GridPenalties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lblSteptwo As System.Windows.Forms.Label
    Friend WithEvents GridPenalties As System.Windows.Forms.DataGridView
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
End Class
