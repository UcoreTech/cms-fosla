﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.OleDb, System.IO
Imports System.Data.Sql
Imports WMPs_Money_Figure_Convert_to_Words
Imports System.Globalization
Public Class frmOverpayment
    Private gcon As New Clsappconfiguration
    Private Empinfo As String
    Private loanNumber As String
    Private pk_Overpmt As String
    Dim newid As String
    Public Sub newguidid()
        newid = Guid.NewGuid.ToString
    End Sub
    Public Property Get_pkOverpmt() As String
        Get
            Return pk_Overpmt

        End Get
        Set(value As String)
            pk_Overpmt = value

        End Set
    End Property


    Public Property getEmpinfo() As String
        Get
            Return Empinfo
        End Get
        Set(ByVal value As String)
            Empinfo = value
        End Set
    End Property
    Public Property GetLoanNo() As String
        Get
            Return loanNumber
        End Get
        Set(ByVal value As String)
            loanNumber = value
        End Set
    End Property

#Region "View Loan Summary Using Employee Number"
    Public Sub ViewOverpayment()

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Overpayment_LoadAll")
            With grdOverpmt
                .DataSource = ds.Tables(0)

                grdOverpmt.Columns("pk_Overpayment").Visible = False

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region
#Region "View Loan Summary Using Employee Number"
    Public Sub ViewExistingLoan(ByVal Empinfo As String)

        Try
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "LoanBalance_Adjustment_LoadExistingloan", _
                                          New SqlParameter("@fcEmployee", Empinfo))
            With grdExistingLoan
                .DataSource = ds.Tables(0)

            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region

#Region "Adjust Loan Payments"
    Private Sub Overpayment_CreditToLoan(ByVal pk_Overpayment As String, ByVal loanno As String, ByVal IsCredit As Boolean)

        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_Overpayment_CreditToLoan", _
                                      New SqlParameter("@pk_Overpayment", pk_Overpayment), _
                                      New SqlParameter("@loanno", loanno), _
                                     New SqlParameter("@fbIsCredited", IsCredit))


            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "Adjust Loan")
        Finally
            mycon.sqlconn.Close()
        End Try

    End Sub
#End Region

    Private Sub ValidateChange()
        If MessageBox.Show("This process is irreversible. Proceed?", _
                      "Loan Overpayment", MessageBoxButtons.YesNo, _
                              MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Call Overpayment_CreditToLoan(Get_pkOverpmt(), GetLoanNo(), 1)

            MsgBox("Loan successfully credited!", MsgBoxStyle.Information, "Loan Overpayment")

        Else
            MsgBox("Process cancelled by user!", MsgBoxStyle.Exclamation, "Loan Overpayment")
            Me.Close()

        End If
    End Sub

    Private Sub ValidateRefund()
        If MessageBox.Show("This process is irreversible. Proceed?", _
                      "Loan Overpayment", MessageBoxButtons.YesNo, _
                              MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Call InsertLoanOverpmtRefund_tBills(Me.getEmpinfo(), Me.grdOverpmt.CurrentRow.Cells("Full Name").Value.ToString(), _
                                         Me.grdOverpmt.CurrentRow.Cells("Amount").Value.ToString(), _
                                         Me.grdOverpmt.CurrentRow.Cells("Date").Value.ToString(), frmMain.username.ToString)

            Call InsertLoanOverpmtRefund_BillsExpenses(Me.getEmpinfo(), Me.grdOverpmt.CurrentRow.Cells("Full Name").Value.ToString(), _
                                               Me.grdOverpmt.CurrentRow.Cells("Amount").Value.ToString(), _
                                               Me.grdOverpmt.CurrentRow.Cells("Date").Value.ToString(), frmMain.username.ToString, grdOverpmt.CurrentRow.Cells("Loan No.").Value.ToString(), grdOverpmt.CurrentRow.Cells("pk_Overpayment").Value.ToString())

            MsgBox("Loan successfully credited!", MsgBoxStyle.Information, "Loan Overpayment")

        Else
            MsgBox("Process cancelled by user!", MsgBoxStyle.Exclamation, "Loan Overpayment")
            Me.Close()

        End If
    End Sub
    Private Sub frmOverpayment_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        Call ViewOverpayment()

        'Dim xForm As New frmCapitalShare_Deposit_and_Withdrawal
        'xForm.xcase = "Loan Overpmt Dep"

    End Sub
    Private Sub InsertLoanOverpmtRefund_tBills(ByVal empno As String, ByVal fullname As String, ByVal amount As Decimal, ByVal transdate As DateTime, ByVal user As String)

        newguidid()
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_LoanOverpayment_Refund", _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@EmpNO", empno), _
                                      New SqlParameter("@FullName", fullname), _
                                      New SqlParameter("@Amnt", CDec(amount)), _
                                      New SqlParameter("@transDate", transdate), _
                                      New SqlParameter("@user", user))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()

        End Try
    End Sub
    Private Sub InsertLoanOverpmtRefund_BillsExpenses(ByVal empno As String, ByVal fullname As String, ByVal amount As Decimal, _
                                                      ByVal transdate As DateTime, ByVal user As String, ByVal loanno As Integer, ByVal pk_overpmt As String)
        Dim mycon As New Clsappconfiguration
        mycon.sqlconn.Open()
        Dim trans As SqlTransaction = mycon.sqlconn.BeginTransaction
        Try
            SqlHelper.ExecuteNonQuery(trans, CommandType.StoredProcedure, "CIMS_LoanOverpayment_Refund_BillsExpenses", _
                                      New SqlParameter("@newid", newid), _
                                      New SqlParameter("@EmpNO", empno), _
                                      New SqlParameter("@FullName", fullname), _
                                      New SqlParameter("@Amnt", CDec(amount)), _
                                      New SqlParameter("@transDate", transdate), _
                                      New SqlParameter("@user", user), _
                                      New SqlParameter("@loanno", loanno), _
                                      New SqlParameter("@pk_Overpmt", pk_overpmt))
            trans.Commit()
        Catch ex As Exception
            trans.Rollback()
            MessageBox.Show(ex.Message, "UpdateStatus")
        Finally
            mycon.sqlconn.Close()
        End Try
    End Sub

    Private Sub grdOverpmt_Click(sender As System.Object, e As System.EventArgs) Handles grdOverpmt.Click

        Me.chkCapShare.Enabled = True
        Me.chkExistingLoan.Enabled = True
        Me.chkRefund.Enabled = True
        Me.chkPenalty.Enabled = True

        Me.chkExistingLoan.Checked = False
        Me.chkCapShare.Checked = False
        Me.chkRefund.Checked = False
        Me.chkPenalty.Checked = False

        If Me.chkExistingLoan.Checked = False Then

            grdExistingLoan.DataSource = Nothing

        End If

        Try
            getEmpinfo() = grdOverpmt.CurrentRow.Cells("Employee No.").Value.ToString()
            Get_pkOverpmt = grdOverpmt.CurrentRow.Cells("pk_Overpayment").Value.ToString()



            Dim gcon As New Clsappconfiguration
            Dim rd As SqlDataReader
            Dim load As String = "select fcEmployeeNo,fcLastName +', '+ fcFirstName +' '+  fcMiddleName [full name], (sum(fdDepositAmount) - sum(fdWithdrawalAmount))[Total Contribution] from dbo.CIMS_m_Member a inner join dbo.CIMS_t_Member_Contributions b on fk_Employee =  pk_employee WHERE fcEmployeeNo =" & "'" & grdOverpmt.CurrentRow.Cells("Employee No.").Value & "'  group by fcEmployeeNo,fcLastName,fcFirstName,fcMiddleName"
            rd = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, load)
            Try
                While rd.Read
                    frmCapitalShare_Deposit_and_Withdrawal.txttotalContri.Text = rd.Item(2).ToString

                End While
                rd.Close()
            Catch ex As Exception
            Finally
                gcon.sqlconn.Close()
            End Try

            'If frmCapitalShare_Deposit_and_Withdrawal.xcase = "Loan Overpmt Dep" Then

            frmCapitalShare_Deposit_and_Withdrawal.lblMemberName.Text = Me.grdOverpmt.CurrentRow.Cells("Full Name").Value.ToString()
            frmCapitalShare_Deposit_and_Withdrawal.lblMemberName.Tag = Me.getEmpinfo()
            frmCapitalShare_Deposit_and_Withdrawal.ComboBox1.SelectedIndex = 1

            frmCapitalShare_Deposit_and_Withdrawal.DateTimePicker1.Value = Me.grdOverpmt.CurrentRow.Cells("Date").Value.ToString()
            frmCapitalShare_Deposit_and_Withdrawal.txtAmount.Text = Me.grdOverpmt.CurrentRow.Cells("Amount").Value.ToString()

            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        

    End Sub


    Private Sub chkExistingLoan_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkExistingLoan.CheckedChanged
        If Me.chkExistingLoan.Checked = True Then
            Me.chkCapShare.Enabled = False
            grdExistingLoan.Enabled = True
            Me.chkRefund.Checked = False
            Me.chkRefund.Enabled = False
            chkPenalty.Enabled = False

            Call ViewExistingLoan(getEmpinfo())


        Else
            Me.chkCapShare.Enabled = True
            Me.chkExistingLoan.Checked = False
            Me.chkExistingLoan.Enabled = True
            Me.chkRefund.Enabled = True
            chkPenalty.Enabled = True

        End If
    End Sub

    Private Sub grdExistingLoan_Click(sender As System.Object, e As System.EventArgs) Handles grdExistingLoan.Click
        Try

            GetLoanNo() = grdExistingLoan.CurrentRow.Cells("Loan No.").Value.ToString()

            Me.btnCreditOverpmt.Enabled = True


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnCreditOverpmt_Click(sender As System.Object, e As System.EventArgs) Handles btnCreditOverpmt.Click
        If chkExistingLoan.Checked = True Then
            Call ValidateChange()
            Call ViewOverpayment()

        ElseIf chkRefund.Checked = True Then
            Call ValidateRefund()
            Call ViewOverpayment()

            End If

    End Sub

    Private Sub chkCapShare_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkCapShare.CheckedChanged
        If Me.chkCapShare.Checked = True Then
            Me.chkExistingLoan.Enabled = False
            Me.chkRefund.Checked = False
            Me.chkRefund.Enabled = False
            Me.chkPenalty.Enabled = False

            grdExistingLoan.Enabled = False

            Dim xForm As New frmCapitalShare_Deposit_and_Withdrawal
            xForm.xcase = "Loan Overpmt Dep"

            frmCapitalShare_Deposit_and_Withdrawal.DateTimePicker1.Value = Me.grdOverpmt.CurrentRow.Cells("Date").Value.ToString()
            frmCapitalShare_Deposit_and_Withdrawal.txtAmount.Text = Me.grdOverpmt.CurrentRow.Cells("Amount").Value.ToString()

            frmCapitalShare_Deposit_and_Withdrawal.ShowDialog()


        ElseIf Me.chkCapShare.Checked = False Then
            Me.chkCapShare.Enabled = True
            Me.chkCapShare.Checked = False
            Me.chkExistingLoan.Enabled = True
            Me.chkRefund.Enabled = True
            Me.chkPenalty.Enabled = True
           

        End If



    End Sub

    Private Sub chkRefund_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkRefund.CheckedChanged
        If chkRefund.Checked = True Then
            btnCreditOverpmt.Enabled = True
        End If
      
    End Sub
End Class