Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared

Public Class frmLoan_ReportOptions

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

#Region "Property Var"
    Public loanNumber As String
    Public Property GetLoanNo() As String
        Get
            Return loanNumber
        End Get
        Set(ByVal value As String)
            loanNumber = value
        End Set
    End Property

    Public AmtWords As String
    Public Property getAmtWords() As String
        Get
            Return AmtWords
        End Get
        Set(ByVal value As String)
            AmtWords = value
        End Set
    End Property
#End Region
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub frmLoanReports_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    End Sub


    Private Sub BtnPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnPreview.Click
        If Me.chkLAP.Checked = True Then

            Try
                Dim reports As New CooperativeReports()
                reports.MdiParent = frmMain
                reports.Show()

                reports.Text = "Loan Application Form"
                Dim gCon As New Clsappconfiguration
                Dim rptSummary As New ReportDocument
                rptSummary.Load(Application.StartupPath & "\LoanReport\LoanApplicationForm.rpt")
                Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                rptSummary.Refresh()
                rptSummary.SetParameterValue("@loanNo", GetLoanNo())
                reports.appreports.ReportSource = rptSummary
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try
        End If
        If Me.chkALD.Checked = True Then

            Try
                Dim reports As New CooperativeReports()
                reports.MdiParent = frmMain
                reports.Show()

                reports.Text = "Approved Loans Details"
                Dim gCon As New Clsappconfiguration
                Dim rptSummary As New ReportDocument
                rptSummary.Load(Application.StartupPath & "\LoanReport\LoanApprovedDetails.rpt") '"\LoanReport\LoanApprovedDetails.rpt")
                Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                rptSummary.Refresh()
                rptSummary.SetParameterValue("@loanNo", GetLoanNo())
                reports.appreports.ReportSource = rptSummary
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try

        End If
        If Me.chkPF.Checked = True Then
            Try
                Try
                    Dim reports As New CooperativeReports()
                    reports.MdiParent = frmMain
                    reports.Show()

                    reports.Text = "Pledge Form"
                    Dim gCon As New Clsappconfiguration
                    Dim rptSummary As New ReportDocument
                    With rptSummary
                        .Load(Application.StartupPath & "\LoanReport\PledgeForm.rpt")
                        Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                        .Refresh()
                        .SetParameterValue("@loanNo", GetLoanNo())
                        .SetParameterValue("@boardMember1", "")
                        .SetParameterValue("@boardMember2", "")
                        .SetParameterValue("@hrRep", "")
                        .SetParameterValue("@acctgRep", "")
                        reports.appreports.ReportSource = rptSummary
                    End With

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End Try

            Catch ex As Exception
                MessageBox.Show("Supply the given Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try


        End If

        If Me.chkEF.Checked = True Then

            Try
                Dim reports As New CooperativeReports()
                reports.MdiParent = frmMain
                reports.Show()

                reports.Text = "Personal Evaluation Sheet"
                Dim gCon As New Clsappconfiguration
                Dim rptSummary As New ReportDocument
                rptSummary.Load(Application.StartupPath & "\LoanReport\Evaluation Sheet.rpt")
                Logon(rptSummary, gCon.Server, gCon.Database, gCon.Username, gCon.Password)
                rptSummary.Refresh()
                reports.appreports.ReportSource = rptSummary
            Catch ex As Exception
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End Try

        End If

        Close()
    End Sub
#Region "Calling report"
    'Try
    '    Dim con As New clsPersonnel
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    Dim objreport As New CrystalDecisions.CrystalReports.Engine.ReportDocument
    '    objreport.Load(Application.StartupPath & "\LoanReport\LoanApplicationForm.rpt")
    '    Logon(objreport, con.servername, con.databasename, con.username1, con.password1)
    '    objreport.Refresh()
    '    objreport.SetParameterValue("@loanNo", loanNumber) 'Me.txttitle.Text)
    '    Applicant_Reports.appreports.ReportSource = objreport
    '    'Applicant_Reports.MdiParent = Form1
    '    Applicant_Reports.ShowDialog()
    'Catch ex As Exception
    '    MessageBox.Show ("Supply the given Parameter", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    'End Try
#End Region


    Private Sub chkALD_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles chkALD.CheckedChanged

    End Sub
End Class