﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmSelectPayCode

    Private con As New Clsappconfiguration

    Public selectedpaycode As Integer

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmSelectPayCode_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadPaycodes()
    End Sub

    Private Sub LoadPaycodes()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "LoanApplication_SelectPayCode")
            dgvList.DataSource = ds.Tables(0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnSelect_Click(sender As System.Object, e As System.EventArgs) Handles btnSelect.Click
        Me.selectedpaycode = CInt(Me.dgvList.SelectedRows(0).Cells(0).Value.ToString)
        Me.Close()
    End Sub

End Class