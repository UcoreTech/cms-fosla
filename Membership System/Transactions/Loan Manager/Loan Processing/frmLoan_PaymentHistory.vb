Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoan_PaymentHistory
    Private gcon As New Clsappconfiguration

    Private fkLoanPayment As String
    Public Property GetSetFkLoanPayment() As String
        Get
            Return fkLoanPayment
        End Get
        Set(ByVal value As String)
            fkLoanPayment = value
        End Set
    End Property

    Private supplierName As String
    Public Property GetSetSupplierName() As String
        Get
            Return supplierName
        End Get
        Set(ByVal value As String)
            supplierName = value
        End Set
    End Property

    Private loanNo As String
    Public Property GetSetLoanNo() As String
        Get
            Return loanNo
        End Get
        Set(ByVal value As String)
            loanNo = value
        End Set
    End Property

    Private loanType As String
    Public Property GetSetLoanType() As String
        Get
            Return loanType
        End Get
        Set(ByVal value As String)
            loanType = value
        End Set
    End Property

    Private balance As Decimal
    Public Property GetSetBalance() As Decimal
        Get
            Return balance
        End Get
        Set(ByVal value As Decimal)
            balance = value
        End Set
    End Property

    Private payment As Decimal
    Public Property GetSetTotalPayment() As Decimal
        Get
            Return payment
        End Get
        Set(ByVal value As Decimal)
            payment = value
        End Set
    End Property


    Private Sub frmLoanPaymentHistory_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim loanPayments As DataSet
        loanPayments = GetLoanPayments(GetSetFkLoanPayment())

        Call InitializeForm()
        Call LoadDataToGrid(loanPayments)
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Function GetLoanPayments(ByVal fkLoanPayment As String) As DataSet

        Dim ds As New DataSet

        Try
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "MSS_LoanTracking_GetPayments", _
                        New SqlParameter("@fk_Member_Loan", fkLoanPayment))


        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return ds
    End Function
    Private Sub LoadDataToGrid(ByVal loanPayment As DataSet)
        grdLoanPayments.DataSource = loanPayment.Tables(0)

        grdLoanPayments.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        grdLoanPayments.Columns("No.").Visible = False
        grdLoanPayments.Columns("fk_Member_Loan").Visible = False
        grdLoanPayments.Columns("Particulars").AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
        grdLoanPayments.Columns("Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        grdLoanPayments.Columns("Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        grdLoanPayments.Columns("Amount").DefaultCellStyle.Format = "##,##0.00"
    End Sub
    Private Sub InitializeForm()
        lblName.Text = GetSetSupplierName()
        lblLoanType.Text = GetSetLoanType().ToString + " Loan No: " + GetSetLoanNo().ToString
        lblBalance.Text = "Balance: " + Format(GetSetBalance(), "##,##0.00")
        lblTotalPayment.Text = "Total Payment: " + Format(GetSetTotalPayment(), "##,##0.00")
    End Sub


    Private Sub frmLoanPaymentHistory_KeyUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyUp
        If e.KeyCode = Keys.Escape Then
            Me.Close()
        End If
    End Sub

    Private Sub frmLoanPaymentHistory_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub frmLoanPaymentHistory_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
End Class