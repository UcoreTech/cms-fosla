﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmBrowseCheck

    Private con As New Clsappconfiguration
    Public doctype As String

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        'Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
    End Sub

    Private Sub LoadDoc(ByVal search As String)
        dgvList.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_LoadDocNumbers_byType",
                                      New SqlParameter("@search", search),
                                      New SqlParameter("@doctype", doctype))
        dgvList.DataSource = ds.Tables(0)
        dgvList.Columns(0).HeaderText = "Document No."
    End Sub

    Private Sub frmBrowseCheck_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        txtSearch.Text = ""
        ActiveControl = txtSearch
        LoadDoc("")
    End Sub

    Private Sub btnSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelect.Click
        'frmChkVoucherEntry.cbDocNum.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
        'Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        'frmChkVoucherEntry.cbDocNum.Text = dgvList.SelectedRows(0).Cells(0).Value.ToString
        'Me.Close()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged
        LoadDoc(txtSearch.Text)
    End Sub

End Class