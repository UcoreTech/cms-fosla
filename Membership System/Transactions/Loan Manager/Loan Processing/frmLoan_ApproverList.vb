﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoan_ApproverList
    Public Employee As String
    Private Approver, fName As String
    Private gcon As New Clsappconfiguration()

    Public vloantype As String = ""

    Private Sub frmLoan_ApproverList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_LoanType()
        ViewApproverPerLoanType(cboLoanType.SelectedValue.ToString())
        gridApprover.ClearSelection()
        cboLoanType.Text = vloantype
    End Sub

    Private Sub ViewApproverPerLoanType(ByVal loanType As String)
        Dim mycon As New Clsappconfiguration
        Dim ds As New DataSet
        Dim ad As New SqlDataAdapter
        Dim cmd As New SqlCommand("CIMS_Masterfiles_Approvers_LoadPerLoanType", mycon.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        mycon.sqlconn.Open()
        With cmd.Parameters
            .Add("@loanType", SqlDbType.VarChar, 50).Value = loanType
        End With

        ad.SelectCommand = cmd
        ad.Fill(ds, "CIMS_m_LoanType_Approvers")
        'ds = SqlHelper.ExecuteDataset(gcon.cnstring, "CIMS_Masterfiles_Approvers_LoadPerLoanType",
        '                               New SqlParameter("@loanType", loanType))
        Try
            Me.gridApprover.DataSource = ds
            Me.gridApprover.DataMember = "CIMS_m_LoanType_Approvers"
            Me.gridApprover.Columns(0).Visible = False
            Me.gridApprover.Columns(1).Visible = False
            Me.gridApprover.Columns(2).Width = 120
            Me.gridApprover.Columns(3).Width = 230
            Me.gridApprover.Columns(4).Visible = False
            Me.gridApprover.Columns(5).Visible = False
            Me.gridApprover.Columns(6).Visible = False
            Me.gridApprover.Columns(7).Visible = False
            ' Me.gridApprover.Columns(8).Visible = False
            mycon.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "ViewApproverPerLoanType")
        End Try
    End Sub
    Private Sub Load_LoanType()
        Dim mycon As New Clsappconfiguration
        Dim ds As DataSet = SqlHelper.ExecuteDataset(mycon.sqlconn, CommandType.StoredProcedure, "CIMS_Masterfiles_LoanType_Load")
        With cboLoanType
            .DataSource = ds.Tables(0)
            .DisplayMember = "Loan Type"
            .ValueMember = "Loan Type"
        End With
    End Sub
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub gridApprover_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridApprover.CellContentClick
        Dim row As DataGridViewRow
        For Each row In gridApprover.SelectedRows
            Approver = row.Cells(2).Value.ToString
            fName = row.Cells(3).Value.ToString
        Next
    End Sub

    Private Sub btnOK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOK.Click
        Try
            frmLoan_Approval.Search_Employee("", "Completed", cboLoanType.Text)
            frmLoan_Approval.txtApprover.Tag = gridApprover.CurrentRow.Cells(2).Value.ToString
            frmLoan_Approval.txtApprover.Text = gridApprover.CurrentRow.Cells(3).Value.ToString
            frmLoan_Approval.gb1.Enabled = True
            frmLoan_Approval.RefreshLoanSummary()
            Me.Close()
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub gridApprover_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles gridApprover.DoubleClick
        frmLoan_Approval.txtApprover.Tag = Approver
        frmLoan_Approval.txtApprover.Text = fName
        frmLoan_Approval.gb1.Enabled = True
        frmLoan_Approval.RefreshLoanSummary()
        Me.Close()
    End Sub

    Private Sub gridApprover_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles gridApprover.CellClick
        Dim row As DataGridViewRow
        For Each row In gridApprover.SelectedRows
            Approver = row.Cells(2).Value.ToString
            fName = row.Cells(3).Value.ToString
        Next
    End Sub

    Private Sub cboLoanType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanType.SelectedValueChanged
        ViewApproverPerLoanType(cboLoanType.SelectedValue.ToString())
        frmLoan_Approval.vLoantype = cboLoanType.Text
        frmLoan_Approval.GenerateHeader()
    End Sub
End Class