﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCheckLoanHistory
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCheckLoanHistory))
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtPrincipal = New System.Windows.Forms.TextBox()
        Me.txtAdvInterest = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.txtAmtPaid = New System.Windows.Forms.TextBox()
        Me.txtNoofPaid = New System.Windows.Forms.TextBox()
        Me.txtNoofUnpaid = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtRate = New System.Windows.Forms.TextBox()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.dgvDeductions = New System.Windows.Forms.DataGridView()
        Me.lblloantype = New System.Windows.Forms.TextBox()
        Me.lblname = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.txtTotDeductions = New System.Windows.Forms.TextBox()
        Me.btnRenew = New System.Windows.Forms.Button()
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClose.Image = CType(resources.GetObject("btnClose.Image"), System.Drawing.Image)
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(745, 329)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 31)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(79, 95)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(64, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Principal :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(26, 124)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(114, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Advance Interest :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(21, 18)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(82, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Loan Type :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(28, 45)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Borrower :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(54, 271)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(88, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Amount Paid :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(88, 151)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Terms :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(90, 206)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(122, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "No. of Paid Months :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(75, 236)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "No. of Unpaid Months :"
        '
        'txtPrincipal
        '
        Me.txtPrincipal.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipal.Location = New System.Drawing.Point(146, 92)
        Me.txtPrincipal.Name = "txtPrincipal"
        Me.txtPrincipal.Size = New System.Drawing.Size(167, 21)
        Me.txtPrincipal.TabIndex = 9
        Me.txtPrincipal.Text = "0.00"
        Me.txtPrincipal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAdvInterest
        '
        Me.txtAdvInterest.Location = New System.Drawing.Point(146, 121)
        Me.txtAdvInterest.Name = "txtAdvInterest"
        Me.txtAdvInterest.Size = New System.Drawing.Size(167, 21)
        Me.txtAdvInterest.TabIndex = 10
        Me.txtAdvInterest.Text = "0.00"
        Me.txtAdvInterest.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtTerms
        '
        Me.txtTerms.Location = New System.Drawing.Point(146, 148)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.Size = New System.Drawing.Size(167, 21)
        Me.txtTerms.TabIndex = 11
        Me.txtTerms.Text = "0.00"
        Me.txtTerms.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtAmtPaid
        '
        Me.txtAmtPaid.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmtPaid.Location = New System.Drawing.Point(148, 263)
        Me.txtAmtPaid.Name = "txtAmtPaid"
        Me.txtAmtPaid.Size = New System.Drawing.Size(167, 21)
        Me.txtAmtPaid.TabIndex = 12
        Me.txtAmtPaid.Text = "0.00"
        Me.txtAmtPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNoofPaid
        '
        Me.txtNoofPaid.Location = New System.Drawing.Point(215, 203)
        Me.txtNoofPaid.Name = "txtNoofPaid"
        Me.txtNoofPaid.Size = New System.Drawing.Size(100, 21)
        Me.txtNoofPaid.TabIndex = 13
        Me.txtNoofPaid.Text = "0.00"
        Me.txtNoofPaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtNoofUnpaid
        '
        Me.txtNoofUnpaid.Location = New System.Drawing.Point(215, 233)
        Me.txtNoofUnpaid.Name = "txtNoofUnpaid"
        Me.txtNoofUnpaid.Size = New System.Drawing.Size(100, 21)
        Me.txtNoofUnpaid.TabIndex = 14
        Me.txtNoofUnpaid.Text = "0.00"
        Me.txtNoofUnpaid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(170, 184)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(42, 13)
        Me.Label9.TabIndex = 15
        Me.Label9.Text = "Rate :"
        '
        'txtRate
        '
        Me.txtRate.Location = New System.Drawing.Point(215, 176)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Size = New System.Drawing.Size(100, 21)
        Me.txtRate.TabIndex = 16
        Me.txtRate.Text = "0.00"
        Me.txtRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalance
        '
        Me.txtBalance.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(148, 290)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Size = New System.Drawing.Size(167, 33)
        Me.txtBalance.TabIndex = 18
        Me.txtBalance.Text = "0.00"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(81, 310)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(61, 13)
        Me.Label10.TabIndex = 17
        Me.Label10.Text = "Balance :"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(326, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(79, 13)
        Me.Label11.TabIndex = 21
        Me.Label11.Text = "Deductions"
        '
        'dgvDeductions
        '
        Me.dgvDeductions.AllowUserToAddRows = False
        Me.dgvDeductions.AllowUserToDeleteRows = False
        Me.dgvDeductions.AllowUserToResizeColumns = False
        Me.dgvDeductions.AllowUserToResizeRows = False
        Me.dgvDeductions.BackgroundColor = System.Drawing.Color.White
        Me.dgvDeductions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDeductions.Location = New System.Drawing.Point(329, 95)
        Me.dgvDeductions.Name = "dgvDeductions"
        Me.dgvDeductions.ReadOnly = True
        Me.dgvDeductions.RowHeadersVisible = False
        Me.dgvDeductions.Size = New System.Drawing.Size(491, 189)
        Me.dgvDeductions.TabIndex = 22
        '
        'lblloantype
        '
        Me.lblloantype.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblloantype.Location = New System.Drawing.Point(146, 8)
        Me.lblloantype.Name = "lblloantype"
        Me.lblloantype.ReadOnly = True
        Me.lblloantype.Size = New System.Drawing.Size(259, 23)
        Me.lblloantype.TabIndex = 23
        '
        'lblname
        '
        Me.lblname.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblname.Location = New System.Drawing.Point(146, 35)
        Me.lblname.Name = "lblname"
        Me.lblname.ReadOnly = True
        Me.lblname.Size = New System.Drawing.Size(259, 23)
        Me.lblname.TabIndex = 24
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(656, 295)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(50, 13)
        Me.Label12.TabIndex = 25
        Me.Label12.Text = "Totals :"
        '
        'txtTotDeductions
        '
        Me.txtTotDeductions.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTotDeductions.Location = New System.Drawing.Point(712, 287)
        Me.txtTotDeductions.Name = "txtTotDeductions"
        Me.txtTotDeductions.Size = New System.Drawing.Size(108, 21)
        Me.txtTotDeductions.TabIndex = 26
        Me.txtTotDeductions.Text = "0.00"
        Me.txtTotDeductions.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnRenew
        '
        Me.btnRenew.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnRenew.Image = CType(resources.GetObject("btnRenew.Image"), System.Drawing.Image)
        Me.btnRenew.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRenew.Location = New System.Drawing.Point(211, 329)
        Me.btnRenew.Name = "btnRenew"
        Me.btnRenew.Size = New System.Drawing.Size(102, 30)
        Me.btnRenew.TabIndex = 27
        Me.btnRenew.Text = "Renew"
        Me.btnRenew.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRenew.UseVisualStyleBackColor = False
        '
        'frmCheckLoanHistory
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(832, 365)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnRenew)
        Me.Controls.Add(Me.txtTotDeductions)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.lblname)
        Me.Controls.Add(Me.lblloantype)
        Me.Controls.Add(Me.dgvDeductions)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtBalance)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.txtRate)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.txtNoofUnpaid)
        Me.Controls.Add(Me.txtNoofPaid)
        Me.Controls.Add(Me.txtAmtPaid)
        Me.Controls.Add(Me.txtTerms)
        Me.Controls.Add(Me.txtAdvInterest)
        Me.Controls.Add(Me.txtPrincipal)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnClose)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmCheckLoanHistory"
        Me.Text = "Loan Balance"
        CType(Me.dgvDeductions, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtPrincipal As System.Windows.Forms.TextBox
    Friend WithEvents txtAdvInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents txtAmtPaid As System.Windows.Forms.TextBox
    Friend WithEvents txtNoofPaid As System.Windows.Forms.TextBox
    Friend WithEvents txtNoofUnpaid As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtRate As System.Windows.Forms.TextBox
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents dgvDeductions As System.Windows.Forms.DataGridView
    Friend WithEvents lblloantype As System.Windows.Forms.TextBox
    Friend WithEvents lblname As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents txtTotDeductions As System.Windows.Forms.TextBox
    Friend WithEvents btnRenew As System.Windows.Forms.Button
End Class
