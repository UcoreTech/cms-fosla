<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_ReportOptions
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_ReportOptions))
        Me.chkLAP = New System.Windows.Forms.CheckBox()
        Me.chkALD = New System.Windows.Forms.CheckBox()
        Me.chkPF = New System.Windows.Forms.CheckBox()
        Me.chkEF = New System.Windows.Forms.CheckBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnclose = New System.Windows.Forms.Button()
        Me.BtnPreview = New System.Windows.Forms.Button()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkLAP
        '
        Me.chkLAP.AutoSize = True
        Me.chkLAP.Location = New System.Drawing.Point(25, 45)
        Me.chkLAP.Name = "chkLAP"
        Me.chkLAP.Size = New System.Drawing.Size(131, 17)
        Me.chkLAP.TabIndex = 56
        Me.chkLAP.Text = "Loan Application Form"
        Me.chkLAP.UseVisualStyleBackColor = True
        '
        'chkALD
        '
        Me.chkALD.AutoSize = True
        Me.chkALD.Location = New System.Drawing.Point(25, 68)
        Me.chkALD.Name = "chkALD"
        Me.chkALD.Size = New System.Drawing.Size(134, 17)
        Me.chkALD.TabIndex = 57
        Me.chkALD.Text = "Approved Loan Details"
        Me.chkALD.UseVisualStyleBackColor = True
        '
        'chkPF
        '
        Me.chkPF.AutoSize = True
        Me.chkPF.Location = New System.Drawing.Point(25, 91)
        Me.chkPF.Name = "chkPF"
        Me.chkPF.Size = New System.Drawing.Size(85, 17)
        Me.chkPF.TabIndex = 58
        Me.chkPF.Text = "Pledge Form"
        Me.chkPF.UseVisualStyleBackColor = True
        '
        'chkEF
        '
        Me.chkEF.AutoSize = True
        Me.chkEF.Location = New System.Drawing.Point(25, 114)
        Me.chkEF.Name = "chkEF"
        Me.chkEF.Size = New System.Drawing.Size(102, 17)
        Me.chkEF.TabIndex = 59
        Me.chkEF.Text = "Evaluation Form"
        Me.chkEF.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnclose)
        Me.PanePanel1.Controls.Add(Me.BtnPreview)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 151)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(273, 30)
        Me.PanePanel1.TabIndex = 55
        '
        'btnclose
        '
        Me.btnclose.Location = New System.Drawing.Point(194, 4)
        Me.btnclose.Name = "btnclose"
        Me.btnclose.Size = New System.Drawing.Size(75, 23)
        Me.btnclose.TabIndex = 1
        Me.btnclose.Text = "Close"
        Me.btnclose.UseVisualStyleBackColor = True
        '
        'BtnPreview
        '
        Me.BtnPreview.Location = New System.Drawing.Point(4, 3)
        Me.BtnPreview.Name = "BtnPreview"
        Me.BtnPreview.Size = New System.Drawing.Size(115, 23)
        Me.BtnPreview.TabIndex = 0
        Me.BtnPreview.Text = "Preview Reports"
        Me.BtnPreview.UseVisualStyleBackColor = True
        '
        'PanePanel2
        '
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.Label1)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel2.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel2.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel2.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(273, 26)
        Me.PanePanel2.TabIndex = 54
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(2, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(125, 19)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Loan Summary"
        '
        'frmLoan_ReportOptions
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(273, 181)
        Me.Controls.Add(Me.chkEF)
        Me.Controls.Add(Me.chkPF)
        Me.Controls.Add(Me.chkALD)
        Me.Controls.Add(Me.chkLAP)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanel2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmLoan_ReportOptions"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reports"
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel2.ResumeLayout(False)
        Me.PanePanel2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents btnclose As System.Windows.Forms.Button
    Friend WithEvents BtnPreview As System.Windows.Forms.Button
    Friend WithEvents chkLAP As System.Windows.Forms.CheckBox
    Friend WithEvents chkALD As System.Windows.Forms.CheckBox
    Friend WithEvents chkPF As System.Windows.Forms.CheckBox
    Friend WithEvents chkEF As System.Windows.Forms.CheckBox
End Class
