<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoan_PreTerminate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoan_PreTerminate))
        Me.gridPreterminate = New System.Windows.Forms.DataGridView()
        Me.cmdPreTerminate = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.panelTool = New System.Windows.Forms.Panel()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblSteptwo = New System.Windows.Forms.Label()
        CType(Me.gridPreterminate, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panelTool.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gridPreterminate
        '
        Me.gridPreterminate.AllowUserToAddRows = False
        Me.gridPreterminate.AllowUserToDeleteRows = False
        Me.gridPreterminate.AllowUserToOrderColumns = True
        Me.gridPreterminate.AllowUserToResizeRows = False
        Me.gridPreterminate.BackgroundColor = System.Drawing.Color.White
        Me.gridPreterminate.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPreterminate.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gridPreterminate.GridColor = System.Drawing.Color.White
        Me.gridPreterminate.Location = New System.Drawing.Point(0, 0)
        Me.gridPreterminate.Name = "gridPreterminate"
        Me.gridPreterminate.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.gridPreterminate.Size = New System.Drawing.Size(468, 263)
        Me.gridPreterminate.TabIndex = 0
        '
        'cmdPreTerminate
        '
        Me.cmdPreTerminate.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdPreTerminate.Image = Global.WindowsApplication2.My.Resources.Resources.alert241
        Me.cmdPreTerminate.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.cmdPreTerminate.Location = New System.Drawing.Point(269, 3)
        Me.cmdPreTerminate.Name = "cmdPreTerminate"
        Me.cmdPreTerminate.Size = New System.Drawing.Size(115, 28)
        Me.cmdPreTerminate.TabIndex = 1
        Me.cmdPreTerminate.Text = "Preterminate"
        Me.cmdPreTerminate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.cmdPreTerminate.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.AccessibleDescription = "tn"
        Me.btnCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCancel.Image = Global.WindowsApplication2.My.Resources.Resources.button_cancel
        Me.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancel.Location = New System.Drawing.Point(390, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 28)
        Me.btnCancel.TabIndex = 2
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'panelTool
        '
        Me.panelTool.Controls.Add(Me.btnCancel)
        Me.panelTool.Controls.Add(Me.cmdPreTerminate)
        Me.panelTool.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.panelTool.Location = New System.Drawing.Point(0, 263)
        Me.panelTool.Name = "panelTool"
        Me.panelTool.Size = New System.Drawing.Size(468, 35)
        Me.panelTool.TabIndex = 3
        '
        'PanePanel1
        '
        Me.PanePanel1.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblSteptwo)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(468, 30)
        Me.PanePanel1.TabIndex = 28
        '
        'lblSteptwo
        '
        Me.lblSteptwo.AutoSize = True
        Me.lblSteptwo.BackColor = System.Drawing.Color.Transparent
        Me.lblSteptwo.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblSteptwo.ForeColor = System.Drawing.Color.White
        Me.lblSteptwo.Location = New System.Drawing.Point(3, 5)
        Me.lblSteptwo.Name = "lblSteptwo"
        Me.lblSteptwo.Size = New System.Drawing.Size(164, 19)
        Me.lblSteptwo.TabIndex = 25
        Me.lblSteptwo.Text = "Loan Pretermination"
        '
        'frmLoan_PreTerminate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(468, 298)
        Me.ControlBox = False
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.gridPreterminate)
        Me.Controls.Add(Me.panelTool)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmLoan_PreTerminate"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Loan Pretermination"
        CType(Me.gridPreterminate, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panelTool.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents gridPreterminate As System.Windows.Forms.DataGridView
    Friend WithEvents cmdPreTerminate As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents panelTool As System.Windows.Forms.Panel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents lblSteptwo As System.Windows.Forms.Label
End Class
