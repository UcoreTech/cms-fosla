﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmPaymentNotification
    'Created by : Vincent Nacar
    'Module : Payment Notification
    'May 26,2014
    Private gcon As New Clsappconfiguration

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoanNotification()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Notify_ScheduledPayments",
                                      New SqlParameter("@datenow", Date.Now))
        dgvList.DataSource = ds.Tables(0)
    End Sub

    Private Sub frmPaymentNotification_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoanNotification()

        If dgvList.RowCount = 0 Then
            pnlNo.Visible = True
        Else
            pnlNo.Visible = False
        End If
    End Sub
End Class