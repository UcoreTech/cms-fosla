﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmBrowseOR
    'Coded by : Vincent Nacar 

    Private gcon As New Clsappconfiguration

    Private Sub frmBrowseOR_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadOR()
    End Sub

    Private Sub LoadOR()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_ORNo")

        dgvList.DataSource = ds.Tables(0)
    End Sub

    Private Sub btnOk_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOk.Click
        ' frmLoanPayment.txtDocNumber.Text = Me.dgvList.SelectedCells(0).Value.ToString
        Me.Close()
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub dgvList_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.DoubleClick
        'frmLoanPayment.txtDocNumber.Text = Me.dgvList.SelectedCells(0).Value.ToString
        Me.Close()
    End Sub

End Class