﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmCashier
    'Coded by : Vincent Nacar 

    Private gcon As New Clsappconfiguration
    Public xMode As String
    Public isUpdate As Boolean = False
    Public ctr1 As Integer
    Public ctr2 As Integer
    Public Credit As Double
    Public Debit As Double
    Public fxkey As String
    Public xCtr As Integer
    Public savingCount As Integer
    Public SearchMode As String

    Private Sub frmCashier_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadORNo()
        LoadUser()
        LoadBankAccounts()
        txtDate.Text = Format(Date.Now, "MM/dd/yyyy")

        GenerateBankAndCheck()
        GenerateLoanPaymentEntry()
    End Sub

    Private Sub LoadORNo()
        cboOrNo.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_ORNo")
        While rd.Read
            With cboOrNo
                .Items.Add(rd("fcDocNumber"))
            End With
        End While
    End Sub


    Private Sub GenerateBankAndCheck()
        ' Dim jvKeyID As New DataGridViewTextBoxColumn
        Dim Bank As New DataGridViewTextBoxColumn
        Dim CheckNo As New DataGridViewTextBoxColumn
        Dim CheckDate As New DataGridViewTextBoxColumn
        Dim Amounts As New DataGridViewTextBoxColumn
        Dim Maker As New DataGridViewTextBoxColumn
        Dim Payee As New DataGridViewTextBoxColumn
        'Dim btn As New DataGridViewButtonColumn

        With Bank
            .HeaderText = "Bank"
            .Name = "Bank"
            .DataPropertyName = "Bank"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With CheckNo
            .HeaderText = "Check Number"
            .Name = "CheckNo"
            .DataPropertyName = "CheckNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With CheckDate
            .HeaderText = "Check Date"
            .Name = "CheckDate"
            .DataPropertyName = "CheckDate"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Amounts
            .HeaderText = "Amounts"
            .Name = "Amounts"
            .DataPropertyName = "Amounts"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Maker
            .HeaderText = "Maker"
            .Name = "Maker"
            .DataPropertyName = "Maker"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Payee
            .HeaderText = "Payee"
            .Name = "Payee"
            .DataPropertyName = "fxKeyNameID"
            .ReadOnly = True
        End With

        With grdListofEntries
            .Columns.Clear()
            .Columns.Add(Bank)
            .Columns.Add(CheckNo)
            .Columns.Add(CheckDate)
            .Columns.Add(Amounts)
            .Columns.Add(Maker)
            .Columns.Add(Payee)
            .Columns("Bank").Width = 100
            .Columns("CheckNo").Width = 150
            .Columns("CheckDate").Width = 150
            .Columns("Amounts").Width = 100
            .Columns("Maker").Width = 200
            .Columns("Payee").Width = 200
        End With

    End Sub

    Private Sub GenerateLoanPaymentEntry()
        Dim IDNo As New DataGridViewTextBoxColumn
        Dim ClientName As New DataGridViewTextBoxColumn
        Dim LoanNo As New DataGridViewTextBoxColumn
        Dim AcctRef As New DataGridViewTextBoxColumn
        Dim Code As New DataGridViewTextBoxColumn
        Dim AccountTitle As New DataGridViewTextBoxColumn
        Dim Debit As New DataGridViewTextBoxColumn
        Dim Credit As New DataGridViewTextBoxColumn

        With IDNo
            .HeaderText = "ID No"
            .Name = "IDNo"
            .DataPropertyName = "IDNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With ClientName
            .HeaderText = "Client Name"
            .Name = "ClientName"
            .DataPropertyName = "ClientName"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With LoanNo
            .HeaderText = "Loan No."
            .Name = "LoanNo"
            .DataPropertyName = "LoanNo"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AcctRef
            .HeaderText = "Acct Ref."
            .Name = "AcctRef"
            .DataPropertyName = "AcctRef"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With Code
            .HeaderText = "Code"
            .Name = "Code"
            .DataPropertyName = "Code"
            .AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .ReadOnly = True
        End With
        With AccountTitle
            .HeaderText = "Account Title"
            .Name = "AccountTitle"
            .DataPropertyName = "AccountTitle"
            .ReadOnly = True
        End With
        With Debit
            .HeaderText = "Debit"
            .Name = "Debit"
            .DataPropertyName = "Debit"
            '.ReadOnly = True
        End With
        With Credit
            .HeaderText = "Credit"
            .Name = "Credit"
            .DataPropertyName = "Credit"
            ' .ReadOnly = True
        End With
        With dgvPaymentEntry
            .Columns.Clear()
            .Columns.Add(IDNo)
            .Columns.Add(ClientName)
            .Columns.Add(LoanNo)
            .Columns.Add(AcctRef)
            .Columns.Add(Code)
            .Columns.Add(AccountTitle)
            .Columns.Add(Debit)
            .Columns.Add(Credit)
            .Columns("Debit").DefaultCellStyle.Format = "##,##0.00"
            .Columns("Credit").DefaultCellStyle.Format = "##,##0.00"
            '.Columns("IDNo").Width = 150
            '.Columns("ClientName").Width = 200
            '.Columns("LoanNo").Width = 100
            '.Columns("AcctRef").Width = 150
            '.Columns("Code").Width = 150
            '.Columns("AccountTitle").Width = 200
            '.Columns("Debit").Width = 150
            '.Columns("Credit").Width = 150
        End With

    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        GenerateBankAndCheck()
        GenerateLoanPaymentEntry()
        ctr1 = 0 'reset counter :D
        ctr2 = 0
        txtDebit.Text = ""
        txtCredit.Text = ""
        lblPayee.Text = "No Selected Payee"
        lblAmount.Text = "0.00"
    End Sub

    Private Sub LoadBankAccounts()
        cboSelectBank.Items.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_BankAccount")
        With cboSelectBank
            .DataSource = ds.Tables(0)
            .ValueMember = "PK_BankID"
            .DisplayMember = "fcBankName"
        End With
    End Sub

    Private Sub btnSelectBank_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectBank.Click
        LoadCheckNo()
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadCheckNo()
        cboCheck.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_CheckNumber",
                                     New SqlParameter("@BankID", cboSelectBank.SelectedValue))
        While rd.Read
            With cboCheck
                .Items.Add(rd("FX_CheckNumber"))
            End With
        End While
        cboCheck.Text = "Select Check"
        cboCheck.Enabled = True
    End Sub

    Private Sub LoadClients()
        dgvClients.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_RegisteredClients_Listing")

        dgvClients.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill
        dgvClients.DataSource = ds.Tables(0)
        'dgvClients.Columns(2).Visible = False
        'dgvClients.Columns(3).Visible = False
        'dgvClients.Columns(4).Visible = False
    End Sub


    Private Sub btnAddClients_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddClients.Click
        Select Case btnAddClients.Text
            Case "Add Account Reference"
                LoadClients()
                SearchMode = "Accounts"
                dgvClients.Visible = True
                pnlSearch.Visible = True
                btnAddClients.Text = "Hide Clients List"
                btnClient.Enabled = False
                btnLoan.Enabled = False
            Case "Hide Clients List"
                dgvClients.Visible = False
                pnlSearch.Visible = False
                btnAddClients.Text = "Add Account Reference"
                btnClient.Enabled = True
                btnLoan.Enabled = True
                SearchMode = ""
        End Select

        xMode = "AC"
    End Sub

    Private Sub btnClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClient.Click
        Select Case btnClient.Text
            Case "Add Client"
                LoadClients()
                dgvClients.Visible = True
                pnlSearch.Visible = True
                btnClient.Text = "Hide Client List"
                btnAddClients.Enabled = False
                btnLoan.Enabled = False
            Case "Hide Client List"
                LoadClients()
                dgvClients.Visible = False
                pnlSearch.Visible = False
                btnClient.Text = "Add Client"
                btnAddClients.Enabled = True
                btnLoan.Enabled = True
        End Select
        xMode = "C"
    End Sub

    'Private Sub btnAddPayee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddPayee.Click
    '    Select Case btnAddPayee.Text
    '        Case "Add Payee"
    '            LoadClients()
    '            dgvClients.Visible = True
    '            pnlSearch.Visible = True
    '            btnAddPayee.Text = "Hide Client List"
    '        Case "Hide Client List"
    '            dgvClients.Visible = False
    '            pnlSearch.Visible = False
    '            btnAddPayee.Text = "Hide Client List"
    '    End Select
    '    xMode = "P"
    'End Sub

    Private Sub dgvClients_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClients.DoubleClick

        dgvPaymentEntry.Rows.Add() 
        Select Case xMode
            Case "AC"
                With dgvPaymentEntry
                    '.Rows(ctr1).Cells(0).Value = dgvClients.SelectedRows(0).Cells(0).Value.ToString
                    '.Rows(ctr1).Cells(1).Value = dgvClients.SelectedRows(0).Cells(1).Value.ToString
                    '.Rows(ctr1).Cells(3).Value = dgvClients.SelectedRows(0).Cells(2).Value.ToString
                    .Rows(ctr1).Cells(4).Value = dgvClients.SelectedRows(0).Cells(0).Value.ToString
                    .Rows(ctr1).Cells(5).Value = dgvClients.SelectedRows(0).Cells(1).Value.ToString
                End With
            Case "C"
                With dgvPaymentEntry
                    .Rows(ctr1).Cells(0).Value = dgvClients.SelectedRows(0).Cells(0).Value.ToString
                    .Rows(ctr1).Cells(1).Value = dgvClients.SelectedRows(0).Cells(1).Value.ToString
                End With
            Case "L"
                With dgvPaymentEntry
                    .Rows(ctr1).Cells(0).Value = dgvClients.SelectedRows(0).Cells(0).Value.ToString
                    .Rows(ctr1).Cells(1).Value = dgvClients.SelectedRows(0).Cells(1).Value.ToString
                    .Rows(ctr1).Cells(2).Value = dgvClients.SelectedRows(0).Cells(2).Value.ToString
                    .Rows(ctr1).Cells(3).Value = dgvClients.SelectedRows(0).Cells(3).Value.ToString
                    .Rows(ctr1).Cells(4).Value = dgvClients.SelectedRows(0).Cells(4).Value.ToString
                    .Rows(ctr1).Cells(5).Value = dgvClients.SelectedRows(0).Cells(5).Value.ToString
                    .Rows(ctr1).Cells(7).Value = dgvClients.SelectedRows(0).Cells(6).Value.ToString
                End With
                'Case "P"
                '    With grdListofEntries
                '        .Rows(ctr2).Cells(5).Value = dgvClients.SelectedRows(0).Cells(1).Value.ToString
                '    End With
        End Select

        '############### Compute Debit and Credit ###################
        'Enteng
        With dgvPaymentEntry
            Debit = Debit + .Rows(ctr1).Cells(6).Value
            Credit = Credit + .Rows(ctr1).Cells(7).Value
            ComputeDebitCredit()
        End With


        ctr1 += 1
        'Count Entry Columns
        'ctr1 = ctr1 + 1

        dgvClients.Visible = False
        pnlSearch.Visible = False
        btnAddClients.Text = "Add Account Reference"
        btnClient.Text = "Add Client"
        btnLoan.Text = "Add Loan Reference"
        btnAddClients.Enabled = True
        btnClient.Enabled = True
        btnLoan.Enabled = True
        xMode = ""
    End Sub

    Private Sub btnLoan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLoan.Click
        Select Case btnLoan.Text
            Case "Add Loan Reference"
                LoadLoans()
                SearchMode = "Loan"
                dgvClients.Visible = True
                pnlSearch.Visible = True
                btnLoan.Text = "Hide Loan Reference"
                btnAddClients.Enabled = False
                btnClient.Enabled = False
            Case "Hide Loan Reference"
                dgvClients.Visible = False
                pnlSearch.Visible = False
                btnLoan.Text = "Add Loan Reference"
                btnAddClients.Enabled = True
                btnClient.Enabled = True
                SearchMode = ""
        End Select
        xMode = "L"
    End Sub

    Private Sub LoadLoans()
        dgvClients.Columns.Clear()
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_Loans")

        dgvClients.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.None
        dgvClients.DataSource = ds.Tables(0)
        dgvClients.Columns(0).HeaderText = "ID No."
    End Sub

    Private Sub ComputeDebitCredit()
        Dim totalCredit As Decimal
        Dim totaldebit As Decimal

        For Each xRow As DataGridViewRow In dgvPaymentEntry.Rows
            'Dim credit As Object = grdGenJournalDetails.Item("Credit", xRow.Index).Value
            Dim crdt As Object = NormalizeValuesInDataGridView(dgvPaymentEntry, "Credit", xRow.Index)

            'Dim debit As Object = grdGenJournalDetails.Item("Debit", xRow.Index).Value
            Dim dbt As Object = NormalizeValuesInDataGridView(dgvPaymentEntry, "Debit", xRow.Index)


            If crdt IsNot Nothing Then
                totalCredit += crdt
            End If

            If dbt IsNot Nothing Then
                totaldebit += dbt
            End If

        Next

        txtDebit.Text = Format(CDec(totaldebit), "##,##0.00")
        txtCredit.Text = Format(CDec(totalCredit), "##,##0.00")
    End Sub

    Public Function NormalizeValuesInDataGridView(ByVal grid As DataGridView, ByVal columnName As String, ByVal rowIndex As Integer)
        Dim fieldName As String
        Try
            If Not IsDBNull(grid.Item(columnName, rowIndex).Value) Then
                If grid.Item(columnName, rowIndex).Value IsNot Nothing Then
                    fieldName = grid.Item(columnName, rowIndex).Value.ToString()

                End If
            End If
        Catch ex As Exception

        End Try
        Return fieldName

    End Function

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        'With dgvPaymentEntry
        '    .SelectedRows.Item(0).Visible = False
        '    .SelectedRows.Item(0).Tag = "Remove"
        'End With
    End Sub

    Private Sub btnDone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDone.Click
        If btnDone.Text = "Select Payee" Then
            dgvPaymentEntry.SelectionMode = DataGridViewSelectionMode.FullRowSelect
            btnDone.Text = "Cancel"
            Exit Sub
        End If
        If btnDone.Text = "Cancel" Then
            dgvPaymentEntry.SelectionMode = DataGridViewSelectionMode.CellSelect
            btnDone.Text = "Select Payee"
            Exit Sub
        End If
        If btnDone.Text = "Done" Then
            If cboCheck.Text = "" Then
                MsgBox("Please Select Check No.", vbInformation, "Oops")
                Exit Sub
            End If

            If MsgBox("Apply Check?", vbQuestion + MsgBoxStyle.YesNo, "Confirmation") = vbYes Then
                Me.grdListofEntries.Rows.Add()
                Me.grdListofEntries.Rows(ctr2).Cells(3).Value = lblAmount.Text
                Me.grdListofEntries.Rows(ctr2).Cells(4).Value = frmMain.lblCoopName.Text
                Me.grdListofEntries.Rows(ctr2).Cells(5).Value = lblPayee.Text
                Me.grdListofEntries.Rows(ctr2).Cells(2).Value = Format(Date.Now, "MM/dd/yyyy")
                Me.grdListofEntries.Rows(ctr2).Cells(0).Value = cboSelectBank.Text
                Me.grdListofEntries.Rows(ctr2).Cells(1).Value = cboCheck.Text
                ctr2 += 1
                'UpdateCheckNumber()
                dgvPaymentEntry.SelectionMode = DataGridViewSelectionMode.CellSelect
                cboCheck.Text = ""
                cboCheck.Enabled = False
                btnDone.Text = "Select Payee"
                lblPayee.Text = "No Selected Payee"
                lblAmount.Text = "0.00"
                btnCancel.Visible = False
            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub dgvPaymentEntry_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPaymentEntry.DoubleClick
        On Error GoTo heaven
        lblPayee.Text = Me.dgvPaymentEntry.SelectedRows(0).Cells(1).Value.ToString
        lblAmount.Text = Me.dgvPaymentEntry.SelectedRows(0).Cells(7).Value.ToString
        btnDone.Text = "Done"
        btnCancel.Visible = True
heaven:
        Exit Sub

    End Sub

    Private Sub LoadUser()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Cashier",
                                      New SqlParameter("@username", mod_UsersAccessibility.xUser))
        While rd.Read
            txtCreatedby.Text = rd("email")
            txtCashier.Text = rd("Fullname")
        End While
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        SavingAllData()
    End Sub

    Private Sub SaveEntry()
        Try
            If GetJVKeyID() = "" Then
                GetJVKeyID() = System.Guid.NewGuid.ToString()
            End If

            Dim MyGuid As Guid = New Guid(GetJVKeyID)
            fxkey = MyGuid.ToString

            Dim maxSerialNo As Integer = Generate_JournalNo_Serial(Date.Now, "OR")
            'Dim empno = dgvPaymentEntry.SelectedRows(0).Cells(0).Value.ToString

            SqlHelper.ExecuteNonQuery(gcon.cnstring, "CAS_t_GeneralJournal_Header_InsertUpdate",
                                  New SqlParameter("@fxKeyJVNo", MyGuid),
                                  New SqlParameter("@fdTransDate", txtDate.Text),
                                  New SqlParameter("@fbAdjust", False),
                                  New SqlParameter("@fcEmployeeNo", " "),
                                  New SqlParameter("@fcMemo", txtMemo.Text),
                                  New SqlParameter("@fdTotAmt", txtCredit.Text),
                                  New SqlParameter("@user", txtCreatedby.Text),
                                  New SqlParameter("@doctypeInitial", "OR"),
                                  New SqlParameter("@maxSerialNo", maxSerialNo),
                                  New SqlParameter("@fiEntryNo", cboOrNo.Text),
                                  New SqlParameter("@fbIsCancelled", False))
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
            frmMsgBox.Show()
        End Try
    End Sub

    Private Sub SaveDetails()
        On Error GoTo jump
        Dim MyGuid As Guid = New Guid(fxkey)
        For xCtr = 0 To dgvPaymentEntry.RowCount - 1
            With dgvPaymentEntry
                Dim accounttitle As String = IIf(IsDBNull(.Item("Code", xCtr).Value), Nothing, .Item("Code", xCtr).Value)
                Dim empno As String = .Item("IDNo", xCtr).Value
                Dim loan As String = .Item("LoanNo", xCtr).Value
                Dim acctref As String = .Item("AcctRef", xCtr).Value
                Dim debit As Double = .Item("Debit", xCtr).Value
                Dim credit As Double = .Item("Credit", xCtr).Value
                SqlHelper.ExecuteNonQuery(gcon.cnstring, "CIMS_t_tJVEntry_details_save",
                         New SqlParameter("@fiEntryNo", cboOrNo.Text),
                         New SqlParameter("@AccountCode", accounttitle),
                         New SqlParameter("@fdDebit", debit),
                         New SqlParameter("@fdCredit", credit),
                         New SqlParameter("@fcMemo", txtMemo.Text),
                         New SqlParameter("@EmployeeNo", empno),
                         New SqlParameter("@fbBillable", False),
                         New SqlParameter("@fxKey", System.Guid.NewGuid),
                         New SqlParameter("@fk_JVHeader", MyGuid),
                         New SqlParameter("@transDate", txtDate.Text),
                         New SqlParameter("@fcLoanRef", loan),
                         New SqlParameter("@fcAccRef", acctref))
            End With
        Next
jump:
        Exit Sub
        xCtr = 0
    End Sub

    Private Function Generate_JournalNo_Serial(ByVal transDate As Date, ByVal docTypeInitial As String) As Integer
        Dim serialno As Integer
        Dim gCon As New Clsappconfiguration
        Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_m_JournalNo_GenerateSerialPerMonthPerYear", _
                                                            New SqlParameter("@date", transDate), _
                                                            New SqlParameter("@doctypeInitial", docTypeInitial))
            If rd.Read() Then
                serialno = rd("maxSerialNo").ToString
            Else
                serialno = ""
            End If
            Return serialno
        End Using
    End Function

    Private jvKeyID As String

    Public Property GetJVKeyID() As String
        Get
            Return jvKeyID
        End Get
        Set(ByVal value As String)
            jvKeyID = value
        End Set
    End Property

    Private Sub UpdateDocNumber()
        SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_DocNumber_Used",
                                   New SqlParameter("@DocNumber", cboOrNo.Text),
                                   New SqlParameter("@DateUsed", txtDate.Text))
    End Sub

    Private Sub dgvPaymentEntry_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPaymentEntry.CellValueChanged

        If e.ColumnIndex <> -1 Then

            If dgvPaymentEntry.Columns(e.ColumnIndex).Name = "Debit" Then
                If NormalizeValuesInDataGridView(dgvPaymentEntry, "Debit", e.RowIndex) = "" Then
                    dgvPaymentEntry.Item("Debit", e.RowIndex).Value = 0
                Else
                    If isUpdate = True Then
                        dgvPaymentEntry.Columns("Debit").DefaultCellStyle.Format = "N2"
                    Else
                        dgvPaymentEntry.Item("Debit", e.RowIndex).Value = Format(CDec(dgvPaymentEntry.Item("Debit", e.RowIndex).Value.ToString()), "##,##0.00")
                    End If
                End If
            End If

            If dgvPaymentEntry.Columns(e.ColumnIndex).Name = "Credit" Then
                If NormalizeValuesInDataGridView(dgvPaymentEntry, "Credit", e.RowIndex) = "" Then
                    dgvPaymentEntry.Item("Credit", e.RowIndex).Value = 0
                Else
                    If isUpdate = True Then
                        dgvPaymentEntry.Columns("Credit").DefaultCellStyle.Format = "N2"
                    Else
                        dgvPaymentEntry.Item("Credit", e.RowIndex).Value = Format(CDec(dgvPaymentEntry.Item("Credit", e.RowIndex).Value.ToString()), "##,##0.00")
                    End If
                End If
            End If

        End If


        If dgvPaymentEntry.Columns(e.ColumnIndex).Name = "Credit" Then
            Try
                Call ComputeDebitCredit()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

        If dgvPaymentEntry.Columns(e.ColumnIndex).Name = "Debit" Then
            Try
                Call ComputeDebitCredit()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If

    End Sub

    Private Sub UpdateCheckNumber()
        On Error GoTo jump
        For i As Integer = 0 To grdListofEntries.RowCount - 1

            With grdListofEntries

                Dim checkno As String = .Item("CheckNo", i).Value
                Dim payee As String = .Item("Payee", i).Value
                Dim amount As String = .Item("Amounts", i).Value
                Dim chkdate As String = .Item("CheckDate", i).Value
                Dim maker As String = .Item("Maker", i).Value
                SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_Used_Check",
                                           New SqlParameter("@FX_CheckNumber", checkno),
                                           New SqlParameter("@DocNumber", cboOrNo.Text),
                                           New SqlParameter("@fbIsUsed", True),
                                           New SqlParameter("@fcPayee", payee),
                                           New SqlParameter("@fnAmount", amount),
                                           New SqlParameter("@fdCheckDate", chkdate),
                                           New SqlParameter("@fcMaker", maker))

            End With
        Next
jump:
        Exit Sub
    End Sub

    Private Sub SavingAllData()
        If cboOrNo.Text = "" Then
            'MsgBox("Please Select Document No.!", vbInformation, "Unable To Proceed!")
            frmMsgBox.txtMessage.Text = "Unable to proceed , Please select document No.!"
            frmMsgBox.ShowDialog()
        Else
            If MsgBox("Are you sure?", vbQuestion + vbYesNo, "Saving Confirmation") = vbYes Then
                'Accounting na ang bahalang magbalance :D 

                'If txtDebit.Text <> txtCredit.Text Then
                '    MsgBox("Total Debit and Credit Amount Not balance", vbExclamation, "Saving Cancelled!")
                '    Exit Sub
                'Else
                SaveEntry()
                SaveDetails()
                'UpdateAmountPaid_Balance() sa accounting na to mangyayare :D
                UpdateDocNumber()
                UpdateCheckNumber()

                frmMsgBox.txtMessage.Text = "Record Successfully Saved!"
                frmMsgBox.ShowDialog()

                GetJVKeyID = ""
                LoadORNo()
                GenerateBankAndCheck()
                GenerateLoanPaymentEntry()
                ctr1 = 0
                ctr2 = 0
                txtDebit.Text = ""
                txtCredit.Text = ""
                'End If

            Else
                Exit Sub
            End If
        End If
    End Sub

    Private Sub UpdateAmountPaid_Balance()

        For i As Integer = 0 To dgvPaymentEntry.RowCount - 1
            With dgvPaymentEntry

                Dim loanNo As String = .Item("LoanNo", i).Value
                Dim amount As String = .Item("Credit", i).Value

                SqlHelper.ExecuteNonQuery(gcon.cnstring, "_Update_AmountPaidAndBalance",
                                           New SqlParameter("@fnLoanNo", loanNo),
                                           New SqlParameter("@Amount", amount))

            End With
        Next

    End Sub

    Private Sub btnSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim ds As DataSet
        Select Case SearchMode
            Case "Loan"
                ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_Loans_Filter",
                                               New SqlParameter("@name", txtSearch.Text.Trim))
                dgvClients.DataSource = ds.Tables(0)
            Case "Accounts"
                ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_RegisteredClients_Listing_Filter",
                                               New SqlParameter("@name", txtSearch.Text.Trim))
                dgvClients.DataSource = ds.Tables(0)
        End Select
    End Sub

    Private Sub txtSearch_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtSearch.TextChanged

        Dim ds As DataSet
        Select Case SearchMode
            Case "Loan"
                ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Select_Loans_Filter",
                                               New SqlParameter("@name", txtSearch.Text.Trim))
                dgvClients.DataSource = ds.Tables(0)
            Case "Accounts"
                ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_RegisteredClients_Listing_Filter",
                                               New SqlParameter("@name", txtSearch.Text.Trim))
                dgvClients.DataSource = ds.Tables(0)
        End Select

    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        cboCheck.Text = ""
        cboCheck.Enabled = False
        lblPayee.Text = "No Selected Payee"
        lblAmount.Text = "0.00"
        dgvPaymentEntry.SelectionMode = DataGridViewSelectionMode.CellSelect
        btnDone.Text = "Select Payee"
        btnCancel.Visible = False
    End Sub

End Class