﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBrowseOR
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnOk = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Location = New System.Drawing.Point(4, 38)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(151, 257)
        Me.dgvList.TabIndex = 62
        '
        'PanePanel1
        '
        Me.PanePanel1.BackColor = System.Drawing.Color.White
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnOk)
        Me.PanePanel1.Controls.Add(Me.btnCancel)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 301)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(157, 32)
        Me.PanePanel1.TabIndex = 61
        '
        'btnOk
        '
        Me.btnOk.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOk.Location = New System.Drawing.Point(3, 3)
        Me.btnOk.Name = "btnOk"
        Me.btnOk.Size = New System.Drawing.Size(75, 23)
        Me.btnOk.TabIndex = 0
        Me.btnOk.Text = "OK"
        Me.btnOk.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold)
        Me.btnCancel.Location = New System.Drawing.Point(79, 3)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 23)
        Me.btnCancel.TabIndex = 1
        Me.btnCancel.Text = "CANCEL"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(157, 32)
        Me.PanePanel5.TabIndex = 60
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Location = New System.Drawing.Point(3, 6)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(118, 19)
        Me.Label1.TabIndex = 14
        Me.Label1.Text = "Issued OR No."
        '
        'frmBrowseOR
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(157, 333)
        Me.Controls.Add(Me.dgvList)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.PanePanel5)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmBrowseOR"
        Me.Text = "frmBrowseOR"
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnOk As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
End Class
