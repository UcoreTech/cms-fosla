﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtClientID = New System.Windows.Forms.TextBox()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.txtCreatedby = New System.Windows.Forms.TextBox()
        Me.txtCashier = New System.Windows.Forms.TextBox()
        Me.txtDate = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cboCheckNo = New System.Windows.Forms.ComboBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.cboBank = New System.Windows.Forms.ComboBox()
        Me.cboMethod = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtMemo = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtPayment = New System.Windows.Forms.TextBox()
        Me.cboOrNo = New System.Windows.Forms.ComboBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnPost = New System.Windows.Forms.Button()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.lblFrom = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(297, 272)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 58
        Me.Label2.Text = "Created by :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 70)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(67, 13)
        Me.Label3.TabIndex = 59
        Me.Label3.Text = "Loan No :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(13, 44)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(71, 13)
        Me.Label4.TabIndex = 60
        Me.Label4.Text = "Client ID :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(297, 70)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 13)
        Me.Label5.TabIndex = 61
        Me.Label5.Text = "OR No :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(297, 44)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(45, 13)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "Date :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(278, 293)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(64, 13)
        Me.Label7.TabIndex = 63
        Me.Label7.Text = "Cashier :"
        '
        'txtClientID
        '
        Me.txtClientID.Enabled = False
        Me.txtClientID.Location = New System.Drawing.Point(94, 36)
        Me.txtClientID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtClientID.Name = "txtClientID"
        Me.txtClientID.Size = New System.Drawing.Size(202, 21)
        Me.txtClientID.TabIndex = 66
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Enabled = False
        Me.txtLoanNo.Location = New System.Drawing.Point(94, 62)
        Me.txtLoanNo.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(202, 21)
        Me.txtLoanNo.TabIndex = 67
        '
        'txtCreatedby
        '
        Me.txtCreatedby.Location = New System.Drawing.Point(387, 264)
        Me.txtCreatedby.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtCreatedby.Name = "txtCreatedby"
        Me.txtCreatedby.Size = New System.Drawing.Size(146, 21)
        Me.txtCreatedby.TabIndex = 68
        '
        'txtCashier
        '
        Me.txtCashier.Location = New System.Drawing.Point(353, 290)
        Me.txtCashier.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtCashier.Name = "txtCashier"
        Me.txtCashier.Size = New System.Drawing.Size(180, 21)
        Me.txtCashier.TabIndex = 69
        '
        'txtDate
        '
        Me.txtDate.Enabled = False
        Me.txtDate.Location = New System.Drawing.Point(365, 36)
        Me.txtDate.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.txtDate.Name = "txtDate"
        Me.txtDate.Size = New System.Drawing.Size(172, 21)
        Me.txtDate.TabIndex = 70
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboCheckNo)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.cboBank)
        Me.GroupBox1.Controls.Add(Me.cboMethod)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Location = New System.Drawing.Point(16, 89)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(521, 97)
        Me.GroupBox1.TabIndex = 73
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Payment Method"
        '
        'cboCheckNo
        '
        Me.cboCheckNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCheckNo.Enabled = False
        Me.cboCheckNo.FormattingEnabled = True
        Me.cboCheckNo.Location = New System.Drawing.Point(337, 62)
        Me.cboCheckNo.Name = "cboCheckNo"
        Me.cboCheckNo.Size = New System.Drawing.Size(176, 21)
        Me.cboCheckNo.TabIndex = 3
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(258, 70)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(75, 13)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "Check No :"
        '
        'cboBank
        '
        Me.cboBank.Enabled = False
        Me.cboBank.FormattingEnabled = True
        Me.cboBank.Location = New System.Drawing.Point(315, 31)
        Me.cboBank.Name = "cboBank"
        Me.cboBank.Size = New System.Drawing.Size(198, 21)
        Me.cboBank.TabIndex = 77
        '
        'cboMethod
        '
        Me.cboMethod.FormattingEnabled = True
        Me.cboMethod.Location = New System.Drawing.Point(75, 31)
        Me.cboMethod.Name = "cboMethod"
        Me.cboMethod.Size = New System.Drawing.Size(126, 21)
        Me.cboMethod.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(14, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Select :"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(207, 34)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(107, 13)
        Me.Label10.TabIndex = 76
        Me.Label10.Text = "Bank Account : "
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(13, 195)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 13)
        Me.Label9.TabIndex = 74
        Me.Label9.Text = "Memo :"
        '
        'txtMemo
        '
        Me.txtMemo.Location = New System.Drawing.Point(72, 192)
        Me.txtMemo.Multiline = True
        Me.txtMemo.Name = "txtMemo"
        Me.txtMemo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMemo.Size = New System.Drawing.Size(224, 93)
        Me.txtMemo.TabIndex = 75
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(302, 189)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(72, 13)
        Me.Label11.TabIndex = 78
        Me.Label11.Text = "Payment :"
        '
        'txtPayment
        '
        Me.txtPayment.Font = New System.Drawing.Font("Verdana", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPayment.Location = New System.Drawing.Point(305, 205)
        Me.txtPayment.Multiline = True
        Me.txtPayment.Name = "txtPayment"
        Me.txtPayment.Size = New System.Drawing.Size(232, 43)
        Me.txtPayment.TabIndex = 79
        Me.txtPayment.Text = "0.00"
        '
        'cboOrNo
        '
        Me.cboOrNo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboOrNo.FormattingEnabled = True
        Me.cboOrNo.Location = New System.Drawing.Point(365, 62)
        Me.cboOrNo.Name = "cboOrNo"
        Me.cboOrNo.Size = New System.Drawing.Size(168, 21)
        Me.cboOrNo.TabIndex = 80
        '
        'PanePanel1
        '
        Me.PanePanel1.BackColor = System.Drawing.Color.White
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.btnClose)
        Me.PanePanel1.Controls.Add(Me.btnPost)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 317)
        Me.PanePanel1.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(546, 39)
        Me.PanePanel1.TabIndex = 65
        '
        'btnClose
        '
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources._23
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(436, 3)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(100, 31)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "CLOSE"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPost
        '
        Me.btnPost.Font = New System.Drawing.Font("Verdana", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPost.Image = Global.WindowsApplication2.My.Resources.Resources.doc_pen
        Me.btnPost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPost.Location = New System.Drawing.Point(330, 3)
        Me.btnPost.Name = "btnPost"
        Me.btnPost.Size = New System.Drawing.Size(100, 31)
        Me.btnPost.TabIndex = 0
        Me.btnPost.Text = "POST"
        Me.btnPost.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPost.UseVisualStyleBackColor = True
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.lblFrom)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(546, 32)
        Me.PanePanel5.TabIndex = 57
        '
        'lblFrom
        '
        Me.lblFrom.AutoSize = True
        Me.lblFrom.BackColor = System.Drawing.Color.Transparent
        Me.lblFrom.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblFrom.ForeColor = System.Drawing.Color.White
        Me.lblFrom.Location = New System.Drawing.Point(4, 6)
        Me.lblFrom.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lblFrom.Name = "lblFrom"
        Me.lblFrom.Size = New System.Drawing.Size(141, 19)
        Me.lblFrom.TabIndex = 14
        Me.lblFrom.Text = "Receive Payment"
        '
        'frmLoanPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(546, 356)
        Me.Controls.Add(Me.cboOrNo)
        Me.Controls.Add(Me.txtPayment)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.txtMemo)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtDate)
        Me.Controls.Add(Me.txtCashier)
        Me.Controls.Add(Me.txtCreatedby)
        Me.Controls.Add(Me.txtLoanNo)
        Me.Controls.Add(Me.txtClientID)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.PanePanel5)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "frmLoanPayment"
        Me.Text = "frmLoanPayment"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents lblFrom As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents txtClientID As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents txtCreatedby As System.Windows.Forms.TextBox
    Friend WithEvents txtCashier As System.Windows.Forms.TextBox
    Friend WithEvents txtDate As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboMethod As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents txtMemo As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents cboBank As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtPayment As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnPost As System.Windows.Forms.Button
    Friend WithEvents cboOrNo As System.Windows.Forms.ComboBox
    Friend WithEvents cboCheckNo As System.Windows.Forms.ComboBox
End Class
