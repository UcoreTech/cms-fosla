'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: August 17, 2010                    ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports WMPs_Money_Figure_Convert_to_Words

Public Class frmCashPayment_ReceivePayment

    Private gCon As New Clsappconfiguration
    Public fcMode As String = ""
    Public fKeyEmployee As String = ""
    Public fcInterestRefund As Decimal = 0
    Private fxKeyORNo As String = Guid.NewGuid.ToString
    Public fcInterestRefundAdjustment As Decimal = 0
    Private Sub GenerateORNo()
        Dim xCtr As Integer = 1
        Dim ORNoAvailable As Boolean = False
        Dim sSqlCmd As String
        While ORNoAvailable = False
            sSqlCmd = "select * from dbo.CIMS_t_CashPayments where fnOrNumber = '" & Format(xCtr, "##00000") & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        sSqlCmd = "select * from dbo.CIMS_T_TempOR where fcORno = '" & Format(xCtr, "##00000") & "'"
                        Try
                            Using rd2 As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                                If rd2.HasRows = False Then
                                    ORNoAvailable = True
                                    txtOR.Text = Format(xCtr, "##00000")
                                    sSqlCmd = "insert into dbo.CIMS_T_TempOR (pk_TempOR, fcORno) values ('" & fxKeyORNo & "','" & Format(xCtr, "##00000") & "')"
                                    Try
                                        SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                                    Catch ex As Exception
                                        MsgBox("Error at GenerateORNo in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                                    End Try
                                Else
                                    xCtr += 1
                                End If
                            End Using
                        Catch ex As Exception
                            MsgBox("Error at GenerateORNo in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                        End Try
                    Else
                        xCtr += 1
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at GenerateORNo in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
            End Try
        End While
    End Sub
    Private Sub LoadPaymentMethod()
        Dim DTPymntMthd As New DataTable("PaymentMethod")
        Dim DR As DataRow
        Dim sSqlCmd As String = "SELECT	pk_PaymentMode, fcPaymentMode FROM	dbo.CIMS_m_PaymentMode"
        DTPymntMthd.Columns.Add("fcPaymentMode", System.Type.GetType("System.String"))
        DTPymntMthd.Columns.Add("pk_PaymentMode", System.Type.GetType("System.String"))
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DR = DTPymntMthd.NewRow
                    DR("fcPaymentMode") = rd.Item("fcPaymentMode").ToString
                    DR("pk_PaymentMode") = rd.Item("pk_PaymentMode").ToString
                    DTPymntMthd.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadPaymentMethod in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        MtcboPymntMethd.Items.Clear()
        MtcboPymntMethd.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtcboPymntMethd.SourceDataString = New String(1) {"fcPaymentMode", "pk_PaymentMode"}
        MtcboPymntMethd.SourceDataTable = DTPymntMthd
        MtcboPymntMethd.SelectedIndex = 0
    End Sub
    Private Sub LoadBankAcnt()
        Dim DTBankAcnt As New DataTable("BankAcnt")
        Dim DR As DataRow
        Dim sSqlCmd As String = "CIMS_Payments_BankAccounts_Load"
        DTBankAcnt.Columns.Add("acnt_name", System.Type.GetType("System.String"))
        DTBankAcnt.Columns.Add("acnt_id", System.Type.GetType("System.String"))
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    DR = DTBankAcnt.NewRow
                    DR("acnt_name") = rd.Item("acnt_name").ToString
                    DR("acnt_id") = rd.Item("acnt_id").ToString
                    DTBankAcnt.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadBankAcnt in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        MTcboBackAcnt.Items.Clear()
        MTcboBackAcnt.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MTcboBackAcnt.SourceDataString = New String(1) {"acnt_name", "acnt_id"}
        MTcboBackAcnt.SourceDataTable = DTBankAcnt
    End Sub
    Private Sub frmReceivePayment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Dim sSqlCmd As String = "delete dbo.CIMS_T_TempOR where pk_TempOR = '" & fxKeyORNo & "'"
        Try
            SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
        Catch ex As Exception
            MsgBox("Error at frmReceivePayment_FormClosed in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Sub
    Private Function GetContribution() As Decimal
        Dim sSqlCmd As String = "select isnull(fdContribution,0) as fdContribution from dbo.CIMS_m_Member where pk_Employee = '" & fKeyEmployee & "'"
        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                rd.Read()
                Return rd.Item(0)
            End Using
        Catch ex As Exception
            MsgBox("Error at GetContribution in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
    End Function
    Private Sub frmReceivePayment_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        DtDate.Value = Now
        LoadPaymentMethod()
        LoadBankAcnt()
        GenerateORNo()
        Select Case fcMode
            Case "Loan Pretermination"
                Me.Text = "Receive Payments - Loan Pretermination"
                lblLoan.Visible = True
                lblLoanNo.Visible = True
                Label1.Visible = False
                txtTerm.Visible = False

            Case "Amortization Settlement"
                Me.Text = "Receive Payments - Amortization Settlement"
                lblLoan.Visible = True
                lblLoanNo.Visible = True
                Label1.Visible = False
                txtTerm.Visible = False

            Case "Contribution"
                Me.Text = "Receive Payments - Contribution"
                txtPayment.Text = Format(GetContribution, "##,##0.00")
                lblLoan.Visible = False
                lblLoanNo.Visible = False
                Label1.Visible = False
                txtTerm.Visible = False

            Case "Membership Fee"
                Me.Text = "Receive Payments - Membership Fee"
                lblLoan.Visible = False
                lblLoanNo.Visible = False
                Label1.Visible = False
                txtTerm.Visible = False

            Case "Savings"
                Me.Text = "Savings Deposit - Savings"
                lblLoan.Visible = False
                lblLoanNo.Visible = False
                Label1.Visible = False
                txtTerm.Visible = False

            Case "Time Deposit"
                Me.Text = "Time Deposit - Time Deposit"
                lblLoan.Visible = False
                lblLoanNo.Visible = False
                Label1.Visible = True
                txtTerm.Visible = True
                btnPay.Enabled = False

        End Select
        txtMemo.Text = fcMode & " - LN" & lblLoanNo.Text & " - " & DtDate.Text
    End Sub
    Private Sub frmReceivePayment_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub
    Private Sub frmReceivePayment_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub
    Private Sub MtcboPymntMethd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MtcboPymntMethd.SelectedIndexChanged
        If MtcboPymntMethd.SelectedItem.Col1 = "Check" Then
            txtChkNo.Enabled = True
        Else
            txtChkNo.Text = ""
            txtChkNo.Enabled = False
        End If
    End Sub
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        GenerateORNo()
    End Sub
    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        btnPay.Enabled = False
    End Sub
    Private Sub btnPay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPay.Click
        Dim xResponse As MsgBoxResult = MsgBox("Are you sure you want to pay this?", MsgBoxStyle.Question + MsgBoxStyle.YesNo, "Confirmation")
        If xResponse = MsgBoxResult.Yes Then
            If MTcboBackAcnt.SelectedItem IsNot Nothing Then
                Dim sSqlCmd As String = "CIMS_Payment_ReceivePaymentPerType '" & fcMode & "', '" & fKeyEmployee & _
                                        "', '" & txtOR.Text & "', '" & MTcboBackAcnt.SelectedItem.Col2 & "', '" & _
                                        lblLoanNo.Text & "' , '" & DtDate.Value.ToString & "', '" & _
                                        MtcboPymntMethd.SelectedItem.Col2 & "', '" & txtChkNo.Text & _
                                        "', '" & Format(CDec(txtPayment.Text), "####0.00") & "', '" & txtMemo.Text & _
                                        "', '" & fcInterestRefund & "', '" & fcInterestRefundAdjustment & "', '" & IIf(txtTerm.Text = "", 0, txtTerm.Text) & "'"
                Try
                    SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
                Catch ex As Exception
                    MsgBox("Error at btnPay_Click in frmReceivePayment." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                End Try
                MsgBox("Your payment has been credited.", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Payment received")
                Me.Close()

                Call PrintOfficialReceipt()
            Else
                MsgBox("Please select a bank account.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Bank Account needed.")
            End If
        End If
    End Sub
    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub txtPayment_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPayment.GotFocus
        txtPayment.Text = Format(CDec(txtPayment.Text), "####0.00")
    End Sub
    Private Sub txtPayment_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPayment.KeyPress
        If e.KeyChar <> Chr(46) Then
            If Not Char.IsDigit(e.KeyChar) Then e.Handled = True
            If e.KeyChar = Chr(8) Then e.Handled = False
            If e.KeyChar = Chr(13) Then txtPayment.Focus()
        End If
    End Sub

    Private Sub PrintOfficialReceipt()
        Dim CIMSFunctions As New ClsCIMSFunctions()
        Dim paymentInWords As New WPMs

        'Load Required Parameters
        With frmRpt_OfficialReceipt
            .GetUser() = CIMSFunctions.GetLoggedInUserFullName()
            .GetEmployeePkID() = fKeyEmployee
            .GetORNo() = txtOR.Text
            .getPaymentInWords() = paymentInWords.gGenerateCurrencyInWords(txtPayment.Text)
        End With

        'Finally Load the Report
        frmRpt_OfficialReceipt.MdiParent = frmMain
        frmRpt_OfficialReceipt.Show()
    End Sub

    Private Sub DtDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DtDate.ValueChanged
        txtMemo.Text = fcMode & " - LN" & lblLoanNo.Text & " - " & DtDate.Text
    End Sub
    Private Sub txtTerm_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTerm.KeyPress
        If Not (e.KeyChar.IsDigit(e.KeyChar)) And _
            e.KeyChar <> ChrW(Keys.Back) Then
            e.Handled = True

        End If
    End Sub
    Private Sub txtTerm_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTerm.TextChanged
        If txtTerm.Text = "" Or "0" Then
            btnPay.Enabled = False
        Else : btnPay.Enabled = True
        End If
    End Sub

    Private Sub txtOR_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtOR.KeyPress
        If Not (e.KeyChar.IsDigit(e.KeyChar)) And _
                   e.KeyChar <> ChrW(Keys.Back) Then
            e.Handled = True

        End If
    End Sub
End Class