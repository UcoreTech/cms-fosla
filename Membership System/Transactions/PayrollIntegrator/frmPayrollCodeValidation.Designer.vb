<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollCodeValidation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayrollCodeValidation))
        Me.gridPersonnellist = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnChangePayrollcode = New System.Windows.Forms.Button()
        Me.btnMembermaster = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblPayrollcode = New System.Windows.Forms.Label()
        CType(Me.gridPersonnellist, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'gridPersonnellist
        '
        Me.gridPersonnellist.AllowUserToAddRows = False
        Me.gridPersonnellist.AllowUserToDeleteRows = False
        Me.gridPersonnellist.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridPersonnellist.Dock = System.Windows.Forms.DockStyle.Left
        Me.gridPersonnellist.Location = New System.Drawing.Point(0, 33)
        Me.gridPersonnellist.Name = "gridPersonnellist"
        Me.gridPersonnellist.ReadOnly = True
        Me.gridPersonnellist.Size = New System.Drawing.Size(344, 392)
        Me.gridPersonnellist.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(350, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(367, 57)
        Me.Label1.TabIndex = 13
        Me.Label1.Text = "The following members do not belong " & Global.Microsoft.VisualBasic.ChrW(13) & "to this payroll code."
        '
        'Label2
        '
        Me.Label2.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(350, 96)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(367, 57)
        Me.Label2.TabIndex = 14
        Me.Label2.Text = "Please make sure you selected the right payroll code. " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "You can also update the p" & _
            "ayroll code for the following" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "members  in the Member Master." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(350, 177)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(317, 19)
        Me.Label3.TabIndex = 15
        Me.Label3.Text = "You should update first before you can proceed." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnChangePayrollcode)
        Me.GroupBox1.Controls.Add(Me.btnMembermaster)
        Me.GroupBox1.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(354, 298)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(363, 115)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "What do you want to do ?"
        '
        'btnChangePayrollcode
        '
        Me.btnChangePayrollcode.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnChangePayrollcode.Location = New System.Drawing.Point(30, 68)
        Me.btnChangePayrollcode.Name = "btnChangePayrollcode"
        Me.btnChangePayrollcode.Size = New System.Drawing.Size(175, 27)
        Me.btnChangePayrollcode.TabIndex = 1
        Me.btnChangePayrollcode.Text = "Change Payroll Code"
        Me.btnChangePayrollcode.UseVisualStyleBackColor = True
        '
        'btnMembermaster
        '
        Me.btnMembermaster.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnMembermaster.Location = New System.Drawing.Point(30, 35)
        Me.btnMembermaster.Name = "btnMembermaster"
        Me.btnMembermaster.Size = New System.Drawing.Size(175, 27)
        Me.btnMembermaster.TabIndex = 0
        Me.btnMembermaster.Text = "Go to Member Master"
        Me.btnMembermaster.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblPayrollcode)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(729, 33)
        Me.PanePanel1.TabIndex = 11
        '
        'lblPayrollcode
        '
        Me.lblPayrollcode.AutoSize = True
        Me.lblPayrollcode.BackColor = System.Drawing.Color.Transparent
        Me.lblPayrollcode.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.lblPayrollcode.ForeColor = System.Drawing.Color.White
        Me.lblPayrollcode.Location = New System.Drawing.Point(3, 6)
        Me.lblPayrollcode.Name = "lblPayrollcode"
        Me.lblPayrollcode.Size = New System.Drawing.Size(21, 19)
        Me.lblPayrollcode.TabIndex = 2
        Me.lblPayrollcode.Text = "..."
        '
        'frmPayrollCodeValidation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(729, 425)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.gridPersonnellist)
        Me.Controls.Add(Me.PanePanel1)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmPayrollCodeValidation"
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Payroll Code Validation"
        CType(Me.gridPersonnellist, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents lblPayrollcode As System.Windows.Forms.Label
    Friend WithEvents gridPersonnellist As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnChangePayrollcode As System.Windows.Forms.Button
    Friend WithEvents btnMembermaster As System.Windows.Forms.Button
End Class
