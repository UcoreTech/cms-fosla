<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMember_Master
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMember_Master))
        Me.tabMember = New System.Windows.Forms.TabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.chkBereaveYes = New System.Windows.Forms.CheckBox()
        Me.chkBereaveNo = New System.Windows.Forms.CheckBox()
        Me.mem_WithdrawDate = New System.Windows.Forms.DateTimePicker()
        Me.mem_PaycontDate = New System.Windows.Forms.DateTimePicker()
        Me.chkNo = New System.Windows.Forms.CheckBox()
        Me.Label110 = New System.Windows.Forms.Label()
        Me.txtpayrollcontriamount = New System.Windows.Forms.TextBox()
        Me.chkyes = New System.Windows.Forms.CheckBox()
        Me.Label109 = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.txtwithdrawal = New System.Windows.Forms.TextBox()
        Me.mem_MemberDate = New System.Windows.Forms.DateTimePicker()
        Me.Label111 = New System.Windows.Forms.Label()
        Me.Label108 = New System.Windows.Forms.Label()
        Me.Label65 = New System.Windows.Forms.Label()
        Me.Label105 = New System.Windows.Forms.Label()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.cboemp_status = New System.Windows.Forms.ComboBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.cboPaycode = New System.Windows.Forms.ComboBox()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.cbotaxcode = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboPositionLevel = New System.Windows.Forms.ComboBox()
        Me.emp_company = New System.Windows.Forms.TextBox()
        Me.Label114 = New System.Windows.Forms.Label()
        Me.emp_Ytenures = New System.Windows.Forms.TextBox()
        Me.Label100 = New System.Windows.Forms.Label()
        Me.txtlocalofficenumber = New System.Windows.Forms.TextBox()
        Me.btnrestricted = New System.Windows.Forms.Button()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.txtofficenumber = New System.Windows.Forms.TextBox()
        Me.txtofficeadd = New System.Windows.Forms.TextBox()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.emp_Datehired = New System.Windows.Forms.DateTimePicker()
        Me.cbopayroll = New System.Windows.Forms.ComboBox()
        Me.Label62 = New System.Windows.Forms.Label()
        Me.cborate = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.cboEmp_type = New System.Windows.Forms.ComboBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label69 = New System.Windows.Forms.Label()
        Me.cbotitledesignation = New System.Windows.Forms.ComboBox()
        Me.txtbasicpay = New System.Windows.Forms.TextBox()
        Me.Label59 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.btnUpdateBA = New System.Windows.Forms.Button()
        Me.btnDeleteBA = New System.Windows.Forms.Button()
        Me.btnNewBA = New System.Windows.Forms.Button()
        Me.lvlBankInfo = New System.Windows.Forms.ListView()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.btnUpdateSInc = New System.Windows.Forms.Button()
        Me.btnDeleteSInc = New System.Windows.Forms.Button()
        Me.btnsaveSInc = New System.Windows.Forms.Button()
        Me.lvlSourceIncome = New System.Windows.Forms.ListView()
        Me.Label36 = New System.Windows.Forms.Label()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.TabPage17 = New System.Windows.Forms.TabPage()
        Me.BtnEditContact = New System.Windows.Forms.Button()
        Me.BtnDeleteContact = New System.Windows.Forms.Button()
        Me.btnaddContact = New System.Windows.Forms.Button()
        Me.lvlNearestRelatives = New System.Windows.Forms.ListView()
        Me.Label98 = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.LvlEmployeeDependent = New System.Windows.Forms.ListView()
        Me.Label68 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.emp_pic = New System.Windows.Forms.PictureBox()
        Me.temp_marks = New System.Windows.Forms.TextBox()
        Me.txtitemno = New System.Windows.Forms.TextBox()
        Me.txtremarks = New System.Windows.Forms.TextBox()
        Me.temp_add3 = New System.Windows.Forms.TextBox()
        Me.txtparent_ID = New System.Windows.Forms.TextBox()
        Me.txtmonthreqular = New System.Windows.Forms.TextBox()
        Me.txtkeysection = New System.Windows.Forms.TextBox()
        Me.temp_height = New System.Windows.Forms.TextBox()
        Me.txtdescsection = New System.Windows.Forms.TextBox()
        Me.txtsalarygrade = New System.Windows.Forms.TextBox()
        Me.txtdescdepartment = New System.Windows.Forms.TextBox()
        Me.temp_add2 = New System.Windows.Forms.TextBox()
        Me.txtdescdivision = New System.Windows.Forms.TextBox()
        Me.txtweight = New System.Windows.Forms.TextBox()
        Me.txtkeydepartment = New System.Windows.Forms.TextBox()
        Me.txtdateregular = New System.Windows.Forms.TextBox()
        Me.txtkeydivision = New System.Windows.Forms.TextBox()
        Me.Dateregular = New System.Windows.Forms.DateTimePicker()
        Me.txtkeycompany = New System.Windows.Forms.TextBox()
        Me.emp_extphone = New System.Windows.Forms.MaskedTextBox()
        Me.cbosalarygrade = New System.Windows.Forms.ComboBox()
        Me.temp_citizen = New System.Windows.Forms.TextBox()
        Me.temp_designation = New System.Windows.Forms.TextBox()
        Me.txtType_employee = New System.Windows.Forms.TextBox()
        Me.txtfxkeypositionlevel = New System.Windows.Forms.TextBox()
        Me.txtfullname = New System.Windows.Forms.TextBox()
        Me.TXTRECID = New System.Windows.Forms.TextBox()
        Me.cbodept = New System.Windows.Forms.TextBox()
        Me.Dateresigned = New System.Windows.Forms.DateTimePicker()
        Me.TXTKEYEMPLOYEEID = New System.Windows.Forms.TextBox()
        Me.btnemp_editdepdts = New System.Windows.Forms.Button()
        Me.btndelete_dep = New System.Windows.Forms.Button()
        Me.btnadd_dep = New System.Windows.Forms.Button()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.others = New System.Windows.Forms.TabPage()
        Me.btnEditMobile = New System.Windows.Forms.Button()
        Me.gridMobileNos = New System.Windows.Forms.DataGridView()
        Me.txtEloadingLimit = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.txtEloaderPIN = New System.Windows.Forms.TextBox()
        Me.chkShowPINChar = New System.Windows.Forms.CheckBox()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btnAddMobileNo = New System.Windows.Forms.Button()
        Me.chkIsEloader = New System.Windows.Forms.CheckBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtMobileNo = New System.Windows.Forms.MaskedTextBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.Cbomem_Status = New System.Windows.Forms.ComboBox()
        Me.Label107 = New System.Windows.Forms.Label()
        Me.txtMemberID = New System.Windows.Forms.TextBox()
        Me.temp_placebirth = New System.Windows.Forms.TextBox()
        Me.txtage1 = New System.Windows.Forms.TextBox()
        Me.lblage = New System.Windows.Forms.Label()
        Me.EMP_DATEbirth = New System.Windows.Forms.DateTimePicker()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TextBox15 = New System.Windows.Forms.TextBox()
        Me.TextBox16 = New System.Windows.Forms.TextBox()
        Me.TextBox17 = New System.Windows.Forms.TextBox()
        Me.TextBox18 = New System.Windows.Forms.TextBox()
        Me.TextBox19 = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.MaskedTextBox5 = New System.Windows.Forms.MaskedTextBox()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ErrorProvider1 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtEmployeeNo = New System.Windows.Forms.TextBox()
        Me.temp_fname = New System.Windows.Forms.TextBox()
        Me.temp_lname = New System.Windows.Forms.TextBox()
        Me.temp_midname = New System.Windows.Forms.TextBox()
        Me.txtprovincialadd = New System.Windows.Forms.TextBox()
        Me.cboemp_gender = New System.Windows.Forms.ComboBox()
        Me.cboemp_civil = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtresidencephone = New System.Windows.Forms.MaskedTextBox()
        Me.txtEmailAddress = New System.Windows.Forms.TextBox()
        Me.txtorgchart = New System.Windows.Forms.TextBox()
        Me.Label85 = New System.Windows.Forms.Label()
        Me.txtResidenceadd = New System.Windows.Forms.TextBox()
        Me.Label86 = New System.Windows.Forms.Label()
        Me.txtDepartment2 = New System.Windows.Forms.TextBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer()
        Me.Label66 = New System.Windows.Forms.Label()
        Me.Label67 = New System.Windows.Forms.Label()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.txtpk_Employee = New System.Windows.Forms.TextBox()
        Me.Label112 = New System.Windows.Forms.Label()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.grpPayrollStatus = New System.Windows.Forms.GroupBox()
        Me.rdoExempt = New System.Windows.Forms.RadioButton()
        Me.rdoNonExempt = New System.Windows.Forms.RadioButton()
        Me.btnemp_close = New System.Windows.Forms.Button()
        Me.PanePanel2 = New WindowsApplication2.PanePanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.btnemp_delete = New System.Windows.Forms.Button()
        Me.btnemp_edit = New System.Windows.Forms.Button()
        Me.btnemp_add = New System.Windows.Forms.Button()
        Me.btnemp_next = New System.Windows.Forms.Button()
        Me.BTNSEARCH = New System.Windows.Forms.Button()
        Me.btnemp_previous = New System.Windows.Forms.Button()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.lblemployee_name = New System.Windows.Forms.Label()
        Me.Label44 = New System.Windows.Forms.Label()
        Me.tabMember.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage1.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage17.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.others.SuspendLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpPayrollStatus.SuspendLayout()
        Me.PanePanel2.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabMember
        '
        Me.tabMember.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.tabMember.Controls.Add(Me.TabPage5)
        Me.tabMember.Controls.Add(Me.TabPage1)
        Me.tabMember.Controls.Add(Me.TabPage2)
        Me.tabMember.Controls.Add(Me.TabPage4)
        Me.tabMember.Controls.Add(Me.TabPage17)
        Me.tabMember.Controls.Add(Me.TabPage3)
        Me.tabMember.Controls.Add(Me.others)
        Me.tabMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tabMember.ItemSize = New System.Drawing.Size(120, 18)
        Me.tabMember.Location = New System.Drawing.Point(7, 303)
        Me.tabMember.Name = "tabMember"
        Me.tabMember.SelectedIndex = 0
        Me.tabMember.Size = New System.Drawing.Size(996, 205)
        Me.tabMember.TabIndex = 16
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage5.Controls.Add(Me.grpPayrollStatus)
        Me.TabPage5.Controls.Add(Me.chkBereaveYes)
        Me.TabPage5.Controls.Add(Me.chkBereaveNo)
        Me.TabPage5.Controls.Add(Me.mem_WithdrawDate)
        Me.TabPage5.Controls.Add(Me.mem_PaycontDate)
        Me.TabPage5.Controls.Add(Me.chkNo)
        Me.TabPage5.Controls.Add(Me.Label110)
        Me.TabPage5.Controls.Add(Me.txtpayrollcontriamount)
        Me.TabPage5.Controls.Add(Me.chkyes)
        Me.TabPage5.Controls.Add(Me.Label109)
        Me.TabPage5.Controls.Add(Me.Label37)
        Me.TabPage5.Controls.Add(Me.txtwithdrawal)
        Me.TabPage5.Controls.Add(Me.mem_MemberDate)
        Me.TabPage5.Controls.Add(Me.Label111)
        Me.TabPage5.Controls.Add(Me.Label108)
        Me.TabPage5.Controls.Add(Me.Label65)
        Me.TabPage5.Controls.Add(Me.Label105)
        Me.TabPage5.Controls.Add(Me.PictureBox8)
        Me.TabPage5.Controls.Add(Me.cboemp_status)
        Me.TabPage5.Controls.Add(Me.Label11)
        Me.TabPage5.Location = New System.Drawing.Point(4, 22)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Size = New System.Drawing.Size(988, 179)
        Me.TabPage5.TabIndex = 4
        Me.TabPage5.Text = "Member Information"
        Me.TabPage5.UseVisualStyleBackColor = True
        '
        'chkBereaveYes
        '
        Me.chkBereaveYes.AutoSize = True
        Me.chkBereaveYes.Location = New System.Drawing.Point(152, 65)
        Me.chkBereaveYes.Name = "chkBereaveYes"
        Me.chkBereaveYes.Size = New System.Drawing.Size(46, 19)
        Me.chkBereaveYes.TabIndex = 117
        Me.chkBereaveYes.Text = "Yes"
        Me.chkBereaveYes.UseVisualStyleBackColor = True
        '
        'chkBereaveNo
        '
        Me.chkBereaveNo.AutoSize = True
        Me.chkBereaveNo.Location = New System.Drawing.Point(210, 65)
        Me.chkBereaveNo.Name = "chkBereaveNo"
        Me.chkBereaveNo.Size = New System.Drawing.Size(42, 19)
        Me.chkBereaveNo.TabIndex = 116
        Me.chkBereaveNo.Text = "No"
        Me.chkBereaveNo.UseVisualStyleBackColor = True
        '
        'mem_WithdrawDate
        '
        Me.mem_WithdrawDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_WithdrawDate.Location = New System.Drawing.Point(600, 36)
        Me.mem_WithdrawDate.Name = "mem_WithdrawDate"
        Me.mem_WithdrawDate.Size = New System.Drawing.Size(27, 21)
        Me.mem_WithdrawDate.TabIndex = 110
        '
        'mem_PaycontDate
        '
        Me.mem_PaycontDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_PaycontDate.Location = New System.Drawing.Point(806, 33)
        Me.mem_PaycontDate.Name = "mem_PaycontDate"
        Me.mem_PaycontDate.Size = New System.Drawing.Size(104, 21)
        Me.mem_PaycontDate.TabIndex = 1
        Me.mem_PaycontDate.Visible = False
        '
        'chkNo
        '
        Me.chkNo.AutoSize = True
        Me.chkNo.Location = New System.Drawing.Point(211, 38)
        Me.chkNo.Name = "chkNo"
        Me.chkNo.Size = New System.Drawing.Size(42, 19)
        Me.chkNo.TabIndex = 1
        Me.chkNo.Text = "No"
        Me.chkNo.UseVisualStyleBackColor = True
        '
        'Label110
        '
        Me.Label110.AutoSize = True
        Me.Label110.Location = New System.Drawing.Point(758, 33)
        Me.Label110.Name = "Label110"
        Me.Label110.Size = New System.Drawing.Size(33, 15)
        Me.Label110.TabIndex = 109
        Me.Label110.Text = "Date"
        Me.Label110.Visible = False
        '
        'txtpayrollcontriamount
        '
        Me.txtpayrollcontriamount.AcceptsTab = True
        Me.txtpayrollcontriamount.Location = New System.Drawing.Point(495, 63)
        Me.txtpayrollcontriamount.Name = "txtpayrollcontriamount"
        Me.txtpayrollcontriamount.Size = New System.Drawing.Size(104, 21)
        Me.txtpayrollcontriamount.TabIndex = 0
        Me.txtpayrollcontriamount.Text = "0.00"
        Me.txtpayrollcontriamount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'chkyes
        '
        Me.chkyes.AutoSize = True
        Me.chkyes.Location = New System.Drawing.Point(152, 38)
        Me.chkyes.Name = "chkyes"
        Me.chkyes.Size = New System.Drawing.Size(46, 19)
        Me.chkyes.TabIndex = 0
        Me.chkyes.Text = "Yes"
        Me.chkyes.UseVisualStyleBackColor = True
        '
        'Label109
        '
        Me.Label109.AutoSize = True
        Me.Label109.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label109.Location = New System.Drawing.Point(329, 64)
        Me.Label109.Name = "Label109"
        Me.Label109.Size = New System.Drawing.Size(129, 15)
        Me.Label109.TabIndex = 109
        Me.Label109.Text = "Contribution Amount:"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(13, 40)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(65, 15)
        Me.Label37.TabIndex = 115
        Me.Label37.Text = "Employed:"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'txtwithdrawal
        '
        Me.txtwithdrawal.Location = New System.Drawing.Point(495, 36)
        Me.txtwithdrawal.Name = "txtwithdrawal"
        Me.txtwithdrawal.Size = New System.Drawing.Size(104, 21)
        Me.txtwithdrawal.TabIndex = 6
        '
        'mem_MemberDate
        '
        Me.mem_MemberDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.mem_MemberDate.Location = New System.Drawing.Point(152, 95)
        Me.mem_MemberDate.Name = "mem_MemberDate"
        Me.mem_MemberDate.Size = New System.Drawing.Size(116, 21)
        Me.mem_MemberDate.TabIndex = 5
        '
        'Label111
        '
        Me.Label111.AutoSize = True
        Me.Label111.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label111.Location = New System.Drawing.Point(329, 39)
        Me.Label111.Name = "Label111"
        Me.Label111.Size = New System.Drawing.Size(160, 15)
        Me.Label111.TabIndex = 109
        Me.Label111.Text = "Members Withdrawal Date:"
        '
        'Label108
        '
        Me.Label108.AutoSize = True
        Me.Label108.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label108.Location = New System.Drawing.Point(13, 96)
        Me.Label108.Name = "Label108"
        Me.Label108.Size = New System.Drawing.Size(110, 15)
        Me.Label108.TabIndex = 102
        Me.Label108.Text = "Membership Date:"
        Me.Label108.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label65
        '
        Me.Label65.AutoSize = True
        Me.Label65.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label65.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label65.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label65.Location = New System.Drawing.Point(15, 5)
        Me.Label65.Name = "Label65"
        Me.Label65.Size = New System.Drawing.Size(150, 18)
        Me.Label65.TabIndex = 9
        Me.Label65.Text = "Member Information"
        '
        'Label105
        '
        Me.Label105.AutoSize = True
        Me.Label105.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label105.Location = New System.Drawing.Point(13, 64)
        Me.Label105.Name = "Label105"
        Me.Label105.Size = New System.Drawing.Size(86, 15)
        Me.Label105.TabIndex = 102
        Me.Label105.Text = "Bereavement:"
        Me.Label105.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PictureBox8
        '
        Me.PictureBox8.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox8.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox8.Location = New System.Drawing.Point(9, 5)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(813, 19)
        Me.PictureBox8.TabIndex = 32
        Me.PictureBox8.TabStop = False
        '
        'cboemp_status
        '
        Me.cboemp_status.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_status.FormattingEnabled = True
        Me.cboemp_status.Location = New System.Drawing.Point(152, 122)
        Me.cboemp_status.Name = "cboemp_status"
        Me.cboemp_status.Size = New System.Drawing.Size(116, 23)
        Me.cboemp_status.TabIndex = 3
        Me.cboemp_status.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(13, 125)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(125, 15)
        Me.Label11.TabIndex = 20
        Me.Label11.Text = "Employement Status:"
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label11.Visible = False
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage1.Controls.Add(Me.cboPaycode)
        Me.TabPage1.Controls.Add(Me.Label38)
        Me.TabPage1.Controls.Add(Me.cbotaxcode)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Controls.Add(Me.cboPositionLevel)
        Me.TabPage1.Controls.Add(Me.emp_company)
        Me.TabPage1.Controls.Add(Me.Label114)
        Me.TabPage1.Controls.Add(Me.emp_Ytenures)
        Me.TabPage1.Controls.Add(Me.Label100)
        Me.TabPage1.Controls.Add(Me.txtlocalofficenumber)
        Me.TabPage1.Controls.Add(Me.btnrestricted)
        Me.TabPage1.Controls.Add(Me.Label34)
        Me.TabPage1.Controls.Add(Me.txtofficenumber)
        Me.TabPage1.Controls.Add(Me.txtofficeadd)
        Me.TabPage1.Controls.Add(Me.Label23)
        Me.TabPage1.Controls.Add(Me.Label22)
        Me.TabPage1.Controls.Add(Me.Label21)
        Me.TabPage1.Controls.Add(Me.Label20)
        Me.TabPage1.Controls.Add(Me.emp_Datehired)
        Me.TabPage1.Controls.Add(Me.cbopayroll)
        Me.TabPage1.Controls.Add(Me.Label62)
        Me.TabPage1.Controls.Add(Me.cborate)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.cboEmp_type)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label69)
        Me.TabPage1.Controls.Add(Me.cbotitledesignation)
        Me.TabPage1.Controls.Add(Me.txtbasicpay)
        Me.TabPage1.Controls.Add(Me.Label59)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.PictureBox3)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(988, 179)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Employment Information"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'cboPaycode
        '
        Me.cboPaycode.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPaycode.FormattingEnabled = True
        Me.cboPaycode.Location = New System.Drawing.Point(567, 107)
        Me.cboPaycode.Name = "cboPaycode"
        Me.cboPaycode.Size = New System.Drawing.Size(208, 23)
        Me.cboPaycode.TabIndex = 112
        '
        'Label38
        '
        Me.Label38.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.Location = New System.Drawing.Point(481, 108)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(61, 15)
        Me.Label38.TabIndex = 111
        Me.Label38.Text = "Pay Code:"
        '
        'cbotaxcode
        '
        Me.cbotaxcode.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotaxcode.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbotaxcode.FormattingEnabled = True
        Me.cbotaxcode.Location = New System.Drawing.Point(871, 84)
        Me.cbotaxcode.Name = "cbotaxcode"
        Me.cbotaxcode.Size = New System.Drawing.Size(116, 23)
        Me.cbotaxcode.TabIndex = 107
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(791, 88)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 15)
        Me.Label1.TabIndex = 108
        Me.Label1.Text = "Tax Code:"
        '
        'cboPositionLevel
        '
        Me.cboPositionLevel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboPositionLevel.FormattingEnabled = True
        Me.cboPositionLevel.Location = New System.Drawing.Point(567, 81)
        Me.cboPositionLevel.Name = "cboPositionLevel"
        Me.cboPositionLevel.Size = New System.Drawing.Size(208, 23)
        Me.cboPositionLevel.TabIndex = 50
        '
        'emp_company
        '
        Me.emp_company.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_company.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_company.Location = New System.Drawing.Point(111, 33)
        Me.emp_company.Name = "emp_company"
        Me.emp_company.Size = New System.Drawing.Size(345, 21)
        Me.emp_company.TabIndex = 103
        '
        'Label114
        '
        Me.Label114.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label114.AutoSize = True
        Me.Label114.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label114.Location = New System.Drawing.Point(481, 85)
        Me.Label114.Name = "Label114"
        Me.Label114.Size = New System.Drawing.Size(87, 15)
        Me.Label114.TabIndex = 49
        Me.Label114.Text = "Position Level:"
        '
        'emp_Ytenures
        '
        Me.emp_Ytenures.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_Ytenures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_Ytenures.Location = New System.Drawing.Point(110, 130)
        Me.emp_Ytenures.Name = "emp_Ytenures"
        Me.emp_Ytenures.ReadOnly = True
        Me.emp_Ytenures.Size = New System.Drawing.Size(220, 21)
        Me.emp_Ytenures.TabIndex = 106
        '
        'Label100
        '
        Me.Label100.AutoSize = True
        Me.Label100.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label100.Location = New System.Drawing.Point(3, 131)
        Me.Label100.Name = "Label100"
        Me.Label100.Size = New System.Drawing.Size(95, 15)
        Me.Label100.TabIndex = 45
        Me.Label100.Text = "Years of Tenure:"
        '
        'txtlocalofficenumber
        '
        Me.txtlocalofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtlocalofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtlocalofficenumber.Location = New System.Drawing.Point(110, 106)
        Me.txtlocalofficenumber.MaxLength = 255
        Me.txtlocalofficenumber.Name = "txtlocalofficenumber"
        Me.txtlocalofficenumber.Size = New System.Drawing.Size(292, 21)
        Me.txtlocalofficenumber.TabIndex = 4
        '
        'btnrestricted
        '
        Me.btnrestricted.ForeColor = System.Drawing.Color.Red
        Me.btnrestricted.Location = New System.Drawing.Point(108, 155)
        Me.btnrestricted.Name = "btnrestricted"
        Me.btnrestricted.Size = New System.Drawing.Size(85, 25)
        Me.btnrestricted.TabIndex = 8
        Me.btnrestricted.Text = "Confidential"
        Me.btnrestricted.UseVisualStyleBackColor = True
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label34.Location = New System.Drawing.Point(11, 8)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(172, 18)
        Me.Label34.TabIndex = 19
        Me.Label34.Text = "Employee Information"
        '
        'txtofficenumber
        '
        Me.txtofficenumber.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficenumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficenumber.Location = New System.Drawing.Point(110, 82)
        Me.txtofficenumber.MaxLength = 255
        Me.txtofficenumber.Name = "txtofficenumber"
        Me.txtofficenumber.Size = New System.Drawing.Size(292, 21)
        Me.txtofficenumber.TabIndex = 3
        '
        'txtofficeadd
        '
        Me.txtofficeadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtofficeadd.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtofficeadd.Location = New System.Drawing.Point(111, 58)
        Me.txtofficeadd.MaxLength = 50
        Me.txtofficeadd.Name = "txtofficeadd"
        Me.txtofficeadd.Size = New System.Drawing.Size(345, 21)
        Me.txtofficeadd.TabIndex = 2
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(3, 109)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(60, 15)
        Me.Label23.TabIndex = 5
        Me.Label23.Text = "Local No.:"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(3, 85)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(65, 15)
        Me.Label22.TabIndex = 4
        Me.Label22.Text = "Office No.:"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(4, 62)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(90, 15)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Office Address:"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(4, 36)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(62, 15)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Company:"
        '
        'emp_Datehired
        '
        Me.emp_Datehired.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.emp_Datehired.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_Datehired.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.emp_Datehired.Location = New System.Drawing.Point(567, 152)
        Me.emp_Datehired.Name = "emp_Datehired"
        Me.emp_Datehired.Size = New System.Drawing.Size(94, 21)
        Me.emp_Datehired.TabIndex = 14
        Me.emp_Datehired.Value = New Date(2006, 5, 3, 0, 0, 0, 0)
        '
        'cbopayroll
        '
        Me.cbopayroll.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbopayroll.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbopayroll.FormattingEnabled = True
        Me.cbopayroll.Location = New System.Drawing.Point(871, 33)
        Me.cbopayroll.Name = "cbopayroll"
        Me.cbopayroll.Size = New System.Drawing.Size(116, 23)
        Me.cbopayroll.TabIndex = 11
        '
        'Label62
        '
        Me.Label62.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label62.AutoSize = True
        Me.Label62.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label62.Location = New System.Drawing.Point(791, 36)
        Me.Label62.Name = "Label62"
        Me.Label62.Size = New System.Drawing.Size(79, 15)
        Me.Label62.TabIndex = 15
        Me.Label62.Text = "Payroll Code:"
        '
        'cborate
        '
        Me.cborate.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cborate.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cborate.FormattingEnabled = True
        Me.cborate.Location = New System.Drawing.Point(871, 59)
        Me.cborate.Name = "cborate"
        Me.cborate.Size = New System.Drawing.Size(116, 23)
        Me.cborate.TabIndex = 14
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(481, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(84, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Position Type:"
        '
        'cboEmp_type
        '
        Me.cboEmp_type.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboEmp_type.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboEmp_type.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEmp_type.FormattingEnabled = True
        Me.cboEmp_type.Location = New System.Drawing.Point(567, 31)
        Me.cboEmp_type.Name = "cboEmp_type"
        Me.cboEmp_type.Size = New System.Drawing.Size(208, 23)
        Me.cboEmp_type.TabIndex = 5
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(481, 59)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(82, 15)
        Me.Label18.TabIndex = 27
        Me.Label18.Text = "Position Title:"
        '
        'Label69
        '
        Me.Label69.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label69.AutoSize = True
        Me.Label69.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label69.Location = New System.Drawing.Point(791, 62)
        Me.Label69.Name = "Label69"
        Me.Label69.Size = New System.Drawing.Size(65, 15)
        Me.Label69.TabIndex = 34
        Me.Label69.Text = "Rate Type:"
        '
        'cbotitledesignation
        '
        Me.cbotitledesignation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbotitledesignation.FormattingEnabled = True
        Me.cbotitledesignation.Location = New System.Drawing.Point(567, 56)
        Me.cbotitledesignation.Name = "cbotitledesignation"
        Me.cbotitledesignation.Size = New System.Drawing.Size(208, 23)
        Me.cbotitledesignation.TabIndex = 6
        '
        'txtbasicpay
        '
        Me.txtbasicpay.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtbasicpay.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtbasicpay.Location = New System.Drawing.Point(108, 159)
        Me.txtbasicpay.Name = "txtbasicpay"
        Me.txtbasicpay.Size = New System.Drawing.Size(85, 21)
        Me.txtbasicpay.TabIndex = 6
        Me.txtbasicpay.Text = "0.00"
        '
        'Label59
        '
        Me.Label59.AutoSize = True
        Me.Label59.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label59.Location = New System.Drawing.Point(1, 161)
        Me.Label59.Name = "Label59"
        Me.Label59.Size = New System.Drawing.Size(59, 15)
        Me.Label59.TabIndex = 12
        Me.Label59.Text = "Basic Pay:"
        '
        'Label13
        '
        Me.Label13.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(481, 155)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(70, 15)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Date Hired:"
        '
        'PictureBox3
        '
        Me.PictureBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox3.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox3.Location = New System.Drawing.Point(4, 6)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(1065, 22)
        Me.PictureBox3.TabIndex = 18
        Me.PictureBox3.TabStop = False
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage2.Controls.Add(Me.btnUpdateBA)
        Me.TabPage2.Controls.Add(Me.btnDeleteBA)
        Me.TabPage2.Controls.Add(Me.btnNewBA)
        Me.TabPage2.Controls.Add(Me.lvlBankInfo)
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.PictureBox4)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Size = New System.Drawing.Size(988, 179)
        Me.TabPage2.TabIndex = 18
        Me.TabPage2.Text = "Bank Information"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'btnUpdateBA
        '
        Me.btnUpdateBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateBA.Image = CType(resources.GetObject("btnUpdateBA.Image"), System.Drawing.Image)
        Me.btnUpdateBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateBA.Location = New System.Drawing.Point(684, 153)
        Me.btnUpdateBA.Name = "btnUpdateBA"
        Me.btnUpdateBA.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.btnUpdateBA.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateBA.TabIndex = 24
        Me.btnUpdateBA.Text = "Edit"
        Me.btnUpdateBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateBA.UseVisualStyleBackColor = True
        '
        'btnDeleteBA
        '
        Me.btnDeleteBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteBA.Image = CType(resources.GetObject("btnDeleteBA.Image"), System.Drawing.Image)
        Me.btnDeleteBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteBA.Location = New System.Drawing.Point(754, 153)
        Me.btnDeleteBA.Name = "btnDeleteBA"
        Me.btnDeleteBA.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteBA.TabIndex = 25
        Me.btnDeleteBA.Text = "Delete"
        Me.btnDeleteBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteBA.UseVisualStyleBackColor = True
        '
        'btnNewBA
        '
        Me.btnNewBA.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNewBA.Image = CType(resources.GetObject("btnNewBA.Image"), System.Drawing.Image)
        Me.btnNewBA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNewBA.Location = New System.Drawing.Point(613, 153)
        Me.btnNewBA.Name = "btnNewBA"
        Me.btnNewBA.Size = New System.Drawing.Size(68, 24)
        Me.btnNewBA.TabIndex = 23
        Me.btnNewBA.Text = "New"
        Me.btnNewBA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnNewBA.UseVisualStyleBackColor = True
        '
        'lvlBankInfo
        '
        Me.lvlBankInfo.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlBankInfo.Location = New System.Drawing.Point(5, 31)
        Me.lvlBankInfo.Name = "lvlBankInfo"
        Me.lvlBankInfo.Size = New System.Drawing.Size(817, 120)
        Me.lvlBankInfo.TabIndex = 22
        Me.lvlBankInfo.UseCompatibleStateImageBehavior = False
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label35.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label35.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label35.Location = New System.Drawing.Point(11, 5)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(136, 18)
        Me.Label35.TabIndex = 21
        Me.Label35.Text = "Bank Information"
        '
        'PictureBox4
        '
        Me.PictureBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox4.Location = New System.Drawing.Point(5, 3)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(900, 22)
        Me.PictureBox4.TabIndex = 20
        Me.PictureBox4.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage4.Controls.Add(Me.btnUpdateSInc)
        Me.TabPage4.Controls.Add(Me.btnDeleteSInc)
        Me.TabPage4.Controls.Add(Me.btnsaveSInc)
        Me.TabPage4.Controls.Add(Me.lvlSourceIncome)
        Me.TabPage4.Controls.Add(Me.Label36)
        Me.TabPage4.Controls.Add(Me.PictureBox6)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(988, 179)
        Me.TabPage4.TabIndex = 19
        Me.TabPage4.Text = "Source of Income"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'btnUpdateSInc
        '
        Me.btnUpdateSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnUpdateSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnUpdateSInc.Image = CType(resources.GetObject("btnUpdateSInc.Image"), System.Drawing.Image)
        Me.btnUpdateSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnUpdateSInc.Location = New System.Drawing.Point(686, 153)
        Me.btnUpdateSInc.Name = "btnUpdateSInc"
        Me.btnUpdateSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnUpdateSInc.TabIndex = 28
        Me.btnUpdateSInc.Text = "Edit"
        Me.btnUpdateSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnUpdateSInc.UseVisualStyleBackColor = True
        '
        'btnDeleteSInc
        '
        Me.btnDeleteSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnDeleteSInc.Image = CType(resources.GetObject("btnDeleteSInc.Image"), System.Drawing.Image)
        Me.btnDeleteSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDeleteSInc.Location = New System.Drawing.Point(755, 153)
        Me.btnDeleteSInc.Name = "btnDeleteSInc"
        Me.btnDeleteSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnDeleteSInc.TabIndex = 27
        Me.btnDeleteSInc.Text = "Delete"
        Me.btnDeleteSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnDeleteSInc.UseVisualStyleBackColor = True
        '
        'btnsaveSInc
        '
        Me.btnsaveSInc.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnsaveSInc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnsaveSInc.Image = CType(resources.GetObject("btnsaveSInc.Image"), System.Drawing.Image)
        Me.btnsaveSInc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnsaveSInc.Location = New System.Drawing.Point(614, 153)
        Me.btnsaveSInc.Name = "btnsaveSInc"
        Me.btnsaveSInc.Size = New System.Drawing.Size(68, 24)
        Me.btnsaveSInc.TabIndex = 26
        Me.btnsaveSInc.Text = "New"
        Me.btnsaveSInc.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnsaveSInc.UseVisualStyleBackColor = True
        '
        'lvlSourceIncome
        '
        Me.lvlSourceIncome.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlSourceIncome.Location = New System.Drawing.Point(6, 30)
        Me.lvlSourceIncome.Name = "lvlSourceIncome"
        Me.lvlSourceIncome.Size = New System.Drawing.Size(816, 120)
        Me.lvlSourceIncome.TabIndex = 25
        Me.lvlSourceIncome.UseCompatibleStateImageBehavior = False
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label36.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label36.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label36.Location = New System.Drawing.Point(7, 5)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(142, 18)
        Me.Label36.TabIndex = 24
        Me.Label36.Text = "Source of Income"
        '
        'PictureBox6
        '
        Me.PictureBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox6.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox6.Location = New System.Drawing.Point(6, 3)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(900, 22)
        Me.PictureBox6.TabIndex = 23
        Me.PictureBox6.TabStop = False
        '
        'TabPage17
        '
        Me.TabPage17.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage17.Controls.Add(Me.BtnEditContact)
        Me.TabPage17.Controls.Add(Me.BtnDeleteContact)
        Me.TabPage17.Controls.Add(Me.btnaddContact)
        Me.TabPage17.Controls.Add(Me.lvlNearestRelatives)
        Me.TabPage17.Controls.Add(Me.Label98)
        Me.TabPage17.Controls.Add(Me.PictureBox1)
        Me.TabPage17.Location = New System.Drawing.Point(4, 22)
        Me.TabPage17.Name = "TabPage17"
        Me.TabPage17.Size = New System.Drawing.Size(988, 179)
        Me.TabPage17.TabIndex = 17
        Me.TabPage17.Text = "Nearest Relatives"
        Me.TabPage17.UseVisualStyleBackColor = True
        '
        'BtnEditContact
        '
        Me.BtnEditContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnEditContact.Image = CType(resources.GetObject("BtnEditContact.Image"), System.Drawing.Image)
        Me.BtnEditContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnEditContact.Location = New System.Drawing.Point(685, 153)
        Me.BtnEditContact.Name = "BtnEditContact"
        Me.BtnEditContact.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.BtnEditContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnEditContact.TabIndex = 20
        Me.BtnEditContact.Text = "Edit"
        Me.BtnEditContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnEditContact.UseVisualStyleBackColor = True
        '
        'BtnDeleteContact
        '
        Me.BtnDeleteContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnDeleteContact.Image = CType(resources.GetObject("BtnDeleteContact.Image"), System.Drawing.Image)
        Me.BtnDeleteContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnDeleteContact.Location = New System.Drawing.Point(754, 153)
        Me.BtnDeleteContact.Name = "BtnDeleteContact"
        Me.BtnDeleteContact.Size = New System.Drawing.Size(68, 24)
        Me.BtnDeleteContact.TabIndex = 21
        Me.BtnDeleteContact.Text = "Delete"
        Me.BtnDeleteContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnDeleteContact.UseVisualStyleBackColor = True
        '
        'btnaddContact
        '
        Me.btnaddContact.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnaddContact.Image = CType(resources.GetObject("btnaddContact.Image"), System.Drawing.Image)
        Me.btnaddContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnaddContact.Location = New System.Drawing.Point(616, 153)
        Me.btnaddContact.Name = "btnaddContact"
        Me.btnaddContact.Size = New System.Drawing.Size(68, 24)
        Me.btnaddContact.TabIndex = 19
        Me.btnaddContact.Text = "New"
        Me.btnaddContact.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnaddContact.UseVisualStyleBackColor = True
        '
        'lvlNearestRelatives
        '
        Me.lvlNearestRelatives.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lvlNearestRelatives.Location = New System.Drawing.Point(7, 29)
        Me.lvlNearestRelatives.Name = "lvlNearestRelatives"
        Me.lvlNearestRelatives.Size = New System.Drawing.Size(815, 120)
        Me.lvlNearestRelatives.TabIndex = 4
        Me.lvlNearestRelatives.UseCompatibleStateImageBehavior = False
        '
        'Label98
        '
        Me.Label98.AutoSize = True
        Me.Label98.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label98.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label98.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label98.Location = New System.Drawing.Point(10, 5)
        Me.Label98.Name = "Label98"
        Me.Label98.Size = New System.Drawing.Size(135, 18)
        Me.Label98.TabIndex = 3
        Me.Label98.Text = "Nearest Relatives"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox1.Location = New System.Drawing.Point(7, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(898, 21)
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.SystemColors.Window
        Me.TabPage3.Controls.Add(Me.LvlEmployeeDependent)
        Me.TabPage3.Controls.Add(Me.Label68)
        Me.TabPage3.Controls.Add(Me.GroupBox3)
        Me.TabPage3.Controls.Add(Me.btnemp_editdepdts)
        Me.TabPage3.Controls.Add(Me.btndelete_dep)
        Me.TabPage3.Controls.Add(Me.btnadd_dep)
        Me.TabPage3.Controls.Add(Me.PictureBox5)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(988, 179)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Dependents"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'LvlEmployeeDependent
        '
        Me.LvlEmployeeDependent.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LvlEmployeeDependent.Location = New System.Drawing.Point(14, 29)
        Me.LvlEmployeeDependent.Name = "LvlEmployeeDependent"
        Me.LvlEmployeeDependent.Size = New System.Drawing.Size(896, 121)
        Me.LvlEmployeeDependent.TabIndex = 11
        Me.LvlEmployeeDependent.UseCompatibleStateImageBehavior = False
        '
        'Label68
        '
        Me.Label68.AutoSize = True
        Me.Label68.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label68.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label68.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label68.Location = New System.Drawing.Point(17, 5)
        Me.Label68.Name = "Label68"
        Me.Label68.Size = New System.Drawing.Size(168, 18)
        Me.Label68.TabIndex = 8
        Me.Label68.Text = "Employee Dependents"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.emp_pic)
        Me.GroupBox3.Controls.Add(Me.temp_marks)
        Me.GroupBox3.Controls.Add(Me.txtitemno)
        Me.GroupBox3.Controls.Add(Me.txtremarks)
        Me.GroupBox3.Controls.Add(Me.temp_add3)
        Me.GroupBox3.Controls.Add(Me.txtparent_ID)
        Me.GroupBox3.Controls.Add(Me.txtmonthreqular)
        Me.GroupBox3.Controls.Add(Me.txtkeysection)
        Me.GroupBox3.Controls.Add(Me.temp_height)
        Me.GroupBox3.Controls.Add(Me.txtdescsection)
        Me.GroupBox3.Controls.Add(Me.txtsalarygrade)
        Me.GroupBox3.Controls.Add(Me.txtdescdepartment)
        Me.GroupBox3.Controls.Add(Me.temp_add2)
        Me.GroupBox3.Controls.Add(Me.txtdescdivision)
        Me.GroupBox3.Controls.Add(Me.txtweight)
        Me.GroupBox3.Controls.Add(Me.txtkeydepartment)
        Me.GroupBox3.Controls.Add(Me.txtdateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeydivision)
        Me.GroupBox3.Controls.Add(Me.Dateregular)
        Me.GroupBox3.Controls.Add(Me.txtkeycompany)
        Me.GroupBox3.Controls.Add(Me.emp_extphone)
        Me.GroupBox3.Controls.Add(Me.cbosalarygrade)
        Me.GroupBox3.Controls.Add(Me.temp_citizen)
        Me.GroupBox3.Controls.Add(Me.temp_designation)
        Me.GroupBox3.Controls.Add(Me.txtType_employee)
        Me.GroupBox3.Controls.Add(Me.txtfxkeypositionlevel)
        Me.GroupBox3.Controls.Add(Me.txtfullname)
        Me.GroupBox3.Controls.Add(Me.TXTRECID)
        Me.GroupBox3.Controls.Add(Me.cbodept)
        Me.GroupBox3.Controls.Add(Me.Dateresigned)
        Me.GroupBox3.Controls.Add(Me.TXTKEYEMPLOYEEID)
        Me.GroupBox3.Enabled = False
        Me.GroupBox3.Location = New System.Drawing.Point(326, 22)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(242, 161)
        Me.GroupBox3.TabIndex = 67
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Omitted items in Personnel New Version"
        Me.GroupBox3.Visible = False
        '
        'emp_pic
        '
        Me.emp_pic.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar
        Me.emp_pic.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_pic.Location = New System.Drawing.Point(96, 95)
        Me.emp_pic.Name = "emp_pic"
        Me.emp_pic.Size = New System.Drawing.Size(126, 124)
        Me.emp_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.emp_pic.TabIndex = 0
        Me.emp_pic.TabStop = False
        '
        'temp_marks
        '
        Me.temp_marks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_marks.Location = New System.Drawing.Point(104, 182)
        Me.temp_marks.MaxLength = 50
        Me.temp_marks.Name = "temp_marks"
        Me.temp_marks.Size = New System.Drawing.Size(137, 21)
        Me.temp_marks.TabIndex = 9
        '
        'txtitemno
        '
        Me.txtitemno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtitemno.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtitemno.Location = New System.Drawing.Point(57, 37)
        Me.txtitemno.Name = "txtitemno"
        Me.txtitemno.Size = New System.Drawing.Size(35, 21)
        Me.txtitemno.TabIndex = 19
        '
        'txtremarks
        '
        Me.txtremarks.Location = New System.Drawing.Point(66, 208)
        Me.txtremarks.Multiline = True
        Me.txtremarks.Name = "txtremarks"
        Me.txtremarks.Size = New System.Drawing.Size(39, 22)
        Me.txtremarks.TabIndex = 43
        '
        'temp_add3
        '
        Me.temp_add3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add3.Location = New System.Drawing.Point(104, 208)
        Me.temp_add3.MaxLength = 255
        Me.temp_add3.Name = "temp_add3"
        Me.temp_add3.Size = New System.Drawing.Size(156, 21)
        Me.temp_add3.TabIndex = 5
        '
        'txtparent_ID
        '
        Me.txtparent_ID.Location = New System.Drawing.Point(166, 183)
        Me.txtparent_ID.Name = "txtparent_ID"
        Me.txtparent_ID.Size = New System.Drawing.Size(28, 21)
        Me.txtparent_ID.TabIndex = 97
        Me.txtparent_ID.Visible = False
        '
        'txtmonthreqular
        '
        Me.txtmonthreqular.Location = New System.Drawing.Point(125, 60)
        Me.txtmonthreqular.MaxLength = 3
        Me.txtmonthreqular.Name = "txtmonthreqular"
        Me.txtmonthreqular.Size = New System.Drawing.Size(39, 21)
        Me.txtmonthreqular.TabIndex = 15
        '
        'txtkeysection
        '
        Me.txtkeysection.Location = New System.Drawing.Point(132, 183)
        Me.txtkeysection.Name = "txtkeysection"
        Me.txtkeysection.Size = New System.Drawing.Size(28, 21)
        Me.txtkeysection.TabIndex = 96
        Me.txtkeysection.Visible = False
        '
        'temp_height
        '
        Me.temp_height.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_height.Location = New System.Drawing.Point(78, 62)
        Me.temp_height.MaxLength = 10
        Me.temp_height.Name = "temp_height"
        Me.temp_height.Size = New System.Drawing.Size(51, 21)
        Me.temp_height.TabIndex = 7
        '
        'txtdescsection
        '
        Me.txtdescsection.Location = New System.Drawing.Point(166, 159)
        Me.txtdescsection.Name = "txtdescsection"
        Me.txtdescsection.Size = New System.Drawing.Size(59, 21)
        Me.txtdescsection.TabIndex = 95
        Me.txtdescsection.Visible = False
        '
        'txtsalarygrade
        '
        Me.txtsalarygrade.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtsalarygrade.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtsalarygrade.Location = New System.Drawing.Point(183, 32)
        Me.txtsalarygrade.Name = "txtsalarygrade"
        Me.txtsalarygrade.Size = New System.Drawing.Size(39, 21)
        Me.txtsalarygrade.TabIndex = 47
        Me.txtsalarygrade.Visible = False
        '
        'txtdescdepartment
        '
        Me.txtdescdepartment.Location = New System.Drawing.Point(166, 136)
        Me.txtdescdepartment.Name = "txtdescdepartment"
        Me.txtdescdepartment.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdepartment.TabIndex = 94
        Me.txtdescdepartment.Visible = False
        '
        'temp_add2
        '
        Me.temp_add2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_add2.Location = New System.Drawing.Point(84, 136)
        Me.temp_add2.Name = "temp_add2"
        Me.temp_add2.Size = New System.Drawing.Size(25, 21)
        Me.temp_add2.TabIndex = 10
        Me.temp_add2.Visible = False
        '
        'txtdescdivision
        '
        Me.txtdescdivision.Location = New System.Drawing.Point(166, 113)
        Me.txtdescdivision.Name = "txtdescdivision"
        Me.txtdescdivision.Size = New System.Drawing.Size(59, 21)
        Me.txtdescdivision.TabIndex = 93
        Me.txtdescdivision.Visible = False
        '
        'txtweight
        '
        Me.txtweight.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtweight.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtweight.Location = New System.Drawing.Point(202, 134)
        Me.txtweight.MaxLength = 10
        Me.txtweight.Name = "txtweight"
        Me.txtweight.Size = New System.Drawing.Size(32, 21)
        Me.txtweight.TabIndex = 8
        '
        'txtkeydepartment
        '
        Me.txtkeydepartment.Location = New System.Drawing.Point(132, 159)
        Me.txtkeydepartment.Name = "txtkeydepartment"
        Me.txtkeydepartment.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydepartment.TabIndex = 92
        Me.txtkeydepartment.Visible = False
        '
        'txtdateregular
        '
        Me.txtdateregular.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtdateregular.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtdateregular.Location = New System.Drawing.Point(9, 91)
        Me.txtdateregular.Name = "txtdateregular"
        Me.txtdateregular.Size = New System.Drawing.Size(69, 21)
        Me.txtdateregular.TabIndex = 57
        Me.txtdateregular.Visible = False
        '
        'txtkeydivision
        '
        Me.txtkeydivision.Location = New System.Drawing.Point(132, 136)
        Me.txtkeydivision.Name = "txtkeydivision"
        Me.txtkeydivision.Size = New System.Drawing.Size(28, 21)
        Me.txtkeydivision.TabIndex = 91
        Me.txtkeydivision.Visible = False
        '
        'Dateregular
        '
        Me.Dateregular.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateregular.Location = New System.Drawing.Point(84, 91)
        Me.Dateregular.Name = "Dateregular"
        Me.Dateregular.Size = New System.Drawing.Size(21, 21)
        Me.Dateregular.TabIndex = 49
        Me.Dateregular.Value = New Date(2006, 7, 17, 0, 0, 0, 0)
        Me.Dateregular.Visible = False
        '
        'txtkeycompany
        '
        Me.txtkeycompany.Location = New System.Drawing.Point(132, 113)
        Me.txtkeycompany.Name = "txtkeycompany"
        Me.txtkeycompany.Size = New System.Drawing.Size(28, 21)
        Me.txtkeycompany.TabIndex = 90
        Me.txtkeycompany.Visible = False
        '
        'emp_extphone
        '
        Me.emp_extphone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.emp_extphone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.emp_extphone.Location = New System.Drawing.Point(191, 89)
        Me.emp_extphone.Name = "emp_extphone"
        Me.emp_extphone.Size = New System.Drawing.Size(45, 21)
        Me.emp_extphone.TabIndex = 7
        '
        'cbosalarygrade
        '
        Me.cbosalarygrade.FormattingEnabled = True
        Me.cbosalarygrade.Location = New System.Drawing.Point(84, 86)
        Me.cbosalarygrade.Name = "cbosalarygrade"
        Me.cbosalarygrade.Size = New System.Drawing.Size(156, 23)
        Me.cbosalarygrade.TabIndex = 7
        '
        'temp_citizen
        '
        Me.temp_citizen.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_citizen.Location = New System.Drawing.Point(97, 110)
        Me.temp_citizen.MaxLength = 50
        Me.temp_citizen.Name = "temp_citizen"
        Me.temp_citizen.Size = New System.Drawing.Size(137, 21)
        Me.temp_citizen.TabIndex = 6
        '
        'temp_designation
        '
        Me.temp_designation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_designation.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_designation.Location = New System.Drawing.Point(67, 95)
        Me.temp_designation.Name = "temp_designation"
        Me.temp_designation.Size = New System.Drawing.Size(25, 21)
        Me.temp_designation.TabIndex = 16
        Me.temp_designation.Visible = False
        '
        'txtType_employee
        '
        Me.txtType_employee.Location = New System.Drawing.Point(118, 53)
        Me.txtType_employee.Name = "txtType_employee"
        Me.txtType_employee.Size = New System.Drawing.Size(42, 21)
        Me.txtType_employee.TabIndex = 80
        Me.txtType_employee.Visible = False
        '
        'txtfxkeypositionlevel
        '
        Me.txtfxkeypositionlevel.Location = New System.Drawing.Point(118, 76)
        Me.txtfxkeypositionlevel.Name = "txtfxkeypositionlevel"
        Me.txtfxkeypositionlevel.Size = New System.Drawing.Size(42, 21)
        Me.txtfxkeypositionlevel.TabIndex = 81
        Me.txtfxkeypositionlevel.Visible = False
        '
        'txtfullname
        '
        Me.txtfullname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtfullname.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtfullname.Location = New System.Drawing.Point(99, 95)
        Me.txtfullname.MaxLength = 50
        Me.txtfullname.Name = "txtfullname"
        Me.txtfullname.Size = New System.Drawing.Size(22, 21)
        Me.txtfullname.TabIndex = 62
        Me.txtfullname.Visible = False
        '
        'TXTRECID
        '
        Me.TXTRECID.Location = New System.Drawing.Point(145, 65)
        Me.TXTRECID.Name = "TXTRECID"
        Me.TXTRECID.Size = New System.Drawing.Size(72, 21)
        Me.TXTRECID.TabIndex = 52
        Me.TXTRECID.Visible = False
        '
        'cbodept
        '
        Me.cbodept.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.cbodept.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbodept.Location = New System.Drawing.Point(100, 123)
        Me.cbodept.MaxLength = 50
        Me.cbodept.Name = "cbodept"
        Me.cbodept.Size = New System.Drawing.Size(22, 21)
        Me.cbodept.TabIndex = 72
        Me.cbodept.Visible = False
        '
        'Dateresigned
        '
        Me.Dateresigned.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Dateresigned.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.Dateresigned.Location = New System.Drawing.Point(67, 122)
        Me.Dateresigned.Name = "Dateresigned"
        Me.Dateresigned.Size = New System.Drawing.Size(28, 21)
        Me.Dateresigned.TabIndex = 58
        Me.Dateresigned.Value = New Date(2006, 5, 30, 0, 0, 0, 0)
        Me.Dateresigned.Visible = False
        '
        'TXTKEYEMPLOYEEID
        '
        Me.TXTKEYEMPLOYEEID.Location = New System.Drawing.Point(145, 89)
        Me.TXTKEYEMPLOYEEID.Name = "TXTKEYEMPLOYEEID"
        Me.TXTKEYEMPLOYEEID.Size = New System.Drawing.Size(72, 21)
        Me.TXTKEYEMPLOYEEID.TabIndex = 51
        Me.TXTKEYEMPLOYEEID.Visible = False
        '
        'btnemp_editdepdts
        '
        Me.btnemp_editdepdts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_editdepdts.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_editdepdts.Image = CType(resources.GetObject("btnemp_editdepdts.Image"), System.Drawing.Image)
        Me.btnemp_editdepdts.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_editdepdts.Location = New System.Drawing.Point(686, 154)
        Me.btnemp_editdepdts.Name = "btnemp_editdepdts"
        Me.btnemp_editdepdts.Size = New System.Drawing.Size(68, 24)
        Me.btnemp_editdepdts.TabIndex = 10
        Me.btnemp_editdepdts.Text = "Edit"
        Me.btnemp_editdepdts.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_editdepdts.UseVisualStyleBackColor = True
        '
        'btndelete_dep
        '
        Me.btndelete_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btndelete_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btndelete_dep.Image = CType(resources.GetObject("btndelete_dep.Image"), System.Drawing.Image)
        Me.btndelete_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btndelete_dep.Location = New System.Drawing.Point(754, 154)
        Me.btndelete_dep.Name = "btndelete_dep"
        Me.btndelete_dep.Size = New System.Drawing.Size(68, 24)
        Me.btndelete_dep.TabIndex = 5
        Me.btndelete_dep.Text = "Delete"
        Me.btndelete_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btndelete_dep.UseVisualStyleBackColor = True
        '
        'btnadd_dep
        '
        Me.btnadd_dep.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnadd_dep.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnadd_dep.Image = CType(resources.GetObject("btnadd_dep.Image"), System.Drawing.Image)
        Me.btnadd_dep.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnadd_dep.Location = New System.Drawing.Point(618, 154)
        Me.btnadd_dep.Name = "btnadd_dep"
        Me.btnadd_dep.Size = New System.Drawing.Size(68, 24)
        Me.btnadd_dep.TabIndex = 3
        Me.btnadd_dep.Text = "New"
        Me.btnadd_dep.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnadd_dep.UseVisualStyleBackColor = True
        '
        'PictureBox5
        '
        Me.PictureBox5.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox5.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox5.Location = New System.Drawing.Point(13, 5)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(896, 18)
        Me.PictureBox5.TabIndex = 1
        Me.PictureBox5.TabStop = False
        '
        'others
        '
        Me.others.Controls.Add(Me.btnEditMobile)
        Me.others.Controls.Add(Me.gridMobileNos)
        Me.others.Controls.Add(Me.txtEloadingLimit)
        Me.others.Controls.Add(Me.Label31)
        Me.others.Controls.Add(Me.txtEloaderPIN)
        Me.others.Controls.Add(Me.chkShowPINChar)
        Me.others.Controls.Add(Me.Label24)
        Me.others.Controls.Add(Me.Label15)
        Me.others.Controls.Add(Me.btnAddMobileNo)
        Me.others.Controls.Add(Me.chkIsEloader)
        Me.others.Controls.Add(Me.Label4)
        Me.others.Controls.Add(Me.txtMobileNo)
        Me.others.Controls.Add(Me.PictureBox9)
        Me.others.Controls.Add(Me.PictureBox7)
        Me.others.Location = New System.Drawing.Point(4, 22)
        Me.others.Name = "others"
        Me.others.Padding = New System.Windows.Forms.Padding(3)
        Me.others.Size = New System.Drawing.Size(988, 179)
        Me.others.TabIndex = 20
        Me.others.Text = "Others"
        Me.others.UseVisualStyleBackColor = True
        '
        'btnEditMobile
        '
        Me.btnEditMobile.Location = New System.Drawing.Point(423, 72)
        Me.btnEditMobile.Name = "btnEditMobile"
        Me.btnEditMobile.Size = New System.Drawing.Size(43, 21)
        Me.btnEditMobile.TabIndex = 45
        Me.btnEditMobile.Text = "Edit"
        Me.btnEditMobile.UseVisualStyleBackColor = True
        '
        'gridMobileNos
        '
        Me.gridMobileNos.AllowUserToAddRows = False
        Me.gridMobileNos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.gridMobileNos.BackgroundColor = System.Drawing.Color.White
        Me.gridMobileNos.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.gridMobileNos.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        Me.gridMobileNos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.gridMobileNos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.gridMobileNos.Location = New System.Drawing.Point(203, 45)
        Me.gridMobileNos.Name = "gridMobileNos"
        Me.gridMobileNos.Size = New System.Drawing.Size(214, 125)
        Me.gridMobileNos.TabIndex = 44
        '
        'txtEloadingLimit
        '
        Me.txtEloadingLimit.Location = New System.Drawing.Point(571, 73)
        Me.txtEloadingLimit.Name = "txtEloadingLimit"
        Me.txtEloadingLimit.Size = New System.Drawing.Size(115, 21)
        Me.txtEloadingLimit.TabIndex = 43
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(484, 78)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(87, 15)
        Me.Label31.TabIndex = 42
        Me.Label31.Text = "Eloading Limit:"
        '
        'txtEloaderPIN
        '
        Me.txtEloaderPIN.Location = New System.Drawing.Point(571, 45)
        Me.txtEloaderPIN.MaxLength = 4
        Me.txtEloaderPIN.Name = "txtEloaderPIN"
        Me.txtEloaderPIN.Size = New System.Drawing.Size(115, 21)
        Me.txtEloaderPIN.TabIndex = 40
        Me.txtEloaderPIN.UseSystemPasswordChar = True
        '
        'chkShowPINChar
        '
        Me.chkShowPINChar.AutoSize = True
        Me.chkShowPINChar.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkShowPINChar.Location = New System.Drawing.Point(692, 49)
        Me.chkShowPINChar.Name = "chkShowPINChar"
        Me.chkShowPINChar.Size = New System.Drawing.Size(95, 17)
        Me.chkShowPINChar.TabIndex = 39
        Me.chkShowPINChar.Text = "Show PIN Char."
        Me.chkShowPINChar.UseVisualStyleBackColor = True
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.BackColor = System.Drawing.Color.White
        Me.Label24.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.ForeColor = System.Drawing.Color.Black
        Me.Label24.Location = New System.Drawing.Point(7, 29)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(124, 16)
        Me.Label24.TabIndex = 38
        Me.Label24.Text = "E-Loading Service"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(484, 51)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(30, 15)
        Me.Label15.TabIndex = 36
        Me.Label15.Text = "PIN:"
        '
        'btnAddMobileNo
        '
        Me.btnAddMobileNo.Location = New System.Drawing.Point(423, 45)
        Me.btnAddMobileNo.Name = "btnAddMobileNo"
        Me.btnAddMobileNo.Size = New System.Drawing.Size(43, 21)
        Me.btnAddMobileNo.TabIndex = 27
        Me.btnAddMobileNo.Text = "Add"
        Me.btnAddMobileNo.UseVisualStyleBackColor = True
        '
        'chkIsEloader
        '
        Me.chkIsEloader.AutoSize = True
        Me.chkIsEloader.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkIsEloader.Location = New System.Drawing.Point(571, 98)
        Me.chkIsEloader.Name = "chkIsEloader"
        Me.chkIsEloader.Size = New System.Drawing.Size(131, 19)
        Me.chkIsEloader.TabIndex = 35
        Me.chkIsEloader.Text = "Registered Member"
        Me.chkIsEloader.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.Label4.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.Label4.Location = New System.Drawing.Point(5, 7)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(125, 16)
        Me.Label4.TabIndex = 34
        Me.Label4.Text = "Apps Registrations"
        '
        'txtMobileNo
        '
        Me.txtMobileNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMobileNo.Location = New System.Drawing.Point(487, 134)
        Me.txtMobileNo.Name = "txtMobileNo"
        Me.txtMobileNo.ReadOnly = True
        Me.txtMobileNo.Size = New System.Drawing.Size(199, 21)
        Me.txtMobileNo.TabIndex = 13
        Me.txtMobileNo.Visible = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox9.BackColor = System.Drawing.SystemColors.AppWorkspace
        Me.PictureBox9.Location = New System.Drawing.Point(3, 6)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(896, 19)
        Me.PictureBox9.TabIndex = 33
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = Global.WindowsApplication2.My.Resources.Resources.img_eload
        Me.PictureBox7.Location = New System.Drawing.Point(9, 48)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(188, 124)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'Cbomem_Status
        '
        Me.Cbomem_Status.FormattingEnabled = True
        Me.Cbomem_Status.Location = New System.Drawing.Point(383, 53)
        Me.Cbomem_Status.Name = "Cbomem_Status"
        Me.Cbomem_Status.Size = New System.Drawing.Size(153, 21)
        Me.Cbomem_Status.TabIndex = 4
        '
        'Label107
        '
        Me.Label107.AutoSize = True
        Me.Label107.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label107.Location = New System.Drawing.Point(276, 56)
        Me.Label107.Name = "Label107"
        Me.Label107.Size = New System.Drawing.Size(101, 15)
        Me.Label107.TabIndex = 102
        Me.Label107.Text = "Members Status:"
        '
        'txtMemberID
        '
        Me.txtMemberID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMemberID.BackColor = System.Drawing.SystemColors.Window
        Me.txtMemberID.Location = New System.Drawing.Point(866, 54)
        Me.txtMemberID.MaxLength = 30
        Me.txtMemberID.Name = "txtMemberID"
        Me.txtMemberID.ReadOnly = True
        Me.txtMemberID.Size = New System.Drawing.Size(138, 20)
        Me.txtMemberID.TabIndex = 2
        '
        'temp_placebirth
        '
        Me.temp_placebirth.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_placebirth.Location = New System.Drawing.Point(115, 114)
        Me.temp_placebirth.MaxLength = 255
        Me.temp_placebirth.Name = "temp_placebirth"
        Me.temp_placebirth.Size = New System.Drawing.Size(874, 20)
        Me.temp_placebirth.TabIndex = 15
        '
        'txtage1
        '
        Me.txtage1.BackColor = System.Drawing.SystemColors.Window
        Me.txtage1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtage1.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtage1.Location = New System.Drawing.Point(304, 80)
        Me.txtage1.MaxLength = 3
        Me.txtage1.Name = "txtage1"
        Me.txtage1.Size = New System.Drawing.Size(25, 14)
        Me.txtage1.TabIndex = 45
        Me.txtage1.Text = "0"
        '
        'lblage
        '
        Me.lblage.AutoSize = True
        Me.lblage.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblage.Location = New System.Drawing.Point(278, 80)
        Me.lblage.Name = "lblage"
        Me.lblage.Size = New System.Drawing.Size(30, 14)
        Me.lblage.TabIndex = 44
        Me.lblage.Text = "Age "
        '
        'EMP_DATEbirth
        '
        Me.EMP_DATEbirth.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.EMP_DATEbirth.Location = New System.Drawing.Point(115, 76)
        Me.EMP_DATEbirth.Name = "EMP_DATEbirth"
        Me.EMP_DATEbirth.Size = New System.Drawing.Size(150, 20)
        Me.EMP_DATEbirth.TabIndex = 10
        Me.EMP_DATEbirth.Value = New Date(2006, 3, 17, 0, 0, 0, 0)
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.BackColor = System.Drawing.SystemColors.Window
        Me.Label19.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.ForeColor = System.Drawing.Color.Black
        Me.Label19.Location = New System.Drawing.Point(3, 81)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(81, 15)
        Me.Label19.TabIndex = 0
        Me.Label19.Text = "Date of Birth:"
        '
        'TextBox15
        '
        Me.TextBox15.Location = New System.Drawing.Point(92, 145)
        Me.TextBox15.Name = "TextBox15"
        Me.TextBox15.Size = New System.Drawing.Size(156, 20)
        Me.TextBox15.TabIndex = 11
        '
        'TextBox16
        '
        Me.TextBox16.Location = New System.Drawing.Point(92, 119)
        Me.TextBox16.Name = "TextBox16"
        Me.TextBox16.Size = New System.Drawing.Size(156, 20)
        Me.TextBox16.TabIndex = 10
        '
        'TextBox17
        '
        Me.TextBox17.Location = New System.Drawing.Point(92, 93)
        Me.TextBox17.Name = "TextBox17"
        Me.TextBox17.Size = New System.Drawing.Size(156, 20)
        Me.TextBox17.TabIndex = 9
        '
        'TextBox18
        '
        Me.TextBox18.Location = New System.Drawing.Point(92, 67)
        Me.TextBox18.Name = "TextBox18"
        Me.TextBox18.Size = New System.Drawing.Size(156, 20)
        Me.TextBox18.TabIndex = 8
        '
        'TextBox19
        '
        Me.TextBox19.Location = New System.Drawing.Point(92, 41)
        Me.TextBox19.Name = "TextBox19"
        Me.TextBox19.Size = New System.Drawing.Size(156, 20)
        Me.TextBox19.TabIndex = 7
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 152)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(54, 13)
        Me.Label25.TabIndex = 6
        Me.Label25.Text = "Address 3"
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(17, 126)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(54, 13)
        Me.Label26.TabIndex = 5
        Me.Label26.Text = "Address 2"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(17, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(54, 13)
        Me.Label27.TabIndex = 4
        Me.Label27.Text = "Address 1"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(17, 74)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(45, 13)
        Me.Label28.TabIndex = 3
        Me.Label28.Text = "Religion"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(17, 48)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(70, 13)
        Me.Label29.TabIndex = 2
        Me.Label29.Text = "Place of Birth"
        '
        'MaskedTextBox5
        '
        Me.MaskedTextBox5.Location = New System.Drawing.Point(92, 15)
        Me.MaskedTextBox5.Mask = "LLLL,00,0000"
        Me.MaskedTextBox5.Name = "MaskedTextBox5"
        Me.MaskedTextBox5.Size = New System.Drawing.Size(79, 20)
        Me.MaskedTextBox5.TabIndex = 1
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(17, 22)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(66, 13)
        Me.Label30.TabIndex = 0
        Me.Label30.Text = "Date of Birth"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ErrorProvider1
        '
        Me.ErrorProvider1.ContainerControl = Me
        '
        'Label3
        '
        Me.Label3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(775, 31)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 15)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Employee No.:"
        '
        'txtEmployeeNo
        '
        Me.txtEmployeeNo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmployeeNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtEmployeeNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtEmployeeNo.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmployeeNo.Location = New System.Drawing.Point(866, 29)
        Me.txtEmployeeNo.MaxLength = 50
        Me.txtEmployeeNo.Name = "txtEmployeeNo"
        Me.txtEmployeeNo.Size = New System.Drawing.Size(138, 21)
        Me.txtEmployeeNo.TabIndex = 1
        '
        'temp_fname
        '
        Me.temp_fname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_fname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_fname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_fname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_fname.Location = New System.Drawing.Point(68, 3)
        Me.temp_fname.MaxLength = 50
        Me.temp_fname.Name = "temp_fname"
        Me.temp_fname.Size = New System.Drawing.Size(278, 25)
        Me.temp_fname.TabIndex = 0
        '
        'temp_lname
        '
        Me.temp_lname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_lname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_lname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_lname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_lname.Location = New System.Drawing.Point(67, 3)
        Me.temp_lname.MaxLength = 50
        Me.temp_lname.Name = "temp_lname"
        Me.temp_lname.Size = New System.Drawing.Size(259, 25)
        Me.temp_lname.TabIndex = 4
        '
        'temp_midname
        '
        Me.temp_midname.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.temp_midname.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.temp_midname.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.temp_midname.Font = New System.Drawing.Font("Arial", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.temp_midname.Location = New System.Drawing.Point(78, 3)
        Me.temp_midname.MaxLength = 50
        Me.temp_midname.Name = "temp_midname"
        Me.temp_midname.Size = New System.Drawing.Size(214, 25)
        Me.temp_midname.TabIndex = 6
        '
        'txtprovincialadd
        '
        Me.txtprovincialadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtprovincialadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtprovincialadd.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtprovincialadd.Location = New System.Drawing.Point(115, 161)
        Me.txtprovincialadd.MaxLength = 1000
        Me.txtprovincialadd.Name = "txtprovincialadd"
        Me.txtprovincialadd.Size = New System.Drawing.Size(874, 21)
        Me.txtprovincialadd.TabIndex = 17
        '
        'cboemp_gender
        '
        Me.cboemp_gender.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_gender.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_gender.FormattingEnabled = True
        Me.cboemp_gender.Items.AddRange(New Object() {"F", "M"})
        Me.cboemp_gender.Location = New System.Drawing.Point(115, 22)
        Me.cboemp_gender.Name = "cboemp_gender"
        Me.cboemp_gender.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_gender.TabIndex = 3
        '
        'cboemp_civil
        '
        Me.cboemp_civil.AutoCompleteCustomSource.AddRange(New String() {"RANK AND FILE", "MANAGERIAL", "SUPERVISOR", "EXECUTIVE"})
        Me.cboemp_civil.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboemp_civil.FormattingEnabled = True
        Me.cboemp_civil.Location = New System.Drawing.Point(115, 49)
        Me.cboemp_civil.Name = "cboemp_civil"
        Me.cboemp_civil.Size = New System.Drawing.Size(150, 23)
        Me.cboemp_civil.TabIndex = 9
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(1, 8)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(66, 14)
        Me.Label5.TabIndex = 14
        Me.Label5.Text = "First Name"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(3, 8)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 14)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Last Name"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(3, 8)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(78, 14)
        Me.Label7.TabIndex = 16
        Me.Label7.Text = "Middle Name"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(3, 30)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(52, 15)
        Me.Label8.TabIndex = 17
        Me.Label8.Text = "Gender:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(3, 58)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(70, 15)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Civil Status:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(3, 165)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(110, 15)
        Me.Label10.TabIndex = 19
        Me.Label10.Text = "Provincial Address:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 31)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(89, 15)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "Org. Structure:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(3, 30)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(104, 15)
        Me.Label14.TabIndex = 23
        Me.Label14.Text = "Residence Phone:"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(3, 54)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(40, 15)
        Me.Label17.TabIndex = 26
        Me.Label17.Text = "Email:"
        '
        'txtresidencephone
        '
        Me.txtresidencephone.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtresidencephone.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtresidencephone.Location = New System.Drawing.Point(111, 27)
        Me.txtresidencephone.Name = "txtresidencephone"
        Me.txtresidencephone.Size = New System.Drawing.Size(211, 21)
        Me.txtresidencephone.TabIndex = 12
        '
        'txtEmailAddress
        '
        Me.txtEmailAddress.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtEmailAddress.Font = New System.Drawing.Font("Arial", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtEmailAddress.Location = New System.Drawing.Point(111, 50)
        Me.txtEmailAddress.MaxLength = 50
        Me.txtEmailAddress.Name = "txtEmailAddress"
        Me.txtEmailAddress.Size = New System.Drawing.Size(312, 21)
        Me.txtEmailAddress.TabIndex = 14
        '
        'txtorgchart
        '
        Me.txtorgchart.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtorgchart.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtorgchart.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtorgchart.Location = New System.Drawing.Point(105, 29)
        Me.txtorgchart.Name = "txtorgchart"
        Me.txtorgchart.Size = New System.Drawing.Size(634, 20)
        Me.txtorgchart.TabIndex = 0
        '
        'Label85
        '
        Me.Label85.AutoSize = True
        Me.Label85.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label85.Location = New System.Drawing.Point(3, 142)
        Me.Label85.Name = "Label85"
        Me.Label85.Size = New System.Drawing.Size(91, 15)
        Me.Label85.TabIndex = 74
        Me.Label85.Text = "Home Address:"
        '
        'txtResidenceadd
        '
        Me.txtResidenceadd.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtResidenceadd.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtResidenceadd.Location = New System.Drawing.Point(115, 138)
        Me.txtResidenceadd.Name = "txtResidenceadd"
        Me.txtResidenceadd.Size = New System.Drawing.Size(874, 20)
        Me.txtResidenceadd.TabIndex = 16
        '
        'Label86
        '
        Me.Label86.AutoSize = True
        Me.Label86.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label86.Location = New System.Drawing.Point(12, 56)
        Me.Label86.Name = "Label86"
        Me.Label86.Size = New System.Drawing.Size(40, 15)
        Me.Label86.TabIndex = 76
        Me.Label86.Text = "Level:"
        '
        'txtDepartment2
        '
        Me.txtDepartment2.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtDepartment2.Location = New System.Drawing.Point(105, 55)
        Me.txtDepartment2.Name = "txtDepartment2"
        Me.txtDepartment2.Size = New System.Drawing.Size(162, 20)
        Me.txtDepartment2.TabIndex = 13
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.SplitContainer2)
        Me.GroupBox1.Location = New System.Drawing.Point(13, 74)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(991, 40)
        Me.GroupBox1.TabIndex = 78
        Me.GroupBox1.TabStop = False
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer2.IsSplitterFixed = True
        Me.SplitContainer2.Location = New System.Drawing.Point(1, 7)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.temp_lname)
        Me.SplitContainer2.Panel1.Controls.Add(Me.Label6)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer2.Size = New System.Drawing.Size(989, 31)
        Me.SplitContainer2.SplitterDistance = 331
        Me.SplitContainer2.TabIndex = 46
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer3)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(654, 31)
        Me.Panel1.TabIndex = 0
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.IsSplitterFixed = True
        Me.SplitContainer3.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.temp_fname)
        Me.SplitContainer3.Panel1.Controls.Add(Me.Label5)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.temp_midname)
        Me.SplitContainer3.Panel2.Controls.Add(Me.Label7)
        Me.SplitContainer3.Size = New System.Drawing.Size(654, 31)
        Me.SplitContainer3.SplitterDistance = 352
        Me.SplitContainer3.TabIndex = 0
        '
        'Label66
        '
        Me.Label66.AutoSize = True
        Me.Label66.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label66.ForeColor = System.Drawing.Color.Black
        Me.Label66.Location = New System.Drawing.Point(3, 0)
        Me.Label66.Name = "Label66"
        Me.Label66.Size = New System.Drawing.Size(140, 14)
        Me.Label66.TabIndex = 11
        Me.Label66.Text = "PERSONAL INFORMATION"
        '
        'Label67
        '
        Me.Label67.AutoSize = True
        Me.Label67.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label67.Location = New System.Drawing.Point(3, 118)
        Me.Label67.Name = "Label67"
        Me.Label67.Size = New System.Drawing.Size(69, 15)
        Me.Label67.TabIndex = 102
        Me.Label67.Text = "Birth Place:"
        '
        'Panel2
        '
        Me.Panel2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Panel2.Controls.Add(Me.Label67)
        Me.Panel2.Controls.Add(Me.temp_placebirth)
        Me.Panel2.Controls.Add(Me.txtprovincialadd)
        Me.Panel2.Controls.Add(Me.Label10)
        Me.Panel2.Controls.Add(Me.Label85)
        Me.Panel2.Controls.Add(Me.txtResidenceadd)
        Me.Panel2.Location = New System.Drawing.Point(7, 114)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(996, 186)
        Me.Panel2.TabIndex = 14
        '
        'txtpk_Employee
        '
        Me.txtpk_Employee.Location = New System.Drawing.Point(111, 3)
        Me.txtpk_Employee.Name = "txtpk_Employee"
        Me.txtpk_Employee.Size = New System.Drawing.Size(139, 20)
        Me.txtpk_Employee.TabIndex = 11
        Me.txtpk_Employee.Visible = False
        '
        'Label112
        '
        Me.Label112.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label112.AutoSize = True
        Me.Label112.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label112.Location = New System.Drawing.Point(774, 55)
        Me.Label112.Name = "Label112"
        Me.Label112.Size = New System.Drawing.Size(86, 15)
        Me.Label112.TabIndex = 107
        Me.Label112.Text = "Members No.:"
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SplitContainer1.IsSplitterFixed = True
        Me.SplitContainer1.Location = New System.Drawing.Point(7, 114)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label66)
        Me.SplitContainer1.Panel1.Controls.Add(Me.txtage1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lblage)
        Me.SplitContainer1.Panel1.Controls.Add(Me.EMP_DATEbirth)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label19)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label9)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label8)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_gender)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cboemp_civil)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtpk_Employee)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtresidencephone)
        Me.SplitContainer1.Panel2.Controls.Add(Me.txtEmailAddress)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label14)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Label17)
        Me.SplitContainer1.Size = New System.Drawing.Size(996, 111)
        Me.SplitContainer1.SplitterDistance = 511
        Me.SplitContainer1.TabIndex = 103
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.PictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(744, 29)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(25, 20)
        Me.PictureBox2.TabIndex = 45
        Me.PictureBox2.TabStop = False
        '
        'grpPayrollStatus
        '
        Me.grpPayrollStatus.Controls.Add(Me.rdoNonExempt)
        Me.grpPayrollStatus.Controls.Add(Me.rdoExempt)
        Me.grpPayrollStatus.Location = New System.Drawing.Point(332, 96)
        Me.grpPayrollStatus.Name = "grpPayrollStatus"
        Me.grpPayrollStatus.Size = New System.Drawing.Size(267, 49)
        Me.grpPayrollStatus.TabIndex = 118
        Me.grpPayrollStatus.TabStop = False
        Me.grpPayrollStatus.Text = "Payroll Status"
        '
        'rdoExempt
        '
        Me.rdoExempt.AutoSize = True
        Me.rdoExempt.Checked = True
        Me.rdoExempt.Location = New System.Drawing.Point(10, 20)
        Me.rdoExempt.Name = "rdoExempt"
        Me.rdoExempt.Size = New System.Drawing.Size(67, 19)
        Me.rdoExempt.TabIndex = 0
        Me.rdoExempt.TabStop = True
        Me.rdoExempt.Text = "Exempt"
        Me.rdoExempt.UseVisualStyleBackColor = True
        '
        'rdoNonExempt
        '
        Me.rdoNonExempt.AutoSize = True
        Me.rdoNonExempt.Location = New System.Drawing.Point(99, 20)
        Me.rdoNonExempt.Name = "rdoNonExempt"
        Me.rdoNonExempt.Size = New System.Drawing.Size(94, 19)
        Me.rdoNonExempt.TabIndex = 1
        Me.rdoNonExempt.Text = "Non-Exempt"
        Me.rdoNonExempt.UseVisualStyleBackColor = True
        '
        'btnemp_close
        '
        Me.btnemp_close.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_close.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_close.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnemp_close.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnemp_close.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_close.Location = New System.Drawing.Point(713, 3)
        Me.btnemp_close.Name = "btnemp_close"
        Me.btnemp_close.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_close.TabIndex = 7
        Me.btnemp_close.Text = "Close"
        Me.btnemp_close.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_close.UseVisualStyleBackColor = True
        '
        'PanePanel2
        '
        Me.PanePanel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel2.Controls.Add(Me.FlowLayoutPanel1)
        Me.PanePanel2.Controls.Add(Me.btnemp_next)
        Me.PanePanel2.Controls.Add(Me.BTNSEARCH)
        Me.PanePanel2.Controls.Add(Me.btnemp_previous)
        Me.PanePanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.PanePanel2.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel2.InactiveGradientHighColor = System.Drawing.Color.LawnGreen
        Me.PanePanel2.InactiveGradientLowColor = System.Drawing.Color.Green
        Me.PanePanel2.Location = New System.Drawing.Point(0, 512)
        Me.PanePanel2.Name = "PanePanel2"
        Me.PanePanel2.Size = New System.Drawing.Size(1007, 35)
        Me.PanePanel2.TabIndex = 83
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.BackColor = System.Drawing.Color.Transparent
        Me.FlowLayoutPanel1.Controls.Add(Me.btnemp_close)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnemp_delete)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnemp_edit)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnemp_add)
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft
        Me.FlowLayoutPanel1.ForeColor = System.Drawing.Color.Black
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(220, 4)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(782, 26)
        Me.FlowLayoutPanel1.TabIndex = 8
        '
        'btnemp_delete
        '
        Me.btnemp_delete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_delete.Image = CType(resources.GetObject("btnemp_delete.Image"), System.Drawing.Image)
        Me.btnemp_delete.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_delete.Location = New System.Drawing.Point(641, 3)
        Me.btnemp_delete.Name = "btnemp_delete"
        Me.btnemp_delete.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_delete.TabIndex = 6
        Me.btnemp_delete.Text = "Delete"
        Me.btnemp_delete.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_delete.UseVisualStyleBackColor = True
        '
        'btnemp_edit
        '
        Me.btnemp_edit.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_edit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_edit.Image = CType(resources.GetObject("btnemp_edit.Image"), System.Drawing.Image)
        Me.btnemp_edit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_edit.Location = New System.Drawing.Point(569, 3)
        Me.btnemp_edit.Name = "btnemp_edit"
        Me.btnemp_edit.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_edit.TabIndex = 5
        Me.btnemp_edit.Text = "Edit"
        Me.btnemp_edit.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_edit.UseVisualStyleBackColor = True
        '
        'btnemp_add
        '
        Me.btnemp_add.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnemp_add.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnemp_add.Image = CType(resources.GetObject("btnemp_add.Image"), System.Drawing.Image)
        Me.btnemp_add.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_add.Location = New System.Drawing.Point(497, 3)
        Me.btnemp_add.Name = "btnemp_add"
        Me.btnemp_add.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_add.TabIndex = 4
        Me.btnemp_add.Text = "New"
        Me.btnemp_add.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnemp_add.UseVisualStyleBackColor = True
        '
        'btnemp_next
        '
        Me.btnemp_next.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_next.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_next.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_next.Location = New System.Drawing.Point(150, 4)
        Me.btnemp_next.Name = "btnemp_next"
        Me.btnemp_next.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_next.TabIndex = 3
        Me.btnemp_next.Text = ">>"
        Me.btnemp_next.UseVisualStyleBackColor = True
        '
        'BTNSEARCH
        '
        Me.BTNSEARCH.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.BTNSEARCH.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BTNSEARCH.Image = CType(resources.GetObject("BTNSEARCH.Image"), System.Drawing.Image)
        Me.BTNSEARCH.Location = New System.Drawing.Point(80, 4)
        Me.BTNSEARCH.Name = "BTNSEARCH"
        Me.BTNSEARCH.Size = New System.Drawing.Size(66, 28)
        Me.BTNSEARCH.TabIndex = 2
        Me.BTNSEARCH.UseVisualStyleBackColor = True
        '
        'btnemp_previous
        '
        Me.btnemp_previous.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnemp_previous.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnemp_previous.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnemp_previous.Location = New System.Drawing.Point(13, 4)
        Me.btnemp_previous.Name = "btnemp_previous"
        Me.btnemp_previous.Size = New System.Drawing.Size(66, 28)
        Me.btnemp_previous.TabIndex = 1
        Me.btnemp_previous.Text = "<<"
        Me.btnemp_previous.UseVisualStyleBackColor = True
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.lblemployee_name)
        Me.PanePanel1.Controls.Add(Me.Label44)
        Me.PanePanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.LawnGreen
        Me.PanePanel1.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(1007, 25)
        Me.PanePanel1.TabIndex = 82
        '
        'lblemployee_name
        '
        Me.lblemployee_name.AutoSize = True
        Me.lblemployee_name.BackColor = System.Drawing.Color.Transparent
        Me.lblemployee_name.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblemployee_name.ForeColor = System.Drawing.Color.White
        Me.lblemployee_name.Location = New System.Drawing.Point(3, 3)
        Me.lblemployee_name.Name = "lblemployee_name"
        Me.lblemployee_name.Size = New System.Drawing.Size(29, 19)
        Me.lblemployee_name.TabIndex = 13
        Me.lblemployee_name.Text = "....."
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.BackColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Label44.Font = New System.Drawing.Font("Arial", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label44.ForeColor = System.Drawing.Color.Red
        Me.Label44.Location = New System.Drawing.Point(643, -3)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(0, 22)
        Me.Label44.TabIndex = 14
        Me.Label44.Visible = False
        '
        'frmMember_Master
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.CancelButton = Me.btnemp_close
        Me.ClientSize = New System.Drawing.Size(1007, 547)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Label112)
        Me.Controls.Add(Me.txtMemberID)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Cbomem_Status)
        Me.Controls.Add(Me.Label107)
        Me.Controls.Add(Me.PanePanel2)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.txtDepartment2)
        Me.Controls.Add(Me.Label86)
        Me.Controls.Add(Me.txtEmployeeNo)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tabMember)
        Me.Controls.Add(Me.txtorgchart)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.PictureBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(781, 581)
        Me.Name = "frmMember_Master"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Existing Members File"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tabMember.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.TabPage5.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage17.ResumeLayout(False)
        Me.TabPage17.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.emp_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.others.ResumeLayout(False)
        Me.others.PerformLayout()
        CType(Me.gridMobileNos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrorProvider1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.PerformLayout()
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel1.PerformLayout()
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.Panel2.PerformLayout()
        Me.SplitContainer3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.Panel2.PerformLayout()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpPayrollStatus.ResumeLayout(False)
        Me.grpPayrollStatus.PerformLayout()
        Me.PanePanel2.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tabMember As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents temp_placebirth As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents temp_add3 As System.Windows.Forms.TextBox
    Friend WithEvents temp_add2 As System.Windows.Forms.TextBox
    Friend WithEvents txtofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtofficeadd As System.Windows.Forms.TextBox
    Friend WithEvents TextBox15 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox16 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox17 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox18 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox19 As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents MaskedTextBox5 As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents temp_marks As System.Windows.Forms.TextBox
    Friend WithEvents temp_height As System.Windows.Forms.TextBox
    Friend WithEvents temp_citizen As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents btndelete_dep As System.Windows.Forms.Button
    Friend WithEvents btnadd_dep As System.Windows.Forms.Button
    Friend WithEvents btnemp_previous As System.Windows.Forms.Button
    Friend WithEvents btnemp_next As System.Windows.Forms.Button
    Friend WithEvents btnemp_add As System.Windows.Forms.Button
    Friend WithEvents btnemp_edit As System.Windows.Forms.Button
    Friend WithEvents btnemp_delete As System.Windows.Forms.Button
    Friend WithEvents btnemp_close As System.Windows.Forms.Button
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents txtbasicpay As System.Windows.Forms.TextBox
    Friend WithEvents Label62 As System.Windows.Forms.Label
    Friend WithEvents Label59 As System.Windows.Forms.Label
    Friend WithEvents cbopayroll As System.Windows.Forms.ComboBox
    Friend WithEvents Label65 As System.Windows.Forms.Label
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents Label68 As System.Windows.Forms.Label
    Friend WithEvents cborate As System.Windows.Forms.ComboBox
    Friend WithEvents Label69 As System.Windows.Forms.Label
    Friend WithEvents EMP_DATEbirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents ErrorProvider1 As System.Windows.Forms.ErrorProvider
    Friend WithEvents Address2DataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnemp_editdepdts As System.Windows.Forms.Button
    Friend WithEvents emp_pic As System.Windows.Forms.PictureBox
    Friend WithEvents BTNSEARCH As System.Windows.Forms.Button
    Friend WithEvents temp_designation As System.Windows.Forms.TextBox
    Friend WithEvents txtEmailAddress As System.Windows.Forms.TextBox
    Friend WithEvents txtresidencephone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents emp_Datehired As System.Windows.Forms.DateTimePicker
    Friend WithEvents cboemp_status As System.Windows.Forms.ComboBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboemp_civil As System.Windows.Forms.ComboBox
    Friend WithEvents cboemp_gender As System.Windows.Forms.ComboBox
    Friend WithEvents txtprovincialadd As System.Windows.Forms.TextBox
    Friend WithEvents temp_midname As System.Windows.Forms.TextBox
    Friend WithEvents temp_lname As System.Windows.Forms.TextBox
    Friend WithEvents temp_fname As System.Windows.Forms.TextBox
    Friend WithEvents txtEmployeeNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboEmp_type As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblemployee_name As System.Windows.Forms.Label
    Friend WithEvents txtlocalofficenumber As System.Windows.Forms.TextBox
    Friend WithEvents txtweight As System.Windows.Forms.TextBox
    Friend WithEvents txtremarks As System.Windows.Forms.TextBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents cbosalarygrade As System.Windows.Forms.ComboBox
    Friend WithEvents btnrestricted As System.Windows.Forms.Button
    Friend WithEvents TXTKEYEMPLOYEEID As System.Windows.Forms.TextBox
    Friend WithEvents TXTRECID As System.Windows.Forms.TextBox
    Friend WithEvents cbotitledesignation As System.Windows.Forms.ComboBox
    Friend WithEvents Dateresigned As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtorgchart As System.Windows.Forms.TextBox
    Friend WithEvents txtfullname As System.Windows.Forms.TextBox
    Friend WithEvents lblage As System.Windows.Forms.Label
    Friend WithEvents txtage1 As System.Windows.Forms.TextBox
    Friend WithEvents cbodept As System.Windows.Forms.TextBox
    Friend WithEvents LvlEmployeeDependent As System.Windows.Forms.ListView
    Friend WithEvents Label85 As System.Windows.Forms.Label
    Friend WithEvents txtResidenceadd As System.Windows.Forms.TextBox
    Friend WithEvents txtDepartment2 As System.Windows.Forms.TextBox
    Friend WithEvents Label86 As System.Windows.Forms.Label
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtType_employee As System.Windows.Forms.TextBox
    Friend WithEvents txtfxkeypositionlevel As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel2 As WindowsApplication2.PanePanel
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
    Friend WithEvents txtkeycompany As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtkeydivision As System.Windows.Forms.TextBox
    Friend WithEvents txtdescsection As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdepartment As System.Windows.Forms.TextBox
    Friend WithEvents txtdescdivision As System.Windows.Forms.TextBox
    Friend WithEvents txtkeysection As System.Windows.Forms.TextBox
    Friend WithEvents txtparent_ID As System.Windows.Forms.TextBox
    Friend WithEvents TabPage17 As System.Windows.Forms.TabPage
    Friend WithEvents lvlNearestRelatives As System.Windows.Forms.ListView
    Friend WithEvents Label98 As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents BtnEditContact As System.Windows.Forms.Button
    Friend WithEvents BtnDeleteContact As System.Windows.Forms.Button
    Friend WithEvents btnaddContact As System.Windows.Forms.Button
    Friend WithEvents Label100 As System.Windows.Forms.Label
    Friend WithEvents txtMemberID As System.Windows.Forms.TextBox
    Friend WithEvents emp_company As System.Windows.Forms.TextBox
    Friend WithEvents Label67 As System.Windows.Forms.Label
    Friend WithEvents Label66 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label108 As System.Windows.Forms.Label
    Friend WithEvents Label107 As System.Windows.Forms.Label
    Friend WithEvents Label105 As System.Windows.Forms.Label
    Friend WithEvents Label111 As System.Windows.Forms.Label
    Friend WithEvents mem_PaycontDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents txtpayrollcontriamount As System.Windows.Forms.TextBox
    Friend WithEvents Label109 As System.Windows.Forms.Label
    Friend WithEvents Label110 As System.Windows.Forms.Label
    Friend WithEvents mem_MemberDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents mem_WithdrawDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Cbomem_Status As System.Windows.Forms.ComboBox
    Friend WithEvents Label112 As System.Windows.Forms.Label
    Friend WithEvents emp_Ytenures As System.Windows.Forms.TextBox
    Friend WithEvents cboPositionLevel As System.Windows.Forms.ComboBox
    Friend WithEvents Label114 As System.Windows.Forms.Label
    Friend WithEvents cbotaxcode As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtwithdrawal As System.Windows.Forms.TextBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents lvlBankInfo As System.Windows.Forms.ListView
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents lvlSourceIncome As System.Windows.Forms.ListView
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents txtpk_Employee As System.Windows.Forms.TextBox
    Friend WithEvents btnUpdateBA As System.Windows.Forms.Button
    Friend WithEvents btnDeleteBA As System.Windows.Forms.Button
    Friend WithEvents btnNewBA As System.Windows.Forms.Button
    Friend WithEvents btnUpdateSInc As System.Windows.Forms.Button
    Friend WithEvents btnDeleteSInc As System.Windows.Forms.Button
    Friend WithEvents btnsaveSInc As System.Windows.Forms.Button
    Friend WithEvents chkNo As System.Windows.Forms.CheckBox
    Friend WithEvents chkyes As System.Windows.Forms.CheckBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents cboPaycode As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtitemno As System.Windows.Forms.TextBox
    Friend WithEvents txtmonthreqular As System.Windows.Forms.TextBox
    Friend WithEvents txtsalarygrade As System.Windows.Forms.TextBox
    Friend WithEvents txtdateregular As System.Windows.Forms.TextBox
    Friend WithEvents Dateregular As System.Windows.Forms.DateTimePicker
    Friend WithEvents emp_extphone As System.Windows.Forms.MaskedTextBox
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents btnAddMobileNo As System.Windows.Forms.Button
    Friend WithEvents others As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkIsEloader As System.Windows.Forms.CheckBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents chkShowPINChar As System.Windows.Forms.CheckBox
    Friend WithEvents txtEloaderPIN As System.Windows.Forms.TextBox
    Friend WithEvents txtEloadingLimit As System.Windows.Forms.TextBox
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents gridMobileNos As System.Windows.Forms.DataGridView
    Friend WithEvents txtMobileNo As System.Windows.Forms.MaskedTextBox
    Friend WithEvents btnEditMobile As System.Windows.Forms.Button
    Friend WithEvents chkBereaveYes As System.Windows.Forms.CheckBox
    Friend WithEvents chkBereaveNo As System.Windows.Forms.CheckBox
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents grpPayrollStatus As System.Windows.Forms.GroupBox
    Friend WithEvents rdoNonExempt As System.Windows.Forms.RadioButton
    Friend WithEvents rdoExempt As System.Windows.Forms.RadioButton
End Class
