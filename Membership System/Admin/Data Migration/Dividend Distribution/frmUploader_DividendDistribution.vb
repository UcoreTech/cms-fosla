Imports System.Data.OleDb, System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

'TODO:  Loading of History of uploads and deleting 
'       Uploading too
Public Class frmUploader_DividendDistribution
    Private gOleDBCOnn As New OleDbConnection
    Private gCon As New Clsappconfiguration()
    Private isValid As Boolean
    Private dtable_EmpNoValidation As New DataTable

#Region "Events "

    Private Sub toolGenerateTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolGenerateTemplate.Click
        SaveFileDialog_Dividend.ShowDialog()
    End Sub

    Private Sub SaveFileDialog_Dividend_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles SaveFileDialog_Dividend.FileOk
        Try
            Call GenerateTemplate(SaveFileDialog_Dividend.FileName)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub SaveFileDialog_Dividend_HelpRequest(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SaveFileDialog_Dividend.HelpRequest
        MessageBox.Show("Use this to save the template to a specific location.", "Dividend Distribution Template", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub OpenFileDialog_Dividend_FileOk(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles OpenFileDialog_Dividend.FileOk
        Dim xFilePath As String
        xFilePath = OpenFileDialog_Dividend.FileName

        'Determine Excel FileType
        Dim fileType As String
        fileType = IO.Path.GetExtension(xFilePath)

        Call UploadExcelFileToGrid(xFilePath, fileType)
        Call FormatDataGrid()
        Call CleanDatagridview()

        'toolStripFilePath.Text = xFilePath
    End Sub

    Private Sub toolClearData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolClearData.Click
        If grdDividendDistribution.Rows.Count() <> 0 Then
            If MessageBox.Show("Proceed to clear the data?", "Validation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                grdDividendDistribution.Columns.Clear()
                'toolStripFilePath.Text = ""
            End If
        Else
            MessageBox.Show("User Error: There is no data to clear.", "User Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub toolUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolUpload.Click
        If grdDividendDistribution.Rows.Count <> 0 Then
            If MessageBox.Show("Are you sure you want to upload these data? Make sure this is correct.", "Upload Dividends", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Call ValidateMemberIfExisting()
                Call DisplayMarqueeProgressBar("Validating Members")

                'Initiate Validations on Background (Upload Dividend)
                bgwDividend.RunWorkerAsync()
            End If
        Else
            MessageBox.Show("User Error: There is no data to upload.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub ToolStripUndoUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ToolStripUndoUpload.Click
        frmUndoDividendUploads.ShowDialog()
    End Sub
#End Region

#Region "Generate Template "
    'Todo: make a class
    Private Sub GenerateTemplate(ByVal xFilePath As String)
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        xlApp = New Excel.ApplicationClass
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("Sheet1")

        'Set Excel Header
        xlWorkSheet.Cells(1, 1) = "Employee No" '0
        xlWorkSheet.Cells(1, 2) = "Effective Share" '1
        xlWorkSheet.Cells(1, 3) = "Refunds" '2
        xlWorkSheet.Cells(1, 4) = "Dividends" '3
        xlWorkSheet.Cells(1, 5) = "Distribution Date" '4
        xlWorkSheet.Cells(1, 6) = "Amount Due" '5
        xlWorkSheet.Cells(1, 7) = "Dividend Year" '6

        Try
            'Adjust Template

            xlWorkSheet.Columns.AutoFit()

            'Format Table for better viewability
            Dim formatTable As Excel.Range
            formatTable = xlWorkSheet.Range("a1", "g2")
            formatTable.Select()
            formatTable.AutoFormat(Excel.XlRangeAutoFormat.xlRangeAutoFormatColor2)


            'Protect Worksheet and define Editable Cells
            Dim editable As Excel.Range
            editable = xlWorkSheet.Range("a2", xlWorkSheet.Range("g2").End(Excel.XlDirection.xlDown))
            editable.ColumnWidth = 25
            editable.Locked = False

            xlWorkSheet.Protect()
            'TODO: Need to download Microsoft.Office.Tools.Excel.dll to use this function
            'xlWorkSheet.Protect(DrawingObjects:=xlWorkSheet.ProtectDrawingObjects, _
            '            Contents:=True, Scenarios:=xlWorkSheet.ProtectScenarios, _
            '            UserInterfaceOnly:=xlWorkSheet.ProtectionMode, _
            '            AllowInsertingRows:=True, _
            '            AllowDeletingRows:=True, _
            '            AllowSorting:=True, _
            '            AllowFiltering:=True)

            'Save Template
            xlWorkSheet.SaveAs(xFilePath)

            'Close and Release Excel Application
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            MessageBox.Show("Template is created.", "Template Generator", MessageBoxButtons.OK)

            'Activate and Open Template 
            Dim ps As New ProcessStartInfo
            ps.UseShellExecute = True
            ps.FileName = xFilePath
            Process.Start(ps)
        Catch ex As Exception

            'Close and Release Excel Application
            xlWorkBook.Close()
            xlApp.Quit()
            releaseObject(xlApp)
            releaseObject(xlWorkBook)
            releaseObject(xlWorkSheet)

            Throw ex
        End Try



    End Sub

    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
#End Region

#Region "Upload Excel File To Grid "
    Public Sub GetConnection(ByVal FilePath As String, ByVal fileType As String)


        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string
                Dim connectionString As String

                If fileType = ".xlsx" Then
                    connectionString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1""")
                Else
                    connectionString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath)
                End If


                gOleDBCOnn = New OleDbConnection(connectionString)
                'gOleCmd = gOleConn.CreateCommand
                gOleDBCOnn.Open()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UploadExcelFileToGrid(ByVal xFilePath As String, ByVal filetype As String)

        If xFilePath <> "" Then
            Try
                Dim dt As New DataTable()
                dt.Rows.Clear()
                dt.Columns.Clear()

                Me.Cursor = Cursors.WaitCursor
                Me.GetConnection(xFilePath, filetype)
                Dim FileName As String = xFilePath

                Dim oleDA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", gOleDBCOnn)

                'get data from excel
                oleDA.Fill(dt)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = dt.Rows.Count

                If dt.Rows.Count <> 0 Then
                    grdDividendDistribution.DataSource = dt
                End If

            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show(ex.Message, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                gOleDBCOnn.Close()
            End Try
        End If
    End Sub

    Private Sub FormatDataGrid()
        With grdDividendDistribution
            .Columns("Effective Share").DefaultCellStyle.Format = "n2"
            .Columns("Refunds").DefaultCellStyle.Format = "n2"
            .Columns("Dividends").DefaultCellStyle.Format = "n2"
            .Columns("Amount Due").DefaultCellStyle.Format = "n2"

            .Columns("Effective Share").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Refunds").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Dividends").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns("Amount Due").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

            .Columns("Employee No").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Distribution Date").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
            .Columns("Dividend Year").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

        End With
    End Sub

    Private Sub toolBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles toolBrowse.Click
        OpenFileDialog_Dividend.ShowDialog()
    End Sub
#End Region

#Region "Upload From Grid to Database "
    Private Sub UploadDividendsToDB(ByVal dtable As DataTable)
        Dim empNo As String
        Dim effectiveShare As Decimal
        Dim refunds As Decimal
        Dim dividends As Decimal
        Dim distributionDate As Date
        Dim amountDue As Decimal
        Dim dividendYear As String
        Dim user As String = frmLogin.Username.Text

        Dim xCount As Integer = 0
        Try
            For Each xRow As DataGridViewRow In grdDividendDistribution.Rows
                xCount += 1

                empNo = xRow.Cells("Employee No").Value.ToString()
                effectiveShare = xRow.Cells("Effective Share").Value
                refunds = xRow.Cells("Refunds").Value
                dividends = xRow.Cells("Dividends").Value
                distributionDate = xRow.Cells("Distribution Date").Value
                amountDue = xRow.Cells("Amount Due").Value
                dividendYear = xRow.Cells("Dividend Year").Value.ToString()

                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_DividendUpload_Insert", _
                        New SqlParameter("@employeeNo", empNo), _
                        New SqlParameter("@fdEffectiveShare", effectiveShare), _
                        New SqlParameter("@fdRefund", refunds), _
                        New SqlParameter("@fdDividend", dividends), _
                        New SqlParameter("@fdAmountDue", amountDue), _
                        New SqlParameter("@dtDistributionDate", distributionDate), _
                        New SqlParameter("@dtDividendDistributionYear", dividendYear), _
                        New SqlParameter("@fcUploadedBy", user))

            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayMarqueeProgressBar(ByVal title As String)
        With frmStartup
            .MdiParent = frmMain
            .Show()
            .processWait.Style = ProgressBarStyle.Marquee
            .processWait.MarqueeAnimationSpeed = 5
            .lblTitle.Text = title
            .lblProcessStatus.Text = ""
            .lblProgress.Text = ""
        End With
    End Sub
#End Region

#Region "Validations "
    Private Sub grdDividendDistribution_DataError(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdDividendDistribution.DataError
        ' If the data source raises an exception when a cell value is 
        ' commited, display an error message.
        If e.Exception IsNot Nothing Then
            'AndAlso _
            'e.Context = DataGridViewDataErrorContexts.Commit Then
            MessageBox.Show("Wrong input, please correct your entry and try again.")
        End If
    End Sub

    Private Sub ValidateMemberIfExisting()
        Dim xCount As Integer = 0
        Dim currentItem As String
        Dim empNo As String

        Dim dRow As DataRow

        dtable_EmpNoValidation.Rows.Clear()
        dtable_EmpNoValidation.Columns.Clear()
        dtable_EmpNoValidation.Columns.Add("EmpNo")

        For Each xRow As DataGridViewRow In grdDividendDistribution.Rows
            xCount += 1

            empNo = xRow.Cells(0).Value.ToString

            Using rd As SqlDataReader = (SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "CIMS_Admin_CheckIfEmployeeNoExists", _
                    New SqlParameter("@employeeNo", empNo)))
                If rd.Read() Then
                    dRow = dtable_EmpNoValidation.NewRow()
                    dRow("EmpNo") = rd.Item("empNo")
                    dtable_EmpNoValidation.Rows.Add(dRow)
                End If
            End Using

            currentItem = "validating " & empNo
            frmStartup.lblProgress.Text = currentItem
        Next
    End Sub

    Private Sub ExportToExcel(ByVal objDT As DataTable)
        Dim Excel As Object = CreateObject("Excel.Application")
        Dim strFilename As String
        Dim intCol, intRow As Integer
        Dim strPath As String = "c:\"

        If Excel Is Nothing Then
            MsgBox("It appears that Excel is not installed on this machine. This operation requires MS Excel to be installed on this machine.", MsgBoxStyle.Critical)
            Return
        End If
        Try
            With Excel
                .SheetsInNewWorkbook = 1
                .Workbooks.Add()
                .Worksheets(1).Select()

                .cells(1, 1).value = "List of Employee No that is non-existing in our DB. Please Update our Member Master first before proceeding." 'Heading of the excel file
                .cells(1, 1).EntireRow.Font.Bold = True


                Dim intI As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    .cells(2, intI).value = objDT.Columns(intCol).ColumnName
                    .cells(2, intI).EntireRow.Font.Bold = True
                    intI += 1
                Next
                intI = 3
                Dim intK As Integer = 1
                For intCol = 0 To objDT.Columns.Count - 1
                    intI = 3
                    For intRow = 0 To objDT.Rows.Count - 1
                        .Cells(intI, intK).Value = objDT.Rows(intRow).ItemArray(intCol)
                        intI += 1
                    Next
                    intK += 1
                Next
                If Mid$(strPath, strPath.Length, 1) <> "\" Then
                    strPath = strPath & "\"
                End If


                strFilename = strPath & "Excel.xlsx"


                .ActiveCell.Worksheet.SaveAs(strFilename)
            End With
            System.Runtime.InteropServices.Marshal.ReleaseComObject(Excel)
            Excel = Nothing
            MsgBox("Data are exported to Excel Succesfully in '" & strFilename & "'", MsgBoxStyle.Information)

            'Activate and Open Template 
            Dim ps As New ProcessStartInfo
            ps.UseShellExecute = True
            ps.FileName = strFilename
            Process.Start(ps)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        '' The excel is created and opened for insert value. We most close this excel using this system
        'Dim pro() As Process = System.Diagnostics.Process.GetProcessesByName("EXCEL")
        'For Each i As Process In pro
        '    i.Kill()
        'Next
    End Sub
#End Region

#Region "Background Tasks "
    Private Sub bgwDividend_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwDividend.DoWork
        Try
            Dim listOfNonExistingMembers As DataTable = dtable_EmpNoValidation
            If listOfNonExistingMembers.Rows.Count <> 0 Then
                isValid = False
                Call ExportToExcel(listOfNonExistingMembers)
            Else
                isValid = True
                Call UploadDividendsToDB(listOfNonExistingMembers)
                MessageBox.Show("Upload Successful!", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub bgwDividend_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwDividend.RunWorkerCompleted
        frmStartup.Close()
    End Sub
#End Region

    Private Sub CleanDatagridview()
        For Each xRow As DataGridViewRow In grdDividendDistribution.Rows
            If xRow.Cells(0).Value.ToString() = "" Then
                grdDividendDistribution.Rows.RemoveAt(xRow.Index)
            End If
        Next
    End Sub
End Class