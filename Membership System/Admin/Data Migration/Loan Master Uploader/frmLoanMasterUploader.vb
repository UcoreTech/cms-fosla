'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: October 13, 2010                   ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports System.Data.OleDb, System.IO

Public Class frmLoanMasterUploader
    Private xFilePath As String = ""
    Private gDT As New DataTable
    Private gOleConn As New OleDbConnection
    Private gOleCmd As New OleDbCommand
    Private UploaderType As String
    Public xMode As String = ""
    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub
    Public Sub GetConnection(ByVal FilePath As String)
        'check if the file exists
        Try
            If Not File.Exists(FilePath) Then
                MessageBox.Show("File does not exist or access is denied!", "Upload", MessageBoxButtons.OK)
            Else
                'connection string
                Dim gCnstring As String
                If UploaderType = ".xlsx" Then
                    gCnstring = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & FilePath & ";Extended Properties=""Excel 12.0 Xml;HDR=YES;IMEX=1""")
                Else
                    gCnstring = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1""", FilePath)
                End If
                gOleConn = New OleDbConnection(gCnstring)
                gOleCmd = gOleConn.CreateCommand
                gOleConn.Open()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub UploadExcelFile()
        If xFilePath <> "" Then
            Me.Cursor = Cursors.WaitCursor
            Me.GetConnection(xFilePath)
            Dim FileName As String = xFilePath

            Dim oleDA As New OleDbDataAdapter("SELECT * FROM [Sheet1$]", gOleConn)
            Try
                'get data from excel
                oleDA.Fill(gDT)
                Me.Cursor = Cursors.Arrow
                Dim rows As Integer = gDT.Rows.Count

                If gDT.Rows.Count <> 0 Then
                    grdUploader.DataSource = gDT
                End If
                Select Case xMode

                    Case "Loan Master"
                        GrdUploader.Columns("Emp No").DefaultCellStyle.Format = "####"
                        GrdUploader.Columns("Terms").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Principal").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Principal").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Interest Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Interest Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Interest Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Service Fee Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Service Fee Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Service Fee Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Capital Share Amount").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Capital Share Amount").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Penalty Rate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Penalty").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Penalty").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Monthly Amortization").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Monthly Amortization").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Paydate FR").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Paydate TO").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Last Update").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Interest Refund").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Interest Refund").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Loan No").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Loan Status").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Loan Classification").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                        GrdUploader.Columns("Total Payable").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Total Payable").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        GrdUploader.Columns("Balance").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Balance").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight

                    Case "Loan Payment"
                        GrdUploader.Columns(7).DefaultCellStyle.Format = "##,##0.00"
                        GrdUploader.Columns(8).DefaultCellStyle.Format = "##,##0.00"
                    Case "Member Contribution"
                        'GrdUploader.Columns(1).DefaultCellStyle.Format = "####"
                        GrdUploader.Columns("Withdrawal").DefaultCellStyle.Format = "n2"
                        GrdUploader.Columns("Deposit").DefaultCellStyle.Format = "n2"

                    Case "Savings"
                        GrdUploader.Columns("Deposit Amount").DefaultCellStyle.Format = "##,##0.00"
                        GrdUploader.Columns("Withdrawal Amount").DefaultCellStyle.Format = "##,##0.00"


                End Select


            Catch ex As Exception
                Me.Cursor = Cursors.Arrow
                MessageBox.Show(ex.Message, "Upload", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Finally
                gOleConn.Close()
            End Try
        End If
    End Sub

    Private Sub GenerateTemplate()
        Dim xlApp As Excel.Application
        Dim xlWorkBook As Excel.Workbook
        Dim xlWorkSheet As Excel.Worksheet
        Dim misValue As Object = System.Reflection.Missing.Value

        xlApp = New Excel.ApplicationClass
        xlWorkBook = xlApp.Workbooks.Add(misValue)
        xlWorkSheet = xlWorkBook.Sheets("Sheet1")

        Select Case xMode
            Case Is = "Loan Master"
                xlWorkSheet.Cells(1, 1) = "Loan Type" '0
                xlWorkSheet.Cells(1, 2) = "Emp No" '1
                xlWorkSheet.Cells(1, 3) = "First Name" '2
                xlWorkSheet.Cells(1, 4) = "Last Name" '3
                xlWorkSheet.Cells(1, 5) = "MI" '4
                xlWorkSheet.Cells(1, 6) = "Loan Date" '5
                xlWorkSheet.Cells(1, 7) = "Terms" '6
                xlWorkSheet.Cells(1, 8) = "Principal" '7
                xlWorkSheet.Cells(1, 9) = "Interest Rate" '8
                xlWorkSheet.Cells(1, 10) = "Interest Amount" '9
                xlWorkSheet.Cells(1, 11) = "Service Fee Rate" '10
                xlWorkSheet.Cells(1, 12) = "Service Fee Amount" '11
                xlWorkSheet.Cells(1, 13) = "Capital Share Rate" '12
                xlWorkSheet.Cells(1, 14) = "Capital Share Amount" '13
                xlWorkSheet.Cells(1, 15) = "Penalty Rate" '14
                xlWorkSheet.Cells(1, 16) = "Penalty" '15
                xlWorkSheet.Cells(1, 17) = "Amount Paid" '16
                xlWorkSheet.Cells(1, 18) = "Total Payable" '17
                xlWorkSheet.Cells(1, 19) = "Balance" '18
                xlWorkSheet.Cells(1, 20) = "Monthly Amortization" '19
                xlWorkSheet.Cells(1, 21) = "Paydate FR" '20
                xlWorkSheet.Cells(1, 22) = "Paydate TO" '21
                xlWorkSheet.Cells(1, 23) = "Last Update" '22
                xlWorkSheet.Cells(1, 24) = "Interest Refund" '23
                xlWorkSheet.Cells(1, 25) = "Settlement Date" '24
                xlWorkSheet.Cells(1, 26) = "Loan No" '25
                xlWorkSheet.Cells(1, 27) = "Loan Status" '26
                xlWorkSheet.Cells(1, 28) = "Loan Classification" '27

                xlWorkSheet.Columns(1).ColumnWidth = 14
                xlWorkSheet.Columns(2).ColumnWidth = 10
                xlWorkSheet.Columns(3).ColumnWidth = 22
                xlWorkSheet.Columns(4).ColumnWidth = 22
                xlWorkSheet.Columns(5).ColumnWidth = 8
                xlWorkSheet.Columns(6).ColumnWidth = 10
                xlWorkSheet.Columns(7).ColumnWidth = 6
                xlWorkSheet.Columns(8).ColumnWidth = 10
                xlWorkSheet.Columns(9).ColumnWidth = 12
                xlWorkSheet.Columns(10).ColumnWidth = 15
                xlWorkSheet.Columns(11).ColumnWidth = 16
                xlWorkSheet.Columns(12).ColumnWidth = 21
                xlWorkSheet.Columns(13).ColumnWidth = 18
                xlWorkSheet.Columns(14).ColumnWidth = 21
                xlWorkSheet.Columns(15).ColumnWidth = 12
                xlWorkSheet.Columns(16).ColumnWidth = 8
                xlWorkSheet.Columns(17).ColumnWidth = 12
                xlWorkSheet.Columns(18).ColumnWidth = 13
                xlWorkSheet.Columns(19).ColumnWidth = 10
                xlWorkSheet.Columns(20).ColumnWidth = 20
                xlWorkSheet.Columns(21).ColumnWidth = 11
                xlWorkSheet.Columns(22).ColumnWidth = 11
                xlWorkSheet.Columns(23).ColumnWidth = 11
                xlWorkSheet.Columns(24).ColumnWidth = 15
                xlWorkSheet.Columns(25).ColumnWidth = 15
                xlWorkSheet.Columns(26).ColumnWidth = 9
                xlWorkSheet.Columns(27).ColumnWidth = 10
                xlWorkSheet.Columns(28).ColumnWidth = 17

            Case "Loan Payment"
                xlWorkSheet.Cells(1, 1) = "Loan Type" '0
                xlWorkSheet.Cells(1, 2) = "Loan No" '1
                xlWorkSheet.Cells(1, 3) = "Emp No" '2
                xlWorkSheet.Cells(1, 4) = "Name" '3
                xlWorkSheet.Cells(1, 5) = "Account" '4
                xlWorkSheet.Cells(1, 6) = "Date" '5
                xlWorkSheet.Cells(1, 7) = "Particulars" '6
                xlWorkSheet.Cells(1, 8) = "Loan" '7
                xlWorkSheet.Cells(1, 9) = "Payment" '8

                xlWorkSheet.Columns(1).ColumnWidth = 10
                xlWorkSheet.Columns(2).ColumnWidth = 10
                xlWorkSheet.Columns(3).ColumnWidth = 10
                xlWorkSheet.Columns(4).ColumnWidth = 23
                xlWorkSheet.Columns(5).ColumnWidth = 32
                xlWorkSheet.Columns(6).ColumnWidth = 11
                xlWorkSheet.Columns(7).ColumnWidth = 45
                xlWorkSheet.Columns(8).ColumnWidth = 10
                xlWorkSheet.Columns(9).ColumnWidth = 10

                'Highlight Required Fields
                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"
                xlWorkSheet.Cells(1, 6).Style = "NewStyle"
                xlWorkSheet.Cells(1, 7).Style = "NewStyle"
                xlWorkSheet.Cells(1, 8).Style = "NewStyle"
                xlWorkSheet.Cells(1, 9).Style = "NewStyle"

            Case "Member Contribution"
                xlWorkSheet.Cells(1, 1) = "Emp No" '0
                xlWorkSheet.Cells(1, 2) = "Name" '1
                xlWorkSheet.Cells(1, 3) = "Account" '2
                xlWorkSheet.Cells(1, 4) = "Date" '3
                xlWorkSheet.Cells(1, 5) = "Particulars" '4
                xlWorkSheet.Cells(1, 6) = "Withdrawal" '5
                xlWorkSheet.Cells(1, 7) = "Deposit" '6

                xlWorkSheet.Columns(1).ColumnWidth = 10
                xlWorkSheet.Columns(2).ColumnWidth = 10
                xlWorkSheet.Columns(3).ColumnWidth = 10
                xlWorkSheet.Columns(4).ColumnWidth = 23
                xlWorkSheet.Columns(5).ColumnWidth = 32
                xlWorkSheet.Columns(6).ColumnWidth = 10
                xlWorkSheet.Columns(7).ColumnWidth = 10

                'Highlight Required Fields
                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 4).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"
                xlWorkSheet.Cells(1, 6).Style = "NewStyle"
                xlWorkSheet.Cells(1, 7).Style = "NewStyle"

            Case "Savings"
                xlWorkSheet.Cells(1, 1) = "Emp No"
                xlWorkSheet.Cells(1, 2) = "Member Name"
                xlWorkSheet.Cells(1, 3) = "Date"
                xlWorkSheet.Cells(1, 4) = "Description"
                xlWorkSheet.Cells(1, 5) = "Deposit Amount"
                xlWorkSheet.Cells(1, 6) = "Withdrawal Amount"

                xlWorkSheet.Columns(1).ColumnWidth = 10
                xlWorkSheet.Columns(2).ColumnWidth = 20
                xlWorkSheet.Columns(3).ColumnWidth = 10
                xlWorkSheet.Columns(4).ColumnWidth = 20
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 20


                'Highlight Required Fields
                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 4).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"
                xlWorkSheet.Cells(1, 6).Style = "NewStyle"

            Case "Paydate Schedule"

                xlWorkSheet.Cells(1, 1) = "Paycode"
                xlWorkSheet.Cells(1, 2) = "Date Period From"
                xlWorkSheet.Cells(1, 3) = "Date Period To"
                xlWorkSheet.Cells(1, 4) = "Paydate"
                xlWorkSheet.Cells(1, 5) = "Year"
                xlWorkSheet.Cells(1, 6) = "Is Deductible"

                xlWorkSheet.Columns(1).ColumnWidth = 10
                xlWorkSheet.Columns(2).ColumnWidth = 20
                xlWorkSheet.Columns(3).ColumnWidth = 10
                xlWorkSheet.Columns(4).ColumnWidth = 20
                xlWorkSheet.Columns(5).ColumnWidth = 20
                xlWorkSheet.Columns(6).ColumnWidth = 20


                'Highlight Required Fields
                Dim style As Excel.Style = xlWorkSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
                style.Font.Bold = True
                style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow)
                xlWorkSheet.Cells(1, 1).Style = "NewStyle"
                xlWorkSheet.Cells(1, 2).Style = "NewStyle"
                xlWorkSheet.Cells(1, 3).Style = "NewStyle"
                xlWorkSheet.Cells(1, 4).Style = "NewStyle"
                xlWorkSheet.Cells(1, 5).Style = "NewStyle"
                xlWorkSheet.Cells(1, 6).Style = "NewStyle"

        End Select
        xlWorkSheet.Columns.Columns.HorizontalAlignment = 3
        xlWorkSheet.SaveAs(xFilePath)

        xlWorkBook.Close()
        xlApp.Quit()

        releaseObject(xlApp)
        releaseObject(xlWorkBook)
        releaseObject(xlWorkSheet)

        'MsgBox("Excel file created , you can find the file c:\")

        Dim ps As New ProcessStartInfo
        ps.UseShellExecute = True
        ps.FileName = xFilePath
        Process.Start(ps)
    End Sub
    Private Sub BtnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        DlgSaveUploader.ShowDialog()
    End Sub

    Private Sub DlgSaveUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgSaveUploader.FileOk
        xFilePath = DlgSaveUploader.FileName
        Call GenerateTemplate()
        MessageBox.Show("Column Highlighted in YELLOW are required.", "Uploader Advice", MessageBoxButtons.OK, MessageBoxIcon.Information)
    End Sub

    Private Sub DlgOpenUploader_FileOk(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles DlgOpenUploader.FileOk
        txtUploader.Text = DlgOpenUploader.FileName
        xFilePath = txtUploader.Text
        UploaderType = IO.Path.GetExtension(xFilePath)
        If GrdUploader.AllowUserToAddRows = True Then
            GrdUploader.AllowUserToAddRows = False
        End If
        gDT.Rows.Clear()
        gDT.Columns.Clear()
        Call UploadExcelFile()
        If GrdUploader.AllowUserToAddRows = False Then
            GrdUploader.AllowUserToAddRows = True
        End If
    End Sub

    Private Sub BtnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBrowse.Click
        DlgOpenUploader.ShowDialog()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Dim xForm As New FrmLoanMasterUploaderProcess
        xForm.xParent = Me
        xForm.ShowDialog()
    End Sub

    Private Sub frmLoanMasterUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Select Case xMode
            Case "Member Master"
                Me.Text = "Member Master Uploader"
                txtTitleLabel.Text = "Member Master Uploader"
                txtDescription.Text = "Upload List of Member here. Make sure you follow the template required for this uploader."

            Case "Loan Master"
                Me.Text = "Loan Master Uploader"
                txtTitleLabel.Text = "Loan Master Uploader"
                txtDescription.Text = "Upload List of Loans here. Make sure you follow the template required for this uploader."

            Case "Loan Payment"
                Me.Text = "Loan Payment Uploader"
                txtTitleLabel.Text = "Loan Payment Uploader"
                txtDescription.Text = "Upload Loan Payments here. Make sure you follow the template required for this uploader."

            Case "Member Contribution"
                Me.Text = "Contribution Uploader"
                txtTitleLabel.Text = "Contribution Uploader"
                txtDescription.Text = "Upload Contribution here. Make sure you follow the template required for this uploader."

            Case "Savings"
                Me.Text = "Savings Uploader"
                txtTitleLabel.Text = "Savings Uploader"
                txtDescription.Text = "Upload Savings here. Make sure you follow the template required for this uploader."

            Case "Paydate Schedule"
                Me.Text = "Paydate Schedule"
                txtTitleLabel.Text = "Paydate Schedule"
                txtDescription.Text = "Upload Paydate Schedule here. Make sure you follow the template required for this uploader."


        End Select
    End Sub

    Private Sub LinkLabel2_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles LinkLabel2.LinkClicked
        DlgSaveUploader.ShowDialog()
    End Sub

End Class