'                   ########################################################
'                   ### Created by: Charl Magne "Ionflux" Onod San Pedro ###
'                   ### Date Created: October 13, 2010                   ###
'                   ### Website: http://www.ionflux.site50.net           ###
'                   ########################################################

Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class FrmLoanMasterUploaderProcess

    Private gCon As New Clsappconfiguration
    Public xParent As frmLoanMasterUploader
    Private xCount As Integer = 0
    Private CurrentItem As String = ""
    Private XCurrentRowIndex As Integer = 0
    Private Process As Boolean
    Private CurrentValidation As String = ""
    Public DTValidatedItem As New DataTable("ValidatedItems")
    Public empnoItem As String = ""


    Private Function ValidateLoanNo() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanNo"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanNo")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            'xCount += 1
            CurrentItem = "validating " & xRow.Cells(1).Value.ToString
            XCurrentRowIndex = xRow.Index
            sSqlCmd = "SELECT	pk_Members_Loan " & _
                    "FROM dbo.CIMS_t_Member_Loans " & _
                    "WHERE	fnLoanNo = '" & xRow.Cells(1).Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells(1).Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanNo") = xRow.Cells(1).Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateLoanNo in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function ValidateLoanType() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanType"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanType")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            'xCount += 1
            CurrentItem = "validating " & xRow.Cells("Loan Type").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_LoanType " & _
                        "FROM dbo.CIMS_m_Loans_LoanType " & _
                        "WHERE fcLoanTypeName = '" & xRow.Cells("Loan Type").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Loan Type").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next
                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanType") = xRow.Cells("Loan Type").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If
                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateLoanType in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Function ValidateLoanStatus() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False

        CurrentValidation = "LoanStatus"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("LoanStatus")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            AlreadyValidated = False
            'xCount += 1
            CurrentItem = "validating " & xRow.Cells("Loan Status").Value.ToString
            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_Status " & _
                        "FROM dbo.CIMS_m_Loans_Status " & _
                        "WHERE fcStatusName = '" & xRow.Cells("Loan Status").Value.ToString & "'"
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Loan Status").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If
                        Next

                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            DR("LoanStatus") = xRow.Cells("Loan Status").Value.ToString
                            DTValidatedItem.Rows.Add(DR)
                        End If
                    End If

                End Using
            Catch ex As Exception
                Throw ex
            End Try
        Next
        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If
    End Function
    Private Function ValidateEmpNo() As Boolean
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim AlreadyValidated As Boolean = False
        Dim dteValidated As Boolean = False

        Dim dteValidation As Date

        CurrentValidation = "EmpNo"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("EmpNo")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows

            AlreadyValidated = False
            CurrentItem = "validating " & xRow.Cells("Emp No").Value.ToString
            XCurrentRowIndex = xRow.Index

            dteValidation = xRow.Cells("Date").Value
            XCurrentRowIndex = xRow.Index

            Dim depositamtValidation As Decimal = xRow.Cells("Deposit Amount").Value
            Dim withdrawalamtValidation As Decimal = xRow.Cells("Withdrawal Amount").Value 

            Dim employeeNo As String = xRow.Cells("Emp No").Value.ToString.Trim

            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, "usp_t_Admin_EmployeeNo_Validate", _
                                                                    New SqlParameter("@employeeNo", employeeNo))

                    If rd.HasRows = False Then
                        For Each xItem As DataRow In DTValidatedItem.Rows
                            If xItem(0).ToString = xRow.Cells("Emp No").Value.ToString Then
                                AlreadyValidated = True
                                Exit For
                            Else
                                AlreadyValidated = False
                            End If

                            Dim transdate As Date = xRow.Cells("Date").Value

                        Next

                        If AlreadyValidated = False Then
                            DR = DTValidatedItem.NewRow()
                            Dim recordNumber As Integer = xRow.Index + 2
                            If employeeNo = "" Then
                                DR("EmpNo") = "Record No.: " & recordNumber.ToString() & " Blank Employee No. on Source File"
                            Else
                                DR("EmpNo") = employeeNo
                            End If

                        End If

                        DTValidatedItem.Rows.Add(DR)

                    End If
                End Using

            Catch ex As Exception
                'MsgBox("Error at ValidateEmpnNo in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
                MessageBox.Show(ex.Message, "Error occur at Validation : Check your source file.")
                Process = False
            End Try
        Next

        If DTValidatedItem.Rows.Count = 0 Then
            Return True
        Else
            Return False
        End If

    End Function
    Private Sub ValidateEmployee()
        Dim sSqlCmd As String
        Dim DR As DataRow
        Dim empno As String

        CurrentValidation = "EmpNo"

        DTValidatedItem.Rows.Clear()
        DTValidatedItem.Columns.Clear()
        DTValidatedItem.Columns.Add("EmpNo")

        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows

            CurrentItem = "validating " & xRow.Cells("Emp No").Value.ToString

            XCurrentRowIndex = xRow.Index

            sSqlCmd = "SELECT pk_Employee " & _
                        "FROM dbo.CIMS_m_Member " & _
                        "WHERE cast(fcEmployeeNo as nvarchar) = CAST('" & xRow.Cells("Emp No").Value.ToString & "' AS nvarchar)" 
            Try
                Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.Text, sSqlCmd)
                    If rd.HasRows = False Then
                        DR = DTValidatedItem.NewRow()
                        DR("EmpNo") = xRow.Cells("Emp No").Value.ToString
                        DTValidatedItem.Rows.Add(DR)
                    End If

                End Using
            Catch ex As Exception
                MsgBox("Error at ValidateEmployee in FrmLoanMasterUploaderProcess module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur")
                Process = False
            End Try


        Next
    End Sub

    Private Sub UploadMasterfiles()
        Dim sSqlCmd As String
        For Each xRow As DataGridViewRow In xParent.GrdUploader.Rows
            xCount += 1
            XCurrentRowIndex = xRow.Index

            Select Case xParent.xMode
                Case "Loan Master"
                    CurrentItem = "Uploading " & xRow.Cells(1).Value.ToString

                    sSqlCmd = "CIMS_Upload_LoanMaster " & _
                                "@empNo = '" & xRow.Cells("Emp No").Value.ToString & "', " & _
                                "@LoanNo = '" & xRow.Cells("Loan No").Value.ToString & "', " & _
                                "@Loan = '" & xRow.Cells("Loan Type").Value.ToString & "', " & _
                                "@LoanStatus = '" & xRow.Cells("Loan Status").Value.ToString & "', " & _
                                "@dtLoanApplicationDate = '" & xRow.Cells("Loan Date").Value & "', " & _
                                "@fnTermsInMonths = '" & xRow.Cells("Terms").Value.ToString & "', " & _
                                "@fdPrincipal = '" & Format(xRow.Cells("Principal").Value, "####0.00") & "', " & _
                                "@fnInterestPercent = " & xRow.Cells("Interest Rate").Value.ToString & ", " & _
                                "@fdInterestAmount = '" & Format(xRow.Cells("Interest Amount").Value, "####0.00") & "', " & _
                                "@fnServiceFeePercent = " & xRow.Cells("Service Fee Rate").Value.ToString & ", " & _
                                "@fdServiceFeeAmount = '" & Format(xRow.Cells("Service Fee Amount").Value, "####0.00") & "', " & _
                                "@fnCapitalSharePercent = " & xRow.Cells("Capital Share Rate").Value.ToString & ", " & _
                                "@fdCapitalShareAmount = '" & Format(xRow.Cells("Capital Share Amount").Value, "####0.00") & "', " & _
                                "@fnPenaltyPercent = " & xRow.Cells("Penalty Rate").Value.ToString & ", " & _
                                "@fdPenaltyAmount = '" & Format(xRow.Cells("Penalty").Value, "####0.00") & "', " & _
                                "@fdAmountPaid = '" & Format(xRow.Cells("Amount Paid").Value, "####0.00") & "', " & _
                                "@fdReceivable = '" & Format(xRow.Cells("Total Payable").Value, "####0.00") & "', " & _
                                "@fdBalance = '" & Format(xRow.Cells("Balance").Value, "####0.00") & "', " & _
                                "@fdAmortization = '" & Format(xRow.Cells("Monthly Amortization").Value, "####0.00") & "', " & _
                                "@dtAmortizationDateFrom = '" & xRow.Cells("Paydate FR").Value & "', " & _
                                "@dtAmortizationDateTo = '" & xRow.Cells("Paydate TO").Value & "', " & _
                                "@fdInterestRefund = '" & Format(xRow.Cells("Interest Refund").Value, "####0.00") & "', " & _
                                "@fdSettlementDate = '" & xRow.Cells("Settlement Date").Value.ToString & "', " & _
                                "@fcClassification = '" & xRow.Cells("Loan Classification").Value.ToString & "'"

                Case "Loan Payment"
                    sSqlCmd = "CIMS_Upload_LoanPayments " & _
                            "@empNo ='" & xRow.Cells(2).Value.ToString & "', " & _
                            "@loanNo ='" & xRow.Cells(1).Value.ToString & "', " & _
                            "@loanType ='" & xRow.Cells(0).Value.ToString & "', " & _
                            "@account ='" & xRow.Cells(4).Value.ToString & "', " & _
                            "@date ='" & xRow.Cells(5).Value.ToString & "', " & _
                            "@particulars ='" & xRow.Cells(6).Value.ToString & "', " & _
                            "@debit ='" & xRow.Cells(7).Value.ToString & "', " & _
                            "@credit ='" & xRow.Cells(8).Value.ToString & "'"

                Case "Member Contribution"
                    sSqlCmd = "CIMS_Upload_MemberContribution " & _
                            "@empNo = '" & xRow.Cells("Emp No").Value.ToString() & "', " & _
                            "@account = '" & xRow.Cells("Account").Value.ToString() & "', " & _
                            "@date = '" & xRow.Cells("Date").Value.ToString() & "', " & _
                            "@description = '" & xRow.Cells("Particulars").Value.ToString() & "', " & _
                            "@withdraw = '" & xRow.Cells("Withdrawal").Value.ToString() & "', " & _
                            "@deposit ='" & xRow.Cells("Deposit").Value.ToString & "'"

                Case "Savings"
                    sSqlCmd = "CIMS_Upload_MemberSavings " & _
                            "@empNo = '" & xRow.Cells("Emp No").Value.ToString() & "', " & _
                            "@date = '" & xRow.Cells("Date").Value.ToString() & "', " & _
                            "@description = '" & xRow.Cells("Description").Value.ToString() & "', " & _
                            "@deposit = '" & xRow.Cells("Deposit Amount").Value.ToString() & "', " & _
                            "@withdraw = '" & xRow.Cells("Withdrawal Amount").Value.ToString & "'"

            End Select

            Try
                SqlHelper.ExecuteNonQuery(gCon.cnstring, CommandType.Text, sSqlCmd)
            Catch ex As Exception
                'MsgBox("Error in Upload at FrmLoanMasterUploaderProcess." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
                Throw ex
                Process = False
            End Try
        Next
        Process = True
    End Sub
    Private Sub FrmLoanMasterUploaderProcess_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'Select Case xParent.xMode

            'Case "Loan Master"
            With xParent.GrdUploader
                ProgressBar.Maximum = .Rows.Count ' * 3
                If .AllowUserToAddRows = True Then
                    .AllowUserToAddRows = False
                End If
                Timer.Enabled = True
                BGWorker.RunWorkerAsync()
            End With

            '    Case "Loan Payment"
            '        With xParent.GrdUploader
            '            ProgressBar.Maximum = .Rows.Count * 3

            '        End With
            'End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub BGWorker_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BGWorker.DoWork
        Select Case xParent.xMode
            Case "Loan Master"
                If ValidateLoanType() = True Then
                    If ValidateLoanStatus() = True Then
                        ValidateEmployee()
                        If DTValidatedItem.Rows.Count = 0 Then
                            Call UploadMasterfiles()
                        Else
                            Process = False
                        End If
                    End If
                Else
                    Process = False
                End If
            Case "Loan Payment"
                If ValidateLoanType() = True Then
                    If ValidateLoanNo() = True Then
                        ValidateEmployee()
                        If DTValidatedItem.Rows.Count = 0 Then
                            Call UploadMasterfiles()
                        Else
                            Process = False
                        End If
                    End If
                Else
                    Process = False
                End If

            Case "Member Contribution"

                Call ValidateEmployee()

                If DTValidatedItem.Rows.Count = 0 Then

                    Call UploadMasterfiles()
                Else
                    Process = False
                    MsgBox("Uploading Failed!", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
                End If

            Case "Savings"

                If ValidateEmpNo() = True Then
                    ValidateEmployee()

                    If DTValidatedItem.Rows.Count = 0 Then
                        Call UploadMasterfiles()
                    Else
                        Process = False
                        MsgBox("Uploading Failed! Check your source file.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
                        End

                    End If

                Else
                    Process = False
                    MsgBox("Uploading Failed! Check your source file.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
                End If


        End Select
    End Sub

    Private Sub BGWorker_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BGWorker.RunWorkerCompleted
        If Process = True Then
            MsgBox("Uploading sucessful!", MsgBoxStyle.Information + MsgBoxStyle.OkOnly, "Access granted")
        Else
            If DTValidatedItem.Rows.Count <> 0 Then
                Dim xForm As New FrmLoanMasterValidationResult
                xForm.xParent = Me
                xForm.xFieldValidated = CurrentValidation
                xForm.ShowDialog()
            Else
                MsgBox("Uploading Failed! Check your source file.", MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Access denied")
            End If
        End If
        With xParent.GrdUploader
            If .AllowUserToAddRows = False Then
                .AllowUserToAddRows = True
            End If
            Timer.Enabled = False
        End With
        Me.Close()
    End Sub

    Private Sub Timer_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer.Tick
        ProgressBar.Value = xCount
        LblProcess.Text = CurrentItem
        LblCounter.Text = XCurrentRowIndex & " of " & ProgressBar.Maximum
    End Sub
End Class