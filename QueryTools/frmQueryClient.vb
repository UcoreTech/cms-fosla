﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmQueryClient

    Private con As New Clsappconfiguration

    Private Sub frmQueryClient_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnExecute_Click(sender As System.Object, e As System.EventArgs) Handles btnExecute.Click
        execQuery()
    End Sub

    Private Sub execQuery()
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, CommandType.Text, txtQuery.Text)
            dgvResult.DataSource = ds.Tables(0)
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class