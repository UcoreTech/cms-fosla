﻿Module _CMS_MsgBox

    Public Sub _Msg(ByVal msg As String, ByVal spu As String, ByVal fnction As String, ByVal dt As String)
        frmMsgBox.txtMessage.Text = msg
        frmMsgBox.txtDetail.Text = "SPU:" + spu + " | Function:" + fnction + " | Date:" + dt
        frmMsgBox.ShowDialog()
    End Sub

End Module
