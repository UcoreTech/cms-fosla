Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing.Point

Imports System.Drawing.Color
Imports System.Data
Imports System.IO
Imports System.Drawing.Imaging
Module Module_app_hired
    Public sSchool(5000) As String
    Public sAddress(5000) As String
    Public sDegree(5000) As String
    Public sGraduate(5000) As String
    Public sMajor(5000) As String
    Public sidnumber(5000) As String
    Public dDateFrom(5000) As String
    Public dDateTo(5000) As String
    Public ikey_ID(5000) As Integer
    Public sawards(5000) As String
    Public scourse(5000) As String
    Public sboard(5000) As String
    Public sscholar(5000) As String

    Public snumber(5000) As String
    Public scompany(5000) As String
    Public sPosition(5000) As String
    Public cAddress(5000) As String
    Public dFrom(5000) As String
    Public dTo(5000) As String
    Public sPhone(5000) As String
    Public sSalary(5000) As String
    Public sReason(5000) As String
    Public nkey_ID(5000) As String
    Public srequirements(50000) As String
    Public scheck(50000) As String
    Public employeeid As Integer
    Public dbpword As String

    Public gblposition As String
    Public check As Boolean
    Public employeeid2 As String
    Public number As String

    'Public Sub update_picture_binary()
    '    Try
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        'Dim myconnection As New SqlConnection(My.Settings.CNString)
    '        Dim cmd As New SqlCommand("UPDATE Temployee_picture SET PICTUREDATE=@PICTUREDATE,IDNUMBER=@IDNUMBER WHERE idnumber=" & "'" & form6.tempid_no.Text & "'", myconnection.sqlconn)
    '        Dim ms As MemoryStream = New MemoryStream()
    '        form6.emp_pic.Image.Save(ms, ImageFormat.Jpeg)
    '        Dim bytBLOBData(ms.Length - 1) As Byte
    '        ms.Position = 0
    '        ms.Read(bytBLOBData, 0, ms.Length)
    '        Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
    '            bytBLOBData.Length, ParameterDirection.Input, False, _
    '            0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '        myconnection.sqlconn.Open()

    '        cmd.Parameters.Add(prm)
    '        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = form6.tempid_no.Text

    '        'myconnection.Open()
    '        cmd.ExecuteNonQuery()
    '        myconnection.sqlconn.Close()
    '    Catch ex As Exception
    '        'MessageBox.Show("There is no picture to be save", "Photo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
    '    End Try
    'End Sub
    'Public Sub UPDATE_picture()
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    'Dim myconnection As New SqlConnection(My.Settings.CNString)
    '    Dim cmd As New SqlCommand("UPDATE Temployee_picture SET PICTUREDATE=@PICTUREDATE,IDNUMBER=@IDNUMBER WHERE idnumber=" & "'" & form6.tempid_no.Text & "'", myconnection.sqlconn)
    '    Dim strBLOBFilePath As String = form6.txtpicture.Text
    '    '"C:\Documents and Settings\All Users\Documents" & _
    '    '"\My Pictures\Sample Pictures\winter.jpg"
    '    Dim fsBLOBFile As New FileStream(strBLOBFilePath, _
    '        FileMode.Open, FileAccess.Read)
    '    Dim bytBLOBData(fsBLOBFile.Length() - 1) As Byte
    '    fsBLOBFile.Read(bytBLOBData, 0, bytBLOBData.Length)
    '    fsBLOBFile.Close()
    '    Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
    '        bytBLOBData.Length, ParameterDirection.Input, False, _
    '        0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '    cmd.Parameters.Add(prm)
    '    myconnection.sqlconn.Open()
    '    cmd.ExecuteNonQuery()
    '    myconnection.sqlconn.Close()
    'End Sub
    '=========================================================
    'system validation that will not accept special characters
    '=========================================================
    'debug.print(e.keychar)'this will show what key do you strike to return value
    Public Function alphanumeric_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsLetterOrDigit(C)) And Not (C = "A") And Not (C = "(") And Not (C = "'") And Not (C = ";") And Not (C = ")") _
            And Not (C = ".") And Not (C = ",") And Not (C = "-") And Not (C = "/") And Not (C = "&") And Not (Microsoft.VisualBasic.AscW(C) = 8) _
            And Not (Microsoft.VisualBasic.AscW(C) = 13) And Not (Microsoft.VisualBasic.AscW(C) = 32) And Not (Microsoft.VisualBasic.AscW(C) = 22) _
            And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MessageBox.Show("Invalid Input! This field allows letters and 0-9 only.", "System Validation")
            Return False
        Else
            Return True
        End If
    End Function
    '=========================================================
    'system validation that will accept numbers only
    '=========================================================
    Public Function numeric_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (C = "-") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) _
        And Not (Microsoft.VisualBasic.AscW(C) = 22) And Not (Microsoft.VisualBasic.AscW(C) = 3) Then
            MessageBox.Show("Invalid Input! This field allows  0-9 only.", "System Validation")
            Return False
        Else
            Return True
        End If
    End Function

End Module
