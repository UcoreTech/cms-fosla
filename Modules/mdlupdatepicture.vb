Imports System.Data.SqlClient.SqlConnection
Imports System.Data.SqlClient
Imports System.Drawing.Point

Imports System.Drawing.Color
Imports System.Data
Imports System.IO
Imports System.Drawing.Imaging
Module mdlupdatepicture
    Public Sub update_employeepicture()
        Try
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            'Dim myconnection As New SqlConnection(My.Settings.CNString)
            Dim cmd As New SqlCommand("UPDATE_EMPLOYEE_PICTURE", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            Dim ms As MemoryStream = New MemoryStream()
            frmMember_Master.emp_pic.Image.Save(ms, ImageFormat.Jpeg)
            Dim bytBLOBData(ms.Length - 1) As Byte
            ms.Position = 0
            ms.Read(bytBLOBData, 0, ms.Length)
            Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
                bytBLOBData.Length, ParameterDirection.Input, False, _
                0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
            myconnection.sqlconn.Open()

            cmd.Parameters.Add(prm)
            cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = frmMember_Master.txtEmployeeNo.Text
            'myconnection.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no picture to update", "Photo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub
    Public Sub insert_picture_binary()
        Try
            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            'Dim myconnection As New SqlConnection(My.Settings.CNString)

            Dim cmd As New SqlCommand("INSERT INTO Temployee_picture (picturedata,idnumber) " & _
                "VALUES (@picturedata,@idnumber)", myconnection.sqlconn)
            Dim ms As MemoryStream = New MemoryStream()
            frmMember_Master.emp_pic.Image.Save(ms, ImageFormat.Jpeg)
            Dim bytBLOBData(ms.Length - 1) As Byte
            ms.Position = 0
            ms.Read(bytBLOBData, 0, ms.Length)
            Dim prm As New SqlParameter("@picturedata", SqlDbType.VarBinary, _
                bytBLOBData.Length, ParameterDirection.Input, False, _
                0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
            myconnection.sqlconn.Open()

            cmd.Parameters.Add(prm)
            cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = frmMember_Master.txtEmployeeNo.Text

            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            'MessageBox.Show("There is no picture to be save", "Photo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
        End Try
    End Sub

    'Public Sub INSERT_HOMESKECTH_FOR_APPLICANT()
    '    Try
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        'Dim myconnection As New SqlConnection(My.Settings.CNString)

    '        Dim cmd As New SqlCommand("INSERT INTO Tapplicant_home_scketch (picture,idnumber) " & _
    '            "VALUES (@picture,@idnumber)", myconnection.sqlconn)
    '        Dim ms As MemoryStream = New MemoryStream()

    '        'Form2.picture_home.Image.Save(ms, ImageFormat.Jpeg)

    '        Dim bytBLOBData(ms.Length - 1) As Byte
    '        ms.Position = 0
    '        ms.Read(bytBLOBData, 0, ms.Length)
    '        Dim prm As New SqlParameter("@picture", SqlDbType.VarBinary, _
    '            bytBLOBData.Length, ParameterDirection.Input, False, _
    '            0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '        myconnection.sqlconn.Open()

    '        cmd.Parameters.Add(prm)

    '        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = Form2.treference_id.Text

    '        cmd.ExecuteNonQuery()
    '        myconnection.sqlconn.Close()
    '    Catch ex As Exception
    '        'MessageBox.Show("There is no picture to be save", "Photo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
    '    End Try
    'End Sub

    'Public Sub update_applicant_homesketch()
    '    Try
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        'Dim myconnection As New SqlConnection(My.Settings.CNString)
    '        Dim cmd As New SqlCommand("USP_APPLICANT_HOME_SKECTH_UPDATE", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure

    '        Dim ms As MemoryStream = New MemoryStream()
    '        Form2.picture_home.Image.Save(ms, ImageFormat.Jpeg)
    '        Dim bytBLOBData(ms.Length - 1) As Byte
    '        ms.Position = 0
    '        ms.Read(bytBLOBData, 0, ms.Length)
    '        Dim prm As New SqlParameter("@picture", SqlDbType.VarBinary, _
    '            bytBLOBData.Length, ParameterDirection.Input, False, _
    '            0, 0, Nothing, DataRowVersion.Current, bytBLOBData)
    '        myconnection.sqlconn.Open()

    '        cmd.Parameters.Add(prm)
    '        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = Form2.treference_id.Text
    '        'myconnection.Open()
    '        cmd.ExecuteNonQuery()
    '        myconnection.sqlconn.Close()
    '    Catch ex As Exception
    '        'MessageBox.Show("There is no picture to update", "Photo", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1)
    '    End Try
    'End Sub

    'Public Sub LOAD_PICTURE_FROM_DBASE(ByVal i As String)
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    'Dim myconnection As New SqlConnection(My.Settings.CNString)

    '    'Dim cmd As New SqlCommand("SELECT BLOBID, " & _
    '    '"BLOBData FROM BLOBTest WHERE BLOBID=", DataGridView1.CurrentRow.Cells(0).Value)
    '    Dim cmd As New SqlCommand
    '    Dim da As New SqlDataAdapter
    '    Dim ds As New DataSet

    '    cmd.CommandText = "select * from Tapplicant_home_scketch where idnumber=" & "'" & Form2.treference_id.Text & "'" 'i & "'"
    '    cmd.CommandType = CommandType.Text
    '    cmd.Connection = myconnection.sqlconn
    '    myconnection.sqlconn.Open()

    '    da.SelectCommand = cmd
    '    da.Fill(ds, "Tapplicant_home_scketch")

    '    Dim c As Integer = ds.Tables("Tapplicant_home_scketch").Rows.Count
    '    If c > 0 Then
    '        Dim bytBLOBData() As Byte = _
    '            ds.Tables("Tapplicant_home_scketch").Rows(c - 1)("picture")
    '        Dim stmBLOBData As New MemoryStream(bytBLOBData)
    '        Form2.picture_home.Image = Image.FromStream(stmBLOBData)
    '    End If
    'End Sub

    'Public Sub CHECK_HOME_ID_PICTURE_APPLICANT()
    '    Try
    '        Dim appRdr As New System.Configuration.AppSettingsReader
    '        Dim myconnection As New Clsappconfiguration
    '        'Dim myconnection As New SqlConnection(My.Settings.CNString)

    '        Dim cmd As New SqlCommand("USP_CHECKAPPLICANTID_PICTURE", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        myconnection.sqlconn.Open()
    '        cmd.Parameters.Add("@IDNUMBER", SqlDbType.VarChar, 50).Value = Form2.treference_id.Text
    '        Dim myreader As SqlDataReader
    '        myreader = cmd.ExecuteReader()

    '        If myreader.HasRows Then
    '            Call update_applicant_homesketch()
    '        Else
    '            Call INSERT_HOMESKECTH_FOR_APPLICANT()
    '        End If

    '        myreader.Close()
    '        myconnection.sqlconn.Close()
    '    Catch ex As Exception

    '    End Try
    'End Sub

    Public Sub CHECK_ID_PICTURE()
        Try

            Dim appRdr As New System.Configuration.AppSettingsReader
            Dim myconnection As New Clsappconfiguration
            'Dim myconnection As New SqlConnection(My.Settings.CNString)

            Dim cmd As New SqlCommand("USP_CHECHPICTURE_IDNUMBER", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            myconnection.sqlconn.Open()
            cmd.Parameters.Add("@IDNUMBER", SqlDbType.VarChar, 50).Value = frmMember_Master.txtEmployeeNo.Text
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader()

            If myreader.HasRows Then
                Call update_employeepicture()
            Else
                Call insert_picture_binary()
            End If

            myreader.Close()
            myconnection.sqlconn.Close()
        Catch ex As Exception

        End Try
    End Sub
End Module
