﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLocMemberSearch

    Private con As New Clsappconfiguration
    Public searchMode As String

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        searchMode = ""
        Me.Close()
    End Sub

    Private Sub TextBox1_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TextBox1.TextChanged
        _LoadSearch(searchMode)
    End Sub

    Private Sub _LoadSearch(ByVal mmode As String)
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(con.cnstring, "_SEARCH_MEMBER_Location",
                                       New SqlParameter("@mode", mmode),
                                       New SqlParameter("@key", TextBox1.Text))
        DataGridView1.DataSource = ds.Tables(0)
    End Sub

    Private Sub frmLocMemberSearch_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadSearch(searchMode)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        _Select()
    End Sub

    Private Sub DataGridView1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles DataGridView1.DoubleClick
        _Select()
    End Sub

    Private Sub DataGridView1_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles DataGridView1.KeyDown
        If e.KeyCode = Keys.Enter Then
            _Select()
        End If
    End Sub

    Private Sub _Select()
        Select Case searchMode
            Case "Group"
                frmMember_Master.cbofGroup.Text = DataGridView1.CurrentRow.Cells(0).Value.ToString
            Case "SubGroup"
                frmMember_Master.cboSubgroup.Text = DataGridView1.CurrentRow.Cells(0).Value.ToString
            Case "Category"
                frmMember_Master.cbofCategory.Text = DataGridView1.CurrentRow.Cells(0).Value.ToString
            Case "Type"
                frmMember_Master.cbofType.Text = DataGridView1.CurrentRow.Cells(0).Value.ToString
            Case Else
                Exit Sub
        End Select
        Me.Close()
    End Sub

End Class