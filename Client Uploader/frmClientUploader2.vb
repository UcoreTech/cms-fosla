﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Public Class frmClientUploader2

    Private con As New Clsappconfiguration

    Private Sub frmClientUploader2_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            Dim filenym As String = OpenFileDialog1.FileName
            Dim fpath As String = Path.GetFullPath(filenym)
            txtPath.Text = fpath            Import()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "CMS")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If MsgBox("Are you sure?", vbYesNo + vbQuestion, "Saving Data") = vbYes Then
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList.Rows(i)
                    Call SaveData(.Cells(0).Value.ToString,
                                  .Cells(1).Value.ToString,
                                  .Cells(2).Value.ToString,
                                  .Cells(3).Value.ToString,
                                  .Cells(4).Value.ToString,
                                  .Cells(5).Value.ToString,
                                  .Cells(6).Value.ToString,
                                  .Cells(7).Value.ToString,
                                  .Cells(8).Value.ToString,
                                  .Cells(9).Value.ToString,
                                  .Cells(10).Value.ToString,
                                  .Cells(11).Value.ToString,
                                  .Cells(12).Value.ToString)
                End With
            Next
            'Call SaveData()
        End If
    End Sub

    Private Sub SaveData(ByVal idno As String,
                         ByVal company As String,
                         ByVal female_maidenlname As String,
                         ByVal placeofbirth As String,
                         ByVal nameofspouse As String,
                         ByVal payrollcode As String,
                         ByVal salary As String,
                         ByVal bankcode As String,
                         ByVal bankname As String,
                         ByVal branch As String,
                         ByVal accttype As String,
                         ByVal atmno As String,
                         ByVal locationcode As String)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "Client_Upload2",
                                      New SqlParameter("@idno", idno),
                                      New SqlParameter("@companyname", company),
                                      New SqlParameter("@female_maiden_lastname", female_maidenlname),
                                      New SqlParameter("@placeofbirth", placeofbirth),
                                      New SqlParameter("@nameofspouse", nameofspouse),
                                      New SqlParameter("@payrollcode", payrollcode),
                                      New SqlParameter("@salary", salary),
                                      New SqlParameter("@bankcode", bankcode),
                                      New SqlParameter("@bankname", bankname),
                                      New SqlParameter("@branch", branch),
                                      New SqlParameter("@accounttype", accttype),
                                      New SqlParameter("@atmno", atmno),
                                      New SqlParameter("@locationcode", locationcode))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class