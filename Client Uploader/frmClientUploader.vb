﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

'Module : Client Uploader
'Programmer : Vincent Nacar
'Minimum Upload Speed : 0.1 Sec
'Maximum Upload Speed : 60 Sec
'-------------------------------------------------------

Public Class frmClientUploader
    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        If cboTemplate.Text = "Client Personal Information" Then
            Try
                OpenFileDialog1.ShowDialog()
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                txtPath.Text = fpath

                AddSelectButton()
                Import()
                Check_ErrorRows()
            Catch ex As Exception
                ' frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
                frmMsgBox.txtMessage.Text = "User Cancelled!"
                frmMsgBox.ShowDialog()
            End Try
        End If
        If cboTemplate.Text = "Referral / Persons Referred" Then
            Try
                OpenFileDialog1.ShowDialog()
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                txtPath.Text = fpath

                'AddSelectButton()
                Import()
                'Check_ErrorRows()
            Catch ex As Exception
                ' frmMsgBox.txtMessage.Text = "Error :" + ex.ToString
                frmMsgBox.txtMessage.Text = "User Cancelled!"
                frmMsgBox.ShowDialog()
            End Try
        End If
        If cboTemplate.Text = "Employment Information" Then
            Try
                OpenFileDialog1.ShowDialog()
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                txtPath.Text = fpath

                Import()
            Catch ex As Exception
                frmMsgBox.txtMessage.Text = "User Cancelled!"
                frmMsgBox.ShowDialog()
            End Try
        End If
        If cboTemplate.Text = "Bank Information" Then
            Try
                OpenFileDialog1.ShowDialog()
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                txtPath.Text = fpath

                Import()
            Catch ex As Exception
                frmMsgBox.txtMessage.Text = "User Cancelled!"
                frmMsgBox.ShowDialog()
            End Try
        End If
        If cboTemplate.Text = "Upload One Field" Then
            Try
                OpenFileDialog1.ShowDialog()
                filenym = OpenFileDialog1.FileName
                fpath = Path.GetFullPath(filenym)
                txtPath.Text = fpath

                Import()
            Catch ex As Exception
                frmMsgBox.txtMessage.Text = "User Cancelled!"
                frmMsgBox.ShowDialog()
            End Try
        End If
    End Sub

    Private Sub AddSelectButton()
        Dim btnselect As New DataGridViewCheckBoxColumn
        With btnselect
            .Name = "btnSelect"
            .HeaderText = "OK"
        End With
        dgvList.Columns.Add(btnselect)
    End Sub

    Private Sub Check_ErrorRows() 'Added 8/9/14 Vince
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                If dgvList.Item("BirthDate", i).Value Is DBNull.Value Or dgvList.Item("DateHired", i).Value Is DBNull.Value Or dgvList.Item("BODApproval", i).Value Is DBNull.Value Or dgvList.Item("MembershipDate", i).Value Is DBNull.Value Then
                    dgvList.Rows(i).DefaultCellStyle.BackColor = Color.Red
                    dgvList.Rows(i).Cells(0).Value = False
                Else
                    dgvList.Rows(i).DefaultCellStyle.BackColor = Color.SkyBlue
                    dgvList.Rows(i).Cells(0).Value = True
                End If
            Next
        Catch ex As Exception

        End Try
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "Net-informations.com")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        If cboTemplate.Text = "Client Personal Information" Then
            itemCount = 0
            Try
                For xCtr As Integer = 0 To dgvList.RowCount - 1
                    With dgvList
                        Dim empno As String = .Item("ClientNo", xCtr).Value.ToString
                        Dim lastname As String = .Item("LastName", xCtr).Value.ToString
                        Dim firstname As String = .Item("FirstName", xCtr).Value.ToString
                        Dim middlename As String = .Item("MiddleName", xCtr).Value.ToString
                        Dim CivilStatus As String = .Item("CivilStatus", xCtr).Value.ToString
                        Dim Gender As String = .Item("Gender", xCtr).Value.ToString
                        Dim Address As String = .Item("Address", xCtr).Value.ToString
                        Dim BirthDate As Date = .Item("BirthDate", xCtr).Value
                        Dim ContactNo As String = .Item("ContactNo", xCtr).Value.ToString
                        Dim EmailAddress As String = .Item("EmailAddress", xCtr).Value.ToString
                        Dim group As String = .Item("Group", xCtr).Value.ToString
                        Dim subgroup As String = .Item("SubGroup", xCtr).Value.ToString 'Added 8/5/2014 Vince
                        Dim Category As String = .Item("Category", xCtr).Value.ToString
                        Dim type As String = .Item("Type", xCtr).Value.ToString
                        Dim status As String = .Item("Status", xCtr).Value.ToString
                        Dim rank As String = .Item("Rank", xCtr).Value.ToString
                        Dim dateHired As Date = .Item("DateHired", xCtr).Value 'Added 8/5/2014 Vince
                        'Added 7/21/14 by Vincent Nacar ###########################
                        Dim BODApproval As Date = .Item("BODApproval", xCtr).Value
                        Dim MembershipDate As Date = .Item("MembershipDate", xCtr).Value
                        Dim TIN As String = .Item("TIN", xCtr).Value.ToString
                        Dim SSS As String = .Item("SSS", xCtr).Value.ToString
                        Dim PHIC As String = .Item("PHIC", xCtr).Value.ToString
                        Dim HDMF As String = .Item("HDMF", xCtr).Value.ToString
                        Dim provincialAddress As String = .Item("ProvincialAddress", xCtr).Value.ToString 'Added 8/5/2014 Vince

                        SqlHelper.ExecuteNonQuery(con.cnstring, "Client_Data_Upload",
                                                 New SqlParameter("@ClientNo", empno),
                                                 New SqlParameter("@Lastname", lastname),
                                                 New SqlParameter("@Firstname", firstname),
                                                 New SqlParameter("@MiddleName", middlename),
                                                 New SqlParameter("@CivilStatus", CivilStatus),
                                                 New SqlParameter("@Gender", Gender),
                                                 New SqlParameter("@Address", Address),
                                                 New SqlParameter("@Birthdate", BirthDate),
                                                 New SqlParameter("@ContactNo", ContactNo),
                                                 New SqlParameter("@EmailAddress", EmailAddress),
                                                 New SqlParameter("@fcGroup", group),
                                                 New SqlParameter("@fcSubgroup", subgroup),
                                                 New SqlParameter("@fcCategory", Category),
                                                 New SqlParameter("@fcType", type),
                                                 New SqlParameter("@fcStatus", status),
                                                 New SqlParameter("@fcRank", rank),
                                                 New SqlParameter("@Date", dateHired),
                                                 New SqlParameter("@BoardApproval", BODApproval),
                                                 New SqlParameter("@MembershipDate", MembershipDate),
                                                 New SqlParameter("@TIN", TIN),
                                                 New SqlParameter("@SSS", SSS),
                                                 New SqlParameter("@PHIC", PHIC),
                                                 New SqlParameter("@HDMF", HDMF),
                                                 New SqlParameter("@ProvincialAddress", provincialAddress))
                    End With

                    itemCount += 1
                Next

                CleanDummyFiles()

            Catch ex As Exception
                frmMsgBox.txtMessage.Text = "Upload Failed in Client No:" + dgvList.Rows(itemCount).Cells(1).Value.ToString + " " + ex.Message
                frmMsgBox.ShowDialog()
                dgvList.Columns.Clear()
                CleanDummyFiles()
            End Try

            frmMsgBox.txtMessage.Text = "Upload Complete , Total No. of Item(s):" + itemCount.ToString
            frmMsgBox.ShowDialog()
        End If
        If cboTemplate.Text = "Referral / Persons Referred" Then
            Try
                _UploadReferrals()
                frmMsgBox.txtMessage.Text = "Upload Complete."
                frmMsgBox.ShowDialog()
            Catch ex As Exception
                frmMsgBox.txtMessage.Text = ex.Message
                frmMsgBox.ShowDialog()
            End Try
        End If
        If cboTemplate.Text = "Employment Information" Then
            Try
                Upload_EmploymentInfo()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        If cboTemplate.Text = "Bank Information" Then
            Try
                UploadBankAccount()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
        If cboTemplate.Text = "Upload One Field" Then
            Try
                OneFieldUploader()
            Catch ex As Exception
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub CleanDummyFiles()
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Clean_ClientMember_Uselessfiles")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnClear_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClear.Click
        dgvList.Columns.Clear()
    End Sub

#Region "Referrals Upload 11-28-14"

    Private Sub _UploadReferrals()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim referredby As String = .Rows(i).Cells(0).Value.ToString
                    Dim personsreferred As String = .Rows(i).Cells(1).Value.ToString

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Referral_InsertUpdate",
                                               New SqlParameter("@fcReferredBy", referredby),
                                               New SqlParameter("@fcPersonsReferred", personsreferred))
                End With
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Employment Information"
    Private Sub Upload_EmploymentInfo()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim EmployeeID As String = .Rows(i).Cells(0).Value.ToString
                    Dim Bereavement As Boolean = .Rows(i).Cells(1).Value.ToString
                    Dim Employed As Boolean = .Rows(i).Cells(2).Value.ToString
                    Dim Company As String = .Rows(i).Cells(3).Value.ToString
                    Dim CompanyAdd As String = .Rows(i).Cells(4).Value.ToString
                    Dim OfficeNo As String = .Rows(i).Cells(5).Value.ToString
                    Dim LocalNo As String = .Rows(i).Cells(6).Value.ToString
                    Dim Salary As Decimal = .Rows(i).Cells(7).Value.ToString
                    Dim PositionType As String = .Rows(i).Cells(8).Value.ToString
                    Dim PositionTitle As String = .Rows(i).Cells(9).Value.ToString
                    Dim PositionLevel As String = .Rows(i).Cells(10).Value.ToString
                    Dim PayCode As String = .Rows(i).Cells(11).Value.ToString
                    Dim PayrollCode As String = .Rows(i).Cells(12).Value.ToString
                    Dim RateType As String = .Rows(i).Cells(13).Value.ToString
                    Dim TaxCode As String = .Rows(i).Cells(14).Value.ToString
                    'Bank Account
                    Dim BankName As String = .Rows(i).Cells(15).Value.ToString
                    Dim Branch As String = .Rows(i).Cells(16).Value.ToString
                    Dim BankAccount As String = .Rows(i).Cells(17).Value.ToString
                    Dim ATMNo As String = .Rows(i).Cells(18).Value.ToString
                    Dim AccountType As String = .Rows(i).Cells(19).Value.ToString
                    'Beneficiary
                    Dim Beneficiary As String = .Rows(i).Cells(20).Value.ToString
                    Dim BRealtionship As String = .Rows(i).Cells(21).Value.ToString
                    Dim BContact As String = .Rows(i).Cells(22).Value.ToString
                    Dim BAddress As String = .Rows(i).Cells(23).Value.ToString
                    'Relative
                    Dim Relative As String = .Rows(i).Cells(24).Value.ToString
                    Dim RRelationship As String = .Rows(i).Cells(25).Value.ToString
                    Dim RContact As String = .Rows(i).Cells(26).Value.ToString
                    Dim RAddress As String = .Rows(i).Cells(27).Value.ToString
                    'Dependents
                    Dim Dependents As String = .Rows(i).Cells(28).Value.ToString
                    Dim DRelationship As String = .Rows(i).Cells(29).Value.ToString
                    Dim DContact As String = .Rows(i).Cells(30).Value.ToString
                    Dim DAddress As String = .Rows(i).Cells(31).Value.ToString
                    'Source of Income
                    Dim SourceIncome As String = .Rows(i).Cells(32).Value.ToString
                    Dim AnnualIncome As Decimal = .Rows(i).Cells(33).Value.ToString

                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "Client_EmploymentInfo_Upload_Analog",
                                              New SqlParameter("@fcEmployeeNo", EmployeeID),
                                              New SqlParameter("@fbBereavementMemberStatus", Bereavement),
                                              New SqlParameter("@fbEmployed", Employed),
                                              New SqlParameter("@fcCompany", Company),
                                              New SqlParameter("@fcCompanyAddress", CompanyAdd),
                                              New SqlParameter("@fcOfficeNo", OfficeNo),
                                              New SqlParameter("@fcLocalNo", LocalNo),
                                              New SqlParameter("@fdSalary", Salary),
                                              New SqlParameter("@Positiontype", PositionType),
                                              New SqlParameter("@PositionTitle", PositionTitle),
                                              New SqlParameter("@PayCode", PayCode),
                                              New SqlParameter("@PayrollCode", PayrollCode),
                                              New SqlParameter("@RateType", RateType),
                                              New SqlParameter("@TaxCode", TaxCode),
                                              New SqlParameter("@fcBankName", BankName),
                                              New SqlParameter("@fcBranch", Branch),
                                              New SqlParameter("@fcBankAccount", BankAccount),
                                              New SqlParameter("@fcAtmNo", ATMNo),
                                              New SqlParameter("@fcAccountType", AccountType),
                                              New SqlParameter("@fcRelativeName", Beneficiary),
                                              New SqlParameter("@fcRelationship", BRealtionship),
                                              New SqlParameter("@fcContactNo", BContact),
                                              New SqlParameter("@fcAddress", BAddress),
                                              New SqlParameter("@fcNearestRelativeName", Relative),
                                              New SqlParameter("@fcNearRelativeRelation", RRelationship),
                                              New SqlParameter("@fcContact", RContact),
                                              New SqlParameter("@fcNearestRelativeAddress", RAddress),
                                              New SqlParameter("@fcDependentName", Dependents),
                                              New SqlParameter("@fcDependentRelationship", DRelationship),
                                              New SqlParameter("@fcDependentContactNo", DContact),
                                              New SqlParameter("@fcDependentAddress", DAddress),
                                              New SqlParameter("@fcSourceOfIncome", SourceIncome),
                                              New SqlParameter("@fcAnnualIncome", AnnualIncome))
                End With
            Next
            MessageBox.Show("Upload Complete", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

#Region "Bank Information"
    Private Sub UploadBankAccount()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim EmployeeID As String = .Rows(i).Cells(0).Value.ToString
                    Dim BankName As String = .Rows(i).Cells(1).Value.ToString
                    Dim Branch As String = .Rows(i).Cells(2).Value.ToString
                    Dim BankAccount As String = .Rows(i).Cells(3).Value.ToString
                    Dim ATMNo As String = .Rows(i).Cells(4).Value.ToString
                    Dim AccountType As String = .Rows(i).Cells(5).Value.ToString

                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "CIMS_Member_BankAccounts_Add_Analog",
                                              New SqlParameter("@fcEmployeeNo", EmployeeID),
                                              New SqlParameter("@fcBankName", BankName),
                                              New SqlParameter("@fcBranch", Branch),
                                              New SqlParameter("@fcBankAccount", BankAccount),
                                              New SqlParameter("@fcAtmNo", ATMNo),
                                              New SqlParameter("@fcAccountType", AccountType))
                End With
            Next
            MessageBox.Show("Upload Complete", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

#Region "Single Field Uploader"
    Private Sub OneFieldUploader()
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim EmployeeID As String = .Rows(i).Cells(0).Value.ToString
                    Dim Group As String = .Rows(i).Cells(1).Value.ToString

                    SqlHelper.ExecuteNonQuery(con.cnstring, CommandType.StoredProcedure, "SingleData_Upload_Analog",
                          New SqlParameter("@fcEmployeeNo", EmployeeID),
                          New SqlParameter("@fcGroup", Group))
                End With
            Next
            MessageBox.Show("Upload Complete", "Upload", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#End Region

End Class