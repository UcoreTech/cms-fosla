﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Public Class frmInvestmentUploader

    Private con As New Clsappconfiguration

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnBrowse_Click(sender As System.Object, e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            Dim filenym As String = OpenFileDialog1.FileName
            Dim fpath As String = Path.GetFullPath(filenym)
            txtpath.Text = fpath            Import()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Import()
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
        MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
        MyCommand.TableMappings.Add("Table", "CMS")
        DtSet = New System.Data.DataSet
        MyCommand.Fill(DtSet)
        dgvList.DataSource = DtSet.Tables(0)
        MyConnection.Close()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        If MsgBox("Are you sure?", vbYesNo, "Save") = vbYes Then
            Try
                SaveDetails()
                SaveAmortSchedule()
                MsgBox("Save Success.", vbInformation, "Done")
            Catch ex As Exception
                MsgBox(ex.Message)
            End Try
        End If
    End Sub

    Dim rd As SqlDataReader 'declared globally
    Private Sub generateSchedule(ByVal method As String,
                                 ByVal amt As String,
                                 ByVal interest As String,
                                 ByVal nopayment As String,
                                 ByVal dtgranted As String,
                                 ByVal dtstart As String,
                                 ByVal mode As String,
                                 ByVal refno As String)
        Dim intrate As Decimal = GetInterest(mode, CDec(interest))
        Select Case method
            Case "Straight"
                rd = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Straight_AddOn",
                               New SqlParameter("@PrincipalAmount", CDec(amt)),
                               New SqlParameter("@InterestRate", intrate),
                               New SqlParameter("@Terms", nopayment),
                               New SqlParameter("@DateGranted", CDate(dtgranted)),
                               New SqlParameter("@FirstPayment", CDate(dtstart)),
                               New SqlParameter("@xmode", mode))
            Case "Fixed"
                rd = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_FixedMonthly",
                               New SqlParameter("@PrincipalAmount", CDec(amt)),
                               New SqlParameter("@InterestRate", intrate),
                               New SqlParameter("@Terms", nopayment),
                               New SqlParameter("@DateGranted", CDate(dtgranted)),
                               New SqlParameter("@FirstPayment", CDate(dtstart)),
                               New SqlParameter("@xmode", mode))
            Case "Compounded"
                rd = SqlHelper.ExecuteReader(con.cnstring, "Calculate_Amortization_Schedule_Compounded",
                               New SqlParameter("@PrincipalAmount", CDec(amt)),
                               New SqlParameter("@InterestRate", intrate),
                               New SqlParameter("@Terms", nopayment),
                               New SqlParameter("@DateGranted", CDate(dtgranted)),
                               New SqlParameter("@FirstPayment", CDate(dtstart)),
                               New SqlParameter("@xmode", mode))
        End Select
        Dim arr() As String
        If rd.HasRows Then
            While rd.Read
                arr = {refno, rd(0).ToString, rd(1).ToString, rd(2).ToString, rd(3).ToString, rd(4).ToString, rd(5).ToString, rd(6).ToString}
                dgvSchedule.Rows.Add(arr)
            End While
        End If
    End Sub

    Private Function GetInterest(ByVal mode As String, ByVal rate As Decimal) As Decimal
        Try
            Dim interestrate As Decimal
            Select Case mode
                Case "DAILY"
                    interestrate = CDec(rate / 360)
                Case "WEEKLY"
                    interestrate = CDec(rate / 48)
                Case "SEMI-MONTHLY"
                    interestrate = CDec(rate / 24)
                Case "MONTHLY"
                    interestrate = CDec(rate / 12)
                Case "QUARTERLY"
                    interestrate = CDec(rate / 3)
                Case "ANNUALLY"
                    interestrate = CDec(rate / 1)
                Case "LUMP SUM"
                    interestrate = CDec(rate)
            End Select
            Return interestrate
        Catch ex As Exception
            Return 0
        End Try
    End Function

    Private Sub btnGenerateAmort_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerateAmort.Click
        Call prepareCols()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList.Rows(i)
                Call generateSchedule(.Cells(4).Value.ToString,
                                      .Cells(12).Value.ToString,
                                      .Cells(5).Value.ToString,
                                      .Cells(8).Value.ToString,
                                      .Cells(9).Value.ToString,
                                      .Cells(10).Value.ToString,
                                      .Cells(7).Value.ToString,
                                      .Cells(1).Value.ToString)
            End With
        Next
    End Sub

    Private Sub prepareCols()
        dgvSchedule.Rows.Clear()
        dgvSchedule.Columns.Clear()
        With dgvSchedule
            .Columns.Add("fcRefNo", "Ref. No")
            .Columns.Add("fdpayno", "No.")
            .Columns.Add("dtDate", "Date")
            .Columns.Add("fdPrincipal", "Principal")
            .Columns.Add("fdInt", "Interest")
            .Columns.Add("fdServicefee", "Service Fee")
            .Columns.Add("fdAmort", "Amortization")
            .Columns.Add("fdBalance", "Balance")
        End With
    End Sub

    Private Sub SaveAmortSchedule() 'by Vincent Nacar
        For i As Integer = 0 To dgvSchedule.RowCount - 1
            With dgvSchedule.Rows(i)
                Dim loanNo As String = .Cells(0).Value.ToString
                Dim payNo As String = .Cells(1).Value.ToString
                Dim fcDate As String = .Cells(2).Value.ToString
                Dim xprincipal As String = .Cells(3).Value.ToString
                Dim xinterest As String = .Cells(4).Value.ToString
                Dim xservicefee As String = .Cells(5).Value.ToString
                Dim xamortization As String = .Cells(6).Value.ToString
                Dim balance As String = .Cells(7).Value.ToString
                Dim method As String = ""
                Dim status As String = "Unpaid"

                SqlHelper.ExecuteNonQuery(con.cnstring, "_Insert_AmortizationSchedule",
                                           New SqlParameter("@pk_KeyID", System.Guid.NewGuid),
                                           New SqlParameter("@fcLoanNo", loanNo),
                                           New SqlParameter("@fdPayNo", payNo),
                                           New SqlParameter("@fcDate", fcDate),
                                           New SqlParameter("@fdPrincipal", xprincipal),
                                           New SqlParameter("@fdInterest", xinterest),
                                           New SqlParameter("@fdServiceFee", xservicefee),
                                           New SqlParameter("@fdAmortization", xamortization),
                                           New SqlParameter("@fdBalance", balance),
                                           New SqlParameter("@fcMethod", method),
                                           New SqlParameter("@fcStatus", status))
            End With
        Next
    End Sub

    Private Sub SaveDetails()
        For i As Integer = 0 To dgvList.RowCount - 1
            With dgvList.Rows(i)
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Application_Submit_Upload",
                                   New SqlParameter("@empid", .Cells(0).Value.ToString),
                                   New SqlParameter("@fcDocRef", .Cells(1).Value.ToString),
                                   New SqlParameter("@fcInvestmentType", .Cells(2).Value.ToString),
                                   New SqlParameter("@fcClassification", .Cells(3).Value.ToString),
                                   New SqlParameter("@fcInterestMethod", .Cells(4).Value.ToString),
                                   New SqlParameter("@fnTerms", .Cells(6).Value.ToString),
                                   New SqlParameter("@fcModeOfPayment", .Cells(7).Value.ToString),
                                   New SqlParameter("@fnNoPayment", .Cells(8).Value.ToString),
                                   New SqlParameter("@dtTransactionDate", .Cells(9).Value.ToString),
                                   New SqlParameter("@dtFirstPayment", .Cells(10).Value.ToString),
                                   New SqlParameter("@dtMaturity", .Cells(11).Value.ToString),
                                   New SqlParameter("@fcPaymentType", ""),
                                   New SqlParameter("@fcBank", ""),
                                   New SqlParameter("@fcCheckNo", ""),
                                   New SqlParameter("@fdAmount", CDec(.Cells(12).Value.ToString)))
            End With
        Next
    End Sub

    Private Sub btnClear_Click(sender As System.Object, e As System.EventArgs) Handles btnClear.Click
        dgvList.DataSource = Nothing
        dgvList.Rows.Clear()
        dgvList.Columns.Clear()

        dgvSchedule.Rows.Clear()
    End Sub
    'end
End Class