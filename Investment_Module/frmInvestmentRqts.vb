﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmInvestmentRqts

    Private con As New Clsappconfiguration
    Dim ds As DataSet
    Private pkrqts As String

    Private Sub frmInvestmentRqts_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isloaded() Then
            Me.txtInvestment.Text = frmInvestmentSetup.lblInvestmentType.Text
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        dgvList.DataSource = Nothing
        dgvList.Columns.Clear()
        txtInvestment.Text = ""
        txtRqts.Text = ""
        Me.Close()
    End Sub

    Private Function isloaded() As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Rqts_Select",
                                           New SqlParameter("@fkInvestment", GetID()))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function GetID() As String
        Try
            Return frmInvestmentSetup.pkkey
        Catch ex As Exception
            Return "0"
        End Try
    End Function

    Private Function isinserted() As Boolean
        Try
            Dim pkid As New Guid(GetID())
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Rqts_Insert",
                                       New SqlParameter("@fkInvestment", pkid),
                                       New SqlParameter("@fcRequirements", txtRqts.Text))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub txtRqts_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRqts.KeyDown
        If e.KeyCode = Keys.Enter Then
            If isinserted() Then
                If isloaded() Then
                    txtRqts.Text = ""
                End If
            End If
        End If
    End Sub

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        With dgvList
            pkrqts = .SelectedRows(0).Cells(0).Value.ToString
            txtRqts.Text = .SelectedRows(0).Cells(1).Value.ToString
        End With
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If pkrqts <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Rqts_Delete",
                           New SqlParameter("@pkInvestmentRqts", pkrqts))
                If isloaded() Then
                    txtRqts.Text = ""
                    pkrqts = ""
                    MsgBox("Delete Success!", vbInformation, "Success...")
                End If
            Else
                MsgBox("No selected item to delete!", vbInformation, "Delete")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class