﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInvestmentApprover

    Private con As New Clsappconfiguration
    Public fkemployee As String
    Private pkapprover As String
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If btnClose.Text = "Close" Then
            txtInvestmentType.Text = ""
            dgvList.DataSource = Nothing
            dgvList.Columns.Clear()
            Me.Close()
        End If
        If btnClose.Text = "Cancel" Then
            RefreshControls()
            Exit Sub
        End If
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        RefreshControls()
        ResetValues()
    End Sub

    Private Sub RefreshControls()
        btnNew.Text = "New"
        btnEdit.Text = "Edit"
        btnNew.Enabled = True
        btnEdit.Enabled = True
        btnDelete.Enabled = True
        btnClose.Text = "Close"
    End Sub

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If btnNew.Text = "New" Then
            ResetValues()
            btnNew.Text = "Save"
            btnClose.Text = "Cancel"
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            Exit Sub
        End If
        If btnNew.Text = "Save" Then
            If txtIDNo.Text <> "" Then
                If isInserted() Then
                    If isloaded() Then
                        ResetValues()
                        fkemployee = ""
                        btnNew.Text = "New"
                        btnClose.Text = "Close"
                        btnEdit.Enabled = True
                        btnDelete.Enabled = True
                        Exit Sub
                    End If
                Else
                    frmMsgBox.Text = "Something wrong..."
                    frmMsgBox.txtMessage.Text = "Select investment type to continue!"
                    frmMsgBox.ShowDialog()
                End If
            Else
                frmMsgBox.Text = "Insert"
                frmMsgBox.txtMessage.Text = "Oops, Supply the required fields!"
                frmMsgBox.ShowDialog()
            End If
        End If
    End Sub

    Private Function isInserted() As Boolean
        Try
            Dim fkinv As New Guid(GetID())
            Dim fkemp As New Guid(fkemployee)
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Approver_Insert",
                                       New SqlParameter("@fkInvestment", fkinv),
                                       New SqlParameter("@fkEmployee", fkemp),
                                       New SqlParameter("@fbActive", rdActive.Checked))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isUpdated() As Boolean
        Try
            Dim pkapp As New Guid(pkapprover)
            Dim fkemp As New Guid(fkemployee)
            SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Approver_Update",
                           New SqlParameter("@pkApprover", pkapp),
                           New SqlParameter("@fkEmployee", fkemp),
                           New SqlParameter("@fbActive", rdActive.Checked))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            btnEdit.Text = "Update"
            btnClose.Text = "Cancel"
            btnNew.Enabled = False
            btnDelete.Enabled = False
            Exit Sub
        End If
        If btnEdit.Text = "Update" Then
            If txtIDNo.Text <> "" Then
                If isUpdated() Then
                    If isloaded() Then
                        ResetValues()
                        fkemployee = ""
                        btnEdit.Text = "Edit"
                        btnClose.Text = "Close"
                        btnNew.Enabled = True
                        btnDelete.Enabled = True
                        Exit Sub
                    End If
                Else
                    frmMsgBox.Text = "Something wrong..."
                    frmMsgBox.txtMessage.Text = "Select investment type to continue!"
                    frmMsgBox.ShowDialog()
                End If
            Else
                frmMsgBox.Text = "Update"
                frmMsgBox.txtMessage.Text = "Oops, Select item to update!"
                frmMsgBox.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If isDeleted() Then
            If isloaded() Then
                ResetValues()
                frmMsgBox.txtMessage.Text = "Delete Success!"
                frmMsgBox.ShowDialog()
            End If
        End If
    End Sub

    Private Function isDeleted() As Boolean
        Try
            If txtIDNo.Text <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Approve_Delete",
                                           New SqlParameter("@pkApprover", pkapprover))
                Return True
            Else
                frmMsgBox.Text = "Delete"
                frmMsgBox.txtMessage.Text = "Oops, no selected item to be deleted!"
                frmMsgBox.ShowDialog()
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Function isloaded() As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Approver_Select",
                                           New SqlParameter("@fkInvestment", GetID()))
            dgvList.DataSource = ds.Tables(0)
            dgvList.Columns(0).Visible = False
            dgvList.Columns(1).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub frmInvestmentApprover_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isloaded() Then
            txtInvestmentType.Text = frmInvestmentSetup.lblInvestmentType.Text
            ResetValues()
        End If
    End Sub

    Private Sub ResetValues()
        fkemployee = ""
        pkapprover = ""
        txtIDNo.Text = ""
        txtName.Text = ""
        rdActive.Checked = False
    End Sub

    Private Sub btnBrowseClient_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseClient.Click
        frmInvestment_Client.StartPosition = FormStartPosition.CenterScreen
        frmInvestment_Client.ShowDialog()
    End Sub

    Private Function GetID() As String
        Try
            Return frmInvestmentSetup.pkkey
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Private Sub dgvList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvList.Click
        If isSelected() Then
            'other stuff here :D haha
        End If
    End Sub

    Private Function isSelected() As Boolean
        Try
            pkapprover = dgvList.SelectedRows(0).Cells(0).Value.ToString
            fkemployee = dgvList.SelectedRows(0).Cells(1).Value.ToString
            txtIDNo.Text = dgvList.SelectedRows(0).Cells(2).Value.ToString
            txtName.Text = dgvList.SelectedRows(0).Cells(3).Value.ToString
            rdActive.Checked = CBool(dgvList.SelectedRows(0).Cells(4).Value)
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

End Class