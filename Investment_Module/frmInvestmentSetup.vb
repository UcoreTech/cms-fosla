﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmInvestmentSetup

    Private con As New Clsappconfiguration
    Public pkkey As String
    Dim ds As DataSet

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        If btnClose.Text = "Close" Then
            Me.Close()
        End If
        If btnClose.Text = "Cancel" Then
            RefreshControls()
            DisableFields()
            ResetValueFields()
        End If
    End Sub

    Private Sub frmInvestmentSetup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If isLoaded() Then
            DisableFields()
        End If
    End Sub

    Private Sub RefreshControls()
        btnNew.Text = "New"
        btnEdit.Text = "Edit"
        btnNew.Enabled = True
        btnEdit.Enabled = True
        btnDelete.Enabled = True
        btnClose.Text = "Close"
        Exit Sub
    End Sub

    Private Function isLoaded() As Boolean
        Try
            ds = SqlHelper.ExecuteDataset(con.cnstring, "_Investment_Master_Select")
            dgvInvestment.DataSource = ds.Tables(0)
            dgvInvestment.Columns(0).Visible = False
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNew.Click
        If btnNew.Text = "New" Then
            btnNew.Text = "Save"
            btnEdit.Enabled = False
            btnDelete.Enabled = False
            btnClose.Text = "Cancel"
            EnableFields()
            ResetValueFields()
            Exit Sub
        End If
        If btnNew.Text = "Save" Then
            If txtCode.Text <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Master_Insert",
                           New SqlParameter("@fcCode", txtCode.Text),
                           New SqlParameter("@fcInvestmentType", txtInvestmentType.Text),
                           New SqlParameter("@fcDescription", txtDesc.Text),
                           New SqlParameter("@fcInterestType", cboInterestType.Text),
                           New SqlParameter("@fdInterestRate", CDec(txtInterestRate.Text)),
                           New SqlParameter("@fdMaximumAmtLimit", CDec(txtMaximumAmtLimit.Text)),
                           New SqlParameter("@fbApplyMaximum", rdApplyMaximum.Checked))
                If isLoaded() Then
                    DisableFields()
                    btnNew.Text = "New"
                    btnEdit.Enabled = True
                    btnDelete.Enabled = True
                    btnClose.Text = "Close"
                    Exit Sub
                End If
            Else
                frmMsgBox.Text = "Something wrong..."
                frmMsgBox.txtMessage.Text = "Supply the required fields!"
                frmMsgBox.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If btnEdit.Text = "Edit" Then
            btnEdit.Text = "Update"
            btnNew.Enabled = False
            btnDelete.Enabled = False
            btnClose.Text = "Cancel"
            EnableFields()
            Exit Sub
        End If
        If btnEdit.Text = "Update" Then
            If txtCode.Text <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Master_Edit",
                       New SqlParameter("@pkInvestment", pkkey),
                       New SqlParameter("@fcCode", txtCode.Text),
                       New SqlParameter("@fcInvestmentType", txtInvestmentType.Text),
                       New SqlParameter("@fcDescription", txtDesc.Text),
                       New SqlParameter("@fcInterestType", cboInterestType.Text),
                       New SqlParameter("@fdInterestRate", CDec(txtInterestRate.Text)),
                       New SqlParameter("@fdMaximumAmtLimit", CDec(txtMaximumAmtLimit.Text)),
                       New SqlParameter("@fbApplyMaximum", rdApplyMaximum.Checked))
                If isLoaded() Then
                    DisableFields()
                    btnEdit.Text = "Edit"
                    btnNew.Enabled = True
                    btnDelete.Enabled = True
                    btnClose.Text = "Close"
                    Exit Sub
                End If
            Else
                frmMsgBox.Text = "Something wrong..."
                frmMsgBox.txtMessage.Text = "Select item to Update!"
                frmMsgBox.ShowDialog()
            End If
        End If
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        RefreshControls()
        ResetValueFields()
        DisableFields()
        pkkey = ""
    End Sub

    Private Sub DisableFields()
        'txtCode.ReadOnly = True
        'txtInvestmentType.ReadOnly = True
        txtDesc.ReadOnly = True
        txtInterestRate.ReadOnly = True
        txtMaximumAmtLimit.ReadOnly = True
        cboInterestType.Enabled = False
        rdApplyMaximum.Enabled = False
    End Sub
    Private Sub EnableFields()
        'txtCode.ReadOnly = False
        'txtInvestmentType.ReadOnly = False
        txtDesc.ReadOnly = False
        txtInterestRate.ReadOnly = False
        txtMaximumAmtLimit.ReadOnly = False
        cboInterestType.Enabled = True
        rdApplyMaximum.Enabled = True
    End Sub
    Private Sub ResetValueFields()
        txtCode.Text = ""
        txtInvestmentType.Text = ""
        txtDesc.Text = ""
        txtInterestRate.Text = "0.00"
        txtMaximumAmtLimit.Text = "0.00"
        cboInterestType.Text = ""
        rdApplyMaximum.Checked = False
        lblInvestmentType.Text = ""
    End Sub

    Private Sub dgvInvestment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvInvestment.Click
        ViewInvestmentDetails(dgvInvestment.SelectedRows(0).Cells(0).Value.ToString)
        pkkey = dgvInvestment.SelectedRows(0).Cells(0).Value.ToString
    End Sub

    Private Sub ViewInvestmentDetails(ByVal key As String)
        Try
            Dim rd As SqlDataReader
            rd = SqlHelper.ExecuteReader(con.cnstring, "_Investment_Master_Select_byID",
                                          New SqlParameter("@pkInvestment", key))
            While rd.Read
                txtCode.Text = rd(0).ToString
                txtInvestmentType.Text = rd(1).ToString
                txtDesc.Text = rd(2).ToString
                cboInterestType.Text = rd(3).ToString
                txtInterestRate.Text = rd(4).ToString
                txtMaximumAmtLimit.Text = Format(CDec(rd(5).ToString), "##,##0.00")
                rdApplyMaximum.Checked = CType(rd(6), Boolean)
            End While
            lblInvestmentType.Text = txtCode.Text + " - " + txtInvestmentType.Text
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub txtMaximumAmtLimit_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMaximumAmtLimit.KeyDown
        If e.KeyCode = Keys.Back Then
            If txtMaximumAmtLimit.Text = "Out of Range" Then
                txtMaximumAmtLimit.ForeColor = Color.Black
                txtMaximumAmtLimit.Text = "0.00"
            End If
        End If
        If e.KeyCode = Keys.Enter Then
            FormatNumericText()
        End If
    End Sub

    Private Sub txtMaximumAmtLimit_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtMaximumAmtLimit.Validated
        FormatNumericText()
    End Sub

    Private Sub FormatNumericText()
        Try
            txtMaximumAmtLimit.Text = Format(CDec(txtMaximumAmtLimit.Text), "##,##0.00")
        Catch ex As Exception
            txtMaximumAmtLimit.ForeColor = Color.Red
            txtMaximumAmtLimit.Text = "Out of Range"
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            If txtCode.Text <> "" Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_Investment_Master_Delete",
                           New SqlParameter("@pkInvestment", pkkey))
                If isLoaded() Then
                    frmMsgBox.Text = "Delete"
                    frmMsgBox.txtMessage.Text = "Delete Success!"
                    frmMsgBox.ShowDialog()
                    RefreshControls()
                    DisableFields()
                    ResetValueFields()
                End If
            Else
                frmMsgBox.Text = "Oops, Something wrong..."
                frmMsgBox.txtMessage.Text = "No Selected Item to delete!"
                frmMsgBox.ShowDialog()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnBrowseAcct_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowseAcct.Click
        frmCOA.StartPosition = FormStartPosition.CenterScreen
        frmCOA.ShowDialog()
    End Sub

    Private Sub btnTerms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTerms.Click
        frmInvestmentTerms.StartPosition = FormStartPosition.CenterScreen
        frmInvestmentTerms.ShowDialog()
    End Sub

    Private Sub btnRequirements_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRequirements.Click
        frmInvestmentRqts.StartPosition = FormStartPosition.CenterScreen
        frmInvestmentRqts.ShowDialog()
    End Sub

    Private Sub btnApprover_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnApprover.Click
        frmInvestmentApprover.StartPosition = FormStartPosition.CenterScreen
        frmInvestmentApprover.ShowDialog()
    End Sub

End Class