<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayrollSettlement
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.bgwLoadReport = New System.ComponentModel.BackgroundWorker
        Me.picLoading = New System.Windows.Forms.PictureBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.crvPayrollSettlement = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.cboMultiPayrollCode = New MTGCComboBox
        Me.lblPayrollCode = New System.Windows.Forms.Label
        Me.dtePaydate = New System.Windows.Forms.DateTimePicker
        Me.lblPayDate = New System.Windows.Forms.Label
        Me.btnLoadReport = New System.Windows.Forms.Button
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'bgwLoadReport
        '
        '
        'picLoading
        '
        Me.picLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.picLoading.Location = New System.Drawing.Point(484, 21)
        Me.picLoading.Name = "picLoading"
        Me.picLoading.Size = New System.Drawing.Size(29, 28)
        Me.picLoading.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picLoading.TabIndex = 1
        Me.picLoading.TabStop = False
        Me.picLoading.Visible = False
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.picLoading)
        Me.Panel1.Controls.Add(Me.btnLoadReport)
        Me.Panel1.Controls.Add(Me.lblPayDate)
        Me.Panel1.Controls.Add(Me.dtePaydate)
        Me.Panel1.Controls.Add(Me.lblPayrollCode)
        Me.Panel1.Controls.Add(Me.cboMultiPayrollCode)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(743, 57)
        Me.Panel1.TabIndex = 2
        '
        'crvPayrollSettlement
        '
        Me.crvPayrollSettlement.ActiveViewIndex = -1
        Me.crvPayrollSettlement.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crvPayrollSettlement.DisplayGroupTree = False
        Me.crvPayrollSettlement.Dock = System.Windows.Forms.DockStyle.Fill
        Me.crvPayrollSettlement.Location = New System.Drawing.Point(0, 57)
        Me.crvPayrollSettlement.Name = "crvPayrollSettlement"
        Me.crvPayrollSettlement.SelectionFormula = ""
        Me.crvPayrollSettlement.ShowGroupTreeButton = False
        Me.crvPayrollSettlement.Size = New System.Drawing.Size(743, 492)
        Me.crvPayrollSettlement.TabIndex = 0
        Me.crvPayrollSettlement.ViewTimeSelectionFormula = ""
        '
        'cboMultiPayrollCode
        '
        Me.cboMultiPayrollCode.BorderStyle = MTGCComboBox.TipiBordi.FlatXP
        Me.cboMultiPayrollCode.CharacterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.cboMultiPayrollCode.ColumnNum = 2
        Me.cboMultiPayrollCode.ColumnWidth = "50;121"
        Me.cboMultiPayrollCode.DisplayMember = "Text"
        Me.cboMultiPayrollCode.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.cboMultiPayrollCode.DropDownArrowBackColor = System.Drawing.Color.FromArgb(CType(CType(136, Byte), Integer), CType(CType(169, Byte), Integer), CType(CType(223, Byte), Integer))
        Me.cboMultiPayrollCode.DropDownBackColor = System.Drawing.Color.FromArgb(CType(CType(193, Byte), Integer), CType(CType(210, Byte), Integer), CType(CType(238, Byte), Integer))
        Me.cboMultiPayrollCode.DropDownForeColor = System.Drawing.Color.Black
        Me.cboMultiPayrollCode.DropDownStyle = MTGCComboBox.CustomDropDownStyle.DropDown
        Me.cboMultiPayrollCode.GridLineColor = System.Drawing.Color.LightGray
        Me.cboMultiPayrollCode.GridLineHorizontal = False
        Me.cboMultiPayrollCode.GridLineVertical = False
        Me.cboMultiPayrollCode.HighlightBorderColor = System.Drawing.Color.Blue
        Me.cboMultiPayrollCode.HighlightBorderOnMouseEvents = True
        Me.cboMultiPayrollCode.LoadingType = MTGCComboBox.CaricamentoCombo.ComboBoxItem
        Me.cboMultiPayrollCode.Location = New System.Drawing.Point(12, 24)
        Me.cboMultiPayrollCode.ManagingFastMouseMoving = True
        Me.cboMultiPayrollCode.ManagingFastMouseMovingInterval = 30
        Me.cboMultiPayrollCode.Name = "cboMultiPayrollCode"
        Me.cboMultiPayrollCode.NormalBorderColor = System.Drawing.Color.Black
        Me.cboMultiPayrollCode.Size = New System.Drawing.Size(245, 21)
        Me.cboMultiPayrollCode.TabIndex = 0
        '
        'lblPayrollCode
        '
        Me.lblPayrollCode.AutoSize = True
        Me.lblPayrollCode.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayrollCode.Location = New System.Drawing.Point(12, 7)
        Me.lblPayrollCode.Name = "lblPayrollCode"
        Me.lblPayrollCode.Size = New System.Drawing.Size(80, 15)
        Me.lblPayrollCode.TabIndex = 1
        Me.lblPayrollCode.Text = "Payroll Code:"
        '
        'dtePaydate
        '
        Me.dtePaydate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtePaydate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtePaydate.Location = New System.Drawing.Point(263, 23)
        Me.dtePaydate.Name = "dtePaydate"
        Me.dtePaydate.Size = New System.Drawing.Size(134, 23)
        Me.dtePaydate.TabIndex = 2
        '
        'lblPayDate
        '
        Me.lblPayDate.AutoSize = True
        Me.lblPayDate.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPayDate.Location = New System.Drawing.Point(260, 7)
        Me.lblPayDate.Name = "lblPayDate"
        Me.lblPayDate.Size = New System.Drawing.Size(85, 15)
        Me.lblPayDate.TabIndex = 3
        Me.lblPayDate.Text = "Payment Date:"
        '
        'btnLoadReport
        '
        Me.btnLoadReport.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnLoadReport.Location = New System.Drawing.Point(403, 23)
        Me.btnLoadReport.Name = "btnLoadReport"
        Me.btnLoadReport.Size = New System.Drawing.Size(75, 23)
        Me.btnLoadReport.TabIndex = 4
        Me.btnLoadReport.Text = "Preview"
        Me.btnLoadReport.UseVisualStyleBackColor = True
        '
        'frmPayrollSettlement
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.CausesValidation = False
        Me.ClientSize = New System.Drawing.Size(743, 549)
        Me.Controls.Add(Me.crvPayrollSettlement)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "frmPayrollSettlement"
        Me.ShowIcon = False
        Me.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Payroll Settlement Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.picLoading, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bgwLoadReport As System.ComponentModel.BackgroundWorker
    Friend WithEvents picLoading As System.Windows.Forms.PictureBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents crvPayrollSettlement As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents btnLoadReport As System.Windows.Forms.Button
    Friend WithEvents lblPayDate As System.Windows.Forms.Label
    Friend WithEvents dtePaydate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblPayrollCode As System.Windows.Forms.Label
    Friend WithEvents cboMultiPayrollCode As MTGCComboBox
End Class
