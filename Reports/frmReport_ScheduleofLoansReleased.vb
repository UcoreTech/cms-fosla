﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmReport_ScheduleofLoansReleased

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub frmReport_ScheduleofLoansReleased_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        cboLoanTypes.Text = "All"
        LoadLTypes()
        lblfrom.Visible = False
        lblto.Visible = False
        dtDateFrom.Visible = False
        dtDateTo.Visible = False
        lblasof.Visible = False
        dtpDate.Visible = False
    End Sub

    Private Sub LoanReports()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoansReceivableSummary_byDate.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanType", cboLoanTypes.SelectedValue)
            rptsummary.SetParameterValue("@Date", dtpDate.Value)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub
    Private Sub LoanReports_Range()
        Try
            AuditTrail._AuditTrail_Insert("CMS - Loan Report", "View loan type :" + cboLoanTypes.Text)
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoansReceivableSummary_byRange.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@LoanType", cboLoanTypes.SelectedValue)
            rptsummary.SetParameterValue("@dtFrom", dtDateFrom.Value)
            rptsummary.SetParameterValue("@dtTo", dtDateTo.Value)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub LoadLTypes()
        Try
            'Dim rd As SqlDataReader
            'rd = SqlHelper.ExecuteReader(gcon.cnstring, "_LoadLoanTypes_001")
            'While rd.Read
            '    cboLoanTypes.Items.Add(rd(0).ToString)
            'End While
            'cboLoanTypes.Items.Add("All")
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(gcon.cnstring, "_Report_LoadLoanType_withCode")
            ds.Tables(0).Rows.Add("All", "All")
            With cboLoanTypes
                .DataSource = ds.Tables(0)
                .ValueMember = "fcLoanTypeName"
                .DisplayMember = "ltype"
            End With
            cboLoanTypes.Text = "All"
        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        AuditTrail._AuditTrail_Insert("CMS - Loan Report", "View loan type :" + cboLoanTypes.Text + " as of " + dtpDate.Value.ToString("MMMM dd, yyyy"))
        If rdAsof.Checked Then
            LoanReports()
        End If
        If rdByRange.Checked Then
            LoanReports_Range()
        End If
    End Sub

    Private Sub cboLoanTypes_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboLoanTypes.SelectedValueChanged
        'LoanReports()
    End Sub

    Private Sub FilterSelect()
        Try
            If rdAsof.Checked Then
                lblfrom.Visible = False
                lblto.Visible = False
                dtDateFrom.Visible = False
                dtDateTo.Visible = False
                lblasof.Visible = True
                dtpDate.Visible = True
            End If
            If rdByRange.Checked Then
                lblfrom.Visible = True
                lblto.Visible = True
                dtDateFrom.Visible = True
                dtDateTo.Visible = True
                lblasof.Visible = False
                dtpDate.Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub rdByRange_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdByRange.CheckedChanged
        FilterSelect()
    End Sub

    Private Sub rdAsof_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rdAsof.CheckedChanged
        FilterSelect()
    End Sub
End Class