﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReport_ScheduleofLoansReleased
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReport_ScheduleofLoansReleased))
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.PanePanel5 = New WindowsApplication2.PanePanel()
        Me.dtDateTo = New System.Windows.Forms.DateTimePicker()
        Me.lblto = New System.Windows.Forms.Label()
        Me.dtDateFrom = New System.Windows.Forms.DateTimePicker()
        Me.lblfrom = New System.Windows.Forms.Label()
        Me.rdByRange = New System.Windows.Forms.RadioButton()
        Me.rdAsof = New System.Windows.Forms.RadioButton()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.dtpDate = New System.Windows.Forms.DateTimePicker()
        Me.lblasof = New System.Windows.Forms.Label()
        Me.cboLoanTypes = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.PanePanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        'Me.CrvRpt.CachedPageNumberPerDoc = 10
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 61)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(1138, 275)
        Me.CrvRpt.TabIndex = 59
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'PanePanel5
        '
        Me.PanePanel5.BackColor = System.Drawing.Color.White
        Me.PanePanel5.BackgroundImage = Global.WindowsApplication2.My.Resources.Resources.background
        Me.PanePanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PanePanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel5.Controls.Add(Me.dtDateTo)
        Me.PanePanel5.Controls.Add(Me.lblto)
        Me.PanePanel5.Controls.Add(Me.dtDateFrom)
        Me.PanePanel5.Controls.Add(Me.lblfrom)
        Me.PanePanel5.Controls.Add(Me.rdByRange)
        Me.PanePanel5.Controls.Add(Me.rdAsof)
        Me.PanePanel5.Controls.Add(Me.btnRefresh)
        Me.PanePanel5.Controls.Add(Me.dtpDate)
        Me.PanePanel5.Controls.Add(Me.lblasof)
        Me.PanePanel5.Controls.Add(Me.cboLoanTypes)
        Me.PanePanel5.Controls.Add(Me.Label1)
        Me.PanePanel5.Controls.Add(Me.btnClose)
        Me.PanePanel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.PanePanel5.Font = New System.Drawing.Font("Verdana", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PanePanel5.InactiveGradientHighColor = System.Drawing.Color.Transparent
        Me.PanePanel5.InactiveGradientLowColor = System.Drawing.Color.Transparent
        Me.PanePanel5.Location = New System.Drawing.Point(0, 0)
        Me.PanePanel5.Name = "PanePanel5"
        Me.PanePanel5.Size = New System.Drawing.Size(1138, 61)
        Me.PanePanel5.TabIndex = 58
        '
        'dtDateTo
        '
        Me.dtDateTo.CustomFormat = "MMMM dd, yyyy"
        Me.dtDateTo.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDateTo.Location = New System.Drawing.Point(737, 29)
        Me.dtDateTo.Name = "dtDateTo"
        Me.dtDateTo.Size = New System.Drawing.Size(200, 21)
        Me.dtDateTo.TabIndex = 11
        '
        'lblto
        '
        Me.lblto.AutoSize = True
        Me.lblto.BackColor = System.Drawing.Color.Transparent
        Me.lblto.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblto.Location = New System.Drawing.Point(710, 35)
        Me.lblto.Name = "lblto"
        Me.lblto.Size = New System.Drawing.Size(21, 13)
        Me.lblto.TabIndex = 10
        Me.lblto.Text = "To"
        '
        'dtDateFrom
        '
        Me.dtDateFrom.CustomFormat = "MMMM dd, yyyy"
        Me.dtDateFrom.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtDateFrom.Location = New System.Drawing.Point(511, 29)
        Me.dtDateFrom.Name = "dtDateFrom"
        Me.dtDateFrom.Size = New System.Drawing.Size(194, 21)
        Me.dtDateFrom.TabIndex = 9
        '
        'lblfrom
        '
        Me.lblfrom.AutoSize = True
        Me.lblfrom.BackColor = System.Drawing.Color.Transparent
        Me.lblfrom.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfrom.Location = New System.Drawing.Point(469, 35)
        Me.lblfrom.Name = "lblfrom"
        Me.lblfrom.Size = New System.Drawing.Size(40, 13)
        Me.lblfrom.TabIndex = 8
        Me.lblfrom.Text = "From "
        '
        'rdByRange
        '
        Me.rdByRange.AutoSize = True
        Me.rdByRange.BackColor = System.Drawing.Color.Transparent
        Me.rdByRange.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdByRange.Location = New System.Drawing.Point(203, 35)
        Me.rdByRange.Name = "rdByRange"
        Me.rdByRange.Size = New System.Drawing.Size(92, 17)
        Me.rdByRange.TabIndex = 7
        Me.rdByRange.TabStop = True
        Me.rdByRange.Text = "Date Range"
        Me.rdByRange.UseVisualStyleBackColor = False
        '
        'rdAsof
        '
        Me.rdAsof.AutoSize = True
        Me.rdAsof.BackColor = System.Drawing.Color.Transparent
        Me.rdAsof.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rdAsof.Location = New System.Drawing.Point(127, 35)
        Me.rdAsof.Name = "rdAsof"
        Me.rdAsof.Size = New System.Drawing.Size(54, 17)
        Me.rdAsof.TabIndex = 6
        Me.rdAsof.TabStop = True
        Me.rdAsof.Text = "As of"
        Me.rdAsof.UseVisualStyleBackColor = False
        '
        'btnRefresh
        '
        Me.btnRefresh.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Image = CType(resources.GetObject("btnRefresh.Image"), System.Drawing.Image)
        Me.btnRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRefresh.Location = New System.Drawing.Point(962, 0)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(87, 59)
        Me.btnRefresh.TabIndex = 5
        Me.btnRefresh.Text = "Preview"
        Me.btnRefresh.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRefresh.UseVisualStyleBackColor = True
        '
        'dtpDate
        '
        Me.dtpDate.CustomFormat = "MMMM dd, yyyy"
        Me.dtpDate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpDate.Location = New System.Drawing.Point(511, 3)
        Me.dtpDate.Name = "dtpDate"
        Me.dtpDate.Size = New System.Drawing.Size(194, 21)
        Me.dtpDate.TabIndex = 4
        '
        'lblasof
        '
        Me.lblasof.AutoSize = True
        Me.lblasof.BackColor = System.Drawing.Color.Transparent
        Me.lblasof.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblasof.Location = New System.Drawing.Point(469, 9)
        Me.lblasof.Name = "lblasof"
        Me.lblasof.Size = New System.Drawing.Size(36, 13)
        Me.lblasof.TabIndex = 3
        Me.lblasof.Text = "As of"
        '
        'cboLoanTypes
        '
        Me.cboLoanTypes.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboLoanTypes.FormattingEnabled = True
        Me.cboLoanTypes.Location = New System.Drawing.Point(127, 6)
        Me.cboLoanTypes.Name = "cboLoanTypes"
        Me.cboLoanTypes.Size = New System.Drawing.Size(336, 21)
        Me.cboLoanTypes.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.Transparent
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(16, 11)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(105, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Select Loan Type"
        '
        'btnClose
        '
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnClose.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnClose.Location = New System.Drawing.Point(1049, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(87, 59)
        Me.btnClose.TabIndex = 0
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'frmReport_ScheduleofLoansReleased
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1138, 336)
        Me.Controls.Add(Me.CrvRpt)
        Me.Controls.Add(Me.PanePanel5)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmReport_ScheduleofLoansReleased"
        Me.Text = "Schedule of Loans Receivable"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.PanePanel5.ResumeLayout(False)
        Me.PanePanel5.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PanePanel5 As WindowsApplication2.PanePanel
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboLoanTypes As System.Windows.Forms.ComboBox
    Friend WithEvents dtpDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblasof As System.Windows.Forms.Label
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents rdByRange As System.Windows.Forms.RadioButton
    Friend WithEvents rdAsof As System.Windows.Forms.RadioButton
    Friend WithEvents dtDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblto As System.Windows.Forms.Label
    Friend WithEvents dtDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblfrom As System.Windows.Forms.Label
End Class
