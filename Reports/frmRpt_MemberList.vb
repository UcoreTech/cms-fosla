﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmRpt_MemberList

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Public ReportMode As String = ""
#Region "Functions"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function

    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_Sortbyname.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub
#End Region
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        ' LoadReport()
    End Sub

    Private Sub BackgroundWorker1_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles BackgroundWorker1.RunWorkerCompleted
        CrvRpt.Visible = True
        ' CrvRpt.ReportSource = rptsummary
        Label1.Visible = False
    End Sub

    Private Sub FrmAccruedInterestIncomeRpt_ResizeBegin(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeBegin
        Me.Opacity = 0.5
    End Sub

    Private Sub FrmAccruedInterestIncomeRpt_ResizeEnd(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.ResizeEnd
        Me.Opacity = 1
    End Sub

    Private Sub frmRpt_MemberList_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'If ReportMode = "Summary" Then
        '    Me.Text = "Summary Report"
        'Else
        'Me.Text = "Detail Report"

        LoadGroup()
        LoadCategory()
        LoadType()
        LoadRank()
        LoadMemberStatus()
        LoadReport()
        LoadYear()
        LoadMonth()
        LoadDate()
        rdbName.Checked = True
        'End If
        BackgroundWorker1.RunWorkerAsync()
    End Sub

    Private Sub rdbName_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbName.Click
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_Sortbyname.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub rdbCode_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbCode.Click
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_sortbycode.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub LoadCategory()
        cboCategory.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Category_GroupAll")
        While rd.Read
            With cboCategory
                .Items.Add(rd("Fc_Category"))
            End With
        End While
        'cboCategory.Text = "Select"
    End Sub

    Private Sub LoadMemberStatus()
        cboMemberStatus.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "CIMS_m_Membership_Status_view")
        While rd.Read
            With cboMemberStatus
                .Items.Add(rd("Status"))
            End With
        End While
        cboMemberStatus.Text = "Select"
    End Sub

    Private Sub btnGenerate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try 
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@from", Format(dtpfrom.Value, "MM/dd/yyyy"))
            rptsummary.SetParameterValue("@To", Format(dtpTo.Value, "MM/dd/yyyy"))
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub cboCategory_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboCategory.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub LoadType()
        cboType.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Type_ALL")
        While rd.Read
            With cboType
                .Items.Add(rd("Fc_Type"))
            End With
        End While
        'cboType.Text = "Select"
    End Sub

    Private Sub LoadGroup()
        cboGroup.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Group")
        While rd.Read
            With cboGroup
                .Items.Add(rd("Fc_GroupDesc"))
            End With
        End While
        cboGroup.Items.Add("All")
        cboGroup.Text = "Select"
    End Sub

    Private Sub cboGroup_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboGroup.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboType_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboType.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboType.SelectedIndexChanged
        'dummy
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadCategory_WhenGroup_All()
        cboCategory.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Category_GroupAll")
        While rd.Read
            With cboCategory
                .Items.Add(rd("Fc_Category"))
            End With
        End While
    End Sub

    Private Sub LoadRank()
        cboRank.Items.Clear()
        Dim rd As SqlDataReader
        rd = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Rank_Master")
        While rd.Read
            With cboRank
                .Items.Add(rd("Description"))
            End With
        End While
    End Sub

    Private Sub cboMemberStatus_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMemberStatus.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub cboRank_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboRank.SelectedValueChanged
        LoadReport_Groups()
    End Sub

    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        LoadGroup()
        LoadCategory()
        LoadType()
        LoadRank()
        LoadMemberStatus()
        LoadReport()
        LoadYear()
        LoadMonth()
        LoadDate()
        rdbName.Checked = True
    End Sub

    Private Sub LoadYear()
        cboYear.Items.Clear()
        With cboYear
            For i As Integer = 0 To 150
                .Items.Add(Date.Now.Year - i)
            Next
        End With
    End Sub

    Private Sub cboYear_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboYear.SelectedValueChanged
        LoadReport_Flex()
    End Sub

    Private Sub cboDate_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDate.SelectedValueChanged
        LoadReport_Flex()
    End Sub

    Private Sub cboMonth_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboMonth.SelectedValueChanged
        LoadReport_Flex()
    End Sub

    Private Sub LoadDate()
        cboDate.Items.Clear()
        With cboDate
            For i As Integer = 1 To 31
                .Items.Add(i)
            Next
        End With
    End Sub

    Private Sub LoadMonth()
        cboMonth.Items.Clear()
        With cboMonth.Items
            .Add("January")
            .Add("February")
            .Add("March")
            .Add("April")
            .Add("May")
            .Add("June")
            .Add("July")
            .Add("August")
            .Add("September")
            .Add("October")
            .Add("November")
            .Add("December")
        End With
    End Sub

    Public Sub LoadReport_Flex()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_filterBirthdate.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", cboGroup.Text)
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)

            rptsummary.SetParameterValue("@Year", cboYear.Text)
            rptsummary.SetParameterValue("@Month", cboMonth.Text)
            rptsummary.SetParameterValue("@Date", cboDate.Text)

            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Try
            CrvRpt.PrintReport()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub btnExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            CrvRpt.ExportReport()
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Public Sub LoadReport_Groups()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\MemberList_GroupAll.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@xGroup", cboGroup.Text)
            rptsummary.SetParameterValue("@Category", cboCategory.Text)
            rptsummary.SetParameterValue("@Type", cboType.Text)
            rptsummary.SetParameterValue("@Rank", cboRank.Text)
            rptsummary.SetParameterValue("@Status", cboMemberStatus.Text)
            CrvRpt.ReportSource = rptsummary
        Catch ex As Exception
            frmMsgBox.txtMessage.Text = "Error : " + ex.ToString
            frmMsgBox.ShowDialog()
        End Try
    End Sub

    Private Sub chkInclude_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkInclude.CheckedChanged
        Select Case chkInclude.Checked
            Case True
                GroupBox4.Enabled = True
            Case False
                GroupBox4.Enabled = False
        End Select
    End Sub

End Class