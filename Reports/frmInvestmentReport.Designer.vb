﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInvestmentReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.pnlMenu = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnPreview = New System.Windows.Forms.Button()
        Me.rdRange = New System.Windows.Forms.RadioButton()
        Me.rdAsof = New System.Windows.Forms.RadioButton()
        Me.cboInvestment = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer()
        Me.lblfrom = New System.Windows.Forms.Label()
        Me.lblto = New System.Windows.Forms.Label()
        Me.dtFrom = New System.Windows.Forms.DateTimePicker()
        Me.dtTo = New System.Windows.Forms.DateTimePicker()
        Me.pnlMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlMenu
        '
        Me.pnlMenu.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.pnlMenu.Controls.Add(Me.dtTo)
        Me.pnlMenu.Controls.Add(Me.dtFrom)
        Me.pnlMenu.Controls.Add(Me.lblto)
        Me.pnlMenu.Controls.Add(Me.lblfrom)
        Me.pnlMenu.Controls.Add(Me.btnClose)
        Me.pnlMenu.Controls.Add(Me.btnPreview)
        Me.pnlMenu.Controls.Add(Me.rdRange)
        Me.pnlMenu.Controls.Add(Me.rdAsof)
        Me.pnlMenu.Controls.Add(Me.cboInvestment)
        Me.pnlMenu.Controls.Add(Me.Label1)
        Me.pnlMenu.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlMenu.Location = New System.Drawing.Point(0, 0)
        Me.pnlMenu.Name = "pnlMenu"
        Me.pnlMenu.Size = New System.Drawing.Size(1059, 100)
        Me.pnlMenu.TabIndex = 0
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(972, 36)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 37)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnPreview
        '
        Me.btnPreview.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPreview.Location = New System.Drawing.Point(891, 36)
        Me.btnPreview.Name = "btnPreview"
        Me.btnPreview.Size = New System.Drawing.Size(75, 37)
        Me.btnPreview.TabIndex = 4
        Me.btnPreview.Text = "Preview"
        Me.btnPreview.UseVisualStyleBackColor = True
        '
        'rdRange
        '
        Me.rdRange.AutoSize = True
        Me.rdRange.Location = New System.Drawing.Point(258, 65)
        Me.rdRange.Name = "rdRange"
        Me.rdRange.Size = New System.Drawing.Size(73, 17)
        Me.rdRange.TabIndex = 3
        Me.rdRange.TabStop = True
        Me.rdRange.Text = "By Range"
        Me.rdRange.UseVisualStyleBackColor = True
        '
        'rdAsof
        '
        Me.rdAsof.AutoSize = True
        Me.rdAsof.Location = New System.Drawing.Point(137, 65)
        Me.rdAsof.Name = "rdAsof"
        Me.rdAsof.Size = New System.Drawing.Size(55, 17)
        Me.rdAsof.TabIndex = 2
        Me.rdAsof.TabStop = True
        Me.rdAsof.Text = "As of"
        Me.rdAsof.UseVisualStyleBackColor = True
        '
        'cboInvestment
        '
        Me.cboInvestment.FormattingEnabled = True
        Me.cboInvestment.Location = New System.Drawing.Point(137, 33)
        Me.cboInvestment.Name = "cboInvestment"
        Me.cboInvestment.Size = New System.Drawing.Size(268, 21)
        Me.cboInvestment.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(28, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Investment Type : "
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        'Me.CrvRpt.CachedPageNumberPerDoc = 10
        Me.CrvRpt.Cursor = System.Windows.Forms.Cursors.Default
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 100)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(1059, 345)
        Me.CrvRpt.TabIndex = 65
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'lblfrom
        '
        Me.lblfrom.AutoSize = True
        Me.lblfrom.Location = New System.Drawing.Point(424, 36)
        Me.lblfrom.Name = "lblfrom"
        Me.lblfrom.Size = New System.Drawing.Size(43, 13)
        Me.lblfrom.TabIndex = 6
        Me.lblfrom.Text = "From :"
        '
        'lblto
        '
        Me.lblto.AutoSize = True
        Me.lblto.Location = New System.Drawing.Point(437, 67)
        Me.lblto.Name = "lblto"
        Me.lblto.Size = New System.Drawing.Size(31, 13)
        Me.lblto.TabIndex = 7
        Me.lblto.Text = "To :"
        '
        'dtFrom
        '
        Me.dtFrom.CustomFormat = "MMMM dd , yyyy"
        Me.dtFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtFrom.Location = New System.Drawing.Point(473, 30)
        Me.dtFrom.Name = "dtFrom"
        Me.dtFrom.Size = New System.Drawing.Size(200, 20)
        Me.dtFrom.TabIndex = 8
        '
        'dtTo
        '
        Me.dtTo.CustomFormat = "MMMM dd , yyyy"
        Me.dtTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtTo.Location = New System.Drawing.Point(473, 61)
        Me.dtTo.Name = "dtTo"
        Me.dtTo.Size = New System.Drawing.Size(200, 20)
        Me.dtTo.TabIndex = 9
        '
        'frmInvestmentReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1059, 445)
        Me.Controls.Add(Me.CrvRpt)
        Me.Controls.Add(Me.pnlMenu)
        Me.Font = New System.Drawing.Font("Consolas", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmInvestmentReport"
        Me.Text = "Investment Report"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pnlMenu.ResumeLayout(False)
        Me.pnlMenu.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents pnlMenu As System.Windows.Forms.Panel
    Friend WithEvents cboInvestment As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rdRange As System.Windows.Forms.RadioButton
    Friend WithEvents rdAsof As System.Windows.Forms.RadioButton
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnPreview As System.Windows.Forms.Button
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents lblto As System.Windows.Forms.Label
    Friend WithEvents lblfrom As System.Windows.Forms.Label
    Friend WithEvents dtTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtFrom As System.Windows.Forms.DateTimePicker
End Class
