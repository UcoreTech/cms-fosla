<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRpt_DividendsReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRpt_DividendsReport))
        Me.CrvRpt = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.DtDate = New System.Windows.Forms.DateTimePicker
        Me.BtnGenerate = New System.Windows.Forms.Button
        Me.lblLoading = New System.Windows.Forms.Label
        Me.BGWorker = New System.ComponentModel.BackgroundWorker
        Me.SuspendLayout()
        '
        'CrvRpt
        '
        Me.CrvRpt.ActiveViewIndex = -1
        Me.CrvRpt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.CrvRpt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CrvRpt.Location = New System.Drawing.Point(0, 0)
        Me.CrvRpt.Name = "CrvRpt"
        Me.CrvRpt.ReuseParameterValuesOnRefresh = True
        Me.CrvRpt.SelectionFormula = ""
        Me.CrvRpt.ShowCloseButton = False
        Me.CrvRpt.ShowGotoPageButton = False
        Me.CrvRpt.ShowGroupTreeButton = False
        Me.CrvRpt.ShowRefreshButton = False
        Me.CrvRpt.Size = New System.Drawing.Size(873, 545)
        Me.CrvRpt.TabIndex = 0
        Me.CrvRpt.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None
        Me.CrvRpt.ViewTimeSelectionFormula = ""
        '
        'DtDate
        '
        Me.DtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.DtDate.Location = New System.Drawing.Point(425, 3)
        Me.DtDate.Name = "DtDate"
        Me.DtDate.Size = New System.Drawing.Size(101, 23)
        Me.DtDate.TabIndex = 1
        '
        'BtnGenerate
        '
        Me.BtnGenerate.Image = Global.WindowsApplication2.My.Resources.Resources.PrintPreview
        Me.BtnGenerate.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.BtnGenerate.Location = New System.Drawing.Point(532, 3)
        Me.BtnGenerate.Name = "BtnGenerate"
        Me.BtnGenerate.Size = New System.Drawing.Size(88, 23)
        Me.BtnGenerate.TabIndex = 2
        Me.BtnGenerate.Text = "Generate"
        Me.BtnGenerate.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnGenerate.UseVisualStyleBackColor = True
        '
        'lblLoading
        '
        Me.lblLoading.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.lblLoading.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLoading.Image = Global.WindowsApplication2.My.Resources.Resources.LoadingData
        Me.lblLoading.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lblLoading.Location = New System.Drawing.Point(383, 211)
        Me.lblLoading.Name = "lblLoading"
        Me.lblLoading.Size = New System.Drawing.Size(106, 118)
        Me.lblLoading.TabIndex = 3
        Me.lblLoading.Text = "Loading . . ."
        Me.lblLoading.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.lblLoading.Visible = False
        '
        'BGWorker
        '
        '
        'frmDividendsReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(873, 545)
        Me.Controls.Add(Me.lblLoading)
        Me.Controls.Add(Me.BtnGenerate)
        Me.Controls.Add(Me.DtDate)
        Me.Controls.Add(Me.CrvRpt)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(639, 432)
        Me.Name = "frmDividendsReport"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.Text = "Average Capital Share and Dividends"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents CrvRpt As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents DtDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents BtnGenerate As System.Windows.Forms.Button
    Friend WithEvents lblLoading As System.Windows.Forms.Label
    Friend WithEvents BGWorker As System.ComponentModel.BackgroundWorker
End Class
