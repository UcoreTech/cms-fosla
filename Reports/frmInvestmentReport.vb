﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInvestmentReport

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration

    Private opt As String

    Private Sub frmInvestmentReport_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        LoadInvestmentType()
    End Sub

    Private Sub LoadInvestmentType()
        Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "Investment_Master_Select")
        If rd.HasRows Then
            While rd.Read
                cboInvestment.Items.Add(rd(0))
            End While
        End If
        cboInvestment.Items.Add("All")
        cboInvestment.Text = "All"
    End Sub

#Region "Config"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If

        Dim obj As ReportObject

        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function
    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        ' for each table apply connection info
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            ' check if logon was successful
            ' if TestConnectivity returns false,
            ' check logon credentials
            If (tbl.TestConnectivity()) Then
                'drop fully qualified table location
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next

        Return True
    End Function
#End Region

    Private Sub LoadReports()
        Try
            AuditTrail._AuditTrail_Insert("CMS - Investment Report", "View investment type :" + cboInvestment.Text)
            rptsummary.Load(Application.StartupPath & "\LoanReport\InvestmentReport.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()
            rptsummary.SetParameterValue("@opt", Me.opt)
            rptsummary.SetParameterValue("@dtfrom", dtFrom.Text)
            rptsummary.SetParameterValue("@dtto", dtTo.Text)
            rptsummary.SetParameterValue("@invtype", cboInvestment.Text)
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        CrvRpt.ReportSource = rptsummary
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnPreview_Click(sender As System.Object, e As System.EventArgs) Handles btnPreview.Click
        LoadReports()
    End Sub

    Private Sub dtOptionChange() Handles rdAsof.CheckedChanged, rdRange.CheckedChanged
        If rdAsof.Checked Then
            lblfrom.Text = "As of:"
            lblto.Visible = False
            dtTo.Visible = False
            Me.opt = "AO"
        End If
        If rdRange.Checked Then
            lblfrom.Text = "From :"
            lblto.Visible = True
            dtTo.Visible = True
            Me.opt = "R"
        End If
    End Sub

End Class