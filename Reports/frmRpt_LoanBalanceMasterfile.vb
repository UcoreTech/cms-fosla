Imports system.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmRpt_LoanBalanceMasterfile

    Private rptsummary As New ReportDocument
    Private gcon As New Clsappconfiguration
    Private xKeyName, xKeyLoanType, xLoanNo As String
    Private xDateFrom, xDateTo As String
    Private xFilterMode As String

    Private Sub frmLoanBalanceMasterfile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        CboFilter.SelectedIndex = 0
    End Sub
    Private Sub LoadNames()
        Dim DTNames As New DataTable("Names")
        Dim Dr As DataRow
        Dim xSelected As String = ""
        Dim sSqlCmd As String = "CIMS_LoanManager_GetMemberList"

        With DTNames.Columns
            .Add("Name")
            .Add("PKey")
        End With

        Dr = DTNames.NewRow()
        Dr("Name") = "All members"
        Dr("PKey") = ""
        DTNames.Rows.Add(Dr)

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    If xSelected = "" Then
                        xSelected = rd.Item("Full Name").ToString
                    End If
                    Dr = DTNames.NewRow()
                    Dr("Name") = rd.Item("Full Name").ToString
                    Dr("PKey") = rd.Item("pk_Employee").ToString
                    DTNames.Rows.Add(Dr)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadNames in frmLoanBalanceMasterfile module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try

        MtcboFilter.Items.Clear()
        MtcboFilter.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtcboFilter.SourceDataString = New String(1) {"Name", "PKey"}
        MtcboFilter.SourceDataTable = DTNames
        MtcboFilter.SelectedIndex = 0
    End Sub
    Private Sub LoadLoanType()
        Dim DTLoanType As New DataTable("LoanType")
        Dim DR As DataRow
        Dim xSelected As String = ""
        Dim sSqlCmd As String = "select fcLoanTypeName, pk_LoanType from dbo.CIMS_m_Loans_LoanType"

        DTLoanType.Columns.Add("fcLoanTypeName")
        DTLoanType.Columns.Add("pk_LoanType")

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.Text, sSqlCmd)
                While rd.Read
                    If xSelected = "" Then
                        xSelected = rd.Item("fcLoanTypeName").ToString
                    End If
                    DR = DTLoanType.NewRow()
                    DR("fcLoanTypeName") = rd.Item("fcLoanTypeName").ToString
                    DR("pk_LoanType") = rd.Item("pk_LoanType").ToString
                    DTLoanType.Rows.Add(DR)
                End While
            End Using
        Catch ex As Exception
            MsgBox("Error at LoadNames in frmLoanBalanceMasterfile module." & vbCr & vbCr & ex.Message, MsgBoxStyle.Critical + MsgBoxStyle.OkOnly, "Error occur!")
        End Try
        MtcboFilter.Items.Clear()
        MtcboFilter.SelectedValue = xSelected
        MtcboFilter.LoadingType = MTGCComboBox.CaricamentoCombo.DataTable
        MtcboFilter.SourceDataString = New String(1) {"fcLoanTypeName", "pk_LoanType"}
        MtcboFilter.SourceDataTable = DTLoanType
        MtcboFilter.SelectedIndex = 0
    End Sub
    Private Sub LoadReport()
        Try
            rptsummary.Load(Application.StartupPath & "\LoanReport\LoanBalanceMaster.rpt")
            Logon(rptsummary, gcon.Server, gcon.Database, gcon.Username, gcon.Password)
            rptsummary.Refresh()

            rptsummary.SetParameterValue("@fxKeyMember", xKeyName)
            rptsummary.SetParameterValue("@fcLoanNo", xLoanNo)
            rptsummary.SetParameterValue("@fxKeyLoanType", xKeyLoanType)
            rptsummary.SetParameterValue("@fcDateFrom", xDateFrom)
            rptsummary.SetParameterValue("@fcDateTo", xDateTo)

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
#Region "Default Report Process"
    Function Logon(ByVal cr As ReportDocument, ByVal server As String, ByVal db As String, ByVal id As String, ByVal pass As String) As Boolean
        Dim ci As New ConnectionInfo
        Dim subObj As SubreportObject
        ci.ServerName = server
        ci.DatabaseName = db
        ci.UserID = id
        ci.Password = pass
        If Not (ApplyLogon(cr, ci)) Then
            Return False
        End If
        Dim obj As ReportObject
        For Each obj In cr.ReportDefinition.ReportObjects()
            If (obj.Kind = ReportObjectKind.SubreportObject) Then
                subObj = CType(obj, SubreportObject)
                If Not (ApplyLogon(cr.OpenSubreport(subObj.SubreportName), ci)) Then
                    Return False
                End If
            End If
        Next
        cr.Refresh()
        Return True
    End Function

    Function ApplyLogon(ByVal cr As ReportDocument, ByVal ci As ConnectionInfo) As Boolean
        Dim li As TableLogOnInfo
        Dim tbl As Table
        For Each tbl In cr.Database.Tables
            li = tbl.LogOnInfo
            li.ConnectionInfo = ci
            tbl.ApplyLogOnInfo(li)
            If (tbl.TestConnectivity()) Then
                If (tbl.Location.IndexOf(".") > 0) Then
                    tbl.Location = tbl.Location.Substring(tbl.Location.LastIndexOf(".") + 1)
                Else
                    tbl.Location = tbl.Location
                End If
            Else
                Return False
            End If
        Next
        Return True
    End Function
#End Region
    Private Sub bgwReportProcess_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles bgwReportProcess.DoWork
        Call LoadReport()
    End Sub
    Private Sub frmLoanBalanceMasterfile_FormClosed(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        rptsummary.Close()
    End Sub
    Private Sub bgwReportProcess_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles bgwReportProcess.RunWorkerCompleted
        Me.crvLoanBalanceMaster.Visible = True
        Me.crvLoanBalanceMaster.ReportSource = rptsummary

        lblLoading.Visible = False
    End Sub

    Private Sub btnDisplayReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisplayReport.Click
        Select Case CboFilter.Text

            Case "Customer Name"
                lblLoading.Visible = True
                If MtcboFilter.SelectedItem.Col1 = "All members" Then
                    xKeyName = "00000000-0000-0000-0000-000000000000"
                Else
                    xKeyName = MtcboFilter.SelectedItem.Col2
                End If
                xLoanNo = Nothing
                xKeyLoanType = Nothing
                xDateFrom = Nothing
                xDateTo = Nothing
                bgwReportProcess.RunWorkerAsync()

            Case "Loan Number"
                If txtFilter.Text <> "" Then
                    lblLoading.Visible = True
                    xLoanNo = txtFilter.Text
                    xKeyName = Nothing
                    xKeyLoanType = Nothing
                    xDateFrom = Nothing
                    xDateTo = Nothing
                    bgwReportProcess.RunWorkerAsync()
                Else
                    MessageBox.Show("User Error! Please provide loan number first.", "User Error!", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If

            Case "Loan Type"
                lblLoading.Visible = True
                xKeyLoanType = MtcboFilter.SelectedItem.Col2
                xLoanNo = Nothing
                xKeyName = Nothing
                xDateFrom = Nothing
                xDateTo = Nothing
                bgwReportProcess.RunWorkerAsync()

            Case "Date Range"
                lblLoading.Visible = True
                xDateFrom = DTFrom.Value.Date
                xDateTo = DTTo.Value.Date
                xLoanNo = Nothing
                xKeyLoanType = Nothing
                xKeyName = Nothing
                bgwReportProcess.RunWorkerAsync()

            Case "As of Date"
                lblLoading.Visible = True
                xLoanNo = Nothing
                xKeyLoanType = Nothing
                xKeyName = Nothing
                xDateFrom = DTFrom.Value.Date
                xDateTo = Nothing






                bgwReportProcess.RunWorkerAsync()

        End Select
    End Sub

    Private Sub CboFilter_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CboFilter.SelectedIndexChanged
        Select Case CboFilter.Text
            Case "Customer Name"
                LblFilter.Text = "Name:"
                txtFilter.Visible = False
                MtcboFilter.Visible = True
                DTFrom.Visible = False
                DTTo.Visible = False
                lblTo.Visible = False
                LoadNames()

            Case "Loan Number"
                LblFilter.Text = "Loan No:"
                txtFilter.Visible = True
                MtcboFilter.Visible = False
                DTFrom.Visible = False
                DTTo.Visible = False
                lblTo.Visible = False
                txtFilter.Text = ""

            Case "Loan Type"
                LblFilter.Text = "Type:"
                txtFilter.Visible = False
                MtcboFilter.Visible = True
                DTFrom.Visible = False
                DTTo.Visible = False
                lblTo.Visible = False
                LoadLoanType()

            Case "Date Range"
                LblFilter.Text = "From:"
                txtFilter.Visible = False
                MtcboFilter.Visible = False
                DTFrom.Visible = True
                DTTo.Visible = True
                lblTo.Visible = True

            Case "As of Date"
                LblFilter.Text = "As of Date:"
                txtFilter.Visible = False
                MtcboFilter.Visible = False
                DTFrom.Visible = True
                DTTo.Visible = False
                lblTo.Visible = False

        End Select
        xFilterMode = CboFilter.Text
    End Sub
End Class