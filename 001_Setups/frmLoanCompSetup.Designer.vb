﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanCompSetup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanCompSetup))
        Me.btnApply = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.btnNew = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.FormName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FPath = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnBrowse = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.rdCalendar = New System.Windows.Forms.RadioButton()
        Me.rdExact = New System.Windows.Forms.RadioButton()
        Me.rdSalary = New System.Windows.Forms.RadioButton()
        Me.rdDirect = New System.Windows.Forms.RadioButton()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnApply
        '
        Me.btnApply.Image = CType(resources.GetObject("btnApply.Image"), System.Drawing.Image)
        Me.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApply.Location = New System.Drawing.Point(25, 181)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 4
        Me.btnApply.Text = "Apply"
        Me.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Image = CType(resources.GetObject("Button2.Image"), System.Drawing.Image)
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button2.Location = New System.Drawing.Point(106, 181)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 5
        Me.Button2.Text = "Close"
        Me.Button2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(25, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(380, 63)
        Me.GroupBox1.TabIndex = 6
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Exact Payment Option"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(194, 28)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 21)
        Me.TextBox1.TabIndex = 1
        Me.TextBox1.Text = "0"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(78, 31)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(110, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Set Count (Day) :"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnTest)
        Me.GroupBox2.Controls.Add(Me.btnNew)
        Me.GroupBox2.Controls.Add(Me.btnSave)
        Me.GroupBox2.Controls.Add(Me.dgvList)
        Me.GroupBox2.Location = New System.Drawing.Point(25, 81)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(580, 10)
        Me.GroupBox2.TabIndex = 7
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Printing Forms"
        Me.GroupBox2.Visible = False
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(173, 226)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(75, 23)
        Me.btnTest.TabIndex = 3
        Me.btnTest.Text = "Test"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'btnNew
        '
        Me.btnNew.Location = New System.Drawing.Point(11, 226)
        Me.btnNew.Name = "btnNew"
        Me.btnNew.Size = New System.Drawing.Size(75, 23)
        Me.btnNew.TabIndex = 2
        Me.btnNew.Text = "Add"
        Me.btnNew.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(92, 226)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.BackgroundColor = System.Drawing.Color.White
        Me.dgvList.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.FormName, Me.FPath, Me.btnBrowse})
        Me.dgvList.Location = New System.Drawing.Point(6, 20)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.Size = New System.Drawing.Size(568, 203)
        Me.dgvList.TabIndex = 0
        '
        'FormName
        '
        Me.FormName.HeaderText = "Form Name"
        Me.FormName.Name = "FormName"
        Me.FormName.Width = 200
        '
        'FPath
        '
        Me.FPath.HeaderText = "File Path"
        Me.FPath.Name = "FPath"
        Me.FPath.ReadOnly = True
        Me.FPath.Width = 220
        '
        'btnBrowse
        '
        Me.btnBrowse.HeaderText = "Select File"
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.ReadOnly = True
        Me.btnBrowse.Text = "Browse File"
        Me.btnBrowse.UseColumnTextForButtonValue = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.rdCalendar)
        Me.GroupBox3.Controls.Add(Me.rdExact)
        Me.GroupBox3.Controls.Add(Me.rdSalary)
        Me.GroupBox3.Controls.Add(Me.rdDirect)
        Me.GroupBox3.Location = New System.Drawing.Point(25, 81)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(380, 94)
        Me.GroupBox3.TabIndex = 8
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Set Default Amortization Payment Date Option"
        '
        'rdCalendar
        '
        Me.rdCalendar.AutoSize = True
        Me.rdCalendar.Location = New System.Drawing.Point(183, 33)
        Me.rdCalendar.Name = "rdCalendar"
        Me.rdCalendar.Size = New System.Drawing.Size(102, 17)
        Me.rdCalendar.TabIndex = 3
        Me.rdCalendar.TabStop = True
        Me.rdCalendar.Text = "Pay Calendar"
        Me.rdCalendar.UseVisualStyleBackColor = True
        '
        'rdExact
        '
        Me.rdExact.AutoSize = True
        Me.rdExact.Location = New System.Drawing.Point(183, 62)
        Me.rdExact.Name = "rdExact"
        Me.rdExact.Size = New System.Drawing.Size(56, 17)
        Me.rdExact.TabIndex = 2
        Me.rdExact.TabStop = True
        Me.rdExact.Text = "Exact"
        Me.rdExact.UseVisualStyleBackColor = True
        '
        'rdSalary
        '
        Me.rdSalary.AutoSize = True
        Me.rdSalary.Location = New System.Drawing.Point(24, 62)
        Me.rdSalary.Name = "rdSalary"
        Me.rdSalary.Size = New System.Drawing.Size(123, 17)
        Me.rdSalary.TabIndex = 1
        Me.rdSalary.TabStop = True
        Me.rdSalary.Text = "Salary Deduction"
        Me.rdSalary.UseVisualStyleBackColor = True
        '
        'rdDirect
        '
        Me.rdDirect.AutoSize = True
        Me.rdDirect.Location = New System.Drawing.Point(24, 33)
        Me.rdDirect.Name = "rdDirect"
        Me.rdDirect.Size = New System.Drawing.Size(113, 17)
        Me.rdDirect.TabIndex = 0
        Me.rdDirect.TabStop = True
        Me.rdDirect.Text = "Direct Payment"
        Me.rdDirect.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.HeaderText = "Form Name"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        Me.DataGridViewTextBoxColumn1.Width = 200
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.HeaderText = "File Path"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 220
        '
        'frmLoanCompSetup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(426, 213)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btnApply)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmLoanCompSetup"
        Me.Text = "Loan Application Setting"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents rdCalendar As System.Windows.Forms.RadioButton
    Friend WithEvents rdExact As System.Windows.Forms.RadioButton
    Friend WithEvents rdSalary As System.Windows.Forms.RadioButton
    Friend WithEvents rdDirect As System.Windows.Forms.RadioButton
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnNew As System.Windows.Forms.Button
    Friend WithEvents FormName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FPath As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnBrowse As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnTest As System.Windows.Forms.Button
End Class
