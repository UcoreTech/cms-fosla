﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmChargesRates
    'vince
    Public loancode As String
    Private con As New Clsappconfiguration
    Private fkaccount As String

    Private Sub frmChargesRates_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Call loadrates()
        Call loadaccounts()
    End Sub

    Private Sub loadrates()
        dgvlist.Rows.Clear()
        dgvlist.Columns.Clear()
        dgvlist.Columns.Add("fdterm", "Term")
        dgvlist.Columns.Add("fdRate", "Rate")
    End Sub

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub loadaccounts()
        cboAccount.DataSource = Nothing
        cboAccount.Items.Clear()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "RateTable_SelectAccounts",
                                                          New SqlParameter("@loanid", loancode))
        If ds.Tables(0).Rows.Count <> 0 Then
            With cboAccount
                .DataSource = ds.Tables(0)
                .ValueMember = "fk_Account"
                .DisplayMember = "Description"
            End With
        End If
    End Sub

    Private Sub getfk_Account() Handles cboAccount.TextChanged
        Try
            fkaccount = cboAccount.SelectedValue.ToString
            'MsgBox(fkaccount)
            If fkaccount <> "" Then
                LoadexistRates()
            End If
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        For i As Integer = 0 To dgvlist.RowCount - 1
            With dgvlist.Rows(i)
                Dim terms As Integer = CInt(.Cells(0).Value)
                Dim rate As Decimal = CDec(.Cells(1).Value)
                If terms <> 0 And rate <> 0 Then
                    Call SaveChanges(fkaccount, loancode, terms, rate)
                End If
            End With
        Next
        MsgBox("Save Success.", vbInformation, "")
    End Sub

    Private Sub SaveChanges(ByVal fkaccnt As String,
                            ByVal fkloantype As String,
                            ByVal fdterms As Integer,
                            ByVal fdrate As Decimal)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "RateTable_InsertUpdate",
                                      New SqlParameter("@fk_account", fkaccnt),
                                      New SqlParameter("@fk_LoantypeID", fkloantype),
                                      New SqlParameter("@fdTerms", fdterms),
                                      New SqlParameter("@fdRate", fdrate))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub LoadexistRates()
        Try
            dgvlist.Rows.Clear()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "RateTable_SelectRate",
                                                              New SqlParameter("@fk_account", fkaccount),
                                                              New SqlParameter("@fk_LoantypeID", loancode))
            If rd.HasRows Then
                While rd.Read
                    dgvlist.Rows.Add(rd(0), rd(1))
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnRemoverow_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoverow.Click
        Dim terms As Integer = CInt(dgvlist.CurrentRow.Cells(0).Value)
        Dim rate As Decimal = CDec(dgvlist.CurrentRow.Cells(1).Value)
        If MsgBox("Remove row with terms :" + terms.ToString + " and rate :" + rate.ToString, vbYesNo + vbQuestion, "Remove") = vbYes Then
            Call DeleteRow(fkaccount, loancode, terms)
            dgvlist.Rows.Remove(dgvlist.CurrentRow)
        End If
    End Sub

    Private Sub DeleteRow(ByVal fkaccnt As String,
                          ByVal fkloantype As String,
                         ByVal terms As Integer)
        Try
            SqlHelper.ExecuteNonQuery(con.cnstring, "RateTable_Delete",
                                      New SqlParameter("@fk_account", fkaccnt),
                                      New SqlParameter("@fk_LoantypeID", fkloantype),
                                      New SqlParameter("@fdTerms", terms))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class