﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoanNoSetup

    Private con As New Clsappconfiguration

    Private Sub frmLoanNoSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        _LoadSetup()
    End Sub

    Private Sub _LoadSetup()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_tblLoanNumberSetup_Select")
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add(rd(0), rd(1), rd(2), rd(3), rd(4))
                    End With
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList.Rows(i)
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_tblLoanNumberSetup_Update",
                                               New SqlParameter("@fcLoanType", .Cells(0).Value.ToString),
                                               New SqlParameter("@fcPrefix", .Cells(1).Value.ToString),
                                               New SqlParameter("@fbSequential", .Cells(2).Value),
                                               New SqlParameter("@fbSelectable", .Cells(3).Value),
                                               New SqlParameter("@fbDefault", .Cells(4).Value))
                End With
            Next
            MsgBox("Update Success.", vbInformation, "...")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class