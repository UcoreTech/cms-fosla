﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmInterestMethod

    Private con As New Clsappconfiguration

    Private Sub frmInterestMethod_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        _LoadSetups()
    End Sub

    Private Sub _LoadSetups()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_tblIntMethodSetup_Select")
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add(rd(0), rd(1))
                    End With
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList.Rows(i)
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_tblIntMethodSetup_Update",
                           New SqlParameter("@fcIntMethod", .Cells(0).Value.ToString),
                           New SqlParameter("@fbVisible", .Cells(1).Value))
                End With
            Next
            MsgBox("Update Success.", vbInformation, "...")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class