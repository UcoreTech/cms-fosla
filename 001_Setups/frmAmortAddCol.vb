﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmortAddCol

    Private con As New Clsappconfiguration

    Private Sub frmAmortAddCol_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        _GEtAcct("")
    End Sub
    Private Sub _GEtAcct(ByVal key As String)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(con.cnstring, "_tblAmortizationColumnsSetup_GetAcct",
                                                          New SqlParameter("@key", key))
            dgvList.DataSource = ds.Tables(0)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub TextBox1_TextChanged(sender As System.Object, e As System.EventArgs) Handles TextBox1.TextChanged
        _GEtAcct(TextBox1.Text)
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        frmAmort_ColSetup._AddItem(dgvList.CurrentRow.Cells(0).Value.ToString)
        Me.Close()
    End Sub
End Class