﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmAmort_ColSetup

    Private con As New Clsappconfiguration
    Private mIndex As Integer

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Me.Close()
    End Sub

    Private Sub _LoadDefCol()
        mIndex = 0
        dgvList.Rows.Clear()
        dgvList.Columns.Clear()
        Dim ccode As New DataGridViewTextBoxColumn
        Dim sname As New DataGridViewTextBoxColumn
        Dim acnt As New DataGridViewTextBoxColumn
        Dim defamt As New DataGridViewTextBoxColumn
        Dim chkUsedSched As New DataGridViewCheckBoxColumn
        Dim chkActive As New DataGridViewCheckBoxColumn
        With ccode
            .Name = "ccode"
            .Visible = False
        End With
        With sname
            .Name = "sname"
            .HeaderText = "Short Name"
            .Width = 150
        End With
        With acnt
            .Name = "acnt"
            .HeaderText = "Account Title"
            .Width = 250
            .ReadOnly = True
        End With
        With defamt
            .Name = "defamt"
            .HeaderText = "Default Amount"
            .DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
        With chkUsedSched
            .Name = "chkUsedSched"
            .HeaderText = "Use Schedule Amount"
        End With
        With chkActive
            .Name = "chkActive"
            .HeaderText = "Active"
        End With
        With dgvList
            .Columns.Add(ccode)
            .Columns.Add(sname)
            .Columns.Add(acnt)
            .Columns.Add(defamt)
            .Columns.Add(chkUsedSched)
            .Columns.Add(chkActive)
        End With
    End Sub

    Private Sub frmAmort_ColSetup_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        _LoadData()
    End Sub

    Private Sub btnAddCol_Click(sender As System.Object, e As System.EventArgs) Handles btnAddCol.Click
        frmAmortAddCol.StartPosition = FormStartPosition.CenterScreen
        frmAmortAddCol.ShowDialog()
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim colcode As String = .Rows(i).Cells(0).Value.ToString
                    Dim sname As String = .Rows(i).Cells(1).Value.ToString
                    Dim acnt As String = .Rows(i).Cells(2).Value.ToString
                    Dim defamt As String = .Rows(i).Cells(3).Value.ToString
                    Dim chkused As Boolean = .Rows(i).Cells(4).Value
                    Dim isActive As Boolean = .Rows(i).Cells(5).Value
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_tblAmortizationColumnsSetup_Insert",
                                               New SqlParameter("@fcColCode", colcode),
                                               New SqlParameter("@fcShortName", sname),
                                               New SqlParameter("@fcAccountDesc", acnt),
                                               New SqlParameter("@fdDefAmount", defamt),
                                               New SqlParameter("@fbUsedSchedule", chkused),
                                               New SqlParameter("@fbActive", isActive))
                End With
            Next
            MsgBox("Save Success.", vbInformation, "...")
            _LoadData()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnRemoveCol_Click(sender As System.Object, e As System.EventArgs) Handles btnRemoveCol.Click
        Try
            If MsgBox("Delete row:" + dgvList.CurrentRow.Cells(1).Value.ToString + " ?", vbYesNo, "Are you sure?") = vbYes Then
                SqlHelper.ExecuteNonQuery(con.cnstring, "_tblAmortizationColumnsSetup_Delete",
                                           New SqlParameter("@fcColCode", dgvList.CurrentRow.Cells(0).Value.ToString))
                dgvList.Rows.Remove(dgvList.CurrentRow)
                mIndex -= 1
            Else
                Exit Sub
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub _AddItem(ByVal acnttitle As String)
        Try
            With dgvList
                .Rows.Add()
                .Item(0, mIndex).Value = ""
                .Item(1, mIndex).Value = ""
                .Item(2, mIndex).Value = acnttitle
                .Item(3, mIndex).Value = "0.00"
                .Item(4, mIndex).Value = False
                .Item(5, mIndex).Value = False
                mIndex += 1
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub _LoadData()
        Try
            _LoadDefCol()
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_tblAmortizationColumnsSetup_Load")
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add()
                        .Item(0, mIndex).Value = rd(0).ToString
                        .Item(1, mIndex).Value = rd(1).ToString
                        .Item(2, mIndex).Value = rd(2).ToString
                        .Item(3, mIndex).Value = rd(3).ToString
                        .Item(4, mIndex).Value = rd(4)
                        .Item(5, mIndex).Value = rd(5)
                        mIndex += 1
                    End With
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class