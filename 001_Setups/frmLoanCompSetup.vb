﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmLoanCompSetup

    Private con As New Clsappconfiguration

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub frmLoanCompSetup_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        _LoadDataForms()
        _LoadDefaultPayMode()
    End Sub

#Region "PRinting forms"

    Private Sub _LoadDataForms()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_tblLoanForms_Select")
            If rd.HasRows Then
                While rd.Read
                    With dgvList
                        .Rows.Add(rd(0), rd(1))
                    End With
                End While
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

    Private Sub dgvList_CellContentClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellContentClick
        Dim dr As DialogResult
        If e.ColumnIndex = 2 Then
            OpenFileDialog1.Filter = "rpt Files (*.rpt) |*.rpt;*.rpt|(*.rpt) |*.rpt|(*.*) |*.*"
            If OpenFileDialog1.ShowDialog() = dr.OK Then
                dgvList.CurrentRow.Cells(1).Value = OpenFileDialog1.FileName.ToString
            End If
        End If
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Try
            For i As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_tblLoanForms_Insert",
                                               New SqlParameter("@fcFormName", .Rows(i).Cells(0).Value.ToString),
                                               New SqlParameter("@fcFilePath", .Rows(i).Cells(1).Value.ToString))
                End With
            Next
            MsgBox("Save Successfull.", vbInformation, "...")
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub btnTest_Click(sender As System.Object, e As System.EventArgs) Handles btnTest.Click
        frmReport_LoanApplication._LoadReport(dgvList.CurrentRow.Cells(1).Value.ToString, "0")
        frmReport_LoanApplication.WindowState = FormWindowState.Maximized
        frmReport_LoanApplication.ShowDialog()
    End Sub


    Private Sub btnApply_Click(sender As System.Object, e As System.EventArgs) Handles btnApply.Click
        _InsertDefaultPayMode()
        MsgBox("Done Applying.", vbInformation, "...")
    End Sub

    Private Sub _InsertDefaultPayMode()
        Dim defaultmode As String
        Try
            If rdDirect.Checked = True Then
                defaultmode = "DP"
            End If
            If rdCalendar.Checked = True Then
                defaultmode = "CAL"
            End If
            If rdExact.Checked = True Then
                defaultmode = "EX"
            End If
            If rdSalary.Checked = True Then
                defaultmode = "SAL"
            End If
            SqlHelper.ExecuteNonQuery(con.cnstring, "_tblPayMode_Insert",
                                       New SqlParameter("@fcDefault", defaultmode))
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
    Private Sub _LoadDefaultPayMode()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_tblPayMode_Select")
            If rd.HasRows Then
                While rd.Read
                    Select Case Trim(rd(0))
                        Case "DP"
                            rdDirect.Checked = True
                        Case "CAL"
                            rdCalendar.Checked = True
                        Case "EX"
                            rdExact.Checked = True
                        Case "SAL"
                            rdSalary.Checked = True
                        Case Else
                            Exit Sub
                    End Select
                End While
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub
End Class
