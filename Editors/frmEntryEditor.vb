﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmEntryEditor

    Private gcon As New Clsappconfiguration
    Public LoanNo As String = ""
    Public empno As String

    Dim i As Integer = 0

    Private Sub btnClose_Click(sender As System.Object, e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub LoadDetails()
        dgvList.Rows.Clear()
        Dim rd1 As SqlDataReader
        Dim rd2 As SqlDataReader

        Dim idno As String = ""
        Dim clientname As String = ""
        Dim xloanno As String = ""

        rd1 = SqlHelper.ExecuteReader(gcon.cnstring, "_Select_Accounts_Detail_ForApproval",
               New SqlParameter("@empNo", empno),
               New SqlParameter("@LoanNo", LoanNo))
        rd2 = SqlHelper.ExecuteReader(gcon.cnstring, "_Generate_Loantype_Charges",
                               New SqlParameter("@LoanNo", LoanNo))
        With dgvList
            While rd1.Read
                .Rows.Add()
                'added 8/15/2014 Vince
                idno = rd1("ID No.").ToString
                clientname = rd1("Name").ToString
                xloanno = rd1("Loan Ref.").ToString
                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanRef", i).Value = xloanno
                .Item("AcctRef", i).Value = " "
                .Item("AcctCode", i).Value = rd1("Code").ToString
                .Item("AcctTitle", i).Value = rd1("Account Title").ToString
                .Item("Debit", i).Value = rd1("Debit").ToString
                .Item("Credit", i).Value = "0.00"
                i += 1
            End While
            While rd2.Read
                .Rows.Add()
                .Item("IDNo", i).Value = idno
                .Item("ClientName", i).Value = clientname
                .Item("LoanRef", i).Value = xloanno
                .Item("AcctRef", i).Value = rd2("Acct. Ref").ToString
                .Item("AcctCode", i).Value = rd2("Code").ToString
                .Item("AcctTitle", i).Value = rd2("Description").ToString
                .Item("Debit", i).Value = rd2("Debit").ToString
                .Item("Credit", i).Value = rd2("Credit").ToString
                i += 1
            End While
        End With
    End Sub

    Private Sub frmEntryEditor_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        i = 0
        Call _StartLoad()
    End Sub

    Private Sub _StartLoad()
        Try
            If _LoadCustomEntry(LoanNo).HasRows Then
                dgvList.Rows.Clear()
                Dim rd As SqlDataReader = _LoadCustomEntry(LoanNo)
                While rd.Read
                    With dgvList
                        .Rows.Add()
                        .Item("IDNo", i).Value = rd(0).ToString
                        .Item("ClientName", i).Value = rd(1).ToString
                        .Item("LoanRef", i).Value = rd(2).ToString
                        .Item("AcctRef", i).Value = rd(3).ToString
                        .Item("AcctCode", i).Value = rd(4).ToString
                        .Item("AcctTitle", i).Value = rd(5).ToString
                        .Item("Debit", i).Value = rd(6).ToString
                        .Item("Credit", i).Value = rd(7).ToString
                        i += 1
                    End With
                End While
            Else
                LoadDetails()
                _LoadRebateLoan()
                _LoadRebateAmt()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub dgvList_CellValueChanged(sender As Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvList.CellValueChanged
        RecomputeNetproceed()
    End Sub
    Private Sub RecomputeNetproceed()
        Dim subtotcredit As Decimal
        Dim subtotdebit As Decimal

        For x As Integer = 0 To dgvList.RowCount - 1
            With dgvList
                subtotcredit = subtotcredit + CDec(.Item("Credit", x).Value)
                subtotdebit = subtotdebit + CDec(.Item("Debit", x).Value)
                .Item("Debit", x).Value = Format(CDec(.Item("Debit", x).Value), "##,##0.00")
                .Item("Credit", x).Value = Format(CDec(.Item("Credit", x).Value), "##,##0.00")
            End With
        Next
        txtDebit.Text = Format(subtotdebit, "##,##0.00")
        txtCredit.Text = Format(subtotcredit, "##,##0.00")
        txtDiff.Text = Format(CDec(txtDebit.Text) - CDec(txtCredit.Text), "##,##0.00")
    End Sub

#Region "Rebates 12-9-14 Vince"

    Private Sub _LoadRebateLoan()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_REBATE_GenerateEntry",
                                                               New SqlParameter("@LoanNo", dgvList.Item("LoanRef", 0).Value.ToString))
            With dgvList
                While rd.Read
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0)
                    .Item("ClientName", i).Value = rd(1)
                    .Item("LoanRef", i).Value = rd(2)
                    .Item("AcctRef", i).Value = " "
                    .Item("AcctCode", i).Value = rd(3)
                    .Item("AcctTitle", i).Value = rd(4)
                    .Item("Debit", i).Value = "0.00"
                    .Item("Credit", i).Value = rd(5)
                    i += 1
                End While
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub _LoadRebateAmt()
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, "_REBATE_GenerateEntry2",
                                                               New SqlParameter("@LoanNo", dgvList.Item("LoanRef", 0).Value.ToString))
            With dgvList
                While rd.Read
                    .Rows.Add()
                    .Item("IDNo", i).Value = rd(0)
                    .Item("ClientName", i).Value = rd(1)
                    .Item("LoanRef", i).Value = rd(2)
                    .Item("AcctRef", i).Value = " "
                    .Item("AcctCode", i).Value = rd(3)
                    .Item("AcctTitle", i).Value = rd(4)
                    .Item("Debit", i).Value = rd(5)
                    .Item("Credit", i).Value = "0.00"
                    i += 1
                End While
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Check if this loan have a customized entry"

    Private Function _LoadCustomEntry(ByVal loanNo As String) As SqlDataReader
        Try
            Return SqlHelper.ExecuteReader(gcon.cnstring, "_tblLoanEntry_Retrieve",
                                            New SqlParameter("@fkLoanNo", loanNo))
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

#End Region

    Private Sub Button3_Click(sender As System.Object, e As System.EventArgs) Handles Button3.Click
        frmAddItem.xmode = "1"
        frmAddItem.StartPosition = FormStartPosition.CenterScreen
        frmAddItem.ShowDialog()
    End Sub

    Public Sub AddItems(ByVal id As String, ByVal clientname As String, ByVal loanref As String, ByVal acctref As String,
                           ByVal code As String, ByVal accttitle As String, ByVal balance As String)
        Try
            With dgvList
                .Rows.Add()
                .Item("IDNo", i).Value = id
                .Item("ClientName", i).Value = clientname
                .Item("LoanRef", i).Value = loanref
                .Item("AcctRef", i).Value = acctref
                .Item("AcctCode", i).Value = code
                .Item("AcctTitle", i).Value = accttitle
                .Item("Debit", i).Value = "0.00"
                .Item("Credit", i).Value = balance
                'totcredit = totcredit + Val(.Item("Credit", i).Value)
                'txtCredit.Text = Format(Val(totcredit), "##,##0.00")
                i += 1
            End With
            'RecomputeNetproceed()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSave_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click
        Try
            _ClearTable(LoanNo)
            For x As Integer = 0 To dgvList.RowCount - 1
                With dgvList
                    Dim _idno As String = .Item("IDNo", x).Value.ToString
                    Dim _clientname As String = .Item("ClientName", x).Value.ToString
                    Dim _loanNo As String = .Item("LoanRef", x).Value.ToString
                    Dim _acctref As String = .Item("AcctRef", x).Value.ToString
                    Dim _code As String = .Item("AcctCode", x).Value.ToString
                    Dim _accttitle As String = .Item("AcctTitle", x).Value.ToString
                    Dim _debit As String = .Item("Debit", x).Value.ToString
                    Dim _credit As String = .Item("Credit", x).Value.ToString

                    SqlHelper.ExecuteNonQuery(gcon.cnstring, "_tblLoanEntry_InsertUpdate",
                                              New SqlParameter("@fkLoanNo", dgvList.Item("LoanRef", 0).Value.ToString),
                                              New SqlParameter("@IDNo", _idno),
                                              New SqlParameter("@LoanRef", _loanNo),
                                              New SqlParameter("@AcctRef", _acctref),
                                              New SqlParameter("@AcctCode", _code),
                                              New SqlParameter("@AcctTitle", _accttitle),
                                              New SqlParameter("@Debit", _debit),
                                              New SqlParameter("@Credit", _credit),
                                              New SqlParameter("@fbActive", True))

                End With
            Next
            MsgBox("Entry successfully saved!", vbInformation, "...")
            frmLoan_Processing._Start_LoadCharge()
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Public Sub _ClearTable(ByVal loanno As String)
        Try
            SqlHelper.ExecuteNonQuery(gcon.cnstring, "_tblLoanEntry_Delete",
                                      New SqlParameter("@fkLoanNo", loanno))

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            If MsgBox("Remove selected row?", vbYesNo, "Delete") = vbYes Then
                dgvList.Rows.Remove(dgvList.CurrentRow)
            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
    End Sub

End Class