﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComputationTableLoan
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.txtCSAccount = New System.Windows.Forms.TextBox()
        Me.chkAdd = New System.Windows.Forms.CheckBox()
        Me.chkMulti = New System.Windows.Forms.CheckBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtCSRate = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCSMaxAmt = New System.Windows.Forms.TextBox()
        Me.txtCSMinAmt = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLoanType = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.chkMonth = New System.Windows.Forms.CheckBox()
        Me.chkYear = New System.Windows.Forms.CheckBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTenureTo = New System.Windows.Forms.TextBox()
        Me.txtTenureFrom = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.txtCTPPercent = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txtCTPAccount = New System.Windows.Forms.TextBox()
        Me.chkMemberAcct = New System.Windows.Forms.CheckBox()
        Me.chkBasicPay = New System.Windows.Forms.CheckBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Loan Type :"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Button1)
        Me.Panel1.Controls.Add(Me.txtCSAccount)
        Me.Panel1.Controls.Add(Me.chkAdd)
        Me.Panel1.Controls.Add(Me.chkMulti)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.txtCSRate)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.txtCSMaxAmt)
        Me.Panel1.Controls.Add(Me.txtCSMinAmt)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(15, 67)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(459, 134)
        Me.Panel1.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(23, 18)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(107, 13)
        Me.Label9.TabIndex = 11
        Me.Label9.Text = "Specify Account :"
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button1.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.Button1.Location = New System.Drawing.Point(427, 13)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(29, 23)
        Me.Button1.TabIndex = 10
        Me.Button1.UseVisualStyleBackColor = False
        '
        'txtCSAccount
        '
        Me.txtCSAccount.Location = New System.Drawing.Point(140, 15)
        Me.txtCSAccount.Name = "txtCSAccount"
        Me.txtCSAccount.Size = New System.Drawing.Size(282, 21)
        Me.txtCSAccount.TabIndex = 9
        '
        'chkAdd
        '
        Me.chkAdd.AutoSize = True
        Me.chkAdd.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkAdd.Location = New System.Drawing.Point(357, 105)
        Me.chkAdd.Name = "chkAdd"
        Me.chkAdd.Size = New System.Drawing.Size(35, 17)
        Me.chkAdd.TabIndex = 8
        Me.chkAdd.Text = "+"
        Me.chkAdd.UseVisualStyleBackColor = True
        '
        'chkMulti
        '
        Me.chkMulti.AutoSize = True
        Me.chkMulti.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMulti.Location = New System.Drawing.Point(357, 85)
        Me.chkMulti.Name = "chkMulti"
        Me.chkMulti.Size = New System.Drawing.Size(33, 17)
        Me.chkMulti.TabIndex = 7
        Me.chkMulti.Text = "*"
        Me.chkMulti.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(284, 85)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(67, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Operator :"
        '
        'txtCSRate
        '
        Me.txtCSRate.Location = New System.Drawing.Point(140, 97)
        Me.txtCSRate.Name = "txtCSRate"
        Me.txtCSRate.Size = New System.Drawing.Size(138, 21)
        Me.txtCSRate.TabIndex = 5
        Me.txtCSRate.Text = "0.00"
        Me.txtCSRate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(92, 105)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Rate :"
        '
        'txtCSMaxAmt
        '
        Me.txtCSMaxAmt.Location = New System.Drawing.Point(140, 69)
        Me.txtCSMaxAmt.Name = "txtCSMaxAmt"
        Me.txtCSMaxAmt.Size = New System.Drawing.Size(138, 21)
        Me.txtCSMaxAmt.TabIndex = 3
        Me.txtCSMaxAmt.Text = "0.00"
        Me.txtCSMaxAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtCSMinAmt
        '
        Me.txtCSMinAmt.Location = New System.Drawing.Point(140, 42)
        Me.txtCSMinAmt.Name = "txtCSMinAmt"
        Me.txtCSMinAmt.Size = New System.Drawing.Size(138, 21)
        Me.txtCSMinAmt.TabIndex = 2
        Me.txtCSMinAmt.Text = "0.00"
        Me.txtCSMinAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(15, 72)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(119, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Maximum Amount :"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(19, 45)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(115, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Minimum Amount :"
        '
        'txtLoanType
        '
        Me.txtLoanType.BackColor = System.Drawing.Color.White
        Me.txtLoanType.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLoanType.Location = New System.Drawing.Point(100, 12)
        Me.txtLoanType.Name = "txtLoanType"
        Me.txtLoanType.ReadOnly = True
        Me.txtLoanType.Size = New System.Drawing.Size(375, 21)
        Me.txtLoanType.TabIndex = 3
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.White
        Me.Panel2.Controls.Add(Me.chkMonth)
        Me.Panel2.Controls.Add(Me.chkYear)
        Me.Panel2.Controls.Add(Me.Label8)
        Me.Panel2.Controls.Add(Me.txtTenureTo)
        Me.Panel2.Controls.Add(Me.txtTenureFrom)
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Label7)
        Me.Panel2.Location = New System.Drawing.Point(15, 230)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(459, 100)
        Me.Panel2.TabIndex = 7
        '
        'chkMonth
        '
        Me.chkMonth.AutoSize = True
        Me.chkMonth.Location = New System.Drawing.Point(209, 15)
        Me.chkMonth.Name = "chkMonth"
        Me.chkMonth.Size = New System.Drawing.Size(76, 17)
        Me.chkMonth.TabIndex = 10
        Me.chkMonth.Text = "Month(s)"
        Me.chkMonth.UseVisualStyleBackColor = True
        '
        'chkYear
        '
        Me.chkYear.AutoSize = True
        Me.chkYear.Location = New System.Drawing.Point(136, 15)
        Me.chkYear.Name = "chkYear"
        Me.chkYear.Size = New System.Drawing.Size(68, 17)
        Me.chkYear.TabIndex = 9
        Me.chkYear.Text = "Year(s)"
        Me.chkYear.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(86, 15)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(44, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Type :"
        '
        'txtTenureTo
        '
        Me.txtTenureTo.Location = New System.Drawing.Point(136, 71)
        Me.txtTenureTo.Name = "txtTenureTo"
        Me.txtTenureTo.Size = New System.Drawing.Size(63, 21)
        Me.txtTenureTo.TabIndex = 7
        Me.txtTenureTo.Text = "0"
        Me.txtTenureTo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTenureFrom
        '
        Me.txtTenureFrom.Location = New System.Drawing.Point(136, 44)
        Me.txtTenureFrom.Name = "txtTenureFrom"
        Me.txtTenureFrom.Size = New System.Drawing.Size(63, 21)
        Me.txtTenureFrom.TabIndex = 6
        Me.txtTenureFrom.Text = "0"
        Me.txtTenureFrom.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(100, 74)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(30, 13)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "To :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(85, 47)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(45, 13)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "From :"
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.White
        Me.Panel3.Controls.Add(Me.txtCTPPercent)
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.Button2)
        Me.Panel3.Controls.Add(Me.txtCTPAccount)
        Me.Panel3.Controls.Add(Me.chkMemberAcct)
        Me.Panel3.Controls.Add(Me.chkBasicPay)
        Me.Panel3.Location = New System.Drawing.Point(15, 359)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(459, 101)
        Me.Panel3.TabIndex = 9
        '
        'txtCTPPercent
        '
        Me.txtCTPPercent.Location = New System.Drawing.Point(195, 71)
        Me.txtCTPPercent.Name = "txtCTPPercent"
        Me.txtCTPPercent.Size = New System.Drawing.Size(59, 21)
        Me.txtCTPPercent.TabIndex = 10
        Me.txtCTPPercent.Text = "0.00"
        Me.txtCTPPercent.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(58, 74)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(131, 13)
        Me.Label11.TabIndex = 9
        Me.Label11.Text = "Take home pay (%) :"
        '
        'Button2
        '
        Me.Button2.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button2.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.Button2.Location = New System.Drawing.Point(427, 40)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(29, 23)
        Me.Button2.TabIndex = 7
        Me.Button2.UseVisualStyleBackColor = False
        Me.Button2.Visible = False
        '
        'txtCTPAccount
        '
        Me.txtCTPAccount.Location = New System.Drawing.Point(145, 42)
        Me.txtCTPAccount.Name = "txtCTPAccount"
        Me.txtCTPAccount.Size = New System.Drawing.Size(277, 21)
        Me.txtCTPAccount.TabIndex = 6
        Me.txtCTPAccount.Visible = False
        '
        'chkMemberAcct
        '
        Me.chkMemberAcct.AutoSize = True
        Me.chkMemberAcct.Location = New System.Drawing.Point(18, 44)
        Me.chkMemberAcct.Name = "chkMemberAcct"
        Me.chkMemberAcct.Size = New System.Drawing.Size(121, 17)
        Me.chkMemberAcct.TabIndex = 1
        Me.chkMemberAcct.Text = "Member Account"
        Me.chkMemberAcct.UseVisualStyleBackColor = True
        Me.chkMemberAcct.Visible = False
        '
        'chkBasicPay
        '
        Me.chkBasicPay.AutoSize = True
        Me.chkBasicPay.Location = New System.Drawing.Point(18, 21)
        Me.chkBasicPay.Name = "chkBasicPay"
        Me.chkBasicPay.Size = New System.Drawing.Size(81, 17)
        Me.chkBasicPay.TabIndex = 0
        Me.chkBasicPay.Text = "Basic Pay"
        Me.chkBasicPay.UseVisualStyleBackColor = True
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(12, 49)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(130, 13)
        Me.Label10.TabIndex = 14
        Me.Label10.Text = "Capital Share Limit"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(12, 212)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(89, 13)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Tenure Limit"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(12, 343)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(144, 13)
        Me.Label13.TabIndex = 16
        Me.Label13.Text = "Capacity to Pay Limit"
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(318, 467)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 17
        Me.btnSave.Text = "Apply"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Button6
        '
        Me.Button6.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Button6.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.Button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Button6.Location = New System.Drawing.Point(399, 467)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 13
        Me.Button6.Text = "Close"
        Me.Button6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Button6.UseVisualStyleBackColor = False
        '
        'frmComputationTableLoan
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(487, 494)
        Me.ControlBox = False
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Button6)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.txtLoanType)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "frmComputationTableLoan"
        Me.Text = "Loan Computation Table Setup"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents chkAdd As System.Windows.Forms.CheckBox
    Friend WithEvents chkMulti As System.Windows.Forms.CheckBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtCSRate As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCSMaxAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtCSMinAmt As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtLoanType As System.Windows.Forms.TextBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents chkMonth As System.Windows.Forms.CheckBox
    Friend WithEvents chkYear As System.Windows.Forms.CheckBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents txtTenureTo As System.Windows.Forms.TextBox
    Friend WithEvents txtTenureFrom As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents chkBasicPay As System.Windows.Forms.CheckBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtCSAccount As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txtCTPAccount As System.Windows.Forms.TextBox
    Friend WithEvents chkMemberAcct As System.Windows.Forms.CheckBox
    Friend WithEvents txtCTPPercent As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
End Class
