﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAmortOtherPayment
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAmortOtherPayment))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.SetupPaymentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RemoveAccountToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvPayment = New System.Windows.Forms.DataGridView()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtTotalPAyment = New System.Windows.Forms.TextBox()
        Me.btnApply = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.txtAmortizedPay = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SetupPaymentsToolStripMenuItem, Me.RemoveAccountToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.MenuStrip1.Size = New System.Drawing.Size(730, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'SetupPaymentsToolStripMenuItem
        '
        Me.SetupPaymentsToolStripMenuItem.Image = Global.WindowsApplication2.My.Resources.Resources.view_right
        Me.SetupPaymentsToolStripMenuItem.Name = "SetupPaymentsToolStripMenuItem"
        Me.SetupPaymentsToolStripMenuItem.Size = New System.Drawing.Size(105, 20)
        Me.SetupPaymentsToolStripMenuItem.Text = "Setup Account"
        '
        'RemoveAccountToolStripMenuItem
        '
        Me.RemoveAccountToolStripMenuItem.Image = CType(resources.GetObject("RemoveAccountToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RemoveAccountToolStripMenuItem.Name = "RemoveAccountToolStripMenuItem"
        Me.RemoveAccountToolStripMenuItem.Size = New System.Drawing.Size(116, 20)
        Me.RemoveAccountToolStripMenuItem.Text = "Remove Account"
        '
        'dgvPayment
        '
        Me.dgvPayment.AllowUserToAddRows = False
        Me.dgvPayment.AllowUserToDeleteRows = False
        Me.dgvPayment.AllowUserToResizeColumns = False
        Me.dgvPayment.AllowUserToResizeRows = False
        Me.dgvPayment.BackgroundColor = System.Drawing.Color.White
        Me.dgvPayment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayment.Location = New System.Drawing.Point(12, 36)
        Me.dgvPayment.Name = "dgvPayment"
        Me.dgvPayment.Size = New System.Drawing.Size(706, 261)
        Me.dgvPayment.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(589, 327)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(129, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total Other Payment "
        '
        'txtTotalPAyment
        '
        Me.txtTotalPAyment.Location = New System.Drawing.Point(560, 303)
        Me.txtTotalPAyment.Name = "txtTotalPAyment"
        Me.txtTotalPAyment.Size = New System.Drawing.Size(158, 21)
        Me.txtTotalPAyment.TabIndex = 4
        Me.txtTotalPAyment.Text = "0.00"
        Me.txtTotalPAyment.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'btnApply
        '
        Me.btnApply.Image = Global.WindowsApplication2.My.Resources.Resources.OK
        Me.btnApply.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnApply.Location = New System.Drawing.Point(12, 303)
        Me.btnApply.Name = "btnApply"
        Me.btnApply.Size = New System.Drawing.Size(75, 23)
        Me.btnApply.TabIndex = 2
        Me.btnApply.Text = "Apply"
        Me.btnApply.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnApply.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.Location = New System.Drawing.Point(93, 303)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 23)
        Me.btnClose.TabIndex = 5
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'txtAmortizedPay
        '
        Me.txtAmortizedPay.Location = New System.Drawing.Point(173, 303)
        Me.txtAmortizedPay.Name = "txtAmortizedPay"
        Me.txtAmortizedPay.Size = New System.Drawing.Size(155, 21)
        Me.txtAmortizedPay.TabIndex = 6
        Me.txtAmortizedPay.Text = "0.00"
        Me.txtAmortizedPay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(209, 327)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(119, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Amortized Payment"
        '
        'frmAmortOtherPayment
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(730, 344)
        Me.ControlBox = False
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtAmortizedPay)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.txtTotalPAyment)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnApply)
        Me.Controls.Add(Me.dgvPayment)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmAmortOtherPayment"
        Me.Text = "Add Other Payment"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgvPayment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents SetupPaymentsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvPayment As System.Windows.Forms.DataGridView
    Friend WithEvents btnApply As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtTotalPAyment As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtAmortizedPay As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents RemoveAccountToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
