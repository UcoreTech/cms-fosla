﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmComputeRebate
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmComputeRebate))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnCompute = New System.Windows.Forms.Button()
        Me.txtAdvIntMonths = New System.Windows.Forms.TextBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtRate = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.txtPArticulars = New System.Windows.Forms.TextBox()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.txtAmountDue = New System.Windows.Forms.TextBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtRebate = New System.Windows.Forms.TextBox()
        Me.txtBalance = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtUnpaidMonths = New System.Windows.Forms.TextBox()
        Me.txtPaidMonths = New System.Windows.Forms.TextBox()
        Me.txtTerms = New System.Windows.Forms.TextBox()
        Me.txtNameofBorrower = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtAdvanceInt = New System.Windows.Forms.TextBox()
        Me.txtPrincipalAmt = New System.Windows.Forms.TextBox()
        Me.txtLoanType = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnFind = New System.Windows.Forms.Button()
        Me.txtLoanNo = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.txtRebateAcct = New System.Windows.Forms.TextBox()
        Me.btnChange = New System.Windows.Forms.Button()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.Desktop
        Me.Label1.Location = New System.Drawing.Point(258, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(155, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Computation of Rebate"
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Controls.Add(Me.btnCompute)
        Me.Panel1.Controls.Add(Me.txtAdvIntMonths)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.txtRate)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.txtPArticulars)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.txtAmountDue)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.txtRebate)
        Me.Panel1.Controls.Add(Me.txtBalance)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.txtUnpaidMonths)
        Me.Panel1.Controls.Add(Me.txtPaidMonths)
        Me.Panel1.Controls.Add(Me.txtTerms)
        Me.Panel1.Controls.Add(Me.txtNameofBorrower)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.txtAdvanceInt)
        Me.Panel1.Controls.Add(Me.txtPrincipalAmt)
        Me.Panel1.Controls.Add(Me.txtLoanType)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.btnFind)
        Me.Panel1.Controls.Add(Me.txtLoanNo)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Location = New System.Drawing.Point(12, 33)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(647, 398)
        Me.Panel1.TabIndex = 1
        '
        'btnCompute
        '
        Me.btnCompute.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompute.ForeColor = System.Drawing.SystemColors.Highlight
        Me.btnCompute.Image = CType(resources.GetObject("btnCompute.Image"), System.Drawing.Image)
        Me.btnCompute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompute.Location = New System.Drawing.Point(264, 220)
        Me.btnCompute.Name = "btnCompute"
        Me.btnCompute.Size = New System.Drawing.Size(115, 23)
        Me.btnCompute.TabIndex = 32
        Me.btnCompute.Text = "Compute"
        Me.btnCompute.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnCompute.UseVisualStyleBackColor = True
        '
        'txtAdvIntMonths
        '
        Me.txtAdvIntMonths.Location = New System.Drawing.Point(539, 184)
        Me.txtAdvIntMonths.Name = "txtAdvIntMonths"
        Me.txtAdvIntMonths.Size = New System.Drawing.Size(86, 21)
        Me.txtAdvIntMonths.TabIndex = 31
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(323, 192)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(210, 13)
        Me.Label17.TabIndex = 30
        Me.Label17.Text = "No. of Months  (Advance Interest) :"
        '
        'txtRate
        '
        Me.txtRate.Location = New System.Drawing.Point(539, 158)
        Me.txtRate.Name = "txtRate"
        Me.txtRate.Size = New System.Drawing.Size(86, 21)
        Me.txtRate.TabIndex = 29
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(491, 164)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(42, 13)
        Me.Label16.TabIndex = 28
        Me.Label16.Text = "Rate :"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(557, 241)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(67, 13)
        Me.Label14.TabIndex = 27
        Me.Label14.Text = "Particulars"
        '
        'txtPArticulars
        '
        Me.txtPArticulars.Location = New System.Drawing.Point(384, 261)
        Me.txtPArticulars.Multiline = True
        Me.txtPArticulars.Name = "txtPArticulars"
        Me.txtPArticulars.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtPArticulars.Size = New System.Drawing.Size(240, 83)
        Me.txtPArticulars.TabIndex = 26
        Me.txtPArticulars.Text = "Full Payment of cash loan (with Rebates) : RefNo "
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnSave.Image = Global.WindowsApplication2.My.Resources.Resources.save2
        Me.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSave.Location = New System.Drawing.Point(124, 370)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(87, 25)
        Me.btnSave.TabIndex = 25
        Me.btnSave.Text = "Save"
        Me.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'txtAmountDue
        '
        Me.txtAmountDue.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAmountDue.Location = New System.Drawing.Point(158, 323)
        Me.txtAmountDue.Name = "txtAmountDue"
        Me.txtAmountDue.Size = New System.Drawing.Size(202, 21)
        Me.txtAmountDue.TabIndex = 23
        Me.txtAmountDue.Text = "0.00"
        Me.txtAmountDue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(65, 331)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 13)
        Me.Label13.TabIndex = 22
        Me.Label13.Text = "Amount Due :"
        '
        'txtRebate
        '
        Me.txtRebate.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRebate.Location = New System.Drawing.Point(158, 288)
        Me.txtRebate.Name = "txtRebate"
        Me.txtRebate.Size = New System.Drawing.Size(202, 21)
        Me.txtRebate.TabIndex = 21
        Me.txtRebate.Text = "0.00"
        Me.txtRebate.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtBalance
        '
        Me.txtBalance.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtBalance.Location = New System.Drawing.Point(158, 261)
        Me.txtBalance.Name = "txtBalance"
        Me.txtBalance.Size = New System.Drawing.Size(202, 21)
        Me.txtBalance.TabIndex = 20
        Me.txtBalance.Text = "0.00"
        Me.txtBalance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(67, 296)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(85, 13)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "Less : Rebate"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(19, 269)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(133, 13)
        Me.Label11.TabIndex = 18
        Me.Label11.Text = "Outstanding Balance :"
        '
        'txtUnpaidMonths
        '
        Me.txtUnpaidMonths.Location = New System.Drawing.Point(539, 131)
        Me.txtUnpaidMonths.Name = "txtUnpaidMonths"
        Me.txtUnpaidMonths.Size = New System.Drawing.Size(86, 21)
        Me.txtUnpaidMonths.TabIndex = 17
        '
        'txtPaidMonths
        '
        Me.txtPaidMonths.Location = New System.Drawing.Point(539, 102)
        Me.txtPaidMonths.Name = "txtPaidMonths"
        Me.txtPaidMonths.Size = New System.Drawing.Size(86, 21)
        Me.txtPaidMonths.TabIndex = 16
        '
        'txtTerms
        '
        Me.txtTerms.Location = New System.Drawing.Point(539, 75)
        Me.txtTerms.Name = "txtTerms"
        Me.txtTerms.Size = New System.Drawing.Size(86, 21)
        Me.txtTerms.TabIndex = 15
        '
        'txtNameofBorrower
        '
        Me.txtNameofBorrower.Location = New System.Drawing.Point(158, 80)
        Me.txtNameofBorrower.Name = "txtNameofBorrower"
        Me.txtNameofBorrower.Size = New System.Drawing.Size(233, 21)
        Me.txtNameofBorrower.TabIndex = 14
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(31, 87)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(121, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Name of Borrower :"
        '
        'txtAdvanceInt
        '
        Me.txtAdvanceInt.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtAdvanceInt.Location = New System.Drawing.Point(158, 161)
        Me.txtAdvanceInt.Name = "txtAdvanceInt"
        Me.txtAdvanceInt.Size = New System.Drawing.Size(136, 21)
        Me.txtAdvanceInt.TabIndex = 12
        Me.txtAdvanceInt.Text = "0.00"
        Me.txtAdvanceInt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtPrincipalAmt
        '
        Me.txtPrincipalAmt.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPrincipalAmt.Location = New System.Drawing.Point(158, 134)
        Me.txtPrincipalAmt.Name = "txtPrincipalAmt"
        Me.txtPrincipalAmt.Size = New System.Drawing.Size(136, 21)
        Me.txtPrincipalAmt.TabIndex = 11
        Me.txtPrincipalAmt.Text = "0.00"
        Me.txtPrincipalAmt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'txtLoanType
        '
        Me.txtLoanType.Location = New System.Drawing.Point(158, 107)
        Me.txtLoanType.Name = "txtLoanType"
        Me.txtLoanType.Size = New System.Drawing.Size(233, 21)
        Me.txtLoanType.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(6, 171)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(146, 13)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "Total Advance Interest :"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(396, 139)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(137, 13)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "No. of Months Unpaid :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(411, 110)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(122, 13)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "No. of Months Paid :"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(481, 83)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(52, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Terms :"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(40, 143)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(112, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Principal Amount :"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(77, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Loan Type :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.Highlight
        Me.Label3.Location = New System.Drawing.Point(292, 53)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(87, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Loan Details"
        '
        'btnFind
        '
        Me.btnFind.Image = Global.WindowsApplication2.My.Resources.Resources.Search
        Me.btnFind.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnFind.Location = New System.Drawing.Point(264, 17)
        Me.btnFind.Name = "btnFind"
        Me.btnFind.Size = New System.Drawing.Size(75, 23)
        Me.btnFind.TabIndex = 2
        Me.btnFind.Text = "Find"
        Me.btnFind.UseVisualStyleBackColor = True
        '
        'txtLoanNo
        '
        Me.txtLoanNo.Location = New System.Drawing.Point(158, 19)
        Me.txtLoanNo.Name = "txtLoanNo"
        Me.txtLoanNo.Size = New System.Drawing.Size(100, 21)
        Me.txtLoanNo.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(90, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(62, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Loan No :"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 449)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(127, 13)
        Me.Label15.TabIndex = 3
        Me.Label15.Text = "Account For Rebate :"
        '
        'txtRebateAcct
        '
        Me.txtRebateAcct.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtRebateAcct.Font = New System.Drawing.Font("Verdana", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtRebateAcct.Location = New System.Drawing.Point(136, 441)
        Me.txtRebateAcct.Name = "txtRebateAcct"
        Me.txtRebateAcct.Size = New System.Drawing.Size(360, 23)
        Me.txtRebateAcct.TabIndex = 4
        '
        'btnChange
        '
        Me.btnChange.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnChange.Location = New System.Drawing.Point(502, 441)
        Me.btnChange.Name = "btnChange"
        Me.btnChange.Size = New System.Drawing.Size(61, 23)
        Me.btnChange.TabIndex = 5
        Me.btnChange.Text = "Change"
        Me.btnChange.UseVisualStyleBackColor = False
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClose.Image = Global.WindowsApplication2.My.Resources.Resources.eventlogError
        Me.btnClose.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnClose.Location = New System.Drawing.Point(600, 437)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(59, 26)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Close"
        Me.btnClose.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'frmComputeRebate
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(674, 476)
        Me.Controls.Add(Me.btnChange)
        Me.Controls.Add(Me.txtRebateAcct)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmComputeRebate"
        Me.Text = "Rebate Computation"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnFind As System.Windows.Forms.Button
    Friend WithEvents txtLoanNo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtNameofBorrower As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents txtAdvanceInt As System.Windows.Forms.TextBox
    Friend WithEvents txtPrincipalAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtLoanType As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtUnpaidMonths As System.Windows.Forms.TextBox
    Friend WithEvents txtPaidMonths As System.Windows.Forms.TextBox
    Friend WithEvents txtTerms As System.Windows.Forms.TextBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents txtRebate As System.Windows.Forms.TextBox
    Friend WithEvents txtBalance As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txtAmountDue As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents txtPArticulars As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents txtRebateAcct As System.Windows.Forms.TextBox
    Friend WithEvents btnChange As System.Windows.Forms.Button
    Friend WithEvents txtRate As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents btnCompute As System.Windows.Forms.Button
    Friend WithEvents txtAdvIntMonths As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
End Class
