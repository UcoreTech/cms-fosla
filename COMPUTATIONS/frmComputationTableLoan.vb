﻿Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Public Class frmComputationTableLoan

    Private con As New Clsappconfiguration

    Private Sub frmComputationTableLoan_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        Me.Close()
    End Sub

    Private Sub _SaveUpdateSettings()
        Try
            Dim op As String = ""
            Dim tenuretype As String = ""
            If chkMulti.Checked = True Then
                op = "*"
            End If
            If chkAdd.Checked = True Then
                op = "+"
            End If
            If chkYear.Checked = True Then
                tenuretype = "Year"
            End If
            If chkMonth.Checked = True Then
                tenuretype = "Month"
            End If
            SqlHelper.ExecuteNonQuery(con.cnstring, "_ComputationTable_InsertUpdate",
                                       New SqlParameter("@LoanType", txtLoanType.Text),
                                       New SqlParameter("@fxCSAccountKey", txtCSAccount.Tag.ToString),
                                       New SqlParameter("@fdCSMinAmount", txtCSMinAmt.Text),
                                       New SqlParameter("@fdCSMaxAmount", txtCSMaxAmt.Text),
                                       New SqlParameter("@fdCSRate", txtCSRate.Text),
                                       New SqlParameter("@fcCSOperator", op),
                                       New SqlParameter("@fcTenureType", tenuretype),
                                       New SqlParameter("@fcTenureFrom", txtTenureFrom.Text),
                                       New SqlParameter("@fcTenureTo", txtTenureTo.Text),
                                       New SqlParameter("@fbCTPBasicPay", chkBasicPay.Checked),
                                       New SqlParameter("@fbCTPAccount", chkMemberAcct.Checked),
                                       New SqlParameter("@fcCTPAccountKey", DBNull.Value),
                                       New SqlParameter("@fdCTPPercent", txtCTPPercent.Text),
                                       New SqlParameter("@dtDateUpdated", Date.Now))
            MsgBox("Saving Success.", vbInformation, "Done")
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If MsgBox("Apply Changes ?", vbYesNo, "...") = vbYes Then
            _SaveUpdateSettings()
        Else
            Exit Sub
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        frmComputationSelectAcct.xmode = "CS"
        frmComputationSelectAcct.StartPosition = FormStartPosition.CenterScreen
        frmComputationSelectAcct.ShowDialog()
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        frmComputationSelectAcct.xmode = "CTP"
        frmComputationSelectAcct.StartPosition = FormStartPosition.CenterScreen
        frmComputationSelectAcct.ShowDialog()
    End Sub

    Public Sub _LoadSettings(ByVal loantype As String)
        Try
            Dim rd As SqlDataReader = SqlHelper.ExecuteReader(con.cnstring, "_ComputationTable_SelectBy_LoanType",
                                                               New SqlParameter("@LoanType", loantype))
            If rd.HasRows Then
                While rd.Read
                    txtCSAccount.Tag = rd(0).ToString
                    txtCSAccount.Text = rd(1).ToString
                    txtCSMinAmt.Text = rd(2).ToString
                    txtCSMaxAmt.Text = rd(3).ToString
                    txtCSRate.Text = rd(4).ToString
                    If rd(5) = "*" Then
                        chkMulti.Checked = True
                    End If
                    If rd(5) = "+" Then
                        chkAdd.Checked = True
                    End If
                    If rd(6) = "Year" Then
                        chkYear.Checked = True
                    End If
                    If rd(6) = "Month" Then
                        chkMonth.Checked = True
                    End If
                    txtTenureFrom.Text = rd(7).ToString
                    txtTenureTo.Text = rd(8).ToString
                    chkBasicPay.Checked = rd(9)
                    chkMemberAcct.Checked = rd(10)
                    txtCTPAccount.Tag = rd(11)
                    txtCTPAccount.Text = rd(12)
                    txtCTPPercent.Text = rd(13)
                End While
            Else
                txtCSAccount.Tag = ""
                txtCSAccount.Text = ""
                txtCSMinAmt.Text = "0.00"
                txtCSMaxAmt.Text = "0.00"
                txtCSRate.Text = "0.00"
                chkMulti.Checked = False
                chkAdd.Checked = False
                chkYear.Checked = False
                chkMonth.Checked = False
                txtTenureFrom.Text = "0"
                txtTenureTo.Text = "0"
                chkBasicPay.Checked = False
                chkMemberAcct.Checked = False
                txtCTPAccount.Tag = ""
                txtCTPAccount.Text = ""
                txtCTPPercent.Text = "0.00"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class