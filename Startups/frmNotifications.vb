Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Imports System.Net.Mail
Imports System.Text
Imports System.Security.Cryptography
Public Class frmNotifications
    Private gcon As New Clsappconfiguration

    Private Sub btnMakeActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMakeActive.Click
        MakeMembersActive()
        Load_MembersOnHold()
    End Sub

    Private Sub frmNotifications_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Load_MembersOnHold()
    End Sub

    Private Sub Load_MembersOnHold()
        Dim ds As DataSet
        Try
            ds = ModifiedSqlHelper.ExecuteDataset(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Notification_OnHoldMembers")
            gridNotifications.DataSource = ds.Tables(0)

            If gridNotifications.RowCount = 0 Then
                Me.Close()
            Else
                Call ArrangeGrid(gridNotifications)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub MakeMembersActive()
        Dim count As Integer
        For count = 0 To Me.gridNotifications.Rows.Count - 1
            If Me.gridNotifications.Item(0, count).Value = True Then

                Try
                    Dim employeeNo As Object = Me.gridNotifications.Item(1, count).Value
                    ModifiedSqlHelper.ExecuteNonQuery(gcon.cnstring, CommandType.StoredProcedure, "CIMS_Notification_MakeMembersActive", _
                                New SqlParameter("@employeeNo", employeeNo))
                Catch ex As Exception
                    MessageBox.Show(ex.Message)
                End Try

            End If
        Next
    End Sub

    Private Sub ArrangeGrid(ByVal grid As DataGridView)
        grid.Columns(0).Width = 50
        grid.Columns(1).Width = 80

        grid.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells

        grid.Columns(0).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
        grid.Columns(1).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter


    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub
End Class