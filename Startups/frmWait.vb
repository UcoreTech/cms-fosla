Public Class frmWait

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        With ProgressBar1
            If .Value < .Maximum Then
                .PerformStep()
                Me.Refresh()
            End If

            If .Value >= .Maximum Then
                Timer1.Enabled = False

                frmLogin.Close()
                frmMain.Close()

                ProgressBar1.Visible = False

                Me.Close()
            End If
        End With
    End Sub

    Private Sub loans_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        With ProgressBar1
            If .Value = .Maximum Then .Value = 0
        End With
        Timer1.Enabled = True
        'Call Timer1_Tick(sender, e)
        ProgressBar1.Visible = True
    End Sub

End Class
