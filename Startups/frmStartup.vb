Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Public Class frmStartup

    Private progressBarValue As Integer
    Public Property PropertyProgressBarValue() As Integer
        Get
            Return progressBarValue
        End Get
        Set(ByVal value As Integer)
            progressBarValue = value
        End Set
    End Property
    Private progressBarMaximum As Integer
    Public Property PropertyProgressBarMax() As Integer
        Get
            Return progressBarMaximum
        End Get
        Set(ByVal value As Integer)
            progressBarMaximum = value
        End Set
    End Property

    Private Function GetTotalProgressCount() As Int32
        Dim gcon As New Clsappconfiguration
        Dim total As Int32

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_Progress_Check")
                If rd.Read Then
                    total = Int32.Parse(rd.Item("totalCount").ToString)
                End If
            End Using
        Catch ex As Exception
            Return 0
            MessageBox.Show(ex.Message)
        End Try

        Return total
    End Function
    Private Function GetProgressCount() As Int32
        Dim gcon As New Clsappconfiguration
        Dim total As Int32

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gcon.cnstring, CommandType.StoredProcedure, "CIMS_t_Progress_Check")
                If rd.Read Then
                    total = Int32.Parse(rd.Item("fnCount").ToString)
                End If

            End Using
        Catch ex As Exception
            Return 0
            MessageBox.Show(ex.Message)
        End Try

        Return total
    End Function

End Class