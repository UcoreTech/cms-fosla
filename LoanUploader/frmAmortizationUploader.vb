﻿Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.FileSystem
Imports System.Data
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data

'Module : Loan Amo Uploader
'Programmer : Vincent Nacar
'Minimum Upload Speed : 0.1 Sec
'Maximum Upload Speed : 60 Sec
'-------------------------------------------------------
Public Class frmAmortizationUploader

    Dim filenym As String
    Dim fpath As String
    Dim itemCount As Long
    Dim con As New Clsappconfiguration

    Private Sub frmAmortizationUploader_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Dim MyConnection As System.Data.OleDb.OleDbConnection
    Dim DtSet As System.Data.DataSet = New System.Data.DataSet
    Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
    'DtSet = New System.Data.DataSet
    Dim sprovider As String

    Private Sub Import() Handles bgwImport.DoWork
        Try
            If sprovider = "Microsoft.Jet.OLEDB.4.0" Then
                MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 8.0;")
            End If
            If sprovider = "Microsoft.ACE.OLEDB.12.0" Then
                MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtPath.Text.ToString + "';Excel 12.0 Xml;HDR=YES")
            End If
            If sprovider = "Microsoft.ACE.OLEDB.12.0 Extended" Then
                MyConnection = New System.Data.OleDb.OleDbConnection("provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + txtPath.Text.ToString + "';Extended Properties=Excel 12.0;")
            End If
            MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
            MyCommand.TableMappings.Add("Table", "Vincent00x21")
            MyCommand.Fill(DtSet)
        Catch ex As Exception
            _CMS_MsgBox._Msg(ex.Message, "", "Import()", Date.Now.ToString("MMMM dd,yyyy"))
            DtSet = Nothing
        End Try
    End Sub
    Private Sub _getProvider() Handles cboProvider.SelectedValueChanged
        sprovider = cboProvider.Text
    End Sub

    'progress 
    Private Sub bgwSave_ProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs) Handles bgwSave.ProgressChanged
        lblCurItem.Text = curItem
        xprogressBar.Value = e.ProgressPercentage
    End Sub

    Private Sub _ImportDone() Handles bgwImport.RunWorkerCompleted
        dgvList.DataSource = DtSet.Tables(0)
        lblItemCount.Text = Format(CDec(DtSet.Tables(0).Rows.Count), "##,##0")
        MyConnection.Close()
        picLoading.Visible = False
    End Sub

    Private Sub btnBrowse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBrowse.Click
        Try
            OpenFileDialog1.ShowDialog()
            filenym = OpenFileDialog1.FileName
            fpath = Path.GetFullPath(filenym)
            txtPath.Text = fpath
            If bgwImport.IsBusy = False Then
                picLoading.Visible = True
                bgwImport.RunWorkerAsync()
            End If
            ' Import()
        Catch ex As Exception
            _CMS_MsgBox._Msg("User Cancelled.", "", "btnBrowse_Click", Date.Now.ToString("MMMM dd,yyyy"))
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub btnUpload_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Select Case MsgBox("Are your sure?", vbYesNo, "Upload Loan Amortization")
            Case vbYes
                rowc = dgvList.RowCount
                'processWait.Style = ProgressBarStyle.Marquee
                'processWait.MarqueeAnimationSpeed = 5
                If bgwSave.IsBusy = False Then
                    picLoading.Visible = True
                    bgwSave.RunWorkerAsync()
                End If
            Case Else
                Exit Sub
        End Select
    End Sub

    Private Sub DeleteExisting()

    End Sub

    Private Sub _UploadDone() Handles bgwSave.RunWorkerCompleted
        frmMsgBox.txtMessage.Text = "Upload Complete , Total No. of Item(s):" + itemCount.ToString
        frmMsgBox.ShowDialog()
        dgvList.Columns.Clear()
        picLoading.Visible = False
    End Sub

    Dim myid As New Guid
    Dim xrow As Integer = 0
    Dim rowc As Integer = 0
    Dim curItem As String
    Private Sub is_Uploaded() Handles bgwSave.DoWork
        Try
            For xCtr As Integer = 0 To dgvList.RowCount - 1

                With dgvList
                    Dim loanNo As String = .Item("LoanNo", xCtr).Value.ToString
                    Dim idno As Integer = .Item("PayNo", xCtr).Value
                    Dim xdate As Date = .Item("Date", xCtr).Value
                    Dim principal As String = .Item("Principal", xCtr).Value.ToString
                    Dim interest As String = .Item("Interest", xCtr).Value.ToString
                    Dim servicefee As String = .Item("ServiceFee", xCtr).Value.ToString
                    myid = Guid.NewGuid

                    SqlHelper.ExecuteNonQuery(con.cnstring, "_Amortization_Upload",
                                               New SqlParameter("@myid", myid),
                                               New SqlParameter("@LoanNo", loanNo),
                                               New SqlParameter("@PayNo", idno),
                                               New SqlParameter("@Date", xdate),
                                               New SqlParameter("@Principal", principal),
                                               New SqlParameter("@Interest", interest))
                    SqlHelper.ExecuteNonQuery(con.cnstring, "_tblAdditionalColStorage_InsertUpdate",
                                              New SqlParameter("@fcLoanNo", loanNo),
                                              New SqlParameter("@fdAmount", servicefee),
                                              New SqlParameter("@fdPayNo", idno),
                                              New SqlParameter("@fdColName", "Service Fee"))
                    curItem = loanNo + " - " + idno.ToString + " - " + xdate.ToString("MMMM dd,yyyy") + " - " + principal + " - " + interest + " - " + servicefee
                End With
                itemCount += 1
                xrow += 1
                'lblItemCount.Text = itemCount.ToString
                Dim percent As Integer = (xrow / rowc) * 100
                bgwSave.ReportProgress(percent)
            Next
        Catch ex As Exception
        End Try
    End Sub

End Class