﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLoanFinalUploader
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmLoanFinalUploader))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.btnAmortTool = New System.Windows.Forms.Button()
        Me.btnTool = New System.Windows.Forms.Button()
        Me.btnGenerateAmortization = New System.Windows.Forms.Button()
        Me.btnClearRows = New System.Windows.Forms.Button()
        Me.btnUpload = New System.Windows.Forms.Button()
        Me.btnBrowse = New System.Windows.Forms.Button()
        Me.txtPath = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.txtTotalMarked = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtNoofItems = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.chkMarkAll = New System.Windows.Forms.CheckBox()
        Me.dgvList = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.cboModeofPayment = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.dgvAmortization = New System.Windows.Forms.DataGridView()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.SkyBlue
        Me.Panel1.Controls.Add(Me.Panel3)
        Me.Panel1.Controls.Add(Me.txtPath)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1186, 22)
        Me.Panel1.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.SkyBlue
        Me.Panel3.Controls.Add(Me.btnAmortTool)
        Me.Panel3.Controls.Add(Me.btnTool)
        Me.Panel3.Controls.Add(Me.btnGenerateAmortization)
        Me.Panel3.Controls.Add(Me.btnClearRows)
        Me.Panel3.Controls.Add(Me.btnUpload)
        Me.Panel3.Controls.Add(Me.btnBrowse)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel3.Location = New System.Drawing.Point(346, 0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(840, 22)
        Me.Panel3.TabIndex = 3
        '
        'btnAmortTool
        '
        Me.btnAmortTool.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnAmortTool.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnAmortTool.Location = New System.Drawing.Point(598, 0)
        Me.btnAmortTool.Name = "btnAmortTool"
        Me.btnAmortTool.Size = New System.Drawing.Size(127, 22)
        Me.btnAmortTool.TabIndex = 9
        Me.btnAmortTool.Text = "Amortization Tool"
        Me.btnAmortTool.UseVisualStyleBackColor = False
        '
        'btnTool
        '
        Me.btnTool.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnTool.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnTool.Location = New System.Drawing.Point(523, 0)
        Me.btnTool.Name = "btnTool"
        Me.btnTool.Size = New System.Drawing.Size(75, 22)
        Me.btnTool.TabIndex = 7
        Me.btnTool.Text = "Tool"
        Me.btnTool.UseVisualStyleBackColor = False
        '
        'btnGenerateAmortization
        '
        Me.btnGenerateAmortization.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnGenerateAmortization.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnGenerateAmortization.ForeColor = System.Drawing.SystemColors.HotTrack
        Me.btnGenerateAmortization.Location = New System.Drawing.Point(355, 0)
        Me.btnGenerateAmortization.Name = "btnGenerateAmortization"
        Me.btnGenerateAmortization.Size = New System.Drawing.Size(168, 22)
        Me.btnGenerateAmortization.TabIndex = 8
        Me.btnGenerateAmortization.Text = "Generate Amortization"
        Me.btnGenerateAmortization.UseVisualStyleBackColor = False
        '
        'btnClearRows
        '
        Me.btnClearRows.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClearRows.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnClearRows.Location = New System.Drawing.Point(251, 0)
        Me.btnClearRows.Name = "btnClearRows"
        Me.btnClearRows.Size = New System.Drawing.Size(104, 22)
        Me.btnClearRows.TabIndex = 5
        Me.btnClearRows.Text = "Clear Rows"
        Me.btnClearRows.UseVisualStyleBackColor = False
        '
        'btnUpload
        '
        Me.btnUpload.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnUpload.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnUpload.Location = New System.Drawing.Point(75, 0)
        Me.btnUpload.Name = "btnUpload"
        Me.btnUpload.Size = New System.Drawing.Size(176, 22)
        Me.btnUpload.TabIndex = 4
        Me.btnUpload.Text = "Upload Marked File"
        Me.btnUpload.UseVisualStyleBackColor = False
        '
        'btnBrowse
        '
        Me.btnBrowse.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnBrowse.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnBrowse.Location = New System.Drawing.Point(0, 0)
        Me.btnBrowse.Name = "btnBrowse"
        Me.btnBrowse.Size = New System.Drawing.Size(75, 22)
        Me.btnBrowse.TabIndex = 3
        Me.btnBrowse.Text = "Browse"
        Me.btnBrowse.UseVisualStyleBackColor = False
        '
        'txtPath
        '
        Me.txtPath.BackColor = System.Drawing.Color.White
        Me.txtPath.Dock = System.Windows.Forms.DockStyle.Left
        Me.txtPath.Location = New System.Drawing.Point(0, 0)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.ReadOnly = True
        Me.txtPath.Size = New System.Drawing.Size(346, 21)
        Me.txtPath.TabIndex = 0
        Me.txtPath.Text = "/ Browse File / "
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.SkyBlue
        Me.Panel2.Controls.Add(Me.btnClose)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 428)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1186, 27)
        Me.Panel2.TabIndex = 1
        '
        'btnClose
        '
        Me.btnClose.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.btnClose.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnClose.Location = New System.Drawing.Point(1111, 0)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(75, 27)
        Me.btnClose.TabIndex = 3
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = False
        '
        'txtTotalMarked
        '
        Me.txtTotalMarked.Location = New System.Drawing.Point(388, 2)
        Me.txtTotalMarked.Name = "txtTotalMarked"
        Me.txtTotalMarked.ReadOnly = True
        Me.txtTotalMarked.Size = New System.Drawing.Size(100, 21)
        Me.txtTotalMarked.TabIndex = 6
        Me.txtTotalMarked.Text = "00"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(230, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(161, 13)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Total No. of Marked Items:"
        '
        'txtNoofItems
        '
        Me.txtNoofItems.Location = New System.Drawing.Point(124, 2)
        Me.txtNoofItems.Name = "txtNoofItems"
        Me.txtNoofItems.ReadOnly = True
        Me.txtNoofItems.Size = New System.Drawing.Size(100, 21)
        Me.txtNoofItems.TabIndex = 4
        Me.txtNoofItems.Text = "00"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(115, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Total No. of Items:"
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.SkyBlue
        Me.Panel4.Controls.Add(Me.chkMarkAll)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 22)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1186, 24)
        Me.Panel4.TabIndex = 4
        '
        'chkMarkAll
        '
        Me.chkMarkAll.AutoSize = True
        Me.chkMarkAll.Location = New System.Drawing.Point(12, 4)
        Me.chkMarkAll.Name = "chkMarkAll"
        Me.chkMarkAll.Size = New System.Drawing.Size(72, 17)
        Me.chkMarkAll.TabIndex = 0
        Me.chkMarkAll.Text = "Mark All"
        Me.chkMarkAll.UseVisualStyleBackColor = True
        '
        'dgvList
        '
        Me.dgvList.AllowUserToAddRows = False
        Me.dgvList.AllowUserToResizeColumns = False
        Me.dgvList.AllowUserToResizeRows = False
        Me.dgvList.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvList.Location = New System.Drawing.Point(0, 46)
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowHeadersVisible = False
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(1186, 192)
        Me.dgvList.TabIndex = 5
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.SkyBlue
        Me.Panel5.Controls.Add(Me.cboModeofPayment)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.txtTotalMarked)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Controls.Add(Me.Label1)
        Me.Panel5.Controls.Add(Me.txtNoofItems)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel5.Location = New System.Drawing.Point(0, 238)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1186, 26)
        Me.Panel5.TabIndex = 6
        '
        'cboModeofPayment
        '
        Me.cboModeofPayment.FormattingEnabled = True
        Me.cboModeofPayment.Items.AddRange(New Object() {"DAILY", "WEEKLY", "SEMI-MONTHLY", "MONTHLY", "QUARTERLY", "ANNUALLY", "LUMP SUM"})
        Me.cboModeofPayment.Location = New System.Drawing.Point(881, 2)
        Me.cboModeofPayment.Name = "cboModeofPayment"
        Me.cboModeofPayment.Size = New System.Drawing.Size(201, 21)
        Me.cboModeofPayment.TabIndex = 8
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(769, 8)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(115, 13)
        Me.Label4.TabIndex = 7
        Me.Label4.Text = "Mode of Payment :"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(494, 10)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(154, 13)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Amortization Schedule"
        '
        'dgvAmortization
        '
        Me.dgvAmortization.AllowUserToAddRows = False
        Me.dgvAmortization.AllowUserToDeleteRows = False
        Me.dgvAmortization.AllowUserToResizeColumns = False
        Me.dgvAmortization.AllowUserToResizeRows = False
        Me.dgvAmortization.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvAmortization.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAmortization.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvAmortization.Location = New System.Drawing.Point(0, 264)
        Me.dgvAmortization.Name = "dgvAmortization"
        Me.dgvAmortization.ReadOnly = True
        Me.dgvAmortization.RowHeadersVisible = False
        Me.dgvAmortization.Size = New System.Drawing.Size(1186, 164)
        Me.dgvAmortization.TabIndex = 7
        '
        'frmLoanFinalUploader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1186, 455)
        Me.Controls.Add(Me.dgvAmortization)
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.dgvList)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Verdana", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmLoanFinalUploader"
        Me.Text = "Loan Uploader v3.0"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        CType(Me.dgvAmortization, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents chkMarkAll As System.Windows.Forms.CheckBox
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnUpload As System.Windows.Forms.Button
    Friend WithEvents btnBrowse As System.Windows.Forms.Button
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents btnClearRows As System.Windows.Forms.Button
    Friend WithEvents txtNoofItems As System.Windows.Forms.TextBox
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents txtTotalMarked As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents dgvAmortization As System.Windows.Forms.DataGridView
    Friend WithEvents btnGenerateAmortization As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents btnTool As System.Windows.Forms.Button
    Friend WithEvents btnAmortTool As System.Windows.Forms.Button
    Friend WithEvents cboModeofPayment As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
End Class
