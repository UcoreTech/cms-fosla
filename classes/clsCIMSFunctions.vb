Imports Microsoft.ApplicationBlocks.Data
Imports System.Data.SqlClient

Public Class ClsCIMSFunctions
    Private gCon As New Clsappconfiguration

    Public Function GetLoggedInUserFullName() As String
        Dim fullName As String = ""
        Dim storedProcedure As String = "CIMS_Admin_GetLoggedInUser_Details"
        Dim loggedInUserID As String = frmMain.GetLoggedInUserID().ToString()

        Try
            Using rd As SqlDataReader = SqlHelper.ExecuteReader(gCon.cnstring, CommandType.StoredProcedure, storedProcedure, _
                        New SqlParameter("@userID", loggedInUserID))
                If rd.Read() Then
                    fullName = rd.Item(0).ToString + " " + rd.Item(2).ToString()
                End If
            End Using

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

        Return fullName
    End Function

End Class
