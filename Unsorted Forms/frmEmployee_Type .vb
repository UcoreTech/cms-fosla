Imports System.Data.SqlClient
Public Class Employee_Position
#Region "Variables"
    Public getrecid As String
    Public getfxkeystatus As String
#End Region
#Region "Get Property"
    Public Property getrecordid() As String
        Get
            Return getrecid
        End Get
        Set(ByVal value As String)
            getrecid = value
        End Set
    End Property
    Public Property getfxkeyempstatus() As String
        Get
            Return getfxkeystatus
        End Get
        Set(ByVal value As String)
            getfxkeystatus = value
        End Set
    End Property
#End Region
    Private Sub btnemp_addposition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_addposition.Click
        Try
            If btnemp_addposition.Text = "New" Then
                Me.tcode.Focus()
                Call cleartxt()
                Label1.Text = "Add Employee Type"
                btnemp_deleteposition.Visible = False
                btnemp_editposition.Visible = False

                btnemp_addposition.Text = "Save"
                btnemp_addposition.Image = My.Resources.save2
                btnemp_position.Text = "Cancel"
                btnemp_position.Location = New System.Drawing.Point(71, 8)
               
            ElseIf btnemp_addposition.Text = "Save" Then
                If Me.tcode.Text <> "" And Me.tdescription.Text <> "" Then
                    Label1.Text = "Employee Type"
                    Call check_typecode()
                    Call Employee_Types_payroll_TK()
                    Call cleartxt()
                    tcode.Focus()
                    btnemp_addposition.Text = "New"
                    btnemp_addposition.Image = My.Resources.new3
                    btnemp_position.Text = "Close"
                    btnemp_position.Location = New System.Drawing.Point(221, 8)
                    btnemp_deleteposition.Visible = True
                    btnemp_editposition.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If

        Catch ex As Exception
        End Try
    End Sub
    Public Sub check_typecode()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Typecode", SqlDbType.VarChar, 10).Value = Me.tcode.Text
        cmd.Parameters.Add("@Typedescription", SqlDbType.VarChar, 50).Value = Me.tdescription.Text
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already exist, please try another code", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call add_position()
                Call load_grid_position()
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try

    End Sub
    Public Sub cleartxt()
        tcode.Text = ""
        tdescription.Text = ""
    End Sub
#Region "Employee Type Payroll & TK update/stored proc"
    Private Sub Employee_type_payroll_update()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_payroll_tk_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@fxKeyEmpType", SqlDbType.Char, 2, ParameterDirection.Output).Value = getfxkeystatus
            .Add("@fcTypeCode", SqlDbType.VarChar, 12).Value = Me.tcode.Text
            .Add("@fcTypeDesc", SqlDbType.VarChar, 30).Value = Me.tdescription.Text
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
    Private Sub btnemp_editposition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_editposition.Click

        Try
            If btnemp_editposition.Text = "Edit" Then
                Me.tcode.Focus()
                Label1.Text = "Edit Employee Type"

                Me.tcode.Text = Me.LvlEmployeeType.SelectedItems(0).Text
                Me.tcode.Tag = Me.LvlEmployeeType.SelectedItems(0).Text
                Me.tdescription.Text = Me.LvlEmployeeType.SelectedItems(0).SubItems(1).Text
                Me.tdescription.Tag = Me.LvlEmployeeType.SelectedItems(0).SubItems(1).Text
                getrecordid = Me.LvlEmployeeType.SelectedItems(0).SubItems(2).Text
                getfxkeyempstatus = Me.LvlEmployeeType.SelectedItems(0).SubItems(3).Text

                btnemp_editposition.Text = "Update"
                btnemp_editposition.Image = My.Resources.save2
                btnemp_position.Text = "Cancel"
                btnemp_position.Location = New System.Drawing.Point(139, 8)

                btnemp_deleteposition.Visible = False
                btnemp_addposition.Enabled = False

            ElseIf btnemp_editposition.Text = "Update" Then
                If Me.tcode.Text <> "" And Me.tdescription.Text <> "" Then
                    If Me.tcode.Text = Me.tcode.Tag And Me.tdescription.Text = Me.tdescription.Tag Then
                        Call employee_type_update()
                        Call Employee_type_payroll_update()
                        Call load_grid_position()
                        Call cleartxt()
                        Label1.Text = "Employee Type"

                        btnemp_editposition.Text = "Edit"
                        btnemp_editposition.Image = My.Resources.edit1
                        btnemp_position.Text = "Cancel"
                        btnemp_position.Location = New System.Drawing.Point(221, 8)
                        btnemp_position.Text = "Close"
                        btnemp_deleteposition.Visible = True
                        btnemp_addposition.Enabled = True
                    ElseIf Me.tcode.Text <> Me.tcode.Tag Then
                        Call check_update_code()
                        Call Employee_type_payroll_update()
                        Call cleartxt()
                        Label1.Text = "Employee Type"

                        btnemp_editposition.Text = "Edit"
                        btnemp_editposition.Image = My.Resources.edit1
                        btnemp_position.Text = "Cancel"
                        btnemp_position.Location = New System.Drawing.Point(221, 8)
                        btnemp_position.Text = "Close"
                        btnemp_deleteposition.Visible = True
                        btnemp_addposition.Enabled = True
                    ElseIf Me.tdescription.Text <> Me.tdescription.Tag Then
                        Call check_updatetypecode_validation()
                        Call Employee_type_payroll_update()
                        Call cleartxt()
                        Label1.Text = "Employee Type"

                        btnemp_editposition.Text = "Edit"
                        btnemp_editposition.Image = My.Resources.edit1
                        btnemp_position.Text = "Cancel"
                        btnemp_position.Location = New System.Drawing.Point(221, 8)
                        btnemp_position.Text = "Close"
                        btnemp_deleteposition.Visible = True
                        btnemp_addposition.Enabled = True
                    End If
                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Employee Type Payroll $ TK /stored proc"
    Private Sub Employee_type_payroll_delete(ByVal keystatus As String)
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_payroll_type_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@fxKeyEmpType", SqlDbType.Int).Value = keystatus
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
    Private Sub btnemp_deleteposition_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_deleteposition.Click
        getrecordid = Me.LvlEmployeeType.SelectedItems(0).SubItems(2).Text
        getfxkeyempstatus = Me.LvlEmployeeType.SelectedItems(0).SubItems(3).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information)
        If x = System.Windows.Forms.DialogResult.OK Then
            
            Call delete_position()
            Call Employee_type_payroll_delete(getfxkeyempstatus)
            Call load_grid_position()

        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
    Public Sub delete_position()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@recid", SqlDbType.Int, ParameterDirection.Input).Value = getrecid 'GRID_EMPLOYEE_POSITION.CurrentRow.Cells(2).Value
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub

    Private Sub btnemp_position_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnemp_position.Click
        If btnemp_position.Text = "Cancel" Then
            Label1.Text = "Employee Type"

            btnemp_addposition.Text = "New"
            btnemp_addposition.Image = My.Resources.new3
            btnemp_editposition.Text = "Edit"
            btnemp_editposition.Image = My.Resources.edit1
            btnemp_editposition.Visible = True
            btnemp_deleteposition.Visible = True
            btnemp_addposition.Enabled = True
            btnemp_position.Text = "Close"
            btnemp_position.Location = New System.Drawing.Point(221, 8)
        ElseIf btnemp_position.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub Employee_Position_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_grid_position()
        Call disabledbutton()
    End Sub
    Private Sub disabledbutton()
        Me.btnemp_addposition.Enabled = False
        Me.btnemp_editposition.Enabled = False
        Me.btnemp_deleteposition.Enabled = False
    End Sub
#Region "Employee Type Insert/stored proc"
    Public Sub add_position()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fxkeyemptype", SqlDbType.Char, 2, ParameterDirection.Output).Value = ""
        cmd.Parameters.Add("@Typecode", SqlDbType.VarChar, 10, ParameterDirection.Input).Value = Me.tcode.Text
        cmd.Parameters.Add("@Typedescription", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = Me.tdescription.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Employee Type Get"
    Public Sub load_grid_position()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_get1", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Connection = myconnection.sqlconn
        myconnection.sqlconn.Open()
        With Me.LvlEmployeeType
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Type Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Type Description", 190, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2)) 'recid
                        .subitems.add(myreader.Item(3)) 'fxkeyempstatus
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With


    End Sub
#End Region
    Public Sub employee_type_update()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@recid", SqlDbType.Int, ParameterDirection.Input).Value = getrecid
        cmd.Parameters.Add("@fxkeyemptype", SqlDbType.Char, 2).Value = getfxkeystatus
        cmd.Parameters.Add("@Typecode", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = tcode.Text
        cmd.Parameters.Add("@Typedescription", SqlDbType.VarChar, 50, ParameterDirection.Input).Value = tdescription.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
    Public Sub check_update_code()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_check_description", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Typecode", SqlDbType.VarChar, 10).Value = Me.tcode.Text
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.Read Then
                MessageBox.Show("That Type code " + " ' " + Me.tcode.Text + " ' " + "is already used, Please try another code", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call employee_type_update()
                Call load_grid_position()
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub
    Public Sub check_updatetypecode_validation()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_type_check_desc", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@typedescription", SqlDbType.VarChar, 50).Value = Me.tdescription.Text
        myconnection.sqlconn.Open()

        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.Read Then
                MessageBox.Show("That Employee Type " + " ' " + Me.tdescription.Text + " ' " + "is already used, Please try another one", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call employee_type_update()
                Call load_grid_position()
            End If
        Finally
            myreader.Close()
            myconnection.sqlconn.Close()
        End Try
    End Sub

    Private Sub tcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tcode.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub tdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

#Region "Employee Type Payroll $ Tk insert/stored proc"
    Private Sub Employee_Types_payroll_TK()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_employee_payroll_types_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        With cmd.Parameters
            .Add("@fxKeyEmpType", SqlDbType.Char, 2, ParameterDirection.Output).Value = ""
            .Add("@fcTypeCode", SqlDbType.VarChar, 12).Value = Me.tcode.Text
            .Add("@fcTypeDesc", SqlDbType.VarChar, 30).Value = Me.tdescription.Text
        End With
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
End Class