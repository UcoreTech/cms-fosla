Imports System.Data.SqlClient
Imports System.Security
Imports System.Security.Principal.WindowsIdentity
Public Class frmEmployee_Benefits
#Region "variables"
    Public getempben_id As String
#End Region
#Region "Get Property"
    Public Property getempbenefitsID() As String
        Get
            Return getempben_id
        End Get
        Set(ByVal value As String)
            getempben_id = value
        End Set
    End Property
#End Region
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Me.Close()
    End Sub

    Private Sub btnsave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsave.Click


        Me.Close()

    End Sub
#Region "Get Employee Benefits/stored proc"
    'Public Sub load_employee_benefits()
    '    Try
    '        Dim myconnection As New Clsappconfiguration
    '        Dim cmd As New SqlCommand("usp_per_employee_benefits_get", myconnection.sqlconn)
    '        cmd.CommandType = CommandType.StoredProcedure
    '        myconnection.sqlconn.Open()
    '        cmd.Parameters.Add("@idnumber", SqlDbType.VarChar, 50).Value = form6.txtreplicateIDnumber.Text 'tempid_no.Text
    '        With form6.LvlEmployeeBenefit
    '            .Clear()
    '            .View = View.Details
    '            .GridLines = True
    '            .FullRowSelect = True
    '            .Columns.Add("Type", 100, HorizontalAlignment.Left)
    '            .Columns.Add("Description", 130, HorizontalAlignment.Left)
    '            .Columns.Add("Amount", 100, HorizontalAlignment.Left)
    '            Dim myreader As SqlDataReader
    '            myreader = cmd.ExecuteReader
    '            Try
    '                While myreader.Read
    '                    With .Items.Add(myreader.Item(0))
    '                        .SubItems.Add(myreader.Item(1))
    '                        .SubItems.Add(myreader.Item(2))
    '                        .SubItems.Add(myreader.Item(3)) 'idnumber
    '                        .subitems.add(myreader.Item(4)) 'key_id
    '                    End With
    '                End While
    '            Finally
    '                myreader.Close()
    '                myconnection.sqlconn.Close()
    '            End Try
    '        End With
    '    Catch ex As Exception
    '    End Try
    'End Sub
#End Region

    '''''''''''''''''''''''''''''''
    ''clear txt form emp-benefits''
    '''''''''''''''''''''''''''''''
    Private Sub cleartxt()
        Me.Ttype.Text = ""
        Me.tamount.Text = ""
        Me.txtdescription.Text = ""
    End Sub


    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click


        Me.Close()
    End Sub

    Private Sub tamount_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles tamount.KeyPress
        e.Handled = Not CurrencyInput_Validate(e.KeyChar)
    End Sub

    Private Sub tamount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tamount.Leave
        'tamount.Text = Format(Integer.Parse(tamount.Text), "###,###,###.00")
    End Sub
    Public Function CurrencyInput_Validate(ByVal C As Char) As Boolean
        If Not (Char.IsDigit(C)) And Not (C = ".") And Not (Microsoft.VisualBasic.AscW(C) = 8) And Not (Microsoft.VisualBasic.AscW(C) = 13) Then
            MsgBox("Invalid Input! This field allows 0-9 only.", MsgBoxStyle.Exclamation, "Erro Message")
            tamount.Focus()
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub tamount_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles tamount.Validating
        If tamount.Text = "" Then
            MessageBox.Show("Please supply the blank space", "eHRMS")
            tamount.Focus()
        Else

            If Not IsNumeric(tamount.Text) Then
                ErrorProvider1.SetError(tamount, "Please write correct number format")
                tamount.Focus()
                ErrorProvider1.SetError(tamount, "")
            Else
                tamount.Text = Format(Decimal.Parse(tamount.Text), "###,###,###.00")

            End If

        End If
    End Sub

    Private Sub Ttype_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles Ttype.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub Employee_Benifits_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class