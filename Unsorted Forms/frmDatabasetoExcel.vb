Imports System.Diagnostics
Imports System.IO
'Imports Excel = Microsoft.Office.Interop.Excel
Imports System.Data
Imports System.Data.SqlClient

Public Class frmDatabasetoExcel
    Dim adapt As SqlDataAdapter
    Dim ds As DataSet
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim mycon As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_Employee_masterList_report_revised", mycon.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure
            mycon.sqlconn.Open()
            adapt = New SqlDataAdapter(cmd)
            Dim sqlbuild As SqlCommandBuilder = New SqlCommandBuilder(adapt)
            ds = New DataSet
            adapt.Fill(ds, "temp_masterfile")
            Me.GridtoExcel.DataSource = ds.Tables("temp_masterfile").DefaultView
            mycon.sqlconn.Close()
            mycon = Nothing
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnsendexcel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsendexcel.Click
        Call sendtoexcelselectedrows()

    End Sub

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        ds.Dispose()
        adapt.Dispose()
        ds = Nothing
        adapt = Nothing
        Me.Close()
    End Sub

    Private Sub chkselectall_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
#Region "SEND TO EXCEl and LUNCH"
    Private Sub sendtoexcelselectedrows()
        Try
            Dim rowcount As Int32 = Me.GridtoExcel.SelectedRows.Count - 1
            If rowcount > -1 Then
                Dim fieldname() As String = {"Employee Number", "First Name", "Middle Name", "Last Name", "Date Hired", "Tenure", "Position", "Job Grade", "Section", _
                                             "Department", "Division", "Company Property", "Date of Birth", "Age", "Gender", "Civil Status", "Regularization Date", "Employment Status", _
                                             "Separation Date", "Reason for Separation", "Basic Pay", "Daily Ecola", "Rate Type", "Tax Code", "Shift Schedule", "ATM No.", "SSS", "Pag-ibig No.", "TIN", _
                                             "Philhealth No.", "Address", "Active"} ', "Inactive"}
                Dim dataarray(rowcount, 32) As Object
                Dim colcount As Int32 = 0

                For rowcounter As Int32 = 0 To rowcount
                    For Each cell As DataGridViewCell In Me.GridtoExcel.SelectedRows(rowcount - rowcounter).Cells
                        dataarray(rowcounter, colcount) = cell.Value
                        colcount = colcount + 1
                    Next
                    colcount = 0
                Next

                'var for excel
                Dim xlapp As New Excel.Application
                Dim workbook As Excel.Workbook = xlapp.Workbooks.Add(Excel.XlWBATemplate.xlWBATWorksheet)
                Dim worksheet As Excel.Worksheet = CType(workbook.Worksheets(1), Excel.Worksheet)
                Dim xlcalc As Excel.XlCalculation
                'save setting
                With xlapp
                    xlcalc = .Calculation
                    .Calculation = Excel.XlCalculation.xlCalculationManual
                End With
                'write the field names and data to the targeting worksheet
                With worksheet
                    .Range(.Cells(1, 1), .Cells(1, 34)).Value = fieldname
                    .Range(.Cells(2, 1), .Cells(rowcount + 2, 32)).Value = dataarray
                End With

                With xlapp
                    .Visible = True
                    .UserControl = True
                    .Calculation = xlcalc
                End With
                'tanggal to memory
                worksheet = Nothing
                workbook = Nothing
                xlapp = Nothing
                GC.Collect()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
#End Region
End Class
