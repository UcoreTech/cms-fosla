Imports System.Data.SqlClient
Public Class Rate_Type
   
    'Dim myconnection As New SqlConnection(My.Settings.CNString)
#Region "Variables"
    Private rateid As String
    Private fcrate As String
#End Region
#Region "Get Property"
    Private Property getRateTyeID() As String
        Get
            Return rateid
        End Get
        Set(ByVal value As String)
            rateid = value
        End Set
    End Property
    Private Property getfcrateID() As String
        Get
            Return fcrate
        End Get
        Set(ByVal value As String)
            fcrate = value
        End Set
    End Property
#End Region
    Private Sub Rate_Type_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call load_ratetype()
        Call disabledbutton()
    End Sub
    Private Sub disabledbutton()
        Me.btnnew.Enabled = False
        Me.btnupdate.Enabled = False
        Me.btndelete.Enabled = False
    End Sub

    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            If btnnew.Text = "New" Then
                Me.txtcode.Focus()
                Label2.Text = "Add Rate Type"
                Call cleartxt()
                btnnew.Text = "Save"
                btnnew.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(74, 7)

                btndelete.Visible = False
                btnupdate.Visible = False
            ElseIf btnnew.Text = "Save" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label2.Text = "Rate Type"
                    Call check_insertrate_validation()
                    Call cleartxt()
                    btnnew.Text = "New"
                    btnnew.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(219, 7)
                    btndelete.Visible = True
                    btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Rate Type code desc validation/stored proc"
    Public Sub check_insertrate_validation()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_codedesc_validation_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Rate_code", SqlDbType.Char, 16).Value = Me.txtcode.Text
        cmd.Parameters.Add("@RateDescription", SqlDbType.VarChar, 80).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already used, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call insert_rate()
                Call load_ratetype()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Rate Type code validation/stored proc"
    Public Sub check_updaterate_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_code_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@Rate_code", SqlDbType.Char, 16).Value = Me.txtcode.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Rate code " + " ' " + Me.txtcode.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_ratetype()
                Call load_ratetype()
            End If
        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Rate Type description validation/stored proc"
    Public Sub check_updatedescription_validation()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_desc_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@RateDescription", SqlDbType.VarChar, 80).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Rate Description " + " ' " + Me.txtdescription.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_ratetype()
                Call load_ratetype()
            End If
        Catch ex As Exception
        End Try
    End Sub
#End Region
#Region "Rate Type Insert/stored proc"
    Public Sub insert_rate()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@fcRateType", SqlDbType.Char, 2, ParameterDirection.Output).Value = ""
        cmd.Parameters.Add("@rate_code", SqlDbType.Char, 16).Value = txtcode.Text
        cmd.Parameters.Add("@ratedescription", SqlDbType.VarChar, 50).Value = txtdescription.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
    'Public Sub check_ratetype_validation()
    '    Dim appRdr As New System.Configuration.AppSettingsReader
    '    Dim myconnection As New Clsappconfiguration
    '    Dim cmd As New SqlCommand("USP_RATETYPE_SELECT_withparameter", myconnection.sqlconn)
    '    cmd.CommandType = CommandType.StoredProcedure
    '    cmd.Parameters.Add("@rate_code", SqlDbType.Char, 16).Value = Me.txtcode.Text
    '    myconnection.sqlconn.Open()
    '    cmd.ExecuteNonQuery()
    '    myconnection.sqlconn.Close()
    'End Sub
#Region "Rate Type listview get/stored proc"
    Public Sub load_ratetype()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.LvlRateType
            .Clear()
            .View = View.Details
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Rate Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Rate Description", 170, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2)) 'rateid
                        .subitems.add(myreader.Item(3)) 'fcratetype
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With


    End Sub
#End Region

    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getRateTyeID = Me.LvlRateType.SelectedItems(0).SubItems(2).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            'If Me.grdrate.SelectedRows.Count > 0 AndAlso _
            '          Not Me.grdrate.SelectedRows(0).Index = _
            '          Me.grdrate.Rows.Count - 1 Then
            Call delete_ratetype()
            Call load_ratetype()
            '    Me.grdrate.Rows.RemoveAt( _
            '        Me.grdrate.SelectedRows(0).Index)
            'End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then

        End If
    End Sub
#Region "Rate Type Delete/stored proc"
    Public Sub delete_ratetype()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_delete", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rateid", SqlDbType.Int).Value = rateid

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtcode.Focus()
                Label2.Text = "Edit Rate Type"
                Me.txtcode.Text = Me.LvlRateType.SelectedItems(0).Text
                Me.txtcode.Tag = Me.LvlRateType.SelectedItems(0).Text
                Me.txtdescription.Text = Me.LvlRateType.SelectedItems(0).SubItems(1).Text
                Me.txtdescription.Tag = Me.LvlRateType.SelectedItems(0).SubItems(1).Text
                getRateTyeID = Me.LvlRateType.SelectedItems(0).SubItems(2).Text
                getfcrateID = Me.LvlRateType.SelectedItems(0).SubItems(3).Text
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(142, 7)

                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnnew.Enabled = False
                btndelete.Visible = False
            ElseIf btnupdate.Text = "Update" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    If Me.txtcode.Text = Me.txtcode.Tag And Me.txtdescription.Text = Me.txtdescription.Tag Then
                        Label2.Text = "Rate Type"
                        Call update_ratetype()
                        Call load_ratetype()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(219, 7)
                        btnclose.Text = "Close"
                        btnnew.Enabled = True
                        btndelete.Visible = True
                    ElseIf Me.txtcode.Text <> Me.txtcode.Tag Then
                        Label2.Text = "Rate Type"
                        Call check_updaterate_validation()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(219, 7)
                        btnclose.Text = "Close"
                        btnnew.Enabled = True
                        btndelete.Visible = True
                    ElseIf Me.txtdescription.Text <> Me.txtdescription.Tag Then
                        Label2.Text = "Rate Type"
                        Call check_updatedescription_validation()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(219, 7)
                        btnclose.Text = "Close"
                        btnnew.Enabled = True
                        btndelete.Visible = True
                    End If

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Rate Type Update/stored proc"
    Public Sub update_ratetype()

        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_rate_type_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@rateid", SqlDbType.Int).Value = rateid
        cmd.Parameters.Add("@fcRateType", SqlDbType.Char, 2).Value = fcrate
        cmd.Parameters.Add("@rate_code", SqlDbType.Char, 10).Value = txtcode.Text
        cmd.Parameters.Add("@ratedescription", SqlDbType.VarChar, 50).Value = txtdescription.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region

    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        Try
            If btnclose.Text = "Cancel" Then
                Label2.Text = "Rate Type"

                btnnew.Text = "New"
                btnnew.Image = My.Resources.new3
                btnupdate.Text = "Edit"
                btnupdate.Image = My.Resources.edit1
                btnupdate.Visible = True
                btndelete.Visible = True
                btnclose.Text = "Close"
                btnclose.Location = New System.Drawing.Point(219, 7)

                btnnew.Enabled = True
            ElseIf btnclose.Text = "Close" Then
                Me.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Sub cleartxt()
        txtcode.Text = ""
        txtdescription.Text = ""
    End Sub

    Private Sub txtcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcode.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub


End Class