Imports System.Data.SqlClient

Public Class Tax_Code
   
    'Dim myconnection As New SqlConnection(My.Settings.CNString)
#Region "Variables"
    Private taxid As String
#End Region
#Region "Get property"
    Private Property getTaxcode() As String
        Get
            Return taxid
        End Get
        Set(ByVal value As String)
            taxid = value
        End Set
    End Property
#End Region
    Private Sub btnnew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnnew.Click
        Try
            If btnnew.Text = "New" Then
                Me.txtcode.Focus()
                Label1.Text = "Add Tax Code"
                Call cleartxt()
                btnnew.Text = "Save"
                btnnew.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(73, 9)
                Me.btnupdate.Visible = False
                btndelete.Visible = False
            ElseIf btnnew.Text = "Save" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    Label1.Text = "Tax Code"
                    Call check_taxcode_validation()
                    Call cleartxt()
                    btnnew.Text = "New"
                    btnnew.Image = My.Resources.new3
                    btnclose.Text = "Close"
                    btnclose.Location = New System.Drawing.Point(228, 9)
                    btndelete.Visible = True
                    Me.btnupdate.Visible = True
                Else
                    MessageBox.Show("Empty field found! ", "Save", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Tax Code Insert/storedproc"
    Public Sub insert_taxcode()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_tax_code_insert", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@tax_code", SqlDbType.VarChar, 4).Value = Me.txtcode.Text
            cmd.Parameters.Add("@TaxDescription", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch EX As Exception
            MessageBox.Show(EX.Message, "New Tax Code", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Tax code Payroll $ Tk insert/stored proc"
    Private Sub Payroll_taxcode_insert()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_taxcode_insert", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure

        cmd.Parameters.Add("@fcTaxCode", SqlDbType.VarChar, 4).Value = Me.txtcode.Text
        cmd.Parameters.Add("@fcTaxDescription", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text

        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
#Region "Tax Code validation insert/store proc"
    Public Sub check_taxcode_validation()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_tax_code_codedesc_valid_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@tax_code", SqlDbType.VarChar, 50).Value = Me.txtcode.Text
        cmd.Parameters.Add("@TaxDescription", SqlDbType.VarChar, 50).Value = txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("Data already used, please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call insert_taxcode()
                Call load_taxcode()
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Tax code validation/stored proc"
    Public Sub check_update_taxcode_validation()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_tax_code_validation_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@tax_code", SqlDbType.VarChar, 50).Value = Me.txtcode.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Tax code " + " ' " + Me.txtcode.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_taxcode()
                Call load_taxcode()
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Tax Description validation/stored proc"
    Public Sub check_updatedescription()
        Dim appRdr As New System.Configuration.AppSettingsReader
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_tax_description_validation_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@TaxDescription", SqlDbType.VarChar, 50).Value = Me.txtdescription.Text
        myconnection.sqlconn.Open()
        Dim myreader As SqlDataReader
        myreader = cmd.ExecuteReader
        Try
            If myreader.HasRows Then
                MessageBox.Show("That Tax Description " + " ' " + Me.txtdescription.Text + " ' " + " is already used , Please try another one", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
            Else
                Call update_taxcode()
                Call load_taxcode()
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region
#Region "Load Tax Code From Payroll"
    Private Sub loadTaxcodePayroll()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_Tpayroll14TaxCode_select", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.LvlTaxCode
            .Clear()
            .View = View.Details
            .GridLines = True
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Tax Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Tax Description", 170, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
#Region "Tax Code listview get/stored proc"
    Public Sub load_taxcode()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_tax_code_get", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        myconnection.sqlconn.Open()
        With Me.LvlTaxCode
            .Clear()
            .View = View.Details
            .GridLines = True
            .GridLines = True
            .FullRowSelect = True
            .Columns.Add("Tax Code", 100, HorizontalAlignment.Left)
            .Columns.Add("Tax Description", 170, HorizontalAlignment.Left)
            Dim myreader As SqlDataReader
            myreader = cmd.ExecuteReader
            Try
                While myreader.Read
                    With .Items.Add(myreader.Item(0))
                        .subitems.add(myreader.Item(1))
                        .subitems.add(myreader.Item(2)) 'taxid
                    End With
                End While
            Finally
                myreader.Close()
                myconnection.sqlconn.Close()
            End Try
        End With
    End Sub
#End Region
    Private Sub btndelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btndelete.Click
        getTaxcode = Me.LvlTaxCode.SelectedItems(0).SubItems(2).Text
        Dim x As New DialogResult
        x = MessageBox.Show("Are you sure you want to permanently delete this record?", "Confirm Deletion", MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1, MessageBoxOptions.DefaultDesktopOnly)
        If x = System.Windows.Forms.DialogResult.OK Then
            'If Me.grdtaxcode.SelectedRows.Count > 0 AndAlso _
            '          Not Me.grdtaxcode.SelectedRows(0).Index = _
            '          Me.grdtaxcode.Rows.Count - 1 Then

            Call delete_taxcode()
            Call load_taxcode()
            '    Me.grdtaxcode.Rows.RemoveAt( _
            '        Me.grdtaxcode.SelectedRows(0).Index)
            'End If
        ElseIf x = System.Windows.Forms.DialogResult.Cancel Then
        End If
    End Sub
#Region "Tax Code Delete/stored proc"
    Public Sub delete_taxcode()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_tax_code_delete", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@taxid", SqlDbType.VarChar, 4).Value = taxid 'grdtaxcode.CurrentRow.Cells(0).Value

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region

    Private Sub btnupdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnupdate.Click
        Try
            If btnupdate.Text = "Edit" Then
                Me.txtcode.Focus()
                Label1.Text = "Edit Tax Code"
                Me.txtcode.Text = Me.LvlTaxCode.SelectedItems(0).Text
                Me.txtcode.Tag = Me.LvlTaxCode.SelectedItems(0).Text
                Me.txtdescription.Text = Me.LvlTaxCode.SelectedItems(0).SubItems(1).Text
                Me.txtdescription.Tag = Me.LvlTaxCode.SelectedItems(0).SubItems(1).Text
                getTaxcode = Me.LvlTaxCode.SelectedItems(0).SubItems(2).Text

                btnupdate.Text = "Update"
                btnupdate.Image = My.Resources.save2
                btnclose.Text = "Cancel"
                btnclose.Location = New System.Drawing.Point(142, 9)

                btndelete.Visible = False
                btnnew.Enabled = False
            ElseIf btnupdate.Text = "Update" Then
                If Me.txtcode.Text <> "" And Me.txtdescription.Text <> "" Then
                    If Me.txtcode.Text = Me.txtcode.Tag And Me.txtdescription.Text = Me.txtdescription.Tag Then
                        Call update_taxcode()
                        Call load_taxcode()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(228, 9)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    ElseIf Me.txtcode.Text <> Me.txtcode.Tag Then
                        Label1.Text = "Tax Code"
                        Call check_update_taxcode_validation()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(228, 9)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    ElseIf Me.txtdescription.Text <> Me.txtdescription.Tag Then
                        Label1.Text = "Tax Code"
                        Call check_updatedescription()
                        Call cleartxt()
                        btnupdate.Text = "Edit"
                        btnupdate.Image = My.Resources.edit1
                        btnclose.Text = "Cancel"
                        btnclose.Location = New System.Drawing.Point(228, 9)
                        btnclose.Text = "Close"
                        btndelete.Visible = True
                        btnnew.Enabled = True
                    End If

                Else
                    MessageBox.Show("Empty field found! ", "Edit", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub
#Region "Tax Code Update/stored proc"
    Public Sub update_taxcode()
        Try
            Dim myconnection As New Clsappconfiguration
            Dim cmd As New SqlCommand("usp_per_tax_code_update", myconnection.sqlconn)
            cmd.CommandType = CommandType.StoredProcedure

            cmd.Parameters.Add("@taxid", SqlDbType.Int).Value = taxid
            cmd.Parameters.Add("@tax_code", SqlDbType.VarChar, 4).Value = txtcode.Text
            cmd.Parameters.Add("@TaxDescription", SqlDbType.VarChar, 50).Value = txtdescription.Text

            myconnection.sqlconn.Open()
            cmd.ExecuteNonQuery()
            myconnection.sqlconn.Close()
        Catch EX As Exception
            MessageBox.Show(EX.Message, "Update Tax Code", MessageBoxButtons.OK)
        End Try
    End Sub
#End Region
#Region "Tax code payroll $ tk update/stored proc"
    Private Sub payroll_taxcode_update()
        Dim myconnection As New Clsappconfiguration
        Dim cmd As New SqlCommand("usp_per_payroll_taxcode_update", myconnection.sqlconn)
        cmd.CommandType = CommandType.StoredProcedure
        'cmd.Parameters.Add("@taxid", SqlDbType.Int).Value = taxid
        cmd.Parameters.Add("@fcTaxCode", SqlDbType.VarChar, 4).Value = txtcode.Text
        cmd.Parameters.Add("@fcTaxDescription", SqlDbType.VarChar, 50).Value = txtdescription.Text
        myconnection.sqlconn.Open()
        cmd.ExecuteNonQuery()
        myconnection.sqlconn.Close()
    End Sub
#End Region
    Private Sub btnclose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnclose.Click
        If btnclose.Text = "Cancel" Then
            Label1.Text = "Tax Code"

            btnnew.Text = "New"
            btnnew.Image = My.Resources.new3
            btnupdate.Text = "Edit"
            btnupdate.Image = My.Resources.edit1
            btnnew.Enabled = True
            btnupdate.Visible = True
            btndelete.Visible = True
            btnclose.Text = "Close"
            btnclose.Location = New System.Drawing.Point(228, 9)
        ElseIf btnclose.Text = "Close" Then
            Me.Close()
        End If
    End Sub

    Private Sub Tax_Code_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Call load_taxcode()
        Call loadTaxcodePayroll()
        Call disabledbutton()
    End Sub
    Private Sub disabledbutton()
        Me.btndelete.Enabled = False
        Me.btnnew.Enabled = False
        Me.btnupdate.Enabled = False
    End Sub
    Public Sub cleartxt()
        txtcode.Text = ""
        txtdescription.Text = ""
    End Sub

    Private Sub txtcode_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtcode.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub

    Private Sub txtdescription_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtdescription.KeyPress
        e.Handled = Not alphanumeric_Validate(e.KeyChar)
    End Sub




End Class