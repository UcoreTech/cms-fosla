<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmFinalStep
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdCancel = New System.Windows.Forms.Button()
        Me.cmdSubmit = New System.Windows.Forms.Button()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtContribution = New System.Windows.Forms.TextBox()
        Me.txtInterest2 = New System.Windows.Forms.TextBox()
        Me.txtServiceFee = New System.Windows.Forms.TextBox()
        Me.txtAmortization = New System.Windows.Forms.TextBox()
        Me.txtAmortizationDate = New System.Windows.Forms.TextBox()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtMaxLoanAmount = New System.Windows.Forms.TextBox()
        Me.txtTotalExistingLoans = New System.Windows.Forms.TextBox()
        Me.txtOthers = New System.Windows.Forms.TextBox()
        Me.txtCompanyLoans = New System.Windows.Forms.TextBox()
        Me.txtGovtLoans = New System.Windows.Forms.TextBox()
        Me.txtOtherCoopLoans = New System.Windows.Forms.TextBox()
        Me.txtWtax = New System.Windows.Forms.TextBox()
        Me.txtPagibig = New System.Windows.Forms.TextBox()
        Me.txtPhilHealth = New System.Windows.Forms.TextBox()
        Me.txtSss = New System.Windows.Forms.TextBox()
        Me.txtTotalAllowDeduct = New System.Windows.Forms.TextBox()
        Me.txtTaxExempCode = New System.Windows.Forms.TextBox()
        Me.txtSalary = New System.Windows.Forms.TextBox()
        Me.txtInterest = New System.Windows.Forms.TextBox()
        Me.txtterms = New System.Windows.Forms.TextBox()
        Me.txtloantype = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtMaxLoanAmount2 = New System.Windows.Forms.TextBox()
        Me.txtPolicyRate = New System.Windows.Forms.TextBox()
        Me.txtCapitalContribution = New System.Windows.Forms.TextBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtNetProceeds = New System.Windows.Forms.TextBox()
        Me.PanePanel1 = New WindowsApplication2.PanePanel()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.PanePanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cmdCancel
        '
        Me.cmdCancel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdCancel.Location = New System.Drawing.Point(288, 444)
        Me.cmdCancel.Name = "cmdCancel"
        Me.cmdCancel.Size = New System.Drawing.Size(265, 32)
        Me.cmdCancel.TabIndex = 13
        Me.cmdCancel.Text = "Cancel"
        Me.cmdCancel.UseVisualStyleBackColor = True
        '
        'cmdSubmit
        '
        Me.cmdSubmit.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cmdSubmit.Location = New System.Drawing.Point(288, 400)
        Me.cmdSubmit.Name = "cmdSubmit"
        Me.cmdSubmit.Size = New System.Drawing.Size(266, 33)
        Me.cmdSubmit.TabIndex = 12
        Me.cmdSubmit.Text = "Submit for Credit Committee Approval"
        Me.cmdSubmit.UseVisualStyleBackColor = True
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.BackColor = System.Drawing.Color.Transparent
        Me.Label26.Font = New System.Drawing.Font("Tahoma", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label26.ForeColor = System.Drawing.Color.White
        Me.Label26.Location = New System.Drawing.Point(29, 7)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(213, 19)
        Me.Label26.TabIndex = 11
        Me.Label26.Text = "What do you want to do?"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(40, 336)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 16)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Gov't Loans"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(40, 308)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(110, 16)
        Me.Label11.TabIndex = 10
        Me.Label11.Text = "Other Coop Loans"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(40, 279)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(37, 16)
        Me.Label10.TabIndex = 9
        Me.Label10.Text = "Wtax"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(40, 251)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(49, 16)
        Me.Label9.TabIndex = 8
        Me.Label9.Text = "Pagibig"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(40, 222)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(64, 16)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "PhilHealth"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(40, 195)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(87, 16)
        Me.Label7.TabIndex = 6
        Me.Label7.Text = "SSS Premium"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(2, 164)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(156, 16)
        Me.Label6.TabIndex = 5
        Me.Label6.Text = "Total Allowable Deduction"
        '
        'txtContribution
        '
        Me.txtContribution.Location = New System.Drawing.Point(143, 130)
        Me.txtContribution.Name = "txtContribution"
        Me.txtContribution.Size = New System.Drawing.Size(124, 21)
        Me.txtContribution.TabIndex = 10
        '
        'txtInterest2
        '
        Me.txtInterest2.Location = New System.Drawing.Point(144, 102)
        Me.txtInterest2.Name = "txtInterest2"
        Me.txtInterest2.Size = New System.Drawing.Size(123, 21)
        Me.txtInterest2.TabIndex = 9
        '
        'txtServiceFee
        '
        Me.txtServiceFee.Location = New System.Drawing.Point(144, 75)
        Me.txtServiceFee.Name = "txtServiceFee"
        Me.txtServiceFee.Size = New System.Drawing.Size(121, 21)
        Me.txtServiceFee.TabIndex = 8
        '
        'txtAmortization
        '
        Me.txtAmortization.Location = New System.Drawing.Point(144, 49)
        Me.txtAmortization.Name = "txtAmortization"
        Me.txtAmortization.Size = New System.Drawing.Size(121, 21)
        Me.txtAmortization.TabIndex = 7
        '
        'txtAmortizationDate
        '
        Me.txtAmortizationDate.Location = New System.Drawing.Point(144, 23)
        Me.txtAmortizationDate.Name = "txtAmortizationDate"
        Me.txtAmortizationDate.Size = New System.Drawing.Size(121, 21)
        Me.txtAmortizationDate.TabIndex = 6
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label25.Location = New System.Drawing.Point(42, 161)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(83, 16)
        Me.Label25.TabIndex = 5
        Me.Label25.Text = "Net Proceeds"
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label24.Location = New System.Drawing.Point(3, 135)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(110, 16)
        Me.Label24.TabIndex = 4
        Me.Label24.Text = "Contribution (0%)"
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label23.Location = New System.Drawing.Point(5, 107)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(85, 16)
        Me.Label23.TabIndex = 3
        Me.Label23.Text = "Interest (9%)"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(5, 80)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(108, 16)
        Me.Label22.TabIndex = 2
        Me.Label22.Text = "Service Fee (1%)"
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label21.Location = New System.Drawing.Point(4, 53)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(80, 16)
        Me.Label21.TabIndex = 1
        Me.Label21.Text = "Amortization"
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(5, 26)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(110, 16)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "Amortization Date"
        '
        'txtMaxLoanAmount
        '
        Me.txtMaxLoanAmount.Location = New System.Drawing.Point(155, 440)
        Me.txtMaxLoanAmount.Name = "txtMaxLoanAmount"
        Me.txtMaxLoanAmount.Size = New System.Drawing.Size(119, 20)
        Me.txtMaxLoanAmount.TabIndex = 31
        '
        'txtTotalExistingLoans
        '
        Me.txtTotalExistingLoans.Location = New System.Drawing.Point(155, 414)
        Me.txtTotalExistingLoans.Name = "txtTotalExistingLoans"
        Me.txtTotalExistingLoans.Size = New System.Drawing.Size(118, 20)
        Me.txtTotalExistingLoans.TabIndex = 30
        '
        'txtOthers
        '
        Me.txtOthers.Location = New System.Drawing.Point(155, 385)
        Me.txtOthers.Name = "txtOthers"
        Me.txtOthers.Size = New System.Drawing.Size(118, 20)
        Me.txtOthers.TabIndex = 29
        '
        'txtCompanyLoans
        '
        Me.txtCompanyLoans.Location = New System.Drawing.Point(155, 358)
        Me.txtCompanyLoans.Name = "txtCompanyLoans"
        Me.txtCompanyLoans.Size = New System.Drawing.Size(118, 20)
        Me.txtCompanyLoans.TabIndex = 28
        '
        'txtGovtLoans
        '
        Me.txtGovtLoans.Location = New System.Drawing.Point(155, 332)
        Me.txtGovtLoans.Name = "txtGovtLoans"
        Me.txtGovtLoans.Size = New System.Drawing.Size(119, 20)
        Me.txtGovtLoans.TabIndex = 27
        '
        'txtOtherCoopLoans
        '
        Me.txtOtherCoopLoans.Location = New System.Drawing.Point(156, 304)
        Me.txtOtherCoopLoans.Name = "txtOtherCoopLoans"
        Me.txtOtherCoopLoans.Size = New System.Drawing.Size(118, 20)
        Me.txtOtherCoopLoans.TabIndex = 26
        '
        'txtWtax
        '
        Me.txtWtax.Location = New System.Drawing.Point(155, 275)
        Me.txtWtax.Name = "txtWtax"
        Me.txtWtax.Size = New System.Drawing.Size(119, 20)
        Me.txtWtax.TabIndex = 25
        '
        'txtPagibig
        '
        Me.txtPagibig.Location = New System.Drawing.Point(155, 247)
        Me.txtPagibig.Name = "txtPagibig"
        Me.txtPagibig.Size = New System.Drawing.Size(120, 20)
        Me.txtPagibig.TabIndex = 24
        '
        'txtPhilHealth
        '
        Me.txtPhilHealth.Location = New System.Drawing.Point(156, 218)
        Me.txtPhilHealth.Name = "txtPhilHealth"
        Me.txtPhilHealth.Size = New System.Drawing.Size(120, 20)
        Me.txtPhilHealth.TabIndex = 23
        '
        'txtSss
        '
        Me.txtSss.Location = New System.Drawing.Point(156, 191)
        Me.txtSss.Name = "txtSss"
        Me.txtSss.Size = New System.Drawing.Size(120, 20)
        Me.txtSss.TabIndex = 22
        '
        'txtTotalAllowDeduct
        '
        Me.txtTotalAllowDeduct.Location = New System.Drawing.Point(155, 160)
        Me.txtTotalAllowDeduct.Name = "txtTotalAllowDeduct"
        Me.txtTotalAllowDeduct.Size = New System.Drawing.Size(121, 20)
        Me.txtTotalAllowDeduct.TabIndex = 21
        '
        'txtTaxExempCode
        '
        Me.txtTaxExempCode.Location = New System.Drawing.Point(155, 133)
        Me.txtTaxExempCode.Name = "txtTaxExempCode"
        Me.txtTaxExempCode.Size = New System.Drawing.Size(121, 20)
        Me.txtTaxExempCode.TabIndex = 20
        '
        'txtSalary
        '
        Me.txtSalary.Location = New System.Drawing.Point(155, 104)
        Me.txtSalary.Name = "txtSalary"
        Me.txtSalary.Size = New System.Drawing.Size(121, 20)
        Me.txtSalary.TabIndex = 19
        '
        'txtInterest
        '
        Me.txtInterest.Location = New System.Drawing.Point(155, 75)
        Me.txtInterest.Name = "txtInterest"
        Me.txtInterest.Size = New System.Drawing.Size(122, 20)
        Me.txtInterest.TabIndex = 18
        '
        'txtterms
        '
        Me.txtterms.Location = New System.Drawing.Point(156, 49)
        Me.txtterms.Name = "txtterms"
        Me.txtterms.Size = New System.Drawing.Size(121, 20)
        Me.txtterms.TabIndex = 17
        '
        'txtloantype
        '
        Me.txtloantype.Location = New System.Drawing.Point(156, 23)
        Me.txtloantype.Name = "txtloantype"
        Me.txtloantype.Size = New System.Drawing.Size(121, 20)
        Me.txtloantype.TabIndex = 16
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(6, 444)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(114, 16)
        Me.Label16.TabIndex = 15
        Me.Label16.Text = "Max. Loan Amount"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(2, 419)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(150, 16)
        Me.Label15.TabIndex = 14
        Me.Label15.Text = "Total Existing Deductions"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.Location = New System.Drawing.Point(40, 389)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(46, 16)
        Me.Label14.TabIndex = 13
        Me.Label14.Text = "Others"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(40, 362)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(98, 16)
        Me.Label13.TabIndex = 12
        Me.Label13.Text = "Company Loans"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(6, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 16)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Salary"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(2, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 16)
        Me.Label5.TabIndex = 4
        Me.Label5.Text = "Tax Exemption Code"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.txtMaxLoanAmount2)
        Me.GroupBox2.Controls.Add(Me.txtPolicyRate)
        Me.GroupBox2.Controls.Add(Me.txtCapitalContribution)
        Me.GroupBox2.Controls.Add(Me.Label19)
        Me.GroupBox2.Controls.Add(Me.Label18)
        Me.GroupBox2.Controls.Add(Me.Label17)
        Me.GroupBox2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Black
        Me.GroupBox2.Location = New System.Drawing.Point(288, 16)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(273, 124)
        Me.GroupBox2.TabIndex = 8
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Capital Contribution"
        '
        'txtMaxLoanAmount2
        '
        Me.txtMaxLoanAmount2.Location = New System.Drawing.Point(142, 78)
        Me.txtMaxLoanAmount2.Name = "txtMaxLoanAmount2"
        Me.txtMaxLoanAmount2.Size = New System.Drawing.Size(125, 21)
        Me.txtMaxLoanAmount2.TabIndex = 5
        '
        'txtPolicyRate
        '
        Me.txtPolicyRate.Location = New System.Drawing.Point(142, 52)
        Me.txtPolicyRate.Name = "txtPolicyRate"
        Me.txtPolicyRate.Size = New System.Drawing.Size(124, 21)
        Me.txtPolicyRate.TabIndex = 4
        '
        'txtCapitalContribution
        '
        Me.txtCapitalContribution.Location = New System.Drawing.Point(142, 26)
        Me.txtCapitalContribution.Name = "txtCapitalContribution"
        Me.txtCapitalContribution.Size = New System.Drawing.Size(125, 21)
        Me.txtCapitalContribution.TabIndex = 3
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label19.Location = New System.Drawing.Point(5, 79)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(110, 16)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "Max Loan Amount"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label18.Location = New System.Drawing.Point(7, 53)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(70, 16)
        Me.Label18.TabIndex = 1
        Me.Label18.Text = "Policy Rate"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(6, 27)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(120, 16)
        Me.Label17.TabIndex = 0
        Me.Label17.Text = "Capital Contribution"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(6, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(52, 16)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Interest"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtMaxLoanAmount)
        Me.GroupBox1.Controls.Add(Me.txtTotalExistingLoans)
        Me.GroupBox1.Controls.Add(Me.txtOthers)
        Me.GroupBox1.Controls.Add(Me.txtCompanyLoans)
        Me.GroupBox1.Controls.Add(Me.txtGovtLoans)
        Me.GroupBox1.Controls.Add(Me.txtOtherCoopLoans)
        Me.GroupBox1.Controls.Add(Me.txtWtax)
        Me.GroupBox1.Controls.Add(Me.txtPagibig)
        Me.GroupBox1.Controls.Add(Me.txtPhilHealth)
        Me.GroupBox1.Controls.Add(Me.txtSss)
        Me.GroupBox1.Controls.Add(Me.txtTotalAllowDeduct)
        Me.GroupBox1.Controls.Add(Me.txtTaxExempCode)
        Me.GroupBox1.Controls.Add(Me.txtSalary)
        Me.GroupBox1.Controls.Add(Me.txtInterest)
        Me.GroupBox1.Controls.Add(Me.txtterms)
        Me.GroupBox1.Controls.Add(Me.txtloantype)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(1, 16)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(281, 470)
        Me.GroupBox1.TabIndex = 7
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Capacity to Pay"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(6, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(100, 16)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Terms (Months)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(6, 27)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(67, 16)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Loan Type"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtNetProceeds)
        Me.GroupBox3.Controls.Add(Me.txtContribution)
        Me.GroupBox3.Controls.Add(Me.txtInterest2)
        Me.GroupBox3.Controls.Add(Me.txtServiceFee)
        Me.GroupBox3.Controls.Add(Me.txtAmortization)
        Me.GroupBox3.Controls.Add(Me.txtAmortizationDate)
        Me.GroupBox3.Controls.Add(Me.Label25)
        Me.GroupBox3.Controls.Add(Me.Label24)
        Me.GroupBox3.Controls.Add(Me.Label23)
        Me.GroupBox3.Controls.Add(Me.Label22)
        Me.GroupBox3.Controls.Add(Me.Label21)
        Me.GroupBox3.Controls.Add(Me.Label20)
        Me.GroupBox3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Black
        Me.GroupBox3.Location = New System.Drawing.Point(288, 153)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(273, 194)
        Me.GroupBox3.TabIndex = 9
        Me.GroupBox3.TabStop = False
        '
        'txtNetProceeds
        '
        Me.txtNetProceeds.Location = New System.Drawing.Point(142, 159)
        Me.txtNetProceeds.Name = "txtNetProceeds"
        Me.txtNetProceeds.Size = New System.Drawing.Size(125, 21)
        Me.txtNetProceeds.TabIndex = 11
        '
        'PanePanel1
        '
        Me.PanePanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PanePanel1.Controls.Add(Me.Label26)
        Me.PanePanel1.Font = New System.Drawing.Font("Verdana", 12.0!, System.Drawing.FontStyle.Bold)
        Me.PanePanel1.GradientMode = System.Drawing.Drawing2D.LinearGradientMode.Vertical
        Me.PanePanel1.InactiveGradientHighColor = System.Drawing.Color.Green
        Me.PanePanel1.InactiveGradientLowColor = System.Drawing.Color.GreenYellow
        Me.PanePanel1.Location = New System.Drawing.Point(288, 352)
        Me.PanePanel1.Name = "PanePanel1"
        Me.PanePanel1.Size = New System.Drawing.Size(273, 34)
        Me.PanePanel1.TabIndex = 14
        '
        'FrmFinalStep
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(578, 502)
        Me.Controls.Add(Me.PanePanel1)
        Me.Controls.Add(Me.cmdCancel)
        Me.Controls.Add(Me.cmdSubmit)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Location = New System.Drawing.Point(430, 105)
        Me.Name = "FrmFinalStep"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Final Step"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.PanePanel1.ResumeLayout(False)
        Me.PanePanel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cmdCancel As System.Windows.Forms.Button
    Friend WithEvents cmdSubmit As System.Windows.Forms.Button
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents txtContribution As System.Windows.Forms.TextBox
    Friend WithEvents txtInterest2 As System.Windows.Forms.TextBox
    Friend WithEvents txtServiceFee As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortization As System.Windows.Forms.TextBox
    Friend WithEvents txtAmortizationDate As System.Windows.Forms.TextBox
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtMaxLoanAmount As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalExistingLoans As System.Windows.Forms.TextBox
    Friend WithEvents txtOthers As System.Windows.Forms.TextBox
    Friend WithEvents txtCompanyLoans As System.Windows.Forms.TextBox
    Friend WithEvents txtGovtLoans As System.Windows.Forms.TextBox
    Friend WithEvents txtOtherCoopLoans As System.Windows.Forms.TextBox
    Friend WithEvents txtWtax As System.Windows.Forms.TextBox
    Friend WithEvents txtPagibig As System.Windows.Forms.TextBox
    Friend WithEvents txtPhilHealth As System.Windows.Forms.TextBox
    Friend WithEvents txtSss As System.Windows.Forms.TextBox
    Friend WithEvents txtTotalAllowDeduct As System.Windows.Forms.TextBox
    Friend WithEvents txtTaxExempCode As System.Windows.Forms.TextBox
    Friend WithEvents txtSalary As System.Windows.Forms.TextBox
    Friend WithEvents txtInterest As System.Windows.Forms.TextBox
    Friend WithEvents txtterms As System.Windows.Forms.TextBox
    Friend WithEvents txtloantype As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents txtMaxLoanAmount2 As System.Windows.Forms.TextBox
    Friend WithEvents txtPolicyRate As System.Windows.Forms.TextBox
    Friend WithEvents txtCapitalContribution As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents txtNetProceeds As System.Windows.Forms.TextBox
    Friend WithEvents PanePanel1 As WindowsApplication2.PanePanel
End Class
